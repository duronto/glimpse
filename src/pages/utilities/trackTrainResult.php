<?php 
include '../common/main.php'; 
session_start();

header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");

if (isset($_POST["track-train"]) && !empty($_POST['track-train'])) { 
  $train = explode("/",$_POST["track-train"]);
  $date = $_POST["jDate"];

  $_SESSION['jDate'] =$_POST["jDate"];
  $_SESSION['train'] =$_POST["track-train"];

}else if(isset($_SESSION['train']) && !empty($_SESSION['train'])) {

  $date= $_SESSION['jDate'];
  $train = explode("/",$_SESSION['train']);
}else{
  setcookie('indexError','Some Problem Occurred Pls. Try Again', time() + (86400 * 30), "/");
  header("Location: train-track.php"); /* Redirect browser */
  exit;
}

$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/runningStatus/?trainNumber='.$train[0].'&startDate='.$date.'&access_token=00034542-a266-442a-a30c-f31c74e27f28';
//echo $url;
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);
if (strpos($http_response_header[0], "200")) {

  if(empty($json)){
      setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com/travelkhana/track-train"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);

  $t=(int)$json_o->status;
  $a=(int)$json_o->data->isCancel;
  if($t == 0){
    setcookie('indexError', "Train does not run on selected date.", time() + (86400 * 30), "/");
    header("Location: https://www.travelkhana.com/travelkhana/track-train");  
    exit;
  }else if($a === 1){
    setcookie('indexError', 'This Train has been Cancelled', time() + (86400 * 30), '/');
    header("Location: train-track.php");
    exit; 
  }else{
    $responseData= $json_o->data;
    $trainNo=$responseData->trainNumber;
    $stationList=$responseData->station; 
  }
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/track-train"); 
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/track-train"); 
    exit;
}else{
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/track-train");
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Train Track-view More</title>
      <!-- CSS -->
      <link rel="shortcut icon" href="http://travelkhana.com/travelkhana/images/favicon.ico" type="image/x-icon" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>

        <link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.5">

      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css?v=0.1">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://stage.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="availability-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h1>Track Train</h1>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">Track Train</h2>
                     <form class="form-inline mobile-form" id="trackTrainForm" action="https://www.travelkhana.com/travelkhana/trackTrainResult" method="post">
                        <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
            <div class="input-warp">
                        <div class="input_bg mobile-bg">
                           <div class="form-group first_section">
                              <input type="text" class="form-control  track-inputWidth" id="track-train" name="track-train" placeholder="Train Name or Number">
                           </div>
                           <div class="form-group second-section">
                              <input type="text" class="form-control datepicker" name="jDate" autocomplete="off" id="jDate" placeholder="yyyy-mm-dd" value="<?php echo date("Y-m-d");?>">
                              <i class="fa fa-calendar" onclick="$('#jDate').focus();"></i>
                           </div>
                             <!-- <div class="form-group second-section">
                              <input type="text" class="form-control  track-inputWidth" id="email-opt" name="email-opt" placeholder="Email Id(optional)"> 
                              
                           </div> -->
                        </div>
                      </form>
            <div class="btn-group">
              <button id="trackTrainBtn" type="button" class="btn btn-arival">Search</button>
            <!-- <button id="departBtn" type="button" class="btn btn-arival btn-dept">DEPARTURE</button> -->
            </div>
           </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of availability-bg-->
         <div id='partiDiv' class="trainTable-content mrtop35">
            <div class="container">
<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>             
               <div class="row">
                  <div class="col-md-12">
                   <div class="avail2-wrap width-95 ">
                     <p >Anxious to know the status of your train!!! With just few clicks, you can track your train status and order processing details with us. Check our smart tool to check your train status, arrival and departure timings.</p>
            <div class="row">
              <div class="col-sm-12 col-md-9 col-xs-12">
            <div class="dept-timing">
            <h3><?php echo $responseData->trainNumber." / ".$responseData->trainName."(".$responseData->fromStation." - ".$responseData->toStation.")";?></h3>
            <?php 
                //$a=(int)$responseData->isCancel;
                $b=(int)$responseData->isStarted;
                $c=(int)$responseData->isTerminate;
                $d=(int)$responseData->isPartiallyCanceled;
                $e=(int)$responseData->isDiverted;
                if($c === 1){$trainStatus="Train Terminated";}
                else if($d === 1){$trainStatus="Train Partially Cancelled";}
                else if($e === 1){$trainStatus="Train Diverted";}
                else if($b === 1){$trainStatus="Train Started";}else{$trainStatus="Yet to start";}

                $originalDate = $responseData->startDate;
                $newDate = date("d M Y", strtotime($originalDate));

                if(strcasecmp($responseData->lastUpdated, "Not given") === 0){
                      $lastUp='-';
                }else{
                    $lastUp=$responseData->lastUpdated;
                }

            foreach($stationList as $item) { //foreach element in $arr


                    if(strpos($responseData->currentStation, $item->stationCode)===0){
                        echo "<ul class='dept-tabel'>
            <li>
            <span class='dept-head'>Current Station  </span>
            <span>".$item->stationName.",".$item->stationCode."</span>
            </li>
            <li>
            <span class='dept-head'>Start Date    </span>
            <span>".$newDate."</span>
            </li>
            <li>
            <span class='dept-head'> Last Updated    </span>
            <span>".$lastUp."</span>
            </li>
            <li>
            <span class='dept-head'>Status               </span>
            <span>".$trainStatus."</span>
            </li>
            <li>
            <span class='dept-head'>Total Journey  </span>
            <span>".$responseData->totalJourney."</span>
            </li>
            </ul>";
                    }     
            }
            ?>
            

            </div>
            <!-- <div class="dept-timing">
            <h3>More Information</h3>
            <ul class="dept-tabel more-info ">
            <li>
            <span class="dept-head">Last Dep. Station  </span>
            <span>SIRPUR KAGAZNGR, SKZR</span>
            </li>
            <li>
            <span class="dept-head"> At (Time)    </span>
            <span>11:29 AM, 26 May </span>
            </li>
            <li>
            <span class="dept-head">  Next Station   </span>
            <span>BALHARSHAH, BPQ</span>
            </li>
            <li>
            <span class="dept-head">  At (Time)               </span>
            <span>12:25 PM, 26 May</span>
            </li>
            </ul>
            </div> -->

            <!-- <div class="next-prev">
             <a href="#" class="prev-btn"> <i class="fa fa-arrow-left" aria-hidden="true"></i> PREVIOUS DAY</a>
             <a href="#" class="prev-btn next-btn">NEXT DAY <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div> -->
            </div>
            <!-- <div class="col-sm-3 col-md-3 col-xs-12 hidden-sm hidden-xs mrTop-50">
             <img src="https://desktop.travelkhana.com/img/map-img.png" alt="" title="map" class="img-responsive">
             </div> -->
            </div>
            
            <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
              <tr>
                  <td>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- utility336x280 -->
                    <ins class="adsbygoogle"
                        style="display:inline-block;width:336px;height:280px"
                        data-ad-client="ca-pub-5798678675120444"
                        data-ad-slot="8254336067"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                  </td>
              </tr>
            </table>
            <div class="view-dropdown" style="margin-top:20px;">
              <div class="dropdown open">
              <button class="btn dropdown-btn " type="button" id="dropdownMenu2"  aria-haspopup="true" aria-expanded="true">
              Detailed View  <i class="fa fa-chevron-down" aria-hidden="true"></i></button>
              <div class="dropdown-menu dropdown-wrap" aria-labelledby="dropdownMenu2">
                <div class="table-responsive">
                  <table class="table table-bordered table-train">
                    <thead>
                    <tr class="tabel-head">
                      <th>Station</th>
                      <th>Actual / Sch. <br>Arrival</th>
                      <th>Actual / Sch. <br>Departure</th>
                      <th>Halt</th>
                      <th>Train<br>Status</th>
                      <th>Day Count</th>
                      <th>Platform</th>
                    </tr>
                    <!-- <tr class="tabel-head">
                      <th>Station</th>
                      <th class="hidden-xs hidden-sm"> sch. Arrival</th>
                      <th class="hidden-xs hidden-sm"> sch. Departure</th>
                      <th class="hidden-xs hidden-sm">Hault</th>
                      <th class="visible-sm visible-xs">Actual <br>Arrival /<br>Departure</th>
                      <th class="hidden-xs hidden-sm">Train Status</th>
                      <th class="visible-sm visible-xs">Train<br>Status</th>
                    </tr> -->
                    </thead>
                    <tbody>
                    <?php
                    foreach($stationList as $item) {

                      if($item->pfNo==0){
                        $platform="N.A";
                      }else{
                        $platform="Platform ".$item->pfNo;
                      }

                      

                      if(strcasecmp($item->actArrTime, "Origin") == 0){
                          $halt="-";
                      }else if(strcasecmp($item->actDepTime, "Destination") == 0){
                          $halt="-";
                      }else{
                          $halt1=(strtotime($item->actDepTime) - strtotime($item->actArrTime))/60;
                          $halt=$halt1." min";
                      }


                      $time=$item->delayDep;
                      if( $time >60){
                      $format = '%02d:%02d';
                      $hours = floor($time / 60);
                      $minutes = ($time % 60);
                      //sprintf($format, $hours, $minutes);
                        if( $minutes >0){
                            $actualTime=$hours.'hr '.$minutes.'min';
                        }else{
                            $actualTime=$hours.'hr';
                        }
                      }else{
                        $actualTime=$item->delayDep." min";
                      }
                      

                      if(strcasecmp($item->delayDep, "0") == 0){
                        $stat="On Time";
                      }else{
                        $stat='Late by '.$actualTime;
                      }


                      //checking for current station
                      $p=(int)$item->isDivertedStn;
                      $t=(int)$item->isDeparted;
                      $day=((int)$item->dayCount)+1;
                      
                      if($p == 1){
                          echo "<tr class='bg-danger'>
                          <td><p>".$item->stationName." (".$item->stationCode.")"."</p></td>
                          <td><p>".$item->actArrTime."/".$item->schArrTime."</p></td>
                          <td><p>".$item->actDepTime."/".$item->schDepTime."</p></td>
                          <td><p>".$halt."</p></td>";

                          echo "<td><span class='warning'>Cancelled </span> </td>";
                          
                          echo "<td><p>".$day."</p></td>
                          <td><p>".$platform."</p></td>
                          </tr>";

                      }else if(strpos($responseData->currentStation, $item->stationCode)===0){
                          echo "<tr bgcolor='#53bf83'>
                          <td><p>".$item->stationName." (".$item->stationCode.")"."</p></td>
                          <td><p>".$item->actArrTime."/".$item->schArrTime."</p></td>
                          <td><p>".$item->actDepTime."/".$item->schDepTime."</p></td>
                          <td><p>".$halt."</p></td>";

                          if(strcasecmp($item->delayDep, "0") == 0){
                            $stat="On Time";
                            echo "<td><span class='text-success'>".$stat." </span> </td>";
                          }else{
                            //$stat='Late by '.$item->delayDep.'min';
                            echo "<td><span class='warning'>".$stat." </span> </td>";
                          }
                      

                          echo "<td><p>".$day."</p></td>
                          <td><p>".$platform."</p></td>
                          </tr>";

                      }else if($t == 1){
                          echo "<tr bgcolor='#b5b6bb'>
                          <td><p>".$item->stationName." (".$item->stationCode.")"."</p></td>
                          <td><p>".$item->actArrTime."/".$item->schArrTime."</p></td>
                          <td><p>".$item->actDepTime."/".$item->schDepTime."</p></td>
                          <td><p>".$halt."</p></td>";

                          if(strcasecmp($item->delayDep, "0") == 0){
                            $stat="On Time";
                            echo "<td><span class='text-success'>Departed</span> /<span class='text-success'>".$stat." </span> </td>";
                          }else{
                           // $stat='Late by '.$item->delayDep.' min';
                            echo "<td><span class='text-success'>Departed</span> /<span class='warning'>".$stat." </span> </td>";
                          }
                      

                          echo "<td><p>".$day."</p></td>
                          <td><p>".$platform."</p></td>
                          </tr>";

                      }else{
                            echo "<tr>
                            <td><p>".$item->stationName." (".$item->stationCode.")"."</p></td>
                            <td><p>".$item->actArrTime."/".$item->schArrTime."</p></td>
                            <td><p>".$item->actDepTime."/".$item->schDepTime."</p></td>
                            <td><p>".$halt."</p></td>";

                            if(strcasecmp($item->delayDep, "0") == 0){
                              $stat="On Time";
                              echo "<td><span class='text-success'>".$stat." </span> </td>";
                            }else{
                            //  $stat='Late by '.$item->delayDep.'min';
                              echo "<td><span class='warning'>".$stat." </span> </td>";
                            }
                      

                            echo "<td><p>".$day."</p></td>
                            <td><p>".$platform."</p></td>
                            </tr>";

                      }

                      
                    }
                    ?>
                    
                    <!-- <tr>
                      <td class="station-name"><p>HYDERABAD DECAN (HYB)</p></td>
                      <td class="hidden-sm hidden-xs"> <p>06:55/07:00</p></td>
                      <td class="hidden-sm hidden-xs"><p>16:24</p></td>
                      <td class="hidden-sm hidden-xs"><p>16:26</p></td>
                      <td class="visible-sm visible-xs"><p>06:55/07:00</p></td>
                      <td class="hidden-sm hidden-xs"><span class="warning">Departed </span>/<span class="departed">10m Delay </span> </td>
                      <td class="visible-sm visible-xs"><span class="warning">Departed </span>/<br> <span class="departed">10m Delay </span> </td>
                    </tr> -->
                    
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

                <div class="trainTable-wrap width-95">
                        <h3>Tracking of train in the best possible way</h3>
                        <span class="head-icon"></span>
                        <p>Attitudes are caught and not taught" - This is the precise expression, which describes the status of Indian Railways. There is a sweeping misconception that the dawn of the Indian railways did not occur after independence, but during the British rule. The Railways gained momentum after independence and a new leaf was turned when it become nationalized. How irritating can things be when one of the prides of the country is not living up to its reputation and things are going downhill. Well, no prizes for guessing as the railways find themselves in such a position. No doubt, with the enhancement of technology and the state of the art infrastructure, Railways have taken a giant stride in providing various facilities but still call for a lot of improvements.</p>
            <p>Food has been a big let down in the Indian railways. The taste and quality are comprised and it is a case of filling your stomach and keeping quiet. But things have changed with the emergence of private food delivery companies and we at TravelKhana offer a new experience in superior customer service. We take pride on our food quality and it is delivered fresh and hot right at the seats.</p>
            <p>Imagine a vacation is round the corner and you are felt stranded for your tickets? Well, this is the reality with the Indian Railways. Getting a confirmed seat for your travel is a nightmare in the Indian railways. You need to plan your journey in advance to be assured of a seat. Considering the busy schedule in the modern world, this is virtually impossible, and there is a mad scramble when you decide that this is the day when you need to travel. In a way, things go overboard when the time of the journey arrives.</p>
            <p>However, things have become a bit easy with the advent of the internet? The online world has made easy and convenient for the traveller to track train with a simple click of a mouse. A glance at the process is as follows-</p>
            <ul class="schedule-list">
                              <li>At the top of the page, you need to select the train number or the name of it.</li>
                              <li> The name of the station has to be selected from the drop down list.</li>
                 <li> Then you need to click on the arrival or the departure button.</li>
               </ul>
                        </div>
        </div>    
                     </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer1.html' ?>
      <!-- footer Ends Here -->
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

          <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script src="/glimpse/src/js/track-train.js"></script>  
      <script src="/glimpse/src/js/trackTrainDate.js?v=0.1"></script>
      <script type="text/javascript">
$(document).ready(function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#trackTrainBtn').offset().top
    }, 'slow');
});
      </script>
   </body>
</html>