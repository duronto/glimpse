<?php include '../common/header.html' ?>
<link rel="stylesheet" href="/glimpse/src/css/seatAvail.css?v=0.3">
<link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.5">
<title>Indian Railways Seat Availability | Indian Railways Reservation Online</title>
<meta name="description" content="Planning for a railway journey & want to check Indian railways seat availability online? Visit us now for Indian railways seat availability & reservation online in minutes." />
<meta name="keywords" content="Indian railways seat availability, Indian railways reservation online" />
</head>
<body>
   <?php 
      $quotaArr = Array(
      'GN' => 'General Quota',
      'LD' => 'Ladies Quota',
      'TQ' => 'Tatkal Quota'
      );
      
      $classArr = Array(
      'SL' => 'Sleeper Class',
      'CC' => 'Chair Class',
      '3A' => 'Third AC',
      '2A' => 'Second AC',
      '1A' => 'Third AC'
      );
      
      
      
          $url="http://api.travelkhana.com/gatimaan/api/v1.0/getSeatAvlData/?access_token=00034542-a266-442a-a30c-f31c74e27f28";
      
      
            $t=0;
            $opts = array(
              'http'=>array(
                'method'=>"GET",
                //'header' => $authKey,
                'header' => "Authorization:",
                'ignore_errors' => '1'                 
              )
            );
            $context = stream_context_create($opts);
      
            $resultJson = file_get_contents($url,false, $context);
            $decodeJson =json_decode($resultJson);
      
          ?>
   <header class="inner-header navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
        </div>
       <!--  <div style="text-align: right">
            <a href="https://www.travelkhana.com/travelkhana/jsp/wow100.jsp"><img src="https://desktop.travelkhana.com/img/first_meal_free_banner.png" class="img-responsive" alt="" title=""/></a>
        </div> -->
      </div>
   </header>
   <section id="" class="inner-search-content">
      <div class="availability-bg">
         <div class="container-fluid">
            <div class="row">
               <div class="  visible-xs">
                  <div class="back-to-order text-center" >
                     <h3 >Indian Railways Seat Availability</h3>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div >
               </div>
               <div class="col-md-12 heading-wrap heading-mb" style="padding-top:10px;">
                  <div class="example_responsive_1"  >
                     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                     <!-- 728x90 TravelKhana -->
                     <ins class="adsbygoogle example_responsive_1"
                        style="display:inline-block;"
                        data-ad-client="ca-pub-5798678675120444"
                        data-ad-slot="3492216211"></ins>
                     <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                     </script>
                  </div>
                  <h1 class="hidden-xs">Indian Railways Seat Availability</h1>
                  <form class="form-inline mobile-form" id="searchAvaiForm" action="seatAvailabilityResult" method="post">
                     <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
                     <div class="input-warp">
                        <div class="input_bg mobile-avail1">
                           <div class="first_sectionpop">
                              <!--<span class="sourcedest-popup hidden-xs"><i class="fa fa-plus-circle" aria-hidden="true"></i> Station</span>-->
                              <div class="form-group first_section">
                                 <input type="text" class="form-control" id="train1" name="train1"  placeholder="Train Name/Number">
                              </div>
                           </div>
                           <!--<div class="or text-center hidden-lg hidden-md visible-xs visible-md"><span>OR</span></div>-->
                           <div class="second-sectionpop" style="display:block;">
                              <!--<div class="train-popup hidden-xs"><i class="fa fa-plus-circle" aria-hidden="true"></i> Train</div>-->
                              <div class="form-group second-section">
                                 <input type="text" class="form-control" id="from1" name="from1" placeholder="Source Station"> 
                              </div>
                              <div class="form-group second-section">
                                 <input type="text" class="form-control" id="from2" name="from2"  placeholder="Destination Station"> 
                              </div>
                              <div class="form-group second-section">
                                 <select  class="form-control"   name="class" id="class" >
                                    <option value="SL">Sleeper Class </option>
                                    <option value="CC">Chair Car  </option>
                                    <option value="3A">Third AC </option>
                                    <option value="2A">Second AC</option>
                                    <option value="1A">First AC</option>
                                    <!--  <option value="EC">Economic Class </option> -->
                                    <!--   <option value="FC">First Class </option> -->
                                 </select>
                              </div>
                              <div class="form-group second-section">
                                 <select  class="form-control"  name="quoto" id="quoto" >
                                    <option VALUE="GN" selected="selected" >General Quota</option>
                                    <option VALUE="LD">Ladies Quota </option>
                                    <option VALUE="TQ" >Tatkal Quota </option>
                                    <!--  <option VALUE="PT">Premium Tatkal Quota </option>
                                       <option VALUE="DF">Defence Quota </option>
                                       
                                       <option VALUE="FT">Foreign Tourist </option>
                                       
                                       <option VALUE="DP">Duty Pass quota </option>
                                       
                                       <option VALUE="HP">Handicaped Quota </option>
                                       
                                       <option VALUE="PH">Parliament House Quota </option>
                                       
                                       <option VALUE="SS">Lower Berth Quota </option>
                                       
                                       <option VALUE="YU">Yuva Quota </option> -->
                                 </select>
                              </div>
                           </div>
                           <div class="form-group second-section">
                              <input type="text" class="form-control datepicker" name="jDateseat2" id="jDateseat2" placeholder="Select date ">
                              <i class="fa fa-calendar" onclick="$('#jDateseat2').focus();"></i>
                           </div>
                        </div>
                        <button id="searchAvai" type="button" class="btn btn-search">SEARCH</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!--end of availability-bg-->
      <div class="availability-content">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="avail-wrap">
                     <div class="example_responsive_1" >
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- 728x90 TravelKhana -->
                        <ins class="adsbygoogle example_responsive_1"
                           style="display:inline-block;"
                           data-ad-client="ca-pub-5798678675120444"
                           data-ad-slot="3492216211"></ins>
                        <script>
                           (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                     </div>
                     <h3>More to Indian Railways Reservation</h3>
                     <span  class="head-icon"></span>
                     <p>Are you one of those who used to travel via trains a lot and admire to take a glance of the country from close by? Ever wished or travelled by train and have a look at the country? Want to get an idea of Indian Railways reservation? Do you give preference to rail transportation whenever in the country as compared to other rail mediums? Want to know about the ways through which people can grab an updated insight about trains, their relevant schedules, routes, movement and location of trains, know about seats available in them, how to do bookings and get tickets in them and more? It is been advisable to everyone to plan everything in a well approachable and planned way when it comes to book tickets, get reservations in trains and more and also stay away from any fuss that could happen at the last minute of journey. The well devised manner to start with is that plan up everything in a way that could help in getting confirmed tickets and reservations relevant to your rail travel and make sure to keep time for that. In India, people love to ride on trains as compared to other transportation mediums and most of the times, if people need to share their rail experiences, they get nostalgic as they are among one of the best moments of their life altogether. Rail travel are fun, full with amazement and best means to spend some great time altogether with loved and special ones. Aside from this, in case you take care of few things in advance then undoubtedly, you will get tickets on time and that too without any hassle. 
                     <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
                        <tr>
                            <td>
                              <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                              <!-- utility336x280 -->
                              <ins class="adsbygoogle"
                                  style="display:inline-block;width:336px;height:280px"
                                  data-ad-client="ca-pub-5798678675120444"
                                  data-ad-slot="8254336067"></ins>
                              <script>
                                  (adsbygoogle = window.adsbygoogle || []).push({});
                              </script>
                            </td>
                        </tr>
                      </table>
                      </p>
                     <p>In general quota, the time span in advance to book tickets is 120 days. It is always practical and effective to get bookings in advance as much as could be possible and stay away from that frustration that could happen at the last point of time. There is no doubt to the fact that when the counter opens, people start booking tickets like crazy ones and all seats get filled at a much speedier pace. Even, the situation worsens at the time of rush season and peak times especially at the time of festivals and special occasions. Adding to this, at the last minute of time, people can cancel up the tickets if something emergency comes up and it takes hardly a few minutes to do that and the payment gets credited back to the account within 5 to 7 business working days. In case, users are not able to get tickets in general quota, they try their luck then in tatkal quota. Well, in whichever quota you have been trying to get tickets, do make sure to be all set and ready as the counter gets open as the people looking for seats are endless and seats are limited for sure. As per Indian Railways reservation, there are different travel quotas and classes in which users can seek for tickets and the counter timings to get open are different for them. So, be aware of all information about them and take decisions accordingly. Just for the record, in general quota, the counter timings to open are in between 8 AM to 8:30 AM, for tatkal quota, the counter timings to get open are 10 AM to 10:30 AM, for non AC tatkal quota, the counter timings to get open are in between 11 AM to 11:30 AM. Know more about different travel classes as well, and go through the following pointers. </p>
                     <ul>
                        <li>
                           <p>First class AC – If you want to travel in uttermost privacy and in luxurious and spacious coaches, it is the class you must opt for. There are many latest features and facilities in them including sliding doors, large cabins and so. Even, passengers are liable to utilize tailored services in them like water service bathroom, cloth hangers and more. </p>
                        </li>
                        <li>
                           <p>Second class AC – It is less spacious as compared to first class AC but perfect for those looking for a bit of privacy and large cabins.</p>
                        </li>
                        <li>
                           <p>Third class AC –  If you just want air conditioned services and is travelling for like an overnight journey or so, it is the class to travel to. Reading lights, AC facility and likewise are been available in them and do make sure to get reservations at its earliest as could be possible as it gets filled at the quicker pace and are affordable.</p>
                        </li>
                        <li>
                           <p>Sleeper class –  If you are travelling on a bit of cash crunch or travel at more frequent intervals in the train, it is the best coach to opt to. The fare is affordable and it doesn’t have any AC facility and the cabins are usually crowded. </p>
                        </li>
                        <li>
                           <p>AC chair car – If you need to travel only for few hours or so, it is the class in which you must travel into. People travelling in this class always can opt to get onboard e-catering food meal services, as per the desire. </p>
                        </li>
                     </ul>
                     <p>To get bookings and information updates related to trains and such, as per Indian Railways reservations, IRCTC is the efficient and best platform to seek and entrust upon. Aside from this, there are other dedicated portals as well like Travel Khana that has been providing hot and fresh food e-catering meals to passengers directly over their seat berth in trains at affordable prices and within time as well as updated details related to train route, schedule, bookings, pnr status, real time running information, about different kinds of trains like premium, superfast, Duronto, Rajdhani and more, seat availability in trains, and more. Food offered to passengers in trains is of uttermost quality and hygienic and there is an assurance of delivery of food meal within defined time. In all, meet your hunger pangs, get reservations in advance in general quota or in tatkal quota, make your journey a memorable one. Be aware of other quotas of travel as defined by the Indian Railways reservation like ladies quota, Yuva quota, Parliament house quota, and more and make sure to get tickets within time and have a fantabulous time in train. </p>
                     <br>
                     <br>
                  </div>
                  <div class="avail-wrap">
                     <div class="trainTable-wrap">
                        <h3>Recent Searches for Seat Availability</h3>
                        <span class="head-icon"></span>
                        <?php 
                           foreach($decodeJson as $item) {  ?>
                        <div class="running-status">
                           <div class="late-train">
                              <span><strong ><i class="fa fa-train"></i><?php echo $item->trainName; ?>(<?php echo $item->trainNo?>) :</strong> Seat Availability of <strong><?php echo $item->trainName; ?> </strong><strong><?php echo $item->trainNo; ?></strong> for <strong><?php echo  $classArr[$item->cls];  ?></strong> in <strong><?php echo $quotaArr[$item->quota]; ?></strong> is <strong><?php echo $item->avlList[0]->avlStatus;  ?></strong> for the date of <strong> <?php echo $item->date; ?></strong></span>
                              <!-- <span><strong>Next Station: </strong>ALIGARH JN </span>
                                 <span class="warning"> <strong>Status:</strong>Late By 34m from its time of arrival </span> -->
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                     <!-- utility336x280 -->
                     <ins class="adsbygoogle"
                        style="display:inline-block;width:336px;height:280px"
                        data-ad-client="ca-pub-5798678675120444"
                        data-ad-slot="8254336067"></ins>
                     <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                     </script>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!--end of availability-content-->
   </section>
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
         </div>
      </div>
   </div>
   <?php include '../common/footer.html' ?>
   <script>
      $(document).ready(function(){
      
      
      
        if ($.cookie('indexError') != null ){
           var value = $.cookie("indexError");
           document.getElementById("myModalLabel").innerHTML =value;
           $('#myModal').modal('show');
          delete_cookie('indexError');
           setTimeout(function(){ $('#myModal').modal('hide');}, 4000);
        }
      });
      var delete_cookie = function(name) {
      document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      };
      
   </script>
   <!--  <script  src="/glimpse/src/js/common/custom.js?v=0.3"></script> -->
   <script src="/glimpse/src/js/seat-aval.js?v=0.6"></script>