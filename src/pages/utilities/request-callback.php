<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Request Call Back</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="/glimpse/src/pages/order.php"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="req-callback-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h3>Request a Call Back</h3>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">Request a Call Back</h2>
                     <form class="form-inline mobile-form" id="submit_form" method="post">
                        <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
                         <div class="input-warp">
                        <div class="input_bg mobile-bg">
                           <div class="form-group">
                              <input type="text" class="form-control req-inputWidth required" data-minimum="2" id="reqname" name="reqname" maxlength="15"  placeholder="Name">
                             
                           </div>
                          <div class="form-group">
                              <input type="text" class="form-control req-inputWidth required integerfiled" id="reqmobile" name="reqmobile" maxlength="10" data-minimum="10" placeholder="Mobile Number"> 
                              
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control req-inputWidth required" id="reqmsg" name="reqmsg"  placeholder="Message"> 
                           </div>
                           
                          </div>
                        <button id="request-btn" type="submit" class="btn btn-search">SUBMIT QUERY</button>
                     </form>
</div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of req-callback-bg-->
         
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer.html' ?>
      <!-- footer Ends Here -->
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
        <div class="modal-dialog" role="document">
         <div class="modal-content" id="h4content">
            
         </div>
        </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script type="text/javascript">
      document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></scr'+'ipt>');
              $("#request-btn").on("click", function(event){
               event.preventDefault();
            var errorfound = false;
            $(".required").each(function () {
                var input = $(this); // This is the jquery object of the input, do what you will

                var id = input.attr('id');
                if (input.val() == '' || input.hasClass('error') || (!$.isNumeric(input.val()) && input.hasClass('integerfiled'))) {
                    input.removeClass('error');
                    var p = input.attr('placeholder');
                    input.val('Provide ' + p);
                    input.addClass('error');
                    errorfound = true;
                } else if (input.data('minimum') != '' && input.val().length < input.data('minimum')) {
                    var p = input.hasClass('integerfiled') ? 'digits' : 'chars';
                    input.val('Minimum ' + input.data('minimum') + ' ' + p);
                    input.addClass('error');
                    errorfound = true;
                }
            });

            if( $('#submit_form #trainnum').length > 0 && validateTrains('trainnum') === false ) {
                errorfound = true;
            }

            if( $('#submit_form #stationcode').length > 0 && validateStations('stationcode') === false ) {
                errorfound = true;
            }

            if (errorfound) {
                return false;
            }
         document.getElementById("request-btn").disabled = true;
         var name = $("#reqname").val();
         var mobile = $("#reqmobile").val();
         var message = $("#reqmsg").val();
         var mainUrl=URL+"requestBooking?name="+name+"&mobile="+mobile+"&message="+message;
         $.ajax({
         url: mainUrl,   
         type: "GET",
         dataType: "json",
         contentType: "application/json; charset=utf-8",
         beforeSend : function( xhr ) {
            xhr.setRequestHeader( "Authorization", authKey);
            },
         success: function(data){
            document.getElementById("request-btn").disabled = false;

            document.getElementById("h4content").innerHTML ="<h4 class='modal-title alert alert-success' id='myModalLabel'></h4>";
            document.getElementById("myModalLabel").innerHTML =data.message;

            $('#myModal').modal('show');
            setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
            document.getElementById("reqname").value="";
            document.getElementById("reqmobile").value="";
            document.getElementById("reqmsg").value="";
         },
            error: function(XMLHttpRequest, textStatus, errorThrown){   
                document.getElementById("request-btn").disabled = false;
                document.getElementById("h4content").innerHTML ="<h4 class='modal-title alert alert-warning' id='myModalLabel'></h4>";
                if (XMLHttpRequest.readyState == 4) {
                  //obj = XMLHttpRequest.responseText;
                  document.getElementById("myModalLabel").innerHTML ="Some problem occured please try again";
                  $('#myModal').modal('show');
                  setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
                   }
               else if (XMLHttpRequest.readyState == 0) {
                  document.getElementById("myModalLabel").innerHTML ="Please check your network conn. and try again";
                  $('#myModal').modal('show');
                  setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
               }else {
                  document.getElementById("myModalLabel").innerHTML ="Some problem occured please try again";
                  $('#myModal').modal('show');
                  setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
               }
            }
         });

            return true;
        });
      </script>    
   </body>
</html>


