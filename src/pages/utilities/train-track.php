<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Track Your Train Online | Track Train Status | Spot Your Train India - TravelKhana.Com</title>
      <meta name="description" content="Want to track your train online to know the status of arrival & departure timings? Visit us now to Spot and track the Train anywhere in India with few clicks." />
      <meta name="keywords" content="Track your Train, Spoturtrain, Track Train Running Status, Train Tracker, Spot my Train" />
      <link rel="shortcut icon" href="http://travelkhana.com/travelkhana/images/favicon.ico" type="image/x-icon" />
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <noscript id="deferred-styles">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>
      <link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.5">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css?v=0.1">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
            <!-- <div style="text-align: right">
               <a href="https://www.travelkhana.com/travelkhana/jsp/wow100.jsp"><img src="https://desktop.travelkhana.com/img/first_meal_free_banner.png" class="img-responsive" alt="" title=""/></a>
            </div> -->
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="availability-bg">
            <div class="container">
               <div class="row">
                <table align="left" style="padding:3px 3px 3px 3px; border:none;">
                    <tr>
                        <td>
                          <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                          <!-- 728x90 TravelKhana -->
                          <ins class="adsbygoogle example_responsive_1"
                            style="display:inline-block;"
                            data-ad-client="ca-pub-5798678675120444"
                            data-ad-slot="3492216211"></ins>
                          <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                          </script>
                        </td>
                    </tr>
                  </table>
                  <div class="back-to-order text-center visible-xs">
                     <h1>Track Train</h1>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb paddingTop-25">
                     <h2 class="hidden-xs">Track Train</h2>
                     <form class="form-inline mobile-form" id="trackTrainForm" action="https://www.travelkhana.com/travelkhana/trackTrainResult" method="post">
                        <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
                        <div class="input-warp">
                           <div class="input_bg mobile-bg">
                              <div class="form-group first_section">
                                 <input type="text" class="form-control  track-inputWidth" id="track-train" name="track-train" placeholder="Train Name or Number">
                              </div>
                              <div class="form-group second-section">
                                 <input type="text" class="form-control datepicker" name="jDate" autocomplete="off" id="jDate" placeholder="yyyy-mm-dd" value="<?php echo date("Y-m-d");?>">
                                 <i class="fa fa-calendar" onclick="$('#jDate').focus();"></i>
                              </div>
                              <!-- <div class="form-group second-section">
                                 <input type="text" class="form-control  track-inputWidth" id="email-opt" name="email-opt" placeholder="Email Id(optional)"> 
                                 
                                 </div> -->
                           </div>
                     </form>
                     <div class="btn-group">
                     <button id="trackTrainBtn" type="button" class="btn btn-arival">Search</button>
                     <!-- <button id="departBtn" type="button" class="btn btn-arival btn-dept">DEPARTURE</button> -->
                     </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of availability-bg-->
         <div class="trainTable-content">
         <div class="container">
         <div class="example_responsive_1" >
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- 728x90 TravelKhana -->
            <ins class="adsbygoogle example_responsive_1"
               style="display:inline-block;"
               data-ad-client="ca-pub-5798678675120444"
               data-ad-slot="3492216211"></ins>
            <script>
               (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="avail2-wrap">
                  <div class="trainTable-wrap">
                     <h3>Track train by using ‘Spot your train’ App</h3>
                     <p>Wondering how to check and know about the live exact train movement and running status?
                     <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
                        <tr>
                            <td>
                              <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                              <!-- utility336x280 -->
                              <ins class="adsbygoogle"
                                  style="display:inline-block;width:336px;height:280px"
                                  data-ad-client="ca-pub-5798678675120444"
                                  data-ad-slot="8254336067"></ins>
                              <script>
                                  (adsbygoogle = window.adsbygoogle || []).push({});
                              </script>
                            </td>
                        </tr>
                      </table>
                     Well, it is indeed simple and easy to do using user friendly and smart tool that will let anyone check and keep a track of running train, know about the exact running status, its related information like the arrival time, departure time, halts within the route, time taken to cover the whole and reach at specific station or so without any fuss. Are you all set to do the same? It is not only time saving but also let you be aware of all updated details related to train status.</p>
                     <span class="head-icon"></span>
                     <p>Undoubtedly, there is no denial to the fact that Indian Railways is been handling one of the biggest rail networks and that too in such an efficacious manner that it is been recognized at a large scale worldwide. And, the introduction of internet has made things simpler for everyone and Indian Railways is also been consistently on the same drive to make it easy for people and travellers by providing with approachable services and solutions. And same goes with the functionality that is been available to track train. If anyone wants to track train, they can always do the same within minutes and without any problem. To do the same, these days now no longer people require to go at the railway counters or stations enquiring about train, their updated schedule, running status and more. Saving both your time, energy and for certainly effort, Spot your train is been a highly intuitive and useful app been introduced by Travel Khana letting people know about all the related live running information of any train within clicks and minutes. It has even been integrated with advanced Google map feature that will assist people check out the running details of any train visually and actually see the running trains operating from one place to other.</p>
                     <p>Having associated with Indian Railways, Travel Khana started with proffering fresh and rich tasty food delivery in trains to passengers directly over their seat berths within time. Gradually, it enhanced its aura of offerings by proffering reliable details and services related to the trains like seat availability, access to Indian Railways time table, track train, book reservations, check PNR status and much more. Spot your train functionality to track train has actually eased out varied needs of passengers from all around and to check on the same, there is absolutely no need to travel in train. Anyone can check out the app and know about the live running status of them at any time and at any of the desired junction, as needed. To be aware of any information like whether the train is been running as per its scheduled time or not, is none but a blessing as it could definitely enhance anyone’s experience. Thinking about how to do access the same and how it works? Well, read on and go through the following steps and you will come to know.</p>
                     <ul class="schedule-list">
                        <li>Open up spotyourtrain.in site and once you open it, there is a box letting people feed on the train name and train number.</li>
                        <li>It will also ask you to mention the travel journey date.</li>
                        <li>Once done, click on the proceed button.</li>
                        <li>There will be a drop down menu, at which you have to select the station name and click on the confirmation button.</li>
                        <li>There will be an optional column asking you to mention your email-id. It is up to you, whether you want to fill in or not.</li>
                        <li>As per the need, click on the departure or arrival button.</li>
                        <li>Once clicked, a new page will be redirected letting you about the live running status of the train along with its position reflected on Google map.</li>
                        <li>Basically, an auto track functionality is been there keeping a tab on everything updated related to the train including its arrival and departure time, train movement, train route and so.</li>
                     </ul>
                     <p>Isn’t it interesting and helpful? Spot your train functionality is not only simple to access but also can be accessed by anyone. Don’t you think so? So, make use of technology and have an access to latest running details of train right away and make your travel plans accordingly. Aside from this, have you tried food delivery services yet provided by e-catering service providers? If not, give them a chance for once and opt for yummy and delicious e-catering food services been provided by Travel Khana not only at affordable prices but also of superior quality. Go through their menu, explore many choices that they have been into, and make sure that what you are having will meet your hunger pangs and cravings. The helpline number is been 08800313131 and for any kind of assistance and support, they are always available at your side for the same. Do call them, let them know what you want to have, you will have to mention your pnr number, train name, station at which food needs to be delivered, coach number and seat number so that your parcel could be delivered bang on time and without any inconvenience on time. Do make sure to at least place an order for about 60 minutes in advance so that there is no fuss for the last minute of time. Spot your train is indeed a smart and robust app and people need to just know about the station name, pnr number, and travel date to access the same. And do always remember that each train running status keeps on changing and updating so either refresh or always place a new enquiry to know about latest related to that train. In case, you are stuck up with something, you can always place an enquiry with them or at IRCTC official site and everything will get answered as shortly as could be possible. Also, mobile app version is been available for the download as well. So, look for them, make use of them and enjoy the travel happily and make fonder memories.</p>
                  </div>
                  <div class="avail2-wrap">
                     <div class="trainTable-wrap">
                        <h3>Meaning of Meaning of Scheduled Arrival and actual arrival</h3>
                        <span class="head-icon"></span>
                        <p>Want to know about and grab access to the meaning of Scheduled Arrival and actual arrival time of the train? Here, you will get some idea related to the same for sure. The scheduled arrival time of the train is the one that is been defined as per the schedule of the train and that is been enlisted on the time table of the train as well. The actual arrival time of train is the one that is exact running status of that train on that date of travel is, and it could be same as of scheduled arrival of train or could be delayed because of technical faults or anything. To know whether the actual arrival time of train is same as of scheduled arrival time of train, the best way is to have access to Spot your train app that is easy to access and highly beneficial to use to. Accessed over web or mobile as per the desire, people just need to mention the train name and train number and the date of travel and instantly, a new page will get open that will let them know about the exact arrival time and departure time of that train, route it is been travelling, stoppages at which it stops by, source station, final station, and more. There is an auto track functionality that keeps on telling people about the relative arrival time of the train and within minutes. So, access technology, be aware of all details and have a smooth journey.</p>
                     </div>
                     <div class="avail2-wrap">
                        <div class="trainTable-wrap">
                           <h3>Meaning of ETA and ETD</h3>
                           <span class="head-icon"></span>
                           <p>Checking out web to know about the meaning of ETA and ETD? ETA depicts for ‘Estimated time of arrival’ and ETD stands for ‘Estimated time of departure’ in Indian Railways. ETA and ETD could be same as of scheduled time of arrival and scheduled time of departure and the best means to check whether they are the same or not is to get access to Spot your train utility. ETA means the timing at which a train is been expected to arrive at a junction and ETD means the timing at which a train is been expected to depart from a junction. To know about the real time train movement and its running status, Spot your train is a utility app that is reliable to use for and lets people know about the exact movement and location of an express within minutes. Even the app is been integrated with Google maps as well that help people know and actually check out the train movement and its travelling route with ease. People can keep a track of train using the app and know about ETA and ETD easily and without any fuss from them. It is highly beneficial and efficient to make use of the technology, know about the running status and schedule of the train and make decisions accordingly. Hope the information is been clear and understandable. Stay tuned for more posts..</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Meaning of ATA and ATD</h3>
                           <span class="head-icon"></span>
                           <p>Checking out online to get to know about the meaning of ATA and ATD? Here, you will understand what it stands for. ATA stands for ‘Actual time of arrival’ and ATD stands for ‘Actual time of departure’. It basically means the actual time at which a train is been leaving a junction and at what time it is been arriving at a junction in real time scenarios. To know about real time ATA and ATD, the best way is to get access to track train using Spot your train utility and so. The utility app not only gives an idea about the actual time at which the specific train is about to leave or arrive at respective station but is highly reliable and useful in keeping a track on train movement and its location at that point of time. Tracking a train in real time scenarios is always helpful and meaningful as it lets people know about ATA and ATD on one end and on the other gives them flexibility to think and make better wise decisions accordingly. Indeed, the app is simple to use and to do the same, you just have to feed in the train name or train number and the date of journey and within a click, a page will get open letting all the information depicted in a clear and concise way. Even, you can keep a track on Google maps as well and see the running movement of trains without any fuss.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Meaning of RT and UA</h3>
                           <span class="head-icon"></span>
                           <p>Checking out online to know about the meaning of RT and UA? Well, here you will definitely get some idea about the same. The term RT means ‘Real Time’ and UA means ‘Update Awaited’. If people get UA status then it simply means that the data is not been updated yet by the en-route staff or so and hence is in awaited status. The best way to get an access to real time running information about trains and its running status is by keeping an eye on the running status, schedule and movement of the train and knowing much more about them using Spot your train utility app. The app in real is very beneficial and let people know about live time information about trains, their schedule, their route, running information, updated arrival time, updated departure time, where they are stopping by and by how many minutes or time the trains will be late or so at those stops, and the best part that in case the train is been running late then by how many hours it is late or delayed. It is useful always to check out track of a train all the time and know about real time updated status so that people can make wise decisions accordingly and make full utilization of their time. To check out the same, simply get an access to Spot your train app, select the desired station name, mention the train name or number, date of journey and that’s it. Within clicks, people will get access to all details and travel safely.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Meaning of CNC and PF</h3>
                           <span class="head-icon"></span>
                           <p>Looking online for the meaning of CNC and PF and how does it relate to Indian Railways? Well, here you will get your answers for sure. The term CNC stands for ‘Cancelled at this station’ and the term PF stands for ‘Expected platform’. To get a status of the same, when you keep a track of any train, it gets easier to get access to the train’s updated information including the expected platform as well as at which station the train is been cancelled along with the reason. Keeping an access to live running status of train helps people to get exact and latest update related to that train’s location and movement at that point of time including the train’s arrival time, departure time, stoppages where it is halting at, by how many minutes or hours the train is been delayed or running in accord to its schedule, and more. It is not only quick to know about the same online, but also it is always helpful and beneficial to know about the real time running status of the train. People can make wise decisions once they get the updated information and make sure that they are making optimal advantage of their time. Adding to this, there are many other terminologies as well, with which they are aware of while knowing about the live status of the train like expected time of arrival, expected time of departure, update awaited, actual time of arrival, actual time of departure and more.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Track Train live on Google Map</h3>
                           <span class="head-icon"></span>
                           <p>Want to know how to track train live on Google Map? Well, with the help of apps like RailRadar or Spot your train, you can check out the live movement and location of the train on Google map at any time. With the help of a train name or a number, users can check the location and movement of that train on Google map and it will let them know where exactly it is at that point of time. Along with finding and keeping a track of that train’s movement, people can find out about other trains as well using the same running on the similar kind of route. Features like zoom in and zoom out are also there and users can click on + or – signs over there to make use of them. It will depict along with information related to which trains have been running late or which ones are being on time. The ones that are late in accord to their schedule will be shown in red colour and the ones that are on time will be shown in blue colour. Once you select on any particular train, it will depict to highlight all related information with that train including its exact arrival time, source and final junctions, distance it is covering, time it is taking, stoppages where it is halting by, and so. So, track train live on Google map and make decisions wisely according to the status and make sure that you are making optimal use of the time and journey. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Can I cancel my confirmed ticket if my train is running very late and I don't want to undertake the journey?</h3>
                           <span class="head-icon"></span>
                           <p>Can I cancel my confirmed ticket if my train is running very late and I don't want to undertake the journey? If you are looking for the answer, you will definitely get the same here. As per the rules defined by the department of Indian Railways, if the train in which you were supposed to board is been running late by more than 3 hours and that too at the respective station from where your journeys starts and in case, you do not want further to undertake the travel then there is a possibility to cancel out the ticket without any problem. For the same, indeed no clerkage fee or cancellation charge is been involved or charged and if you cancel the same, full amount gets refunded in the same account even for those confirmed tickets, whether booked in general quota or in tatkal quota. For e-tickets, it is important to file TDR online prior to the actual train departure timing and afterwards you will get a full refund for the same. Aside from this, for PRS tickets, it is important to surrender the tickets prior to the train’s actual departure timing. Moreover, it is significant to keep a track of a train always and be aware of the exact movement, timing and location of the train at any point of time using Spot your train app. Using the app gives a clear idea of the train’s movement and people can take accordingly decisions related to the status and enjoy a hassle free travel. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Indian Railway Train Enquiry Number</h3>
                           <span class="head-icon"></span>
                           <p>Looking online for Indian Railway Train Enquiry Number? If yes, here you will definitely get to know about the same. To do any of the Indian Railway train enquiry, there is a number also been known as Indian Railways automated help line number that is 139. For train enquiry, related to its arrival and departure timings, people can even send a message to 139 in the format AD <train numbers> <STD code of station>. Aside from train enquiry related to its arrival and departure timings, people face problems with many other things like pnr status, seat availability, fare enquiry, train name enquiry, time table, and current location of train, platform enquiry and so. People can even call at the customer care number to get answers related to their enquiry at 1800-11-321. Though, keeping a track of train’s location and status always helps in being updated about the whereabouts of the train and that could be done by using Spot your train app. The app is easy to use and its mobile app could also be downloaded as per the convenience in your SmartPhone. The app only asks for a train name and number to be entered upon and once it is done, all information related to the same gets reflected within instant. So, make sure to use the same, enter in the desired details and keep an eye on the status of live running information of the train and be aware of its exact timings. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What is the catch siding and slip siding in Indian railway?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about what is the catch siding and slip siding in Indian railway? Well, here you will get the answer for the same. The catch siding is been linked with the dead end siding attached with the use of a spring operated point and these sidings are usually provided in the hilly terrains where the gradients are steep nearby the railway stations. The purpose of these siding is basically to capture the vehicle movement especially if they begin to roll down the grade and gradually foul up with the running lines. Even, as per the sources, an individual siding is been provided outside the rail yard so that vehicles could be collected and assembled there. On the other side, slip sidings are provided at the much lower level end of the station. The purpose of them is in case the vehicles are not been caught in case of catch siding then when they enter the station premise, they will be caught in slip siding for sure. It is always important to check out the running status and location of your train before heading out to station and also check whether it is as per its defined schedule or not. Many a times, due to technical fault or stuff, trains got delayed and stuck by hours and Spot your train is the efficient and reliable means to do check out the same without any hassle and also about catch siding and slip siding too. </p>
                        </div>
                        <!-- 						<div class="trainTable-wrap">
                           <h3>Spot Your Current Train Running Status</h3>
                           <span class="head-icon"></span>
                           <div class="running-status">
                           <div class="late-train">
                           <span><strong class="warning">Kalka Mail(12311) -</strong> Scheduled from KANPUR CENTRAL(CNB) at 12:35 PM on 19 May , will reach ALIGARH JN at 05:54 PM on 20 May </span>
                           <span><strong>Next Station: </strong>ALIGARH JN </span>
                           <span class="warning"> <strong>Status:</strong>Late By 34m from its time of arrival </span>
                           </div>
                           <div class="on-time">
                           <span><strong class="success">Gitanjali Express (PT)(12860) - </strong> Scheduled from Bhusaval Junction(BSL) at 01:10 PM on 19 May , will reach Jalgaon Junction at 01:44 PM on 20 May </span>
                           <span><strong>Next Station: </strong>Jalgaon Junction </span>
                           <span class="success"> <strong>Status:</strong>On time </span>
                           </div>
                           <div class="on-time">
                           <span><strong class="success">Kishanganj - Ajmer Garib Nawaz Express(15715) - </strong>  Scheduled from BARAUNI JN(BJU) at 01:10 PM on 19 May , will reach HAJIPUR JN at 02:48 PM on 19 May </span>
                           <span><strong>Next Station: </strong>HAJIPUR JN </span>
                           <span class="success"> <strong>Status:</strong>On time </span>
                           </div>
                           </div>
                           </div> -->
                     </div>
                  </div>
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- utility336x280 -->
                  <ins class="adsbygoogle"
                    style="display:inline-block;width:336px;height:280px"
                    data-ad-client="ca-pub-5798678675120444"
                    data-ad-slot="8254336067"></ins>
                  <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
               </div>
            </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer1.html' ?>
      <!-- footer Ends Here -->
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
            </div>
         </div>
      </div>
      <!--popup ends here-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script src="/glimpse/src/js/track-train.js"></script>  
      <script src="/glimpse/src/js/trackTrainDate.js?v=0.1"></script> 
      <script type="text/javascript">
         $(document).ready(function(){
         if ($.cookie('indexError') != null ){
           var value = $.cookie("indexError");
           document.getElementById("myModalLabel").innerHTML =value;
           $('#myModal').modal('show');
           delete_cookie('indexError');
           setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
         }});
         var delete_cookie = function(name) {
           document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         };
      </script>
   </body>
</html> 