<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Indian Railways Timetable | IRCTC Train Schedule And Timings | Railway Train Inquiry</title>
      <meta name="description" content=" Planning for a Railway journey & want to check Indian Railways Time Table? Visit us for railway train inquiry, IRCTC Train Schedule or Timings and plan your train journey." />
      <meta name="keywords" content="railway time table, Train Schedule, IRCTC Train Schedule, IRCTC Train timings, Indian Railways Time Table" />
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
      <noscript id="deferred-styles">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
      <link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.5">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
            <!-- <div style="text-align: right">
               <a href="https://www.travelkhana.com/travelkhana/jsp/wow100.jsp"><img src="https://desktop.travelkhana.com/img/first_meal_free_banner.png" class="img-responsive"  alt="" title=""/></a>
            </div> -->
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="trainTable-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h1>Indian Railways Time Table</h1>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">Indian Railways Time Table</h2>
                     <form class="form-inline mobile-form" id="srchTimeTableForm" method="post">
                        <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
                        <div class="input-warp">
                           <div class="input_bg mobile-bg">
                              <div class="form-group second-section">
                                 <input type="text" class="form-control req-inputWidth" id="trainnum" name="trainnum" placeholder="Train Number"> 
                              </div>
                              <span class="or-text or-rail hidden-xs">OR</span>
                              <div class="or or-trianT text-center hidden-lg hidden-md visible-xs visible-md"><span>OR</span></div>
                              <div class="form-group first_section">
                                 <input type="text" class="form-control req-inputWidth ac_input" id="stnfromm" name="traintable" placeholder="From Station" autocomplete="off">
                              </div>
                              <div class="form-group first_section">
                                 <input type="text" class="form-control req-inputWidth ac_input" id="stntoo" name="trainto" placeholder="To Station" autocomplete="off"> 
                              </div>
                              <div class="form-group first_portion">
                                 <input type="text" class="form-control datepicker"  name="jDateseat2" id="jDateseat2" required placeholder="Date" value="" onfocus="blur();">
                                 <i class="fa fa-calendar" onclick="$('#jDateseat2').focus();"></i>
                              </div>
                           </div>
                           <button id="searchTrainTime" type="button" class="btn btn-search">SEARCH</button>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-bg-->
         <div class="trainTable-content">
            <div class="container">
               <div class="example_responsive_1" >
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- 728x90 TravelKhana -->
                  <ins class="adsbygoogle example_responsive_1"
                     style="display:inline-block;"
                     data-ad-client="ca-pub-5798678675120444"
                     data-ad-slot="3492216211"></ins>
                  <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="trainTable-wrap">
                        <h3>More to Indian Railways Time Table</h3>
                        <span class="head-icon"></span>
                        <p>Looking for the significance of online time table and why it is always
                         <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
                          <tr>
                              <td>
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- utility336x280 -->
                                <ins class="adsbygoogle"
                                    style="display:inline-block;width:336px;height:280px"
                                    data-ad-client="ca-pub-5798678675120444"
                                    data-ad-slot="8254336067"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                              </td>
                          </tr>
                         </table>
                         effective and promising to have a look over the same to plan your holidays in a hassle free way? Indian Railways is been effectively handling one of the biggest rail networks and it is been recognized at worldwide scale. Having a look at the Indian Railways time table at IRCTC or other portal could really help in saving your valuable time on one side and on the other assist in planning your travel in a well approachable manner. People adore travelling via trains in India and they have different reasons behind travelling. The reasons are like the train journeys cost affordable to some, for some they make them nostalgic and for some rail travel are meant to be fun and enjoyable that could remain in the memories. And to ease up the concerns of people, even Indian Railways is been putting every possible effort to manage everything proficiently. Looking for the right train for your travel could be gruesome and to check out all about the trains running in between specified destinations, it is healthy to have a look at the online time table. First of all, do understand the fact that there are different types of trains running under the supervision and control of Indian Railways department covering premium trains, local trains, passenger trains, express trains, superfast trains, intercity trains, Duronto, Rajdhani and Shatabdi trains. And time table online can give you glimpse and about the overall schedule and movement of trains within one go.</p>
                        <p>People can know about all the trains running over the similar route as well as respective arrival and departure timings of them at halts and all along the journey. It is must to check out the running days and frequency of trains as not all of them run on daily basis. If you know about the train in which you want to travel, then simply enter in the train name or number at the time table and all details will be depicted or highlighted within a click. In case, seats are available on the desired dates then as per the suitability make sure to do bookings as soon as possible. Aside from IRCTC site, people can check out the time table of Indian Railways at other platforms as well like at Travel Khana, which is primarily engaged in offering hot, fresh and hygienic food meals to travellers on the wheels at cost efficient prices and within time. In addition, you can avail its other offerings as well like check seat availability in trains, about the route covered by trains, policies, fares, and rules as defined by Indian Railways, any changes in the route of the journey or anything, check pnr status and much more with ease. Checking out time table online could assist people even in comparing ticket fares and also get an access to if there is any discounts available to them. Access to online time table is easy and people can trust blindly over the information available in that as it provides reliable and up-to- date information of the train route, train movement, train running information and more with ease. In fact, people can look up the number of seats available in the trains with the help of that and can book even at the last minute of time without any hassle. The internet and technology evolution has definitely helped people in getting access to everything like booking tickets and getting reservations just by the ease of being at home or in office and enabled availability of reliable and authentic train information within minutes in front of you. So, stay ahead with the use of technology, be informed and grab a bite of your favourite meal too with them.</p>
                        <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
                          <tr>
                              <td>
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- utility336x280 -->
                                <ins class="adsbygoogle"
                                    style="display:inline-block;width:336px;height:280px"
                                    data-ad-client="ca-pub-5798678675120444"
                                    data-ad-slot="8254336067"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                              </td>
                          </tr>
                        </table>
                        <p>There are so many positives and advantages that you can avail by looking at the time table of Indian Railways like mentioned in below.</p>
                        <ul class="schedule-list">
                           <li>Be aware of arrival time as well as departure time of trains.</li>
                           <li>Be sure about the overall route followed and taken by the train.</li>
                           <li>Accordingly plan your trips and vacations in advance after checking out the days in which the train is been running and seat availability with all information on rules as well as fares.</li>
                           <li>Be aware of any changes in train schedules, train routes and others.</li>
                           <li>Be aware of premium trains and others running all around.</li>
                        </ul>
                        <p>Though, few of the scenarios like cancellation of trains due to natural bad climatic conditions and striking staff might not be reflected in time table on time. But you can completely trust upon TravelKhana website and platform as it has been providing latest information about trains and related services. The trains are operational and running all along the corners of the country even to those undiscovered or remotest parts where it would be damn difficult to reach using other modes of transportation. In fact, many people consider the same as cheap as well as affordable. To assist you plan your rail travel, online time table is been like bliss as you can check out all the information in the same. Train schedules and routes are all attainable just in a single click of button and that provides the flexibility to plan in an efficient manner. Adding to this, every year Indian Railways introduce print outs of time table booklet meant especially for travellers not having internet connection to check it online or who doesn't want to access the same online. Ensuring to provide valuable and significant piece of information that can be trusted upon, it is all in your hands and you can be aware of everything related to trains by accessing the same and making optimal use of technology.</p>
                        <p>In all, it is always preferable and advisable to passengers access the time table to be updated about trains, everything related to them and plan your trips. Along with this, make full advantage of your journey and enjoy it to fullest by grabbing your favourite food with TravelKhana at any of the desired railway station and food will be delivered at your seat on time and without any trouble. Offering plenty of options in vegetarian and non vegetarian, it is also serving specialties of the cities and towns to people so that they can have taste of the authentic taste and flavour of the country and enjoy their journey. Rail journeys are meant to be memorable and that can get back people with lots and lots of fond memories and nothing else. So, take advantage of technology and enjoy your travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>How to check Train Time Table</h3>
                        <span class="head-icon"></span>
                        <p>Thinking about how to check train table and how it could help in saving your valuable time? Planning rail trips and explore all across the country? It is advisable to always go through the Indian Railways time table and accordingly after digging up your research and getting all the information related to trains, the number of trains running on that route, arrival time, departure time, days in which they run and more, plan your rail trips. You can go and check out the time table at the official site of IRCTC as well as many other platforms like TravelKhana. If your concern is how to check train table, it is very easy, just login the IRCTC website, open up the time table of Indian Railways, fill up the train name or number and once clicked, it will give you all the details related to that specific train. Train table helps to let you know everything including arrival time, departure time, total distance that needs to be travelled, total time taken by the train in covering the journey, and more. Before planning anything, seeing the train table helps in getting so many benefits like get updated of the timings of the train, overall route that it will follow, train schedule, seat availability in them and more with ease. Aside from fetching information from the train table, in case there are no seats available in that train in which you were looking for, you can opt for other express. While travelling in train, make sure to opt for e-catering services and grab your favourite meal with Travel Khana and enjoy journey to the fullest.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Trains time Enquiry through SMS</h3>
                        <span class="head-icon"></span>
                        <p>Want to check out trains time Enquiry through SMS? Looking for a way how to do the same? Here, you will get your answer. Indian Railways has started an SMS service famous and well known as ‘139 Rail Sampark’ service that has been in operation for about 5 years and such. The service that could be handled through this number covers accommodation availability, trains time and arrival/departure enquiry, fare enquiry, ticket status enquiry, time table enquiry, and spot/location of a train based enquiry. It is important to send a SMS to 139 in the correct format in order to get the right answer. For trains arrival or departure enquiry, it is important for people to send a SMS to 139 in the format like AD 12012011. Once the message is been sent, automatically within few minutes the answer will come via SMS and user can be certain about the train’s arrival or departure timing. In case, you have a time table related enquiry; send it to 139 in the format Time and then the train number for example, 12561. Time table online always help passengers to get an idea about the train’s arrival time, departure time, distance it is covering, the stops where it is stopping by, time taken to cover the distance, the days on which the train is running and more. And it is always beneficial to have a look at the same before planning out your rail excursions. So, in case of any enquiry, make use of the same and get your answers shortly. </p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Meaning of arrival time and dep. Time</h3>
                        <span class="head-icon"></span>
                        <p>Seeking for meaning of reserved upto and boarding point when it comes to booking tickets and rail journeys? When a passenger books ticket for a specific train following a particular route, he or she knows from where they have to board the train and it is the boarding point of them and till where they can travel from that ticket, it depicts till where the ticket is been reserved upto and the passenger can travel.</p>
                        <p>In accord to the defined new rules of Indian Railways, all those passengers having e-tickets can change the boarding station online and assuredly prior to 24 hours of the train’s departure time only. In case, a user has modified the boarding station junction, he or she will not be liable to board the train from previous actual boarding station and in situation, he or she is found travelling in that without any legality, penalty will be fined and it will be the price difference in between the changed junction and the original boarding station. This change in boarding point could only and only possible once, and could be changed only and only prior to 24 hours of the departure time of the train from that station. In case the ticket is seized, boarding point change is not granted at all. This option is not at all applied to them who have PNRs with VIKALP facility and also not applicable for ones who have i-tickets. Moreover, boarding point change is not admissible for current booking ticket. Hope, the meaning of both the terms is clear to all. Have a safe travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Swap Feature of Indian Railway Enquiry System</h3>
                        <span class="head-icon"></span>
                        <p>Seeking for information about the Swap feature of Indian Railway Enquiry System? Well, this is kind of a new feature that actually is a utility that helps a lot in knowing more about the train information and train route introduced by Indian Railways. This utility feature is used in this way, just for an example if you are finding out the number of trains from Delhi to Mumbai and fetching details about those trains and related to the route and more and wish to get all the details from Mumbai to Delhi as well, then Swap feature of Indian Railway Enquiry system comes into role as by clicking on that utility, the destinations will automatically gets reversed and people can find out information about trains, train routes and train schedules easily by using this feature in reversed manner.</p>
                        <p>Have you been using Indian Railway time table to know about trains, train routes, train information, and more? The swap feature works in online time table as well and is very efficient and time saving to do. Do utilize the swap feature of this utility and in case of any enquiries, do contact at railway stations or enquire online at IRCTC site and this is certain that every concern will be resolved at its earliest. So, whenever you are accessing information about two specific destinations and trains and want to swap the locations, you can do the same now with ease using the swap feature. Hope the information is clear to you all.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Meaning of Legend Code</h3>
                        <span class="head-icon"></span>
                        <p>Looking for the meaning of Legend code in Indian Railways? Well this post will help you in finding the same. While looking at the online time table of Railways, you can even seek for the legend code for the trains and accordingly plan your rail excursions and save your precious time and energy. People in India simply love to travel via train and seek Indian Railways time table to know about different train routes, train schedules, intermediate stoppages, train journey and more and that too within clicks.</p>
                        <p>There are different kind of terminologies as well as symbols used in Railways like green arrow, blue dot, ETA, ATA, RT, U.A., PF, ETD, ATD, CNC and more and everything comes under legend codes. Green arrow in the downward direction depicts train position at that station, ETA depicts for expected time of arrival, ATA means actual time of arrival, RT means right time, U.A. means update awaited, PF means expected platform, ETD for expected time of departure, ATD for actual time of departure, and CNC depicts for cancelled at this station. By looking at the online time table, passengers can be aware of all the legend codes of any train with ease. Passengers can even seek for the seat availability and others to make sure that they will get reservation in trains or not. So, plan your vacations wisely and have fun filled trip. Be updated of the technology, and cherish your travel to the core. Hope the meaning is clear to you all and stay tuned for more posts.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend Code [#] and [*]</h3>
                        <span class="head-icon"></span>
                        <p>Want to know about the meaning of legend code [#] and [*]? Well, in this post you can find out the meaning of legend codes and how does it relate to Indian Railways. The legend code [#] means and is entitled to temporary trains and depict that temporary trains are only valid for a particular defined period of time. And to know more about the validity and period of time till which it is been authentic, people can go to the originating station and find out about the same.</p>
                        <p>Legend code [*] is been entitled to not a starting station and depicts that the train doesn’t begin from that station and to know more details about the train source station, people can go to the origination station name and get all the details. People can know about the source station name and all the relevant details by checking out Indian Railways time table easily and in a convenient manner. There are many other legend codes as well depicting and entitling to some meaning and it is advisable to be aware of all of them. Stay updated by using Indian Railways time table, know details about everything and grab all details related to the trains like originating station name, final destination station name, distance that is in between the source and final station, total time it will take to cover the overall distance, how many intermediate stations are there in between source and final station, and much more. Access the time table and get to know about everything within clicks.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Meaning of Legend Code [+] and [$]</h3>
                        <span class="head-icon"></span>
                        <p>Seeking for information related to meaning of legend code [+] and [$]? This post will help you in understanding the same without any problem. Legend code [+] depicts trains having same numbers meaning for the trains that have same number as of other trains. Passengers can look for the similar train number just by scrolling the mouse over the train name. On the other side, train number [$] depicts for the lower berth quota and means a lower berth quota has to be provided in sleeper class as well as AC coach for women and senior citizen belonging to above 45 age. For this, 2 passengers in maximum are allowed to travel in any of the combination belonging to this age group.</p>
                        <p>People can actually look out the time table of Indian Railways online and get all the details related to trains from it in a very easy and convenient way. Apart from these two legend codes, there are many others as well depicting some meaning and it is recommended to look out for them and be informed by checking out the time table and know about them. Railways time table let people know about everything related to the trains including train name, number, stations in between, station name or so and without any trouble within clicks. So, make sure to access the time table and stay informed and updated about trains and related information and accordingly plan your journeys. Have a safe and happy travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Traveled station</h3>
                        <span class="head-icon"></span>
                        <p>Seeking for information related to legend code traveled station? There are many legend codes that people can check out and their related meaning as well while looking out at the Indian Railways time table and see how it relates to Railways. To check out the legend code traveled station, people can go over to the originating station name and grab more details related to the same within clicks.</p>
                        <p>Different codes address for different meaning like the train at a particular station, queried station, actual time of arrival, expected time of arrival, update awaited, expected platform, expected time of departure, expected time and more, and it is always recommended to seek online Indian Railways time table and check out the code for traveled station and others, train information, train schedule, train movement and more. It is very simple and convenient for everyone to see the timetable and fetch all details from there including the seat availability and accordingly book tickets on time. There are many codes having different meaning and people can look upon the same without any inconvenience and trouble and the best part is within a matter of minutes. The information fetched from the timetable is actually very handy and useful and makes people aware of latest happenings occurring in trains and more. So, enjoy your travel without any hassle and make full use of information and stay informed. Always look out for them and grab as much details as possibly could be done and enjoy the rail travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Upcoming Station</h3>
                        <span class="head-icon"></span>
                        <p>Want to know the meaning of legend code upcoming station? You can find about the same in this post. People can actually check out legend code upcoming station over the Google map easily. When you check out the Indian railways time table, you can know about the train route and number of stoppages it is going to take and how much time it is going to travel and take to reach at that particular station. When you check out a train movement over Google map, it actually depicts train movement with proper legend codes and legend code upcoming station is shown in blue colour round shape.</p>
                        <p>People can check out visually the meaning of every legend code over the Google map checking out and getting latest information about travelled station, upcoming station, travelled route, un- travelled route, current station, queried station, station in train scheduled on which mouse is been placed, train source station and train destination station. So, check out all the details related to a train without any hassle and be updated about each train movement that is happening. Check out the time table online of Indian Railways from any of the platform whether IRCTC or other private service providers platform and know about every train time table and movement as well as schedule and make full utilization of the information that you get. You can even check out the overall time it will take to reach at that station and so on. Have a safe and happy travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Traveled route</h3>
                        <span class="head-icon"></span>
                        <p>Want to know about the legend code traveled route? It could be easily checked out while seeing the running status of a train on Google maps. Legend code traveled route is been depicted in green coloured line specifying the source and final station names and other train information and movement. It is always advisable to go through online Indian Railways time table to know about certain details related to a train and grab info about each train movement.</p>
                        <p>In case you have been planning to go to a specific destination via train, it is good to go and check out Indian Railways time table and find out the list of all trains running in that specified distance and know about each one of them with ease and within clicks. Along with being aware of all the trains movement including arrival time, departure time, total distance that needs to be covered, time taken by the train in covering the overall distance, and more, checking out the live running status of trains and seeing it on Google maps actually enhances the user experience and make it engaging and fun. Along with traveled route, people can check out source station name, final station name, and much more without any trouble. To know about a particular station or other, people can move and take their mouse to that specified station and know more about the details in like no time. So access the web, make use of technology and enjoy a hassle free travel!</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code UnTraveled Route</h3>
                        <span class="head-icon"></span>
                        <p>Looking online for some information related to legend code untraveled route? Well, when you keep a track of any train and check it live on Google maps, then there are two stations that you can see as source station name and final station name in blue and green coloured icons. The route on which train is been moving and travelling is been highlighted in green coloured line shown in between the source and final station destination names. The legend code untraveled route is been depicted in blue coloured line and in case there comes anything in between the train route that is not been covered could be easily seen and depicted by seeing that blue colour line.</p>
                        <p>It is important to grab rail information about trains, train schedule and train movement especially if you have been planning to travel via train. Grab all the relevant information about trains and even about the legend code untraveled route by accessing the online time table and know about each detail related to train running status on that route and even about the number of trains operating on that specific route. It is always good to be sure and aware about different legend codes and others related to Indian Railways and the best part is simply within clicks and it is hardly a matter of few minutes. Passengers can grab knowledge about traveled route, source station, final station name, untraveled route, and more using the utility and if you also want to enjoy a hassle free and convenient travel, go for the same.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Current station</h3>
                        <span class="head-icon"></span>
                        <p>Checking out online for the meaning of ETA and ETD? This post will help you all in knowing more about these terminologies that are used in Indian railways frequently and passengers should be aware of the same. ETA means ‘Estimated time of arrival’ and ETD means ‘Estimated time of departure’ and it could be different as of actual time of arrival and actual time of departure.</p>
                        <p>To know about ETA and ETD, it is best and advised to keep a track of the train and check out its live running status. To do the same, people have to do nothing and just enter the train name or train number and instantly get the idea about the live running status of that train and it could be even watched on Google maps as well to have a better visual experience. Thinking of how to do the same? Just access Spot your train utility, enter the train name or train number and get detailed information about the running status of that train and the best part is within minutes, information could be fetched and passengers can know about the same including its arrival time, departure time, estimated time of arrival, estimated time of departure and so. So, keep an access to the same and know about each bit of information related to the train’s running status and make full use of the technology. And in case it is different than people can accordingly reach at the station on time and make wise decisions.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Queried station</h3>
                        <span class="head-icon"></span>
                        <p>Checking out information related to Legend code Queried station? You can easily check out the information related to that while keeping a track of live running status and seeing the same on Google maps. When you will do the same, you will come to know about legend code queried station, current station, untraveled route, traveled route, source station, final station, upcoming station and so with ease and within clicks.</p>
                        <p>Also have an access to the time table of Indian railways and you can actually grab all related information about the trains, train route and train movement running in between specified stations without any problem. And for this, you don’t even have to stand in long queues or something and instantly can know about anything you want to. So, stay tuned with the technology and make sure to know about each detail related to the train with ease and without any problem within minutes. When you use Spot your train utility, access the same, you will come to know and see the live running status of the train on Google maps, it would be definitely be a fun filling experience to check out running train depicted in red and blue colours, depicting for something and you can check out the queried station as well on the same page. What are you waiting for? Just check out the same right away and make all your confusions into clarity. Have a safe travel.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Train source station</h3>
                        <span class="head-icon"></span>
                        <p>Checking out online to grab some insight over legend code train source station? This post will help you in providing information related to the same in a brief way. Have you ever checked and visually watched the running status of trains over Google maps? If not, first of all, simply access spot your train utility over maps or enter the train name or train number and see it visually running over Google maps. Once you start seeing the running train over maps, you can see different colours like green, red depicting from where the train started and where it will finish its journey and about the travelled route as well.</p>
                        <p>Legend code train source station is the one from where the train originates and starts its journey and people can check out very clearly about the source station, final destination station, intermediate halts, travelled route in between and so. Well, people can always have an access to Indian Railways time table and know about its details related like source station, final station, travelling distance, travelling route along with other information like its arrival time, departure time, halts it is going to take in between the journey, and much more. So, simply check out time table online either at the official site of IRCTC or other recognized service providers platforms and grab an overview of everything within clicks. People can even check out the list of trains running over specified destinations without any hassle and could enjoy a trouble free journey.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Legend code Train destination station</h3>
                        <span class="head-icon"></span>
                        <p>Finding out relevant meaning to legend code train destination station? Well, here you can actually get an idea of the same. Have you ever checked live running status of a train moving on Google maps? If you check it out, you can see exactly where the train is moving over which route, along with its source station and destination station marked in different colours that is in red and blue. That destination station is been supposed to be known as legend code train destination station and while checking out this, you can be aware of much more than that.</p>
                        <p>There are different legend codes addressing to different train details like its source station, travelled route, upcoming station, untraveled route, current station, queried station, and more and everyone depicts something. Also, have a look at the Indian Railways time table as well and grab details related to train information, its route, stoppages and be aware of other trains as well running over that particular route so that in case of emergencies, you can think of opting for other trains as well. Online time table could be fetched easily over official site of IRCTC or other providers like of Travel Khana and details could be provided without any trouble and so that people can actually enjoy a hassle free and safe journey. So, get online check out time table right away and turn all your confusions into reality.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Railway Enquiry for Train Timing</h3>
                        <span class="head-icon"></span>
                        <p>Want to do a railway enquiry for train timing? Checking out how to do the same? Well, accessing Indian Railways time table is the best and promising means to not only enquire about the train timing but also get an idea about everything like the number of trains that are running over a particular route, its arrival time, departure time, and much more related information.</p>
                        <p>Apart from this, there is ‘139 Rail sampark service’ through which railway enquiry for train timing could be done without any trouble and all people need to do is to send an SMS to this number and get to know about any of the train within minutes. Along with train timing, people can check out pnr status, live running information and status of trains and much related and the information could be fetched within minutes and without any problem. So get to know about all such details, make optimal advantage of the technology and know about any specific train’s arrival, departure time, time needed to travel the same, number of halts within the journey, and even about other trains as well within minutes. So what are you thinking now? If you want to check out and enquire about any of the train, do the same by making use of these services and stay updated. Have a trouble free and enjoyable journey.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Mumbai Local Train Time Table</h3>
                        <span class="head-icon"></span>
                        <p>Want to know about Mumbai local train time table? Local trains are the best and prime means of transportation all across Mumbai city. Most of the professionals, working people, city dwellers and even locals of the city rely upon local trains running all across Mumbai and as per the information, the extensive network is been widespread having three major lines including the Western line, The Harbour line and the Central main line.</p>
                        <p>In case, you want to know train timings and train schedule of Mumbai local trains, the efficient and best way is to go through Indian Railways time table. Accessing online timetable is the key to get all train details and movement running all around Mumbai, covering so many aspects like arrival timing, departure timing, number of stoppages, number of days in which they are running, distance that needs to be covered and more. It is the best way to know latest and updated about train schedule and running information and be informed about them within minutes and without any hassle. All people need is an active internet connection and a system and can easily find out the time table with the help of web. Apart from this, many other things could be checked with the help of technology like pnr status, seat availability, and more. So, get an access to the same, be aware of latest train schedule and train movement and have a hassle free journey.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Which agency set the timetable for Indian Railways?</h3>
                        <span class="head-icon"></span>
                        <p>Wondering about the department that is responsible to set up the time table for the Indian Railways? Well, as per the sources and information, Operating department and Commercial department work in mutual understanding and discuss everything related to the trains and finally come up with the updated or revised time table and other information. These 2 department are entirely responsible to set the time table for Indian Railways covering all aspects like the stoppages where the trains will stop by, stations, platform numbers where the trains will arrive or depart from, timing, etc.</p>
                        <p>Indian Railways time table can be accessed without any hassle online and without any trouble. All it takes a system and an active internet connection and within clicks, people can access the time table, and know about running trains, their schedule, running information, halts during the travel, time taken by them to cover, and much related information. So, plan out your journeys in a hassle free and smooth way by having a glance at the time table online, get all information related to its schedule and such. It is always promising and effective to check out time table first and then only plan up your travel. Once the plans are been made, make sure to have a look at the seats available in trains so that in case they are not available, you have the flexibility to have a look in some other train and ensure a happy journey.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>How is the Indian railway different from Indian mean time?</h3>
                        <span class="head-icon"></span>
                        <p>Do you have a query in mind that how is the Indian Railway different from Indian mean time? If yes, here you will get your answer. Well, there is no difference at all in between Indian Standard Time and Indian Railways time. Indian Railways is known to follow the IST – Indian Standard Time that is GMT +5.30. In earlier times, Indian Railway firms has to fight up with the different cities local times and there has been a rapid and prompt expansion in the train routes with time, known to be extended out from Calcutta – Kolkata at present, Bombay – Mumbai at present, Madras – Chennai at present and Lahore. </p>
                        <p>At the end of 1860s, the situation gets worse as the networks gets linked up. In 1870s, to overrule and overshadow all the concerns and issues, it was decided to follow up Madras time for all railways. But the freedom of both Calcutta and Bombay resulted in the both Presidencies’ following up local times with the onset of 20th century. For all rest of the 19th century, Madras time was been followed and used by the railways. Later in year 1906, single time zone was been established based on Allahabad and there has been an introduction of standard time that Indian Railways has also adopted and come in the line with. In spite of all this, Calcutta was known to kept and follow its own time and Bombay his own till year 1945 in an unofficial way until 1955, when it was made official to follow one standard time.</p>
                     </div>
                     <div class="trainTable-wrap">
                        <h3>Why does the Indian Railway use the 24-hour time format?</h3>
                        <span class="head-icon"></span>
                        <p>Looking for information related to the timing that Indian Railways use? Have you ever thought that why does the Indian Railway use the 24 hour time format? Well, you will definitely get a little brief on the timing used in Indian Railways here. As it started its services in India introduced by the British, initially it was supposed to follow local times depending on the individual railway firms owning any of the regional railway lines. Gradually with time, with the huge expansion of the rail network, things have started getting critical causing a lot of confusion leading to the adoption of Madras time as the standard one because of the observatory for a telegraphic service synchronizing time in an efficient way and the longitude that was roughly midway in between Calcutta and Bombay. Afterwards, when Allahabad was been founded and introduced as the center for IST, the time is been considered as standard later on. The 24 hour military time format is been adopted later just to avoid any sort of confusion that could be there among people as it is always clear to mention 02:15 and 14:15 rather than just saying 02:15 AM and 02:15 PM. In a country that is been having so many cultures, languages, belongings, dialects and more, it was extremely important to adopt a specific standard way of keeping time. Is the information useful? Stay tune for more posts.</p>
                     </div>
                     <div>
                        <div class="trainTable-wrap">
                           <h3>Train 15212 Jan Nayak express time table Amritsar to Darbhanga station</h3>
                           <span class="head-icon"></span>
                           <p>Want to check out and know about the Train 15212 Jan Nayak express time table Amritsar to Darbhanga station? Getting an access to time table online is been easy and must to plan your schedule and travel in a well devised manner. Train 15212 Jan Nayak express is been running from Amritsar junction to Darbhanga junction and is been managed by East Central Indian Railways zone. It runs on daily basis and as per its time table, train 15212 takes 31 hours and 55 minutes to cover the entire distance of 1486 kms.</p>
                           <p>The average running speed of Jan Nayak express 15212 is 47 km/hour. Adding to this, it leaves from Amritsar station every day at 19:05 hours and reaches Darbhanga on second day of the travel at 03:00 hours. The leading halts at which it stops by for more time are Jalandhar city junction, Ludhiana, Ambala Cant, Yamunanagar Jagadhri, Saharanpur, Laksar, Najibabad, Moradabad, Sitapur city, Sitapur Cant, Burhwal, Gonda, Gorakhpur, Kaptanganj, Bagaha, Narkatiaganj, Bettiah, Bapudham Motihari, Muzaffarpur, Samastipur and Laheria Sarai. Do check out time table of any train first before planning anything else as it gives a clear idea about its running days, arrival and departure timings, stoppages, distance to be travelled and more. Accordingly people can have a look at the seats vacant and book them within time to stay away from any last minute hurdle. So, have a look at the same and make your journey well devised and approachable. Stay tuned for more posts. </p>
                        </div>
                     </div>
                     <div>
                        <div class="trainTable-wrap">
                           <h3>Shramjeevi express 12392 rajgir to new delhi</h3>
                           <span class="head-icon"></span>
                           <p>Looking for updated information related to Shramjeevi express 12392 Rajgir to New Delhi? Shramjeevi express is a daily based superfast running train supposed to be heading in between Rajgir station and New Delhi station. Known to be comprised with 24 coaches in total, it has been equipped with 3 AC III tier, 1 AC II tier, 12 sleeper class, 1 AC first class cum AC II tier, 1 pantry car, 2 general compartments and 4 luggage vans and brakes. Train 12392 is been running from New Delhi to Rajgir and as per its schedule, it departs from New Delhi every day at 01:15 hours and reaches very next day at Rajgir station at 10:10 hours. The distance covered by the train is 1103 kms. The leading stoppages it takes are at Moradabad, Bareilly, Lucknow Charbagh, Varanasi, Mughal Sarai, and Patna junctions at which it stops by for more time. It is always promising and effective to have a look at the online time table of the train 12392 at IRCTC site or at other dedicated sites and knows all about its schedule within minutes. The online time table helps in having a look over the same and know about the timings and running days at which it is been operating and according to the details, make sure to have a look at the seats available in the trains on the desired dates and have a safe and smooth travel. So, know about the same and have a safe and happy travel. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Godan Express -11055 ( Lokmanyatilak T to Gorakhpur Junction )</h3>
                           <span class="head-icon"></span>
                           <p>Checking out rail information related to Godan Express 11055 (Lokmanyatilak T to Gorakhpur Junction)? Here, you will get to know the schedule of Godan express 11055 that runs 4 days in a week with an average speed of 55 km/hour. As of now, it has been equipped with 23 coaches in total involving 1 AC II tier, 3 AC III tier, 4 unreserved, 13 sleeper class and 2 unreserved cum luggage and brake vans. It is been running from Lokmanya Tilak Terminus in Mumbai to Gorakhpur across the country and covers a total of 1731 kms. As per its time schedule, it leaves from Lokmanya Tilak Terminus at 10:55 hours and reaches Gorakhpur on second day of the journey at 20:20 hours. Including both source and final destinations, it takes halts at 23 stoppages in between the entire journey. To know more and completely related to Godan express 11055, it is best to have an access to its time table online and get to know about the stoppages, arrival time, departure time, distance travelled, travelling route and everything within clicks and minutes. The coaches are cleaned and it has a decent track in terms of cleanliness and punctuality. And once you have a look at its time table, you can check out the seats on the preferable dates in which you have been planning and do try to get reservations as soon as possible. Checking out the schedule of the train via Indian Railways time table online has its own advantages and helps to do planning in a better way.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Find Train Between Two Stations</h3>
                           <span class="head-icon"></span>
                           <p>Looking for a platform and a way how to find Train between Two Stations? Seeking for a list of all trains that are running in between specified destinations and so? Here, you will definitely get clearer picture of the same. You just have to open and get access to time table of Indian Railways at IRCTC site or any of the other dedicated platform and once it is done, mention there the source station name and the final station name and within a click, a list of all the trains that are in operational mode at present in between those specified destinations automatically will appear. Once you select any one of them, it will detail you with all the details related to that train in brief including the train name, number, station code, where it is stopping by, distance it has been covering, time taken by the train to cover the entire distance, and more. If you mention the date of journey as well, then more sorted list will appear and you will get detailed information there along with the fact that in which train seats are vacant and that too in which class of travel kind of information. So, find train between two stations using the platform and make sure that you got your bookings within time to avoid any hassle and problem for the last minute of time. Have a happy travel! </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Local Train Time Table</h3>
                           <span class="head-icon"></span>
                           <p>Want to get an access to Local Train Time Table online of Indian Railways? Want to know, how accessing train time table could help you in doing reservations and making plans in an efficient way? Indian railways is been handling one of the largest and widespread networks so efficaciously that it has been reckoned even at international scale. And local train time table is the best and quickest means to know about different types of trains that have been running all along across the country including Shatabdis, Rajdhanis, Durontos, Sampark Krantis, and other local trains as well. Adding to this, getting to have a look at and checking out the time table helps people to know all about the train’s whereabouts and schedule including its arrival time, departure time, distance that train covers, time that train takes to cover the distance, halts where it is stopping by, and more. It also gives them an idea about the running days of that train and more. Getting an idea about all the trains’ information in advance will definitely help people to make plans related to their holidays, business meetings and more. And it is always helpful and effective to do check out the time table online either at IRCTC site or other professional platforms been into offering latest update information about the trains and then accordingly make proper plans. Get confirmed seats at its earliest and ensure to have a hassle free and trouble free journey.</p>
                        </div>
                         <div class="trainTable-wrap">
                           <h3>About ntes Time Table</h3>
                           <span class="head-icon"></span>
                           <p>Looking for information related to about NTES Time Table? If yes, you will get an idea about that here. NTES stands for National Train enquiry system and to get access to NTES time table, people need to visit at enquiry.indianrail.gov.in/ntes/. Once train name or train number is been mentioned over there, within a click, it will depict all information related to that train involving the station name, source and final junction name, train schedule, day of journey, stops at which it is going to halt at, distance and time it will take and more enough to make sure and get an idea whether you need to travel in the same or not. Accordingly, it helps people to plan their journeys in a better and promising manner and the information also lets them know about the respective arrival and departure time from each junction and whether they are boarding or de-boarding the same. Aside from grabbing all such details related to time table of the train, people can make use of other features and services related to trains including knowing about the live station, trains in between stations, trains schedule, trains cancelled, rescheduled trains or diverted trains and so. With the help of technology, people can get access to all latest updates of trains online within minutes and accordingly make decisions and make optimal use of their time. So, make your planning better by checking out the ntes time table of the trains and make sure to get bookings at its earliest. </p>
                        </div>
                         <div class="trainTable-wrap">
                           <h3>Train platform inquiry online</h3>
                           <span class="head-icon"></span>
                           <p>Looking for a way how to check out train platform inquiry online? The key to do the same is actually getting an access to Indian Railways time table first and know about the schedule of the train and if still there is an inquiry to be made online, look for the NTES platform or IRCTC site, where they have an inquiry section separately for the users to feed in their concerns and look for the solutions relevant to them. In India, people prefer highly to travel all around via trains as compared to other mass transportation modes and rail travel is undoubtedly fun and memorable one. To make good planning when it comes to rail travel, it is pretty important to do check out first the time table online and get to know about its schedule, timing and so and accordingly afterwards make sure to get the seats and reservations in the same within time and avoid any fuss for the last minute of time. Also, to post train platform inquiry online at these forums, do go through the older and recent posts first and try to get your answers. If it is not resolving your concern, then try to add a new post there and expect to get your answer within shortly. Aside from this, checking out online time table of a train is always helpful and beneficial to make your travel accurate and hassle free. So, always make sure to get access to the same before planning anything further and have a safe journey.  </p>
                        </div>
                         <div class="trainTable-wrap">
                           <h3>What is the difference between intercity and express train? </h3>
                           <span class="head-icon"></span>
                           <p>What is the difference between intercity and express train? If you are looking for the answer, go on and read the following post. The very first thing that everyone must be aware of is that all intercity trains are supposed to be express trains. The other thing is that intercity trains are supposed to head in between two cities and that too with a travel time range of about 7 hours. Most of them complete the trip from both sides within the same day. On the other hand, express trains are supposed to run or operate for longer distances and usually return on next day. Aside from this, intercity trains are known to be operating with single rake and on the other side, express trains have multiple of them, all depending on the duration of the journey as well as its schedule defined. The best way to know more about them and figure out whether a train is an intercity one or an express one or a superfast one, go through the time table of the trains. It covers all basic information about them and people can actually read on the details to know more about them and it will hardly take minutes of yours to ascertain and get knowledge. Along with grabbing an insight about the type of the train, users can also know about the arrival and departure timings of the train, distance travelled, time taken, and so much more just by going through its time table.  </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What is the difference between express and mail train?</h3>
                           <span class="head-icon"></span>
                           <p>What is the difference between express and mail train in Indian Railways? Well, here you will definitely grab an idea of them for sure. Indian Railways is been managing different sort of rail services, and with an average operating speed of 55 km/hour. Express trains and superfast and premium trains are among the most regular service trains of Railways. Mail trains are kind of similar to express trains but with more halts in between the travel and less average speed. Just for the record, the fastest mail train running in India as of now is Maharashtra Sampark Kranti express. Other side, express trains are reckoned as semi priority passenger rail service trains, running with an average operating highest speed of 55 km/hour in India. The speed of them is been determined from the latest Railways time table. Mail trains could be express or even superfast, all depending on the RMS coach attached with the train or not. The RMS term depicts for Rail Mail service that is associated with few of the trains been into operational mode as of now. To know more into detail about a train and whether it is a mail train or an express one or a superfast one, do check out and have a look at the Indian railways time table and know about everything in detail and within few minutes. And after checking out the time table and knowing about the trains schedule and route, do book the reservations and enjoy your travel. </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer1.html' ?>
      <!-- footer Ends Here -->
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
            </div>
         </div>
      </div>
      <!--popup ends here-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/validate-js/2.0.1/validate.min.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
      <script defer type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script src="/glimpse/src/js/seat-aval.js?v=0.6"></script>
      <script src="/glimpse/src/js/timeTable.js?v=0.2"></script>
      <script type="text/javascript">
         $(document).ready(function(){
         if ($.cookie('indexError') != null ){
           var value = $.cookie("indexError");
           document.getElementById("myModalLabel").innerHTML =value;
           $('#myModal').modal('show');
           delete_cookie('indexError');
           setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
         }});
         var delete_cookie = function(name) {
           document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         };
      </script>
   </body>
</html>