<?php 
include '../common/main.php'; 
if (isset($_GET["pnr"]) && !empty($_GET['pnr'])) { 
  $pnr = $_GET["pnr"];
}
$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/pnr/status/'.$pnr.'/?access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);
if (strpos($http_response_header[0], "200")) {

  if(empty($json)){
      setcookie('indexError','{"message":"Some Problem Occurred Please Try Again","pnr":"'.$pnr.'"}', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com/travelkhana/check-pnr-status"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);
   echo $json_o->responseStatus;
  
  if(empty($json_o->trainNo)){
    setcookie('indexError','{"message":"Cannot fetch PNR Detail,Please Try Again","pnr":"'.$pnr.'"}', time() + (86400 * 30), "/");
    header("Location: https://www.travelkhana.com/travelkhana/check-pnr-status");  
    exit;
  }else{
    $trainNo= $json_o->trainNo;
    $pnrStatus=$json_o->passengerDetail;
   }
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError','{"message":"Some Problem Occurred Please Try Again","pnr":"'.$pnr.'"}', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/check-pnr-status"); 
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', '{"message":"Invalid access token","pnr":"'.$pnr.'"}', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/check-pnr-status"); 
    exit;
}else{
    setcookie('indexError', '{"message":"Some Problem Occurred Please Try Again","pnr":"'.$pnr.'"}', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/check-pnr-status");
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Check PNR Status - Get Indian Railway PNR Status - PNR Status Enquiry - TravelKhana.Com</title>
      <!-- CSS -->
      <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css?v=2">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
            <table align="left"  style="padding:3px 3px 3px 3px; width:728px; border:none;">
              <tr>
                  <td>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- 728x90 TravelKhana -->
                    <ins class="adsbygoogle example_responsive_1"
                      style="display:inline-block;"
                      data-ad-client="ca-pub-5798678675120444"
                      data-ad-slot="3492216211"></ins>
                    <script>
                      (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                  </td>
              </tr>
            </table>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="availability-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h1>PNR Status</h1>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">PNR Status</h2>
                     <form class="form-inline mobile-form" id="submit_form" method="post">
                      <div class="input-warp">
                        <div class="input_bg mobile-bg">
                           <div class="form-group">
                              <input type="text" class="form-control pnr-inputwidth required integerfiled" maxlength="10" value="<?php echo $json_o->pnr; ?>" id="pnrstatus" name="pnrstatus" maxlength="15" data-minimum="15" placeholder="PNR Number">
                              
                           </div>
                          </div>
                        <button id="submit_btn" type="button" class="btn btn-search">CHECK STATUS</button>
                     </form>
					 </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of availability-bg-->
         <div class="trainTable-content">
            <div class="container">
<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>            
               <div class="row">
                  <div class="col-md-12">
				  <div class="PNR-wrap">

              <!-- Enuke 12 oct 2017 -->

              <div class="show-status">
                  <h2> <i class="fa fa-check-circle"></i> Charting Status: <?php echo $json_o->chartingStatus; ?></h2>
                  
                  <div class="status-table">
                     <h4>Passenger Details</h4>
                     <div class="status-table-detail">
                        <div class="table-responsive">
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <!-- <td>Passenger</td>
                                    <td>Coach/Seat</td>
                                    <td class="hidden-xs">Booking Status</td>
                                    <td>Status</td> -->
                                    <td>Passenger</td>
                                    <td>Booking Status</td>
                                    <td class="hidden-xs">Current Status</td>
                                 </tr>
                              </thead>
                              <tbody>
                              <?php
                                 
                                 foreach($pnrStatus as $item) { 
                                 echo "<tr>
                                    <td class='text-center'>".$item->passenger."</td>
                                    <td>".$item->bookingStatus."</td>
                                    <td class='hidden-xs'>".$item->currentStatus."</td>
                                    
                                 </tr>";
                              }
                               ?>
                                 <!-- <tr>
                                    <td class="text-center">2</td>
                                    <td>HA1/18</td>
                                    <td class="hidden-xs">W/L22 GNWL</td>
                                    <td><span class="stauts-check">Confirmed</span></td>
                                 </tr> -->
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>

<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>
				  <p class="tab-head text-center">PNR you have searched</p>
				    <ul class="dept-tabel PNR-info ">
						<li>
						<span class="dept-head">PNR  </span>
						<span><?php echo $json_o->pnr; ?></span>
						</li>
						<li>
						<span class="dept-head"> Train      </span>
						<span><?php echo $json_o->trainNo.' / '.$json_o->trainName; ?></span>
						</li>
						<li>
						<span class="dept-head">  From      </span>
						<span><?php echo $json_o->from; ?></span>
						</li>
						<li>
						<span class="dept-head">   To                  </span>
						<span><?php echo $json_o->to; ?></span>
						</li>
						<li>
						<span class="dept-head">    On                  </span>
						<span> <?php echo $json_o->boardingDate; ?></span>
						</li>
						<li>
						<span class="dept-head">  Coach Class               </span>
						<span><?php echo $json_o->coachClass; ?></span>
						</li>
						</ul>
                     <div class="trainTable-wrap">
					    <h3>Confirming your PNR status with TravelKhana</h3>
                        <span class="head-icon"></span>
                        <p>You must have heard about the term PNR. It is one of the most crucial things when it comes to booking train tickets. Do you know what PNR means?</p>
                        <p>You must have heard about the term PNR. It is one of the most crucial things when it comes to booking train tickets. Do you know what PNR means?</p>
						<p>Generally, a PNR number contains passenger details like Name, Sex, Age, DOB and Berth preference. It also has travel details like the Ticket No, Coach No., Seat No., Date, place up to which destination the ticket is reserved, Class and Quota. The PNR number also states the mode of payment that has been used to buy the tickets.<strong>Waiting List Tickets and their Confirmation Precedence</strong></p>
						</div>
                     <div class="trainTable-wrap">
                        <h3>Why is the PNR number so crucial?</h3>
                        <span class="head-icon"></span>
                        <p>The PNR number is absolutely crucial for the Computer Reservation System database to store passenger information. But the PNR number is also extremely important for you. Your PNR number will be listed on the right-hand side top corner of your ticket. It is typically a combination of three and seven digits, thus forming a hyphenated ten-digit number. Once you know this number, you can check PNR Status. Wondering why do you need to check your PNR status? Well, think about it. If you want to double check your reservation a couple of days before your travel, then you can do so with the help of your PNR number.</p>
						<p>The PNR number comes most in handy when you do not have a confirmed ticket. Let's say that you have RAC (Reservation against Cancellation) or a Waitlisted ticket. How do you know whether your ticket has moved up on the list? How do you keep a track on the progress of your booking? Well, all you need to do is know your PNR number, and you can check your booking status either online or via SMS. With just a click of your button, you will be available to access your booking status!</p>
						<span class="sub-pnrh">How do I check my PNR status?</span>
						<p>There are mainly four ways through which you can check PNR Status. First, through the online portals, second, through SMS services, third, through various applications downloaded on your phone or personal computer, and fourth, once the reservation chart is put up four hours before the time of your travel.</p>
						<p>Let us take a look at these options one by one to understand them better:</p>
                        
						<span class="sub-pnrh">Through online portals:</span>
						<p>The most common way to check out the status of your booking with the help of your PNR number is through online portals. Most of us have internet facilities in your phones these days, and hence, accessing the sites is no difficulty at all.</p>
						<p>This is where TravelKhana comes really in handy. TravelKhana provides you with the option of ordering meals and getting them delivered at the desired stations, hot and fresh! It has tie-ups with various restaurants in many Indian cities and hence their service is very effective. Apart from its basic function of making sure that you get the dish that you want to eat on your journey, it also provides you the option of checking the status of your ticket, through the PNR tracking method. All you have to do is visit the page, and it provides you the option to <strong>check PNR Status.</strong></p>
						<p>Another online portal that makes your PNR tracking really easy is IRCTC. They are officially tied with the Indian Railways and every online booking or cancellation of any ticket that is done through IRCTC. You can visit their site,<strong> enter your PNR number and check the status of your booking.</strong></p>
						<p>It is really helpful for your journey if you keep these two online portals in mind.</p>
						<span class="sub-pnrh">Through SMS:</span>
						<p>If you do not have internet connection on your phone or wish for an instant update on your booking, then memorize the number 139. All you have to do is type 'PNR' and send it to 139. Within a few minutes, they will send your ticket details and other necessary information.</p>
						<span class="sub-pnrh">Through applications on your smartphone:</span>
						<p>There are various mobile applications available that provide you an option to check out your PNR status. Download them and keep them on your phone since they come really in handy on the day of your travel to get some last moment checking done.</p>
						<span class="sub-pnrh">Help at the Enquiry counter of the Railways:</span>
						<p>If you want to keep it traditional, then you can take the pain of going to the enquiry helpdesk, give them your PNR number and enquire about your booking status. However, if you are planning to visit the counter on the day of your travel, then make sure that you reach the station with enough time in hand since there just might be a queue outside it! It is best if you avoid this method and opt for any of the above three ones.</p>
						<span class="sub-pnrh">Few things to remember while travelling in trains </span>
						<p>You need to carry your tickets on your journey. There are generally two types of tickets available. I-ticket, the one which you collect directly from the railway booking counter and E-ticket, the one which you book through IRCTC. You need to carry a print out of this ticket or the SMS, which IRCTC will send to your registered mobile number. Also, when you are travelling with an E-ticket, you have to make sure that you carry one age and ID proof with you.</p>
						<p>Lastly, most important of all, do not forget whatever meal you feel like eating while on your journey, TravelKhana is always with you. All you have to do is select the restaurant from the ones they have registered with them (which is quite frankly, a lot!), select a meal of your choice, confirm your order and TravelKhana will have the food waiting for you at the mentioned station!</p>
						<p>You neither have to wait nor do you have to check; once you confirm your order, it is confirmed from TravelKhana!</p>
						<span class="sub-pnrh">Get your reservation status via SMS </span>
						<p>Getting a reserved ticket is one of the most important tasks that you need to take care of when booking a railway ticket with the Indian Railways. More often than not we find ourselves with tickets that are either on the Wait List or the Reservation against Cancellation category. So how do you know whether your name has gone up in the list or not, how do you find out whether your ticket has been confirmed or not!</p>
						<p>All of your doubts can very easily be cleared if you know your PNR number. The PNR number is the Passenger Name Record Number under which all your travel information, like your date of travel, the train, name, age, sex, date of birth, quota (if any), class, etc. are stored. The PNR number was originally started by the airlines to keep track of their passengers who had to change flights for their journey and eventually this system was adopted by the Indian Railways as well.</p>
						<p>Today, you can access your travel details and status of your ticket with the help of your PNR number. There are a number of ways through which you can find out the status of your ticket with the help of the PNR number. The most convenient one is definitely by getting an SMS directly to your registered phone number.</p>
						<span class="sub-pnrh">How to find your PNR number</span>
						<p>Your PNR number will be given on the ticket itself. Look to the top left-hand side corner of your ticket, if you have collected it from a railway counter or you will find your PNR number at the top in a separate cell of your E-ticket. If your mobile number is registered with the website that you will be booking your ticket from, all your travel details, including your PNR number will be sent to your mobile.</p>
						<span class="sub-pnrh">PNR confirmation via SMS</span>
						<p>So, if you want to find out the status of your ticket through SMS, then all you have to do is follow a few simple steps. Let's take a look at them:</p>
						<ul class="schedule-list">
                              <li>First, you will have to write "PNR" followed by the PNR number which is of 10 digits.</li>
                              <li>After that, send it to 139.</li>
							   <li>You will get a SMS stating your status within a few minutes on your phone.</li>
							   <p>However, when sending the SMS, you have to remember that it is not a toll-free number, and hence, you will be charged Rs.3 for every SMS that you send to check on your PNR status.</p>
							   <p>More often than not people find themselves in a fix when they cannot access the online railway websites. This is a line that has huge traffic and hence often we are unable to access these sites from where we can check our PNR status. In cases of emergency, like checking your reservation status just a few moments before your travel, before the chart has been put up, you can always rely on this SMSing service, which will unfailingly deliver the status detail to you.</p>
							    
                           </ul>
                        </div>
						</div>
						</div>
                     </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer1.html' ?>
      <!-- footer Ends Here -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script src="/glimpse/src/js/autocomplete.js"></script>
      <script src="/glimpse/src/js/pnrStatus.js?v=0.1"></script>
      <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
	  
   </body>
</html>


