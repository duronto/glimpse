<?php
include '../common/main.php'; 
$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/train/popular/?access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);

if (strpos($http_response_header[0], "200")) {


  if(!empty($json_o)){
      setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);
  $t=(int)$json_o->status;
  if($t == 0){
    setcookie('indexError', $json_o->message, time() + (86400 * 30), "/");
    header("Location: https://www.travelkhana.com"); /* Redirect browser */
    exit;
  }else{
    //echo 'in success';
    $trainList= $json_o->data;
    //echo $trainList[0]->trainNo;
    $length = count($trainList);
   }
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com"); /* Redirect browser */
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com"); /* Redirect browser */
    exit;
}else{
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com"); /* Redirect browser */
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Popular Train List</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
          <div class="availability-bg">
           <div class="container">
               <div class="row">
          <div class="back-to-order text-center visible-xs">
            <h3>List of Popular Trains</h3>
            <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
          </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">List of Popular Trains</h2>
           <form class="form-inline mobile-form" id="submit_form"  method="post">
           <div class="input-warp">
            <div class="input_bg mobile-bg input-width">
            
             <div class="form-group">
           <input type="text" class="form-control required" id="trainnum" name="trainnum"  placeholder="Train Number/Name" required>
            <input type="hidden" id="from" name="from" value="">
     
              </div>
                </div>
            <button id="popuTrain" type="button" class="btn btn-search">SEARCH</button>
             </form>
             </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of availability-bg-->
     <div class="popular-trainWrap">
     <div class="container">
        <div class="row">
      <div class=" col-md-12 col-sm-12 avail-wrap text-center">
                  <h3>These are some of the trains that we delivered to:</h3>
           <span  class="head-icon"></span>
           </div>
        <div class="col-md-6 col-sm-12">
      <ul class="popular-train">
            <?php
               for ($i = 0; $i < $length/2; $i++) {
                     //print $trainList[$i];
                     echo "<li><i class='fa fa-train' aria-hidden='true'></i><a href='https://www.travelkhana.com/travelkhana/jsp/menu.html?trainNumber=".$trainList[$i]->trainNo."&fromstation=".$trainList[$i]->originStation."'>".$trainList[$i]->trainNo." / ".$trainList[$i]->trainName."</li>";
               }
            ?>
            
          <!--  <li><i class="fa fa-train" aria-hidden="true"></i> 12916 / Ashram Express(DLI-ADI) </li>
                  <li><i class="fa fa-train" aria-hidden="true"></i>12834 / Howrah Ahmedabad Express(HWH-ADI) </li> -->
                 
                  <!-- <a href="#" class="viewMore-link">View More...</a> -->
           </ul>
      </div>
        <div class="col-md-6 col-sm-12">
      <ul class="popular-train">
            <?php
               for ($i = $length/2+1; $i < $length; $i++) {
                     //print $trainList[$i];
                     echo "<li><i class='fa fa-train' aria-hidden='true'></i><a href='https://www.travelkhana.com/travelkhana/jsp/menu.html?trainNumber=".$trainList[$i]->trainNo."&fromstation=".$trainList[$i]->originStation."'>".$trainList[$i]->trainNo." / ".$trainList[$i]->trainName."</a></li>";
               }
            ?>
           <!-- <li><i class="fa fa-train" aria-hidden="true"></i> <a href="https://www.travelkhana.com/travelkhana/jsp/menu.html?trainNumber=12916&fromstation=DLI">12655 / Navjeevan Express(ADI-MAS)</a>  </li>
            <li><i class="fa fa-train" aria-hidden="true"></i> 12987 / Sealdah Ajmer Express(SDAH-AII)  </li>
                 <li><i class="fa fa-train" aria-hidden="true"></i> 12723 / Andhra Pradesh Express(HYB-NDLS) </li> -->
                        <!--  <a href="#" class="viewMore-link" onclick="viewMore()">View More...</a> -->
                <!-- <span id="viewMore" class="viewMore-link" onclick="viewMore()">View More...</span>  -->
           </ul>
      </div>
         </div> 
     </div>
     </div>
      <!--end of popular-trainWrap-->

      
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
      </div>
      </div>
    </div>
  <!--popup ends here-->

     </section>
         <!--footer Start Here-->
            <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

     <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
     <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
     <script src="/glimpse/src/js/custom.js"></script>
     <script type="text/javascript">
    function checkredirect(){

    var trainnum = $("#trainnum").val().split('/');
    console.log("in train"+trainnum[0]);
    var trainno = trainnum[0];

trainno && ("undefined" != typeof ajaxAct && ajaxAct.abort(), ajaxAct = $.ajax({
        url: "https://s3.ap-south-1.amazonaws.com/gatimaancms/trainJson/stationNameByTrain/" + trainno + ".json",

        type: "get",
        success: function(e) {
          //alert("success");
          //  alert(JSON.parse(e));
            var res=JSON.parse(e);
            var station=res[1].station.split('/');
            console.log(station[0]);
            document.getElementById('from').value = station[0];
             var value_1 = $('#trainnum').val();
            //  alert('https://stage.travelkhana.com/travelkhana/jsp/menu.html?trainNumber='+value_1+'&fromstation='+ document.getElementById('from').value);
              window.location = 'https://www.travelkhana.com/travelkhana/jsp/menu.html?trainNumber='+value_1+'&fromstation='+ document.getElementById('from').value;
              return false;
          },
        error: function(error){   
        console.log("failure"); 

        document.getElementById("myModalLabel").innerHTML ="No Food Available In This Train";
        $('#myModal').modal('show');
        setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
        }
    }));

    }
    function validateTrain(e) {
      var t = document.getElementById("trainnum").value;
      return "" == t ? ($("#trainnum").addClass("error"),$("#trainnum").focus(), document.getElementById("trainnum").value = "Enter Train No/ Name", !1) : -1 == t.indexOf("/") || t.indexOf("/") > 5 ? ($("#trainnum").addClass("error"), document.getElementById("trainnum").value = "Enter Train No/ Name", !1) : 0 == $.isNumeric(t.substring(0, t.indexOf("/"))) ? ($("#trainnum").addClass("error"), document.getElementById("trainnum").value = "Enter Train No/ Name", !1) : t.substring(t.indexOf("/") + 1, t.length).length < 5 ? ($("#trainnum").addClass("error"), document.getElementById("trainnum").value = "Enter Train No/ Name", !1) : !0
    }
    $("#popuTrain").on("click", function() {
            if (!validateTrain(1)) return console.log("validateTrain not validated"), !1;
            else{
              checkredirect();
            }
    })
    </script>
   
   </body>
</html>


