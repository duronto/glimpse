<?php 
include '../common/main.php'; 
if (isset($_GET["from"]) && !empty($_GET['from'])) { 
  $from = $_GET["from"];
  $to = $_GET["to"];

  $from1 = explode("/",$_GET["from"]);
  $to1 = explode("/", $_GET["to"]);

  $from2 = $from1[1];
  $to2 = $to1[1];

  $date = $_GET["date"];
}
$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/trainBetweenCities/?from='. urlencode ($from)."&to=".urlencode($to).'&date='.$date.'&access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);
if (strpos($http_response_header[0], "200")) {

  if(empty($json)){
      setcookie('indexError', 'No Train For Provided Input, Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);
  //print_r($json_o);
  /*$trainNo= $json_o->trainNumber;
    echo $trainNo;*/
  $trainBetweenCities= $json_o;
  $length = count($trainBetweenCities);
  if($length==0){
      setcookie('indexError', 'No Train For Provided Input, Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); /* Redirect browser */
      exit;
  }
 /* $t=(int)$json_o->status;
  if($t == 0){
    setcookie('indexError', $json_o->message, time() + (86400 * 30), "/");
    header("Location: order.php");  
    exit;
  }else{
    //echo 'in success';
    $trainNo= $json_o->trainNumber;
    echo $trainNo;
   // $length = count($trainList);
   }*/
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); 
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); 
    exit;
}else{
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table");
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title><?php echo $from2?> to <?php echo $to2?></title>
      <!-- CSS -->
      <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="delhi-panvel-bg">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 static-heading">
                     <h2><?php echo $from2?> to <?php echo $to2?> Trains</h2>
                    
                  </div>
               </div>
            </div>
         </div>
         <!--end of policy-bg-->
         <div class="delhi-panvel-content">
            <div class="container">
<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>
               <div class="row">
			    <div class="col-md-12">
					<div class="content-wrap">
						<div class="table-responsive delhi-panvel-table">
							<table class="table table-bordered">
								<thead class="gray-bg">
									<tr>
										<td>TRAIN NUMBER/ TRAIN</td>
										<td align="center">Origin/ Arrival</td>
										<td align="center">Destination/ Departs</td>
										<td class="hidden-sm hidden-xs" align="center">Runs From Source On</td>
									</tr>
								</thead>
								<tbody>
								<?php 

								 foreach($trainBetweenCities as $item) { //foreach element in $arr
                  					//$item->foodAvailablity;
								echo "
									<tr>
										<td>
											<div class='train-name'>
                        <a href='https://www.travelkhana.com/travelkhana/utilities/trainStations.jsp?train=".$item->trainNumber."'>
												<h2>".$item->trainNumber." / ".$item->trainName."</h2>
                        </a>
											</div>
										</td>
										<td>
											<div class='train-origin'>
												<h4>".$item->fromStationName."</h4>
												<span class='time'>".$item->fromArrivalTime."</time>
											</div>
										</td>
										<td>
											<div class='train-destination'>
												<h4>".$item->toStationName."</h4>
												<span class='time'>".$item->toDepartureTime."</time>
											</div>
										</td>
										<td class='hidden-sm hidden-xs'>
											<div class='train-run'>
												<ul class='list-inline'>";
												if ($item->sunday){echo "<li class='active'>S</li>";}else{echo"<li>S</li>";}
												if ($item->monday){echo "<li class='active'>M</li>";}else{echo"<li>M</li>";}
												if ($item->tuesday){echo "<li class='active'>T</li>";}else{echo"<li>T</li>";}
												if ($item->wednesday){echo "<li class='active'>W</li>";}else{echo"<li>W</li>";}
												if ($item->thursday){echo "<li class='active'>T</li>";}else{echo"<li>T</li>";}
												if ($item->friday){echo "<li class='active'>F</li>";}else{echo"<li>F</li>";}
												if ($item->saturday){echo "<li class='active'>S</li>";}else{echo"<li>S</li>";}
												echo "</ul>
											</div>
										</td>
									</tr>
									";
								}
								?>								
								</tbody>
							</table>
						</div>
                    </div> 
                    
                    
                    
				</div>
               </div>

<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>
               
            </div>
         </div>
         <!--end of policy-content-->
		 </section>
         <!--footer Start Here-->
 		<?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      </section>
   </body>
</html>