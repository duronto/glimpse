<?php 
include '../common/main.php'; 
$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/stations/popular/?access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);

if (strpos($http_response_header[0], "200")) {


  if(!empty($json_o)){
      setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: order.php"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);
  $t=(int)$json_o->status;
  if($t == 0){
    setcookie('indexError', $json_o->message, time() + (86400 * 30), "/");
    header("Location: order.php"); /* Redirect browser */
    exit;
  }else{
    //echo 'in success';
    $trainList= $json_o->data;
    //echo $trainList[0]->trainNo;
    $length = count($trainList);
   }
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: order.php"); /* Redirect browser */
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: order.php"); /* Redirect browser */
    exit;
}else{
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: order.php"); /* Redirect browser */
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title> Station Code</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="stationcode-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h3>Indian Railway Station Code</h3>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">Indian Railway Station Code</h2>
                     <form class="form-inline mobile-form" id="submit_form" action="" method="post">
                      <div class="input-warp">
                        <div class="input_bg mobile-bg">
                           <div class="form-group">
                              <input type="text" class="form-control pnr-inputwidth required ac_input" id="stationcode" name="stationcode" maxlength="15" placeholder="Station Name" autocomplete="off">
                           </div>
                          </div>
                        <button id="search" type="button" class="btn btn-search mrleft-7">Search</button>
                     </form>
					 </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of stationcode-bg-->
        <div class="stationTable-content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
				      <div class="col-table1">
					    <div class="">
				<table class="table table-bordered ">
				  <thead>
					<tr class="tabel-head">
					  <th colspan="3">Station Name</th>
					  <th>Station Code</th>
					 </tr>
				  </thead>
				  <tbody>
             <?php
               for ($i = 0; $i < $length/2; $i++) {
                     //print $trainList[$i];
                     echo "<tr>
					  <th class='station-name width-70' colspan='3'><a href='https://www.travelkhana.com/travelkhana/food-delivery-at-".$trainList[$i]->stationName."-railway-station-".$trainList[$i]->stationCode."'>".$trainList[$i]->stationName."</a></th>
					  <td><a href='https://www.travelkhana.com/travelkhana/food-delivery-at-".$trainList[$i]->stationName."-railway-station-".$trainList[$i]->stationCode."'>".$trainList[$i]->stationCode."</a></td>
					  
					</tr>";
                 }
            ?>
<!-- 					<tr>
					  <th class="station-name width-70" colspan="3">New Delhi Railway Station</th>
					  <td> NDLS   </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Delhi Railway Station	</th>
					  <td>DLI </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Gorakhpur Railway Station 		</th>
					  <td> GKP	 </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Howrah Railway Station	</th>
					  <td>HWH </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Lucknow Railway Station 		</th>
					  <td>LKO </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Madgaon Railway Station 	</th>
					  <td>MAO </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Ernakulam Railway Station 	</th>
					  <td>ERS </td>
					 
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Coimbatore Railway Station	</th>
					  <td>CBE </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Thrissur Railway Station  	</th>
					  <td>TCR </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Varanasi Railway Station	</th>
					  <td>BSB</td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Bhopal Railway Station	</th>
					  <td>BPL </td>
					 </tr> -->
					</tbody>
				</table>
				</div>
				</div>
					  <div class="col-table2">
					     <div class="">
				<table class="table table-bordered ">
				  <thead>
					<tr class="tabel-head colTabel2-head">
					  <th colspan="3">Station Name</th>
					  <th>Station Code</th>
					 </tr>
				  </thead>
				  <tbody>
				   <?php
               for ($i =($length/2)+1; $i < $length; $i++) {
                     //print $trainList[$i];
                     echo "<tr>
					  <th class='station-name width-70' colspan='3'><a href='https://www.travelkhana.com/travelkhana/food-delivery-at-".$trainList[$i]->stationName."-railway-station-".$trainList[$i]->stationCode."'>".$trainList[$i]->stationName."</a></th>
					  <td><a href='https://www.travelkhana.com/travelkhana/food-delivery-at-".$trainList[$i]->stationName."-railway-station-".$trainList[$i]->stationCode."'>".$trainList[$i]->stationCode."</a></td>
					  
					</tr>";
                 }
            ?>
					<!-- <tr>
					  <th class="station-name width-70" colspan="3">Secunderabad Railway Station</th>
					  <td class="col-table-td"> SC  </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Pune Railway Station	</th>
					  <td>PUNE </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Jaipur Railway Station	</th>
					  <td> JP	 </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Ahmedabad Railway Station	</th>
					  <td>ADI</td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Chennai Central Railway Station		</th>
					  <td>MAS </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Bangalore City Railway Station	</th>
					  <td>SBC </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">surat Railway Station	</th>
					  <td>ST</td>
					 
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Tirupati Railway Station	</th>
					  <td>TPTY </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70 " colspan="3">Hubli Railway Station 	</th>
					  <td>UBL</td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Allahabad Railway Station	</th>
					  <td>ALD </td>
					  
					</tr>
					<tr>
					  <th class="station-name width-70" colspan="3">Patna Railway Station	</th>
					  <td>PNBE</td>
					 </tr> -->
					</tbody>
				</table>
				</div>
					  </div>
						</div>
                     </div>
               </div>
            </div>
         </div>
         <!--end of stationTable-content-->
      </section>
      <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
      <!-- footer Ends Here -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
	   <script type="text/javascript">
      function checkredirect(){
        var value_1 = $('#stationcode').val();
        var fields = value_1.split('/');
        var code = fields[0].trim();
        var name = fields[1].trim();
        window.location = 'https://www.travelkhana.com/travelkhana/food-delivery-at-'+name+'-railway-station-'+code;
        return false;
      }
      function checkStationFound(e) {
    return $.inArray(e, keyStations) > -1 ? !0 : !1
}
      function validatesrcstn() {
         return "" == document.getElementById("stationcode").value ? ($("#stationcode").addClass("error"),console.log("error blank"), document.getElementById("stationcode").value = "Enter Boarding Station", !1) : checkStationFound(document.getElementById("stationcode").value) ? !0 : ($("#stationcode").addClass("error"),console.log("2error blank"), document.getElementById("stationcode").value = "Enter Boarding Station", !1)
}
      $("#search").on("click", function() {
            if (!validatesrcstn()) return console.log("validateTrain not validated"), !1;
            else{
              checkredirect();
            }
      })
      </script>
   </body>
</html>



