<?php 
include '../common/main.php';
if (isset($_GET["train"]) && !empty($_GET['train'])) { 
  $train = $_GET["train"];
}

$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/train/'.$train.'/route/?access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);
if (strpos($http_response_header[0], "200")) {

  if(empty($json)){
      setcookie('indexError', 'No Route For This Train, Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table");
      exit;
  }
  $json_o=json_decode($json);


  /*$trainNo= $json_o->trainNumber;
    echo $trainNo;*/
  $traintimetable= $json_o->traintimetable;
 /* $t=(int)$json_o->status;
  if($t == 0){
    setcookie('indexError', $json_o->message, time() + (86400 * 30), "/");
    header("Location: order.php");  
    exit;
  }else{
    //echo 'in success';
    $trainNo= $json_o->trainNumber;
    echo $trainNo;
   // $length = count($trainList);
   }*/
}
else if (strpos($http_response_header[0], "406")) { 
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); 
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table"); 
    exit;
}else{
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com/travelkhana/indian-railways-time-table");
    exit; 
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title> Train Seat Availability, Running Status, TimeTable</title>
  
      <meta name="description" content= "Searching for (null) ? Browse all the details for null  Train & check seat availability, Running Status, Timetable & much more at travelkhana.com"/>

      <!-- CSS -->
      <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="about_us" class="inner-search-content">
         <div class="trainTime-bg">
            <div class="container">
<div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>            
               <div class="row">
                  <div class="col-md-12 heading-wrap">
                     <h1>Train Timetable</h1>
                     <p><?php echo $json_o->trainNumber." / ".$json_o->trainName;?> Route and schedule</p>
                  </div>
               </div>
            </div>
         </div>
         <!--end of trainTime-bg-->
         <div class="table-sec">
		    <div class="container">

        <div class="example_responsive_1" >
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90 TravelKhana -->
<ins class="adsbygoogle example_responsive_1"
     style="display:inline-block;"
     data-ad-client="ca-pub-5798678675120444"
     data-ad-slot="3492216211"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div> 
  
			  <div class="row">
			    <div class="col-md-12">
			    <div class="table-responsive">
				<table class="table table-bordered  table-train ">
				  <thead>
					<tr class="tabel-head">
					  <th colspan="3">Station</th>
					  <th>Arr</th>
					  <th>Dep</th>
					  <th>Halt</th>
                 <th>Day</th>
					</tr>
				  </thead>
				  <tbody>
              <?php
              foreach($traintimetable as $item) { //foreach element in $arr
                  //$item->foodAvailablity;
               if(strcasecmp($item->arrivalTime, "origin") == 0){
                  $halt="origin";
               }else if(strcasecmp($item->departureTime, "destination") == 0){
                  $halt="destination";
               }else{
                  $halt=(strtotime($item->departureTime) - strtotime($item->arrivalTime))/60;
                  //$halt1=$halt." min";
               }

                  echo "<tr>
                 <th class='station-name' colspan='3'>".$item->stationInfo->stationName."(".$item->stationInfo->stationCode.")</th>
                 <td> ".$item->arrivalTime."</td>
                 <td>".$item->departureTime." </td>
                 <td>".$halt."</td>
                 <td>".$item->arrivalDayIncrement."</td>
               </tr>";
                  
               }
               ?>
				  </tbody>
				</table>
				</div>
				
				</div>
			  </div>
			</div>
		 </div>
		  <!--end of table-sec-->
		 </section>
         <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      
   </body>
</html>

