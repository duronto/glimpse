<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Check PNR Status - Get Indian Railway PNR Status - PNR Status Enquiry - TravelKhana.Com</title>
      <meta name="description" content="Want to Check IRCTC PNR Status Online? Visit TravelKhana & get Indian railways pnr status for your pnr checking to get an idea about confirmation against reservation." />
      <meta name="keywords" content="PNR Status, check pnr status, indian railway pnr status, railway pnr status, indian railways pnr status" />
      <!-- CSS -->
      <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <noscript id="deferred-styles">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      </noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
            <!-- <div style="text-align: right">
               <a href="https://www.travelkhana.com/travelkhana/jsp/wow100.jsp"><img src="https://desktop.travelkhana.com/img/first_meal_free_banner.png" class="img-responsive" alt="" title=""/></a>
            </div> -->
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="availability-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h1>PNR Status <a href="/" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a></h1>
                  </div>
                  <div class="col-md-12">
                  <table style="padding:3px 3px 3px 3px; width:728px; border:none;">
                      <tr>
                          <td>
                           <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- 728x90 TravelKhana -->
                            <ins class="adsbygoogle example_responsive_1"
                              style="display:inline-block;"
                              data-ad-client="ca-pub-5798678675120444"
                              data-ad-slot="3492216211"></ins>
                            <script>
                              (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                          </td>
                      </tr>
                    </table>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb paddingTop-25">
                     <h2 class="hidden-xs">PNR Status</h2>
                     <form class="form-inline mobile-form" id="submit_form" method="post">
                        <div class="input-warp">
                           <div class="input_bg mobile-bg">
                              <div class="form-group">
                                 <input type="text" class="form-control pnr-inputwidth required integerfiled" maxlength="10" id="pnrstatus" name="pnrstatus" maxlength="15" data-minimum="15" placeholder="PNR Number">
                              </div>
                           </div>
                           <button id="submit_btn" type="button" class="btn btn-search">CHECK STATUS</button>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of availability-bg-->
         <div class="trainTable-content">
            <div class="container">
               <div class="example_responsive_1" >
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- 728x90 TravelKhana -->
                  <ins class="adsbygoogle example_responsive_1"
                     style="display:inline-block;"
                     data-ad-client="ca-pub-5798678675120444"
                     data-ad-slot="3492216211"></ins>
                  <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="PNR-wrap">
                        <div class="trainTable-wrap">
                           <h3>The Need to Check Pnr Status</h3>
                           <span class="head-icon"></span>
                           <p>Thinking why it is vital and so much crucial to check pnr status?PNR term plays an important role in Indian Railways and it stays and needed with the passenger 
                           <table align="left"  style="padding:3px 3px 3px 3px; width:350px; border:none;">
                            <tr>
                                <td>
                                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                  <!-- utility336x280 -->
                                  <ins class="adsbygoogle"
                                      style="display:inline-block;width:336px;height:280px"
                                      data-ad-client="ca-pub-5798678675120444"
                                      data-ad-slot="8254336067"></ins>
                                  <script>
                                      (adsbygoogle = window.adsbygoogle || []).push({});
                                  </script>
                                </td>
                            </tr>
                           </table>
                         all the time, at the time of booking and while travelling in the train and even is required if you need to place an order for e-catering services and get good quality, fresh and hot food delivery services. Do you know the meaning of term ?PNR? and what PNR number depicts for? If you need to know about pnr, pnr number, pnr status and why it is significant to check pnr status, you can definitely find your answers in this post.</p>
                           <p>PNR connotes for ?Passenger Name Record?, also been well known as a unique ten digit number available at the right hand top side of the ticket and gets stored in the computer database reservation system of Indian Railways. Whenever you book a ticket, no matter whether it is in confirmation status or is in waiting status, PNR number gets generated every time on random way and gets stored in the database for the record of Indian Railways authorities. This concept of generation of pnr number and to travel in trains without any hassle with confirmed pnr status, is been derived in order to make the entire procedure more accessible and efficient for Indian railways as well as for passengers. As per the details, the concept is been used by Airlines priory and then been accessed and used by Railways after seeing its success. PNR number is the perfect and effective way to handle all the processes efficaciously used to store passenger information along with their travel itinerary in safe means. Do you know what a pnr number stores? It basically stores basic passenger related information like his age, sex, date of birth, along with his travel details like ticket number, travelling date, seat number, coach number, source station, final station, berth preference if any, quota or class of travel, payment mode etc. A pnr number is ten digit number usually available in a combination of three and seven digits and once you have this number, you can check pnr status and confirm whether your ticket is confirmed or not and whether you are able to travel or not. To check pnr status, people can do it through different methods and if you want to check out how to do the same, it is very simple and convenient to do. Well, in case your ticket is confirmed, there is no need to check pnr status but if it is in wait listing status or is not confirmed, it is always preferable to do check pnr status and make your decisions wisely and accordingly. To check pnr status takes hardly few minutes of time and it is advisable to do in advance as it helps to get rid of any last time frustrations and travel in peace and with full enjoyment and calmness. There are different means through which pnr status could be checked including through online websites, through SMS, mobile app or getting it checked while seeing the final chart that gets prepared about 4 hours in advance the time of travel. To know about each method, feel free to go through the following pointers.</p>
                           <p>SMS- is one of the efficient and widely used methods to check pnr status. It is not only easy to do but also saves a lot of time and energy of people as all they have to do is pick their phone, type their pnr number and send that message to 139. Within minutes, people will get a SMS back with all pnr details, status and more. Well, the number 139 is not at all toll free number and for each message, Rs. 3 gets deducted.</p>
                           <p>Online websites- It is another efficient and one of the commonest approaches accessed and used by people in big numbers and get details about pnr status. All people need to do is to have a computer system or a laptop and an active internet connection and access the website through which information could be fetched at any point of time and without any hassle. You just have to login IRCTC website and check out the pnr status using its pnr tracking method and get all information by feeding your ten digit pnr number and within click, get all the status. Apart from IRCTC, there are private and leading platforms as well like Travel Khana through which pnr status could be checked and known by entering the pnr number and click on the status button.</p>
                           <p>Railway stations- Pnr status could be known by having a look at the final chart that gets prepared before the departure of the train and people can ask with personnel sitting at the railway counters to know relevant information about their travel status and pnr status. Well, it is advisable to come a few hours prior at the railway station if you are using this method as it is time consuming and could take hours in waiting at the queue at the railway counter at station. SMS and pnr status check through websites is the safest and efficient approaches to go for.</p>
                           <p>While travelling in train, do make sure to carry valid identity proof with you. Now, have you come to known the fact and significance behind checking pnr status? Hope, you do now. IRCTC website is often not accessible due to heavy rush and traffic over the platform and in such scenarios, you can even access other websites like Travel Khana and check pnr status, seat availability, online time table and more information related to trains without any hassle and also order yummy and hygienic food delivery services. So, what are you thinking of? Take full benefit of such platforms and enjoy your rail excursion to fullest. And do remember that pnr number is vital and must to tell the e-caterer while ordering for food or availing other services to travel without any inconvenience in trains.</p>
                           <p>Want to know about PNR status, CNF and RAC? This post will help you all in understanding the meaning of them clearly. In Indian Railways, CNF depicts for a berth or seat available in the train and gets confirmed status. It is been written and available on the ticket of IRCTC and is been reserved for the passenger.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about PNR status CNF and RAC</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about PNR status, CNF and RAC? This post will help you all in understanding the meaning of them clearly. In Indian Railways, CNF depicts for a berth or seat available in the train and gets confirmed status. It is been written and available on the ticket of IRCTC and is been reserved for the passenger.</p>
                           <p>The seat number is allotted to you after the final chart preparation. RAC stands for Reservation against Cancellation and is been considered as a train ticket with confirmed seat but it is waitlisted berth. It also means that the passenger can board the train on the basis of this ticket and occupy the seat also rather than a sleeping berth during night time. PNR means ?Passenger Name Record? and it is pretty much vital to check pnr status to know about the status of your ticket reservation before leaving for the station and travelling in trains. Reckoned to be a unique ten digit number available on the top right hand side of the ticket, it actually gets stored in the Indian Railways computer database for the future reference and record. The PNR number gets generated every time at the time of booking and it is always recommended to know and check pnr status and travel in train without any hassle and inconvenience. Along with this, make sure to carry along valid ID proof while on the travel and save your time and energy by checking everything just by accessing and visiting website of Indian Railways and other private service providers like Travel Khana.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about PNR status PQWL and RLWL</h3>
                           <span class="head-icon"></span>
                           <p>Want to grab knowledge and know about PNR status, PQWL and RLWL? This post can help you in knowing about the same in a clear and concise way. PQWL stands for pooled quota waiting list and is in general shared by different small stations. Operated from the originating station of a train run, there is only one pooled quota for the entire rail excursion. And, it is allotted usually to the passengers travelling from the originating station to a station that is not final destination, from an intermediate stoppage to final station or in between two intermediate stations.</p>
                           <p>RLWL stands for remote location waiting list that means that the ticket is issued for the intermediate halts as they are reckoned to be most important cities or towns on that specific route. Such kinds of tickets are usually given an individual priority and the confirmation depends upon the cancellation of any confirmed ticket. As per the information, remote location stations used to prepare their own chart about 2-3 hours prior the actual departure of a train and there is less probability for them to get confirmed. Now here comes the pnr number and the importance to check pnr status before travelling? PNR status lets people clear about the status of the train ticket and whether they are ready to go and travel in trains or not. PNR number stores all basic details of a passenger and his travel information and it is recommended to check pnr status before travelling as it gives clear status to them to travel in a hassle free way.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about PNR status CKWL and GNWL</h3>
                           <span class="head-icon"></span>
                           <p>Seeking online to know more about PNR status, CKWL and GNWL? This post is definitely going to give you all an idea about the above mentioned terminologies used a lot in Indian Railways. Well, PNR stands for ‘Passenger Name Record’ and is a unique ten digit number that gets randomly generated at the time of booking tickets and it is highly recommended to check pnr status to make sure whether your booking is been confirmed or not.</p>
                           <p>CK is the code for tatkal and CKWL status is been assigned to tatkal tickets and in case the tickets goes up, it instantly and directly gets confirmed and need not to go through RAC status unlike GNWL. At the time of final chart preparation, GNWL is been given preference to CKWL and there are lesser chances for them to get confirmed. GNWL stands for general waiting list tickets that are issued when the passengers start their journey from the source station of the travel or from one of the stations near to the same and it has the highest number of seats available in any of the train and has the best chances and probability to get confirmed at the time of final charting. Make sure to be informed and updated about all such things and travel without any problem. Also, do remember to check out latest pnr status and grab all details about the travel and assure whether the ticket is been confirmed or not. Have a safe and happy travel!</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about PNR status RLGN</h3>
                           <span class="head-icon"></span>
                           <p>Looking online to grab more details about PNR status and RLGN? RLGN depicts for remote location general quota waiting list tickets and they are issued for the intermediate stations and nearby destination stations. As it is been already listed under general quota category thus it doesn’t have any RAC provision. Well, the tickets only get confirmed in case any passenger cancels the ticket from that station and is only be informed at the time of final chart preparation that is done 4 hours before the scheduled departure of the train</p>
                           <p>Now wondering the importance behind the PNR status and why it is vital to do the check? PNR depicts for ‘Passenger Name Record’ and before going at the station to board the train, it is important to keep a check of your pnr status and be sure whether it is been confirmed or not. In case, your ticket gets confirmed, you have a green signal to board the train and travel and in case, it is not, you cannot travel in the same train with an unconfirmed ticket. It could be done through several processes like via SMS, website, phone call or so and these days, in just few minutes of time, everything could be done without any hassle. So, keep an eye on the same and have a satisfactory and fun filling rail journey. And also, while travelling make sure to carry an authentic and relevant identity proof with you to avoid any thing that can affect the mood and your travel.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about CAN and MOD</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about CAN and MOD and how does it relate to Indian Railways? This post will definitely give you an idea about these terminologies in a brief way and its relation with Railways as well. Often it happens that when people check out their pnr status, what they get is CAN/MOD status and people are often feel excited and curious to know about their meanings as well.</p>
                           <p>CAN stands for cancellation and it depicts that Indian Railways has assigned a specific PNR number to a ticket but it is been marked for cancellation. That PNR number for sure will not be assigned to any other ticket and with time and gradually will get flushed. MOD on the other hand stands for modified and depicts the fact that it is been modified once and it is been allocated with a PNR number. It usually happens when railway officials or a customer change or modify travel itinerary or details. It could be a small change in anything like in passenger name, date of journey, travel class and so. So, be aware of everything and make sure to check out your pnr status to be sure of the fact whether your ticket gets confirmed or not. Enjoy the hassle free and have a smooth journey and with the technology, be updated about all the terminologies used and related to Indian Railways.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about WL# and RAC</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about WL# and RAC? Well, here you can get an idea related to them. WL# depicts for waiting list status and RAC depicts for Reservation against Cancellation status. IN RAC status, it means that the user will definitely get a seat but need to be sharing it with someone else too. There are likely high chances that your RAC status ticket gets converted into a confirmed ticket and the user will get a full seat in return and that need not be shared with anyone.</p>
                           <p>WL# ticket depicts that as the ticket is not in confirmed status so you will not get a confirmed seat. Make sure if you have booked your ticket online, then unless it got confirmed or upgraded to RAC, you cannot travel in the train as it gets cancelled automatically. Thinking which one to opt for or which one is actually better? Well, it is entirely up to you. It is advisable to always travel with the confirmed ticket irrespective of the fact whether it is a short distance journey or long distance journey. In case, the distance is less and user has RAC ticket, and it is a matter of urgent circumstance, then he or she can travel with that but for long journeys, even that is not being recommended. Along with this, it is always preferable to check your pnr status online as hardly it is a matter of few minutes and if done, you get an actual idea of whether your ticket got confirmed or not. So, travel safely and be aware of all such things that could be done and checked!</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about REGRET/WL</h3>
                           <span class="head-icon"></span>
                           <p>Want to know the real meaning about Regret/WL? Want to know how it relates to Indian Railways? Regret in IRCTC depicts that no further booking is allowed as the quota is been already exceeded. The booking is not allowed and the waiting list number has already reached its optimal capacity. Along with this, WL depicts for waiting list status, CKWL as tatkal quota waiting list status. Known as the status meant for waiting list status of tatkal quota, there is very less probability to get the confirmation for tatkal tickets.</p>
                           <p>It is always advisable that in case your ticket status is been Regret or WL, then it is better to seek for other options, as the probability to get tickets in confirmed status is very less. Adding to this, it is highly recommended to do check your pnr status and know about the status of whether your ticket gets confirmed or not. Checking pnr status is very important to do and in travelling, it is best to reach at the station only and only after checking and confirming everything out. Checking the status is very simple to do, all people need to do is enter their pnr number online and click on the go button and within a minute, get all the information related to confirmation status or still in waiting list status or else. So, before leaving to the station, make sure to check the same and enjoy a hassle free journey.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about released</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about released status? Basically released status depicts that the ticket is not been cancelled but in spite of that, alternate accommodation will be provided. Whenever you do the booking, a pnr number gets generated and it is utterly important to keep a check on the pnr status to be sure of the fact whether your ticket got confirmed or not. A pnr number is a 10 digit number that gets generated randomly by the Indian Railways and is supposed to store your travel details and itinerary for future reference.</p>
                           <p>In case your ticket gets released status, then in that situation Indian Railways will provide you an alternate accommodation. So, do make sure to check out pnr status every time whenever you go for the booking and reservation. Once you check out the same, all travellers need to access is to open the official site of IRCTC or other sites and enter in there 10 digit PNR number and when it got clicked, you will know about its status within a minute and without any hassle. So, make sure before reaching at station whether your ticket gets confirmed or not or is in waiting list status and travel and make your plans accordingly. So, have an access to technology, check out the same and have a hassle free and happy journey.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>How is a PNR number generated for a train in the Indian railway?</h3>
                           <span class="head-icon"></span>
                           <p>Want to access current status of a train and don’t know how to work train tracker location? It is indeed very simple to do and if you are also wondering the same, read on to the following as it will help you in knowing about the live running status of a train and that too within minutes. Simply access Spot your train utility, a widely in use train tracker app, feed in there train name or number, along with the date of journey and click on the submit button. Once it is been done, you will get the details related to exact location of the train at that time, its expected and actual time of arrival, expected and actual time of departure, number of halts within, distance that is still remained to cover the journey, and much more.</p>
                           <p>The best part of using the app is to know about the fact whether it is running in accord to its scheduled arrival and departure time or not. In case, the train is been running late or so, people can know about the information and make their plans accordingly. Many a times, people get late or so and cannot avoid important meetings or such and in those cases, this real time train tracker acts as a boon. Don’t you think, it is very simple to use? Just open up the website, enter the details and grab latest information about the train running status at that time. Happy travelling!</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What is the meaning of NR/0 in PNR status?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about the meaning of NR/0 in PNR status? Read on to the following as this post will give you an insight on the same. NR depicts for No Room and if you get the status of your ticket shown as NR/0, it is always advisable to check your pnr status as the Indian Railways system will definitely show the updated details and status whether it is confirmed, RAC or WL with berth details of the ticket.</p>
                           <p>Still, in case if the status shown is NR/0 after the final chart is been prepared, then it could means that the ticket might have confirmed but is still not been updated on the IRCTC website. You can even try to check the status by sending a SMS to number ‘139’ by writing PNR space and then your PNR number. Sometimes, it happens that people get this status but most of the times, it got confirmed and been updated. The likely probability and higher chances are that the ticket gets confirmed. And people can check out the same at the final chart without any hassle. So, what are you thinking of? Simply take out your ticket, check your pnr status via web or SMS as per the preference and get to know about whether NR/0 gets confirmed or not. In other ways also, when you are going to travel in trains, it is preferable to check out the PNR status to be sure of the status of the travel.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>How does IRCTC PNR prediction apps work?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about how does IRCTC PNR prediction apps work? Well, there are firms that are sharing information related to the PNR prediction and they use machine learning as well as pre devised algorithms based on different factors including booking trends, seasonality, station quota, holidays, running days in a week and likewise several other parameters. On the basis of all of them, they compute and let people know about their predictions. </p>
                           <p>The algorithms that are created are actually based upon a lot of old PNR numbers, their final status that is been provided by IRCTC and so. It uses the data in order to predict the related pattern for that train, its travelled route, and on the basis of that provides the probability that whether the ticket will get confirmed or not. Such predictions are also based and related to RAC status tickets as if the passenger has a RAC ticket, he or she is liable to board the train. People can even download their apps on their mobile and get prediction over their PNR number status. Is the information relevant to the question asked and did the answer resolve your concern? Stay tuned for more posts. And do check your PNR status to make sure about the status of your ticket and travel. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>How is a PNR (passenger name record) allocated to a passenger?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know more of the truth that how is a PNR (Passenger Name Record) allocated to a passenger? If yes, read on to the following. PNR is basically is a ten digit unique pointer that is stored as a record in the Indian Railways database supposed to be containing comprehensive journey information. As per the sources, the first 3 digits of the PNR number derived depend on the zone of the train with respect to the source station of the train. And the 2 digits depict for the specific PRS. On the other side, the last 7 digits are randomly generated numbers and depict for as such no specific information. A particular PNR number lasts for about 9 months of time and afterwards usually it gets eradicated or simply flushed out. After a year, the same PNR number could be generated without any problem. So, make sure to check out PNR status to be confirming whether your ticket is in waiting status or confirmation. It could be done via several ways like through web, mobile app or SMS basis. Hardly, checking out PNR status is a minute activity and let passengers know about the exact status of the ticket without any hassle. So, be informed of the same and have an enjoyable and trouble free journey. Do remember that a PNR number doesn’t store any data by itself but in fact points to a unique record that is been in the central database holding all the details related to the journey.  </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What does the PNR (passenger name record) number on Indian railway tickets signify?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about what does the PNR (Passenger Name Record) number on Indian Railway tickets signify? PNR number is basically a unique pointer that is been addressed to be pointing for a record in the Indian Railway database. It comprises comprehensive journey information of a passenger including passenger details like name, age, sex, berth preference, transaction and payment details, transaction ID, ticket charge and payment mode, ticket details like train number, from, to, date of journey, reservation upto, boarding station, berth, quota and so. </p>
                           <p>The first 3 digits of the PNR number depict the PRS – Passenger Reservation System from which the respective ticket has been booked. It is been devised by the CRIS and depends on the zone of the train with respect to the source station. Aside from this, the last 7 digits of the PNR number are randomly generated numbers and as such do not represent anything related to the travel or the ticket. It is just done to make the PNR number unique and distinctive. Did you understand and get the idea now and clearly understands what does PNR number stands for and what does it depict? It is important to check PNR status before travel and make sure whether your ticket is in confirmation state or not. There are many ways to do the same, so, know about them and do make sure to check out PNR status before heading out towards railway station. Stay tuned for more posts.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What is PNR status prediction?</h3>
                           <span class="head-icon"></span>
                           <p>What is PNR status prediction? If you are curious to know about it, go on and read on to the following. Basically, PNR status prediction is kind of a way that predicts the chances of your train ticket confirmation in an intelligent way. It involves the risks that could be involved in the same and take the decisions in an according way covering whether the ticket is been in confirmation state or not. There are apps and platforms that does this PNR status predictions using machine learning and getting status on the basis of existing as well as past booking trends, season in which the ticket is been booked, station quota, days of the week, holidays, and many other parameters while deriving the results.  </p>
                           <p>The prediction could vary based on whether it is weekend or a holiday. The ticket confirmation chances even depend highly on the day on which ticket is been booked and the time whether it is peak time or not so predictions are also quite related to the same. Adding to this, these platforms consider RAC tickets as confirmed tickets as the people carrying them at least are allowed to travel in trains. ConfirmTKT is one of the platforms, been into PNR status prediction. People are free to download the app and get prediction whenever they wish as per the convenience. It is certainly none but a smart way to know about the chances whether the tickets have probability to get confirmation or not. Do access the same and have a safe travel. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Current PNR Status Check Using SMS</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about how to do current PNR Status Check using SMS? This post will let you know the same. PNR stands for Passenger Name Record and is meant to be a unique 10 digit number that is been stored in the database of Indian Railways and is been holding all the important information related to traveller’s sex, name, age, berth preference, travel itinerary, coach, seat, berth, mode of payment and much more. PNR status could be checked easily and vastly using SMS method and it is also been among the most common forms of checking up the status. It is simple to do on one end and on the other saves a lot of time and energy of everyone as people require to do is to get their phone, feed in the relevant pnr number, and send the same and message it to 139. Within minutes, people will get a message back depicting all about pnr details, its status and more. Just for the record, do make sure that the number 139 is not at all toll free and for each one of them, Rs. 3 will get deducted. Apart from SMS, there are other ways as well to check PNR status including via online websites and asking the personnel at the stations. So, understand all about them and make sure to check pnr status to know about whether your ticket is been confirmed or not. Also, do make sure to carry along with you an authentic identity proof while travelling. Have a safe travel!. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>What does PNR number include</h3>
                           <span class="head-icon"></span>
                           <p>Want to know about what does PNR number include? In this post, you can actually get an idea about the same. A PNR number is a unique number that is been generated at the time when passenger books a ticket and it is known to cover passenger details involving name, sex, age, berth preference, ticket itinerary and details involving date, train number, from, to, reservation upto, boarding staton, berth, class and quota, payment or transaction information involving payment mode, transaction ID and ticket charge. It is a 10 digit number and the first 3 digits let people know about the PRS or the passenger reservation system in accord to which that ticket is been booked. The starting digit of the PNR number depends highly on the zone of the train with respect to the source train station. The number code zone as per Indian Railways is been like 1 for SCR Secunderabad PRS, 2, 3 for NR, NCR, NWR, NER New Delhi PRS, 4, 5 for SR, SWR, SCR Chennai PRS, 6, 7 for NFR, ECR, ER, ECoR, SER, SECR Calcutta PRS and 8, 9 for CR, WCR, WR Mumbai PRS. The other 7 digits are randomly generated numbers and as such address no specific information related to the journey. They simply give the PNR number unique identity. It is important to check out the pnr status and it could be done by SMS, call or even at the railway stations where pnr status check counters are been available.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Is PNR number allotted to unreserved ticket?</h3>
                           <span class="head-icon"></span>
                           <p>Want to know whether or not; is PNR number allotted to unreserved ticket? Well, you can find your answer here for sure. For all reserved tickets that are booked either via IRCTC website or via railway reservation counters, PNR number gets generated and is available on the ticket. For online tickets, the 10 digit number PNR number is printed on the topmost row just below the transaction id and for all those that are booked on the railway reservation counters, the PNR number is been printed at the top leftmost corner just beneath the PNR number heading. Make it a point and do understand that PNR number is been allotted only and only to reserved tickets and for unreserved tickets, no PNR is been allotted. The reason behind is very simple as the ticket is in unreserved status, Indian Railways hasn’t booked or reserved any seat or berth in that case and the passenger can stand or sit anywhere in the designated coach in the train in this scenario. It is always promising to check pnr status before heading out to station or travelling to know about the confirmed status of your reservation and booking. PNR status could be checked through different ways and it could be done either via web, phone, other methods and so. So, make sure to check out the pnr status before travelling and make sure whether the ticket you got is in reserved state or in unreserved status.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>How to find pnr number using transaction id?</h3>
                           <span class="head-icon"></span>
                           <p>Wondering about how to find pnr number using transaction id? Well, it is very simple to do and you will get your answer here. First of all login to the site first of all, by giving a visit to irctc. Make sure that the user id and password you have been entering is authentic. Once it is done, go to print E ticket column, or cancelled history all depending on the journey status or the booked history. Once it is open, press ctrl + f and enter there your transaction ID and click on the enter button. It not only depicts your exact and current transaction status but also you can check out and know about your pnr number from there easily. Isn’t it that simple and easy to do? Well, it is pivotal and very much significant to keep a check of your pnr status so that you know exactly about the status of your reservation and so. It could be done via different ways like by online, mobile, or by asking at railway stations. The important thing is to do check your pnr status and know about every minute detail related to the status of your ticket reservation and whether it gets confirmed or not or is in waitlisted status or in RAC status or so. Along with in the travel, it is important for you to carry a valid ID proof along with for the verification purpose. There are even many dedicated platforms available that will help you to check out your pnr status, so make use of them and have a safe travel. </p>
                        </div>
                          <div class="trainTable-wrap">
                           <h3>Can I change the boarding station ? </h3>
                           <span class="head-icon"></span>
                           <p>Want to know whether it is possible for a user if he or she is wondering that - can I change the boarding station? Here, you will get the answer. Yes, it could be possible to change the boarding station of your travel. In case you have booked your e-ticket using IRCTC platform, you can edit or change it to any of the other station name that falls in between the source and destination junctions. It could be changed only and only prior to 24 hours of the scheduled departure time of the train. This service could only and only be possible with e-ticket reservation and is not at all possible with i-tickets at all. Adding to this, the change of boarding station could only be done via IRCTC site and in case of e-tickets for only once. And in case, a user has changed the boarding station once, he or she will definitely lose all the rights related to boarding the train from the original station/boarding junction. Hope this information is helpful. Also, make sure to keep an eye and do check out the pnr status before heading out to station and be sure whether your ticket is in confirmation state or not. It could be done either via web or through SMS or by asking at the railway personnel at station and it will hardly take few minutes to check out the same and be sure whether the ticket is in confirmation state or in waitlisted status.  </p>
                        </div>
                        </div>
                          <div class="trainTable-wrap">
                           <h3>How to get pnr status alerts on mobile </h3>
                           <span class="head-icon"></span>
                           <p>Want to know about how to get pnr status alerts on mobile? Here, you will come to know about the same. First of all, visit the site mypnrstatus.com, enter the respective pnr number along with the mobile number where users want to know about the status and click on submit button. Don’t you think it is so simple to do and hardly takes a few minutes to do that? People can also send their pnr number to the toll free number that is 139 and within minutes, they will get the information related to their pnr status on the same number with which the message is been sent. It is very important to check out pnr status online and be sure about the status that your rail ticket is been holding, whether it is in confirmed state, or in RAC status or is in waitlisted criteria. So, know about the same online using different ways like via online or via SMS or via asking at the railway stations. There are many professional sites and portals where people can go through the pnr status check and once they feed in there the pnr number and click on submit button, they will get to know all about the status within minutes and without any hassle. Checking up on pnr status and grabbing an idea about the latest check on the same helps people to be clear of the confirmation and reservation status of their ticket and making sure they are going to have a trouble free journey. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>The PNR status of my train ticket is CNF/B1/16/NC. Does that mean my ticket is confirmed or not? </h3>
                           <span class="head-icon"></span>
                           <p>The PNR status of my train ticket is CNF/B1/16/NC. Does that mean my ticket is confirmed or not? Are you looking for the answer of the same? If yes, you will definitely get your answer here. First of all, do understand that the train ticket number as CNF/B1/16/NC means that the rail ticket is been confirmed and there is no need to worry about anything else. As per this number, the coach number is B1 along with the seat number as 16. NC depicts for ‘No Choice’ and that means that at the time when you are booking tickets in order to get reservation, there is no preference been provided for the seats in particular. It could be anything including the side lower berth, side upper berth, middle berth, upper berth or lower berth. Hope there is no confusion now and the information mentioned related to the concern is been clear and informative too. Aside from this, it is vital and must to do check out the pnr status of a ticket to make sure in case the seat is been reserved or confirmed or is in waitlisted criteria or in RAC status. The PNR status could be checked through different ways including online, web or by asking queries from the personnel at the railway junction and people can confirm whether their tickets got confirmed or not. Do the same for sure before leaving out for station and have a safe and fuss free travel.</p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about WEBCAN  </h3>
                           <span class="head-icon"></span>
                           <p>Looking online to know about WEBCAN? Searching for the meaning of WEBCAN and how it is related to Indian Railways? Well, here you will definitely get your answer. WEBCAN depicts for a railway counter ticket that means that a user has cancelled his or her ticket online and the refund is not been collected yet. There is another term too related to the same that is WEBCANRF that is also a railway counter ticket and it means that a user has cancelled his or her ticket online and the refund has already been collected. The similar kind of breakouts actually make users understand more about them and how they are playing a pivotal role in the rail travel and train journeys. It is pretty much important to know about your ticket status and understand in detail what it means and signifies. And the pnr status check online could let people know whether the ticket is in confirmation state, RAC state, waitlisted state and so. It is important to be aware of them and do know whether you are going to travel in the train or not. RAC status also means that you can travel in a train but full berth is not allocated to you. So, do know of the same, make sure to check out pnr status either via web or via SMS or other approach, and know about the current status in hardly a few minutes of time. </p>
                        </div>
                        <div class="trainTable-wrap">
                           <h3>Know about WEBCANRF  </h3>
                           <span class="head-icon"></span>
                           <p>Looking for the meaning and know about WEBCANRF and how does it relate to Indian Railways? Well, here you will definitely get to know about the same. WEBCANRF is a railway counter ticket and it means that the ticket is been cancelled online and the refund is also been collected. There are many other related terms that are important to be aware of like WEBCAN that is also a railway counter ticket and it means that the ticket is been cancelled online and the refund is still not been collected. It is very much important to be aware of your rail ticket status and in which state and stage it is in at that time. The terms are been related to PNR status and seat availability and it is always helpful and informative too to be aware of them. PNR status should be checked for sure to confirm and make sure about the status of your ticket via web or via SMS or via seeking answers from the railway personnel at the station. No matter which approach you choose to find out about the PNR status, it is important to be aware of them and most importantly the update and status and be sure whether your ticket’s status is in confirmation state or not. Even if a user has ticket in RAC state, then also he or she is liable to travel in the train. So, know about them and have a safe travel. </p>
                        </div>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- utility336x280 -->
                        <ins class="adsbygoogle"
                           style="display:inline-block;width:336px;height:280px"
                           data-ad-client="ca-pub-5798678675120444"
                           data-ad-slot="8254336067"></ins>
                        <script>
                           (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--footer Start Here-->
      <?php include '../common/footer1.html' ?>
      <!-- footer Ends Here -->
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
            </div>
         </div>
      </div>
      <!--popup ends here-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script src="/glimpse/src/js/load-js-css.js?v=0.1"></script>
      <script src="/glimpse/src/js/pnrStatus.js?v=0.1"></script>
      <script type="text/javascript">
         $(document).ready(function(){
         if ($.cookie('indexError') != null ){
           var value = JSON.parse($.cookie("indexError"));
           document.getElementById("myModalLabel").innerHTML =value.message;
           $('#myModal').modal('show');
           var pnr=value.pnr;
           delete_cookie('indexError');
           setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
           if(pnr != undefined){
              document.getElementById('pnrstatus').value=pnr;
           }
         }
         
         });
         var delete_cookie = function(name) {
           document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         };
      </script>
   </body>
</html>