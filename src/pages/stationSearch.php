<?php
session_start();
include 'common/main.php';
$stationDesc='';
$station='';
$stationSearched='';
$indexError='';
$urlPage=$_SERVER['REQUEST_URI'];
if(isset($_SESSION['indexError']) && !empty($_SESSION['indexError'])) {
    $indexError=$_SESSION['indexError'];
    unset($_SESSION['indexError']);
  }
if(isset($_SESSION['urlPage']) && !empty($_SESSION['urlPage'])) {
    unset($_SESSION['urlPage']);
  }
$_SESSION['urlPage'] =$_SERVER['REQUEST_URI'];  

if (isset($_GET["station"]) && !empty($_GET['station'])) {
   $stationSearched=$_GET["station"];
} 
if (isset($_GET["stationCode"]) && !empty($_GET['stationCode'])) {
   $stationCode=$_GET["stationCode"];
}

$t=0;
$url='http://api.travelkhana.com/gatimaan/api/v1.0/station/'.$stationCode.'/?access_token=00034542-a266-442a-a30c-f31c74e27f28';
$opts = array(
  'http'=>array(
    'method'=>"GET",
    //'header' => $authKey,
    //'header' => "Authorization:".$authKey,
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);

if (strpos($http_response_header[0], "200")) {


  if(!empty($json_o)){
      $stationDesc='';
      $station=$stationCode;
      setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), "/");
      header("Location: https://www.travelkhana.com?err=Some Problem Occurred Pls. Try Again"); /* Redirect browser */
      exit;
  }
  $json_o=json_decode($json);
  $t=(int)$json_o->status;
  if($t == 0){
    $stationDesc='';
    $station=$stationCode;
    setcookie('indexError', $json_o->message, time() + (86400 * 30), "/");
    header("Location: https://www.travelkhana.com?err=".$json_o->message); /* Redirect browser */
    exit;
  }else{
    $stationDesc=$json_o->data->description;
    $station=$json_o->data->stationName;
   }
}
else if (strpos($http_response_header[0], "406")) { 
    $stationDesc='';
    $station=$stationCode;
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com?err=Some Problem Occurred Pls. Try Again"); /* Redirect browser */
    exit;
}else if (strpos($http_response_header[0], "401")) { 
    $stationDesc='';
    $station=$stationCode;
    setcookie('indexError', 'Invalid access token', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com?err=Some Problem Occurred Pls. Try Again"); /* Redirect browser */
    exit;
}else{
    $stationDesc='';
    $station=$stationCode;
    setcookie('indexError', 'Some Problem Occurred Pls. Try Again', time() + (86400 * 30), '/');
    header("Location: https://www.travelkhana.com?err=Some Problem Occurred Pls. Try Again"); /* Redirect browser */
    exit; 
}
?>   
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Food Delivery in Train at <?php  echo strtoupper($station);?> Railway Station</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

<noscript id="deferred-styles">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
</noscript>
      <link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.2">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2">
      <link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.5">
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="https://www.travelkhana.com"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="trainTable-bg">
            <div class="container">
               <div class="row">
                  <div class="back-to-order text-center visible-xs">
                     <h3>Food Delivery in Train at <?php  echo strtoupper($station); ?></h3>
                     <a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back-arrow.png" alt="back" title="back"/></a>
                  </div>
                  <div class="col-md-12 heading-wrap heading-mb">
                     <h2 class="hidden-xs">Food delivery in Train at <?php  echo strtoupper($station); ?></h2>
                     <input type="hidden" id="indexError" name="indexError" value="<?php echo $indexError ?>"  >
                     <form class="form-inline mobile-form" id="srchTimeTableForm" method="post" action="https://www.travelkhana.com/travelkhana/jsp/menu.html">
                     <h2 class="hidden-sm hidden-md hidden-lg">Enter Your Detail</h2>
                     <div class="input-warp">
                        <div class="input_bg mobile-bg">
                           <div class="form-group second-section">
                              <!-- <input type="text" class="form-control req-inputWidth" id="trainnum" name="trainnum" placeholder="Train Number"> -->
                              <input type="text" class="form-control req-inputWidth" id="trainnum1" name="trainnum1" placeholder="Train Name/Number">
                              <!-- <input type="hidden" id="train1" name="train1"> -->
                              <input type="hidden" id="from1" name="from1" value="<?php echo $stationCode ?>"  > 
                              <input type="hidden" name="stsess" id="stsess" >
                              <input type="hidden" name="stationSearched" id="stationSearched" value="<?php echo $stationSearched ?>">
                           </div>
                           <div class="form-group second-section">
                              <input type="text" class="form-control datepicker" name="jDate" autocomplete="off" id="jDate" placeholder="yyyy-mm-dd" value="<?php echo date("Y-m-d");?>">
                              <i class="fa fa-calendar" onclick="$('#jDate').focus();"></i>
                           </div>
                        </div>
                        <button id="searchStation" type="button" class="btn btn-search">SEARCH</button>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-bg-->
         <div class="trainTable-content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="trainTable-wrap">
                     <?php  echo  $stationDesc;?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of trainTable-content-->
      </section>
      <!--update Detail Popup Here -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
      </div>
      </div>
    </div>
  <!--popup ends here-->
      <!--footer Start Here-->
      <?php include './common/footer1.html' ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

      <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script> -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

      <script type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
      <script src="/glimpse/src/js/common/load-js-css.js?v0.1"></script>
      <script src="/glimpse/src/js/stationSearch.js"></script>
      <script src="/glimpse/src/js/dateCheck.js?v=0.2"></script>
   </body>
</html>
<script type="text/javascript">
var delete_cookie = function(name) {
    document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  };

  $(document).ready(function(){
    if ($.cookie('indexError') != null ){
      var value = JSON.parse($.cookie("indexError"));
      document.getElementById("myModalLabel").innerHTML =value.message;
        $('#myModal').modal('show');
        delete_cookie('indexError');
        setTimeout(function(){ $('#myModal').modal('hide');}, 4000);
      }else if(document.getElementById("indexError").value!=''){
        var value = document.getElementById("indexError").value;
        document.getElementById("myModalLabel").innerHTML =value;
        $('#myModal').modal('show');
        setTimeout(function(){ $('#myModal').modal('hide');}, 4000);
      }
    })
</script>
