<?php
session_start();
// or this would remove all the variables in the session, but not the session itself 
 session_unset(); 

 // this would destroy the session variables 
 session_destroy();



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>confirm</title>
    <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>

    <!-- CSS -->
    <link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.4">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.3">
<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2">
 -->
    
	
<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="https://desktop.travelkhana.com/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://desktop.travelkhana.com/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://desktop.travelkhana.com/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://desktop.travelkhana.com/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://desktop.travelkhana.com/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://desktop.travelkhana.com/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://desktop.travelkhana.com/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://desktop.travelkhana.com/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://desktop.travelkhana.com/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://desktop.travelkhana.com/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://desktop.travelkhana.com/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://desktop.travelkhana.com/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://desktop.travelkhana.com/img/favicon-16x16.png">
<!-- <link rel="manifest" href="https://desktop.travelkhana.com/img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://desktop.travelkhana.com/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
	
  </head>
  <body>
    <?php

    if(!isset($_COOKIE['trackOrderResponse']))
    {
    header("Location: order.jsp"); /* Redirect browser */
    exit;
    }else
    {
    	$trackOrderResponse = json_decode($_COOKIE["trackOrderResponse"],true);
    	
    	/*echo $trackOrderResponse[0]['isUndeliver'];*/
    	
    }


 if(isset($_COOKIE['utm_source']))
 {

	$utm_val=$_COOKIE['utm_source'];

	switch ($utm_val) {
	    case "vcommission": ?>
	        <iframe src="https://tracking.vcommission.com/SL1Rm?adv_sub=<?php echo $trackOrderResponse[0]['userOrderId']?>&amount=<?php echo $trackOrderResponse[0]['payable_amount']?>&adv_sub2=<?php echo 'COD'?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
	       <?php break;
	    case "Icubewire": ?>
	       <!--  echo "Your favorite color is blue!"; -->
	       <?php break;
	    case "OMG": ?>
	        <script type='text/javascript' src="https://track.in.omgpm.com/1030051/transaction.asp?APPID=<?php echo $trackOrderResponse[0]['userOrderId']?>&MID=1030051&PID=29248&status=<?php echo $trackOrderResponse[0]['payable_amount']?>"></script>
 			<noscript><img src="https://track.in.omgpm.com/apptag.asp?APPID=<?php echo $trackOrderResponse[0]['userOrderId']?>&MID=1030051&PID=29248&status=<?php echo $trackOrderResponse[0]['payable_amount']?>" border="0" height="1" width="1"></noscript>
	        <?php break;
	    case "adcanopus": ?>
	      <img src="https://adcanopus.oneffect.us/affiliate/track/162/<?php echo $trackOrderResponse[0]['userOrderId']?>/<?php echo $trackOrderResponse[0]['payable_amount']?>/" height="1" width="1" />
	       <?php break;
	    case "Payoom": ?>
	     <iframe src="https://payoom.go2cloud.org/aff_l?offer_id=675&adv_sub=<?php echo $trackOrderResponse[0]['userOrderId']?>&amount=<?php echo $trackOrderResponse[0]['payable_amount']?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
	       <?php break;
	    case "Yahoo-India": ?>
	       <script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||

			[];w[u].push({'projectId':'10000','properties':{'pixelId':'10013559'}});var

			s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var

			y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded")

			{return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p)

			{y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)

			[0],par=scr.parentNode;par.insertBefore(s,scr)})

			(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
	       <?php break;
	    case "Optimize": ?>
	      	<script type='text/javascript' src="https://track.in.omgpm.com/1030051/transaction.asp?APPID=<?php echo $trackOrderResponse[0]['userOrderId']?>&MID=1030051&PID=29248&status=<?php echo $trackOrderResponse[0]['payable_amount']?>"></script>
 			<noscript><img src="https://track.in.omgpm.com/apptag.asp?APPID=<?php echo $trackOrderResponse[0]['userOrderId']?>&MID=1030051&PID=29248&status=<?php echo $trackOrderResponse[0]['payable_amount']?>" border="0" height="1" width="1"></noscript>
	       <?php break;
	    case "GEOAd": ?>
	       <iframe src="http://tracking.geoadmedia.com/aff_l?offer_id=392&adv_sub=SUB_ID" scrolling="no" frameborder="0" width="1" height="1"></iframe>
	       <?php break;
	    default:
	        
	}

 }

     ?>
	
	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  
			  <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>

			</div>
		
		</div>
	</header>
	
	<div class="order-success-container">
		<section class="success-message">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="order-success-msg">
							<div class="success-content">

									<img class="" src="https://desktop.travelkhana.com/img/success-img.png" alt="...">
									<h4 class="media-heading">Order Placed Successfully</h4>
									<h5 >Order <span class="orderId"><?php echo $trackOrderResponse[0]['userOrderId']?></span></h5>
									<?php if(!isset($_GET['x4Zx67@33fj']))
									{
									$status='';
									}
									else
									{
									 $status=$_GET['x4Zx67@33fj'];
									}

									if($status=='0')
									{ ?>
										<h3 class="hidden-xs hidden-sm" ><p style="color:red;">Your online transaction has been failed.Order placed as COD .</p>  <p><b>Thanks for ordering with us!</b></p></h3>
										<?php } else if($status=='1'){ ?>
										<h3 class="hidden-xs hidden-sm" >Your online transaction has been successful.You will recieve the confirmation message shortly.   <p><b>Thanks for ordering with us!</b></p></h3>
									<?php	}else 
										{
									?>
									<h3 class="hidden-xs hidden-sm">Your order has been placed successfully. You will recieve the confirmation message shortly. <p><b>Thanks for ordering with us!</b></p></h3>
										<?php } ?>
										<div class="pass_detail_box hidden-xs">
											<ul class="list-inline">
												<li class="pas_detail">
												<h1>Passenger Detail</h1>
												<h3 class="passengerName"><?php echo $trackOrderResponse[0]['name']?></h3>
												<span class="passengerContact"><?php echo $trackOrderResponse[0]['contactNo']?></span>
												</li>
												
												<li class="pas_detail">
												<h1>Coach/Seat</h1>
												<h3 class="passengerSeatCoach"><?php if($trackOrderResponse[0]['coach']==''){echo "NA";}else{echo $trackOrderResponse[0]['coach'];}?>/<?php if($trackOrderResponse[0]['seat']==''){echo "NA";}else{echo $trackOrderResponse[0]['seat'];} ?></h3>
												
												</li>
												
											</ul>
										</div>
										<div class="pass_detail_mobile hidden-sm hidden-md hidden-lg">
										    <ul class="list-inline">
										       <li class="passengerName"><?php echo $trackOrderResponse[0]['name']?></li>
										       <li class="passengerCoach fa fa-train" ><i aria-hidden="true" ></i> <?php echo $trackOrderResponse[0]['coach']?></li>
										       <li class="passengerSeat fa fa-bed" ><i  aria-hidden="true" ></i> <?php echo $trackOrderResponse[0]['seat']?></li>
										      <!--  <li><i class="fa fa-pencil r_edit" aria-hidden="true"></i></li> -->
										       </ul>
										          </div>
										          
									<button type="button" class="btn btn-update openModal" data-toggle="modal" data-target="#UpdateDetail" >Update</button>
									
							</div>
						</div>
					</div>
					
					 <div class="col-md-3 col-sm-4 col-xs-12">
						<div class="amount-box">
							<?php if($status=='0') 
							{?>
								<h1>Amount To be Paid <span>Rs. <?php echo $trackOrderResponse[0]['payable_amount']?></span></h1>
								<h4>Cash On Delivery</h4>
								<!-- <b>Pay only <i class="fa fa-inr"></i>
								<?php echo $trackOrderResponse[0]['onlinePayableAmount']?>
								</h3> if payment done through Paytm or Payu </b> -->
								<button type="button" class="btn btn-payonline" data-toggle="modal" data-target="#online_payment" >Pay Online Now (<i class="fa fa-inr"></i><?php echo $trackOrderResponse[0]['onlinePayableAmount']?>)</button>
							<?php }else if($status=='1')
							{?>
								<h1>Amount Paid <span>Rs. <?php echo $trackOrderResponse[0]['onlinePayableAmount']?></span></h1>
								<h4 style="color:green;">Online Paid</h4>
								<script>
								// Clevertap
							        clevertap.event.push("RS-Online Payment", {
							            "status": "Online paid",
							            "orderId": <?php echo $trackOrderResponse[0]['userOrderId']?>,
							            "channel":channel,
							            
							        });
								</script>
							<?php }else {?>
								<h1>Amount To be Paid <span>Rs. <?php echo $trackOrderResponse[0]['payable_amount']?></span></h1>
								<h4>Cash On Delivery</h4>
								<!-- <b>Pay only <i class="fa fa-inr"></i>
								<?php echo $trackOrderResponse[0]['onlinePayableAmount']?> 
								</h3> if payment done through Paytm or Payu </b>-->
								<button type="button" class="btn btn-payonline" data-toggle="modal" data-target="#online_payment"  >Pay Online Now (<i class="fa fa-inr"></i><?php echo $trackOrderResponse[0]['onlinePayableAmount']?>)</button>
							<?php } ?>
							<!-- <h5>+25%; Cashback using paytm</h5> -->
						</div>
					</div> 
					
					
					
					
				</div>
			</div>
		</section>
		<div id="paymentForm" style="display:none;">

		</div>
		<section class="order-detail-container hidden-xs hidden-sm hidden-lg hidden-md">
			<div class="container">
				<div class="row">
					
							<div class="detail-list-view hidden-xs hidden-sm">
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="detail-list text-center">
										<h3>Order #</h3>
										<h4>700123</h4>
									</div>
								</div>
								
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="detail-list text-center">
										<h3>Station</h3>
										<h4>New Delhi (NDLS)</h4>
									</div>
								</div>
								
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="detail-list text-center">
										<h3>Delivery TIme</h3>
										<h4><time>21:00 Today</time></h4>
									</div>
								</div>
								
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="detail-list text-center">
										<h3>Passenger Detail</h3>
										<h4>Anjali Sharma</h4>
									</div>
								</div>
								
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="detail-list text-center">
										<h3>Coach/Seat</h3>
										<h4>S1/32</h4>
									</div>
								</div>
								
								<div class="col-md-2 col-sm-2 col-xs-12">
									<div class="text-center">
										<button type="button" class="btn btn-update">Update</button>
									</div>
								</div>
							</div>
							
							
							<div class="detail-list-mobile-view visible-xs visible-sm hidden-lg hidden-md">
								<ul class="list-inline text-center">
									<li><a href="#">Anjali Sharma</a></li>
									<li><a href="#"><i class="fa fa-train"></i> 5</a></li>
									<li><a href="#"><i class="fa fa-bed"></i>  345</a></li>
									<li class="edit-list"><a href="#"><i class="fa fa-pencil"></i></a></li>
								</ul>
							</div>
							
					
				</div>
			</div>
		</section>
		
		<section class="more-option hidden-xs hidden-sm hidden-lg hidden-md">
			<div class="container">
				<div class="row">
					<div class="">
						<div class="more-option-list text-center">
							<ul class="list-inline">
								<li><span class="ico icon-trip"><img src="https://desktop.travelkhana.com/img/icon-trip.png" alt="" title=""/></span> Back To Trip</li>
								<li><span class="ico icon-another-trip"><img src="https://desktop.travelkhana.com/img/icon-another.png" alt="" title=""/></span> Add Another Trip</li>
								<li><span class="ico icon-track"><img src="https://desktop.travelkhana.com/img/icon-track.png" alt="" title=""/></span> Track Order</li>
								<li><span class="ico icon-invoice"><img src="https://desktop.travelkhana.com/img/icon-invoice.png" alt="" title=""/></span> Print Invoice</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		
		</section>
		
			
		
	</div>
	
	<!--update Detail Popup Here -->
		<!-- Modal -->
		<div class="modal fade" id="UpdateDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document" style="width: 367px;">
			<div class="modal-content">
				
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Update Detail</h4>
			  </div>
			  <div class=" alert alert-success  updateDivSuccess" style="display:none;">
					<span class="successUpdate"></span>
				</div>
				<div class=" alert alert-danger updateDivFailed" style="display:none;">
					<span class="errorUpdate"></span>
				</div>
			  <div class="modal-body">
					<form id="modal-form">
					  <div class="form-group">
						<input type="text" class="form-control nameUpdate" name="name" placeholder="Name" value=<?php echo $trackOrderResponse[0]['name']?> maxlength="40">
					  </div>
					  
					  <?php 
					  
					  	if($trackOrderResponse[0]['discount']===0)
					  	{
					  ?>
					  <div class="form-group">
						<input type="text" class="form-control numeric mobileUpdate" maxlength="10" name="mobile_number" value=<?php echo $trackOrderResponse[0]['contactNo']?> placeholder="Mobile Number">
					  </div>

					 <?php } ?>
					 <input type="hidden" class="mobileUpdate" value=<?php echo $trackOrderResponse[0]['contactNo']?> />
					  <div class="form-group">
						<input type="text" class="form-control seatUpdate" name="seat" maxlength="15" placeholder="Seat" value=<?php echo $trackOrderResponse[0]['seat']?> >
					  </div>
					  <div class="form-group">
						<input type="text" class="form-control coachUpdate" name="coach" maxlength="15" placeholder="Coach" value=<?php echo $trackOrderResponse[0]['coach']?> >
					  </div>
					  <div>
					  <button type="submit" class="btn btn-update-detail ">Update</button>
					  <span style='height:35px;width:35px;display:none;' class="pull-right spinner startSpin"></span>
						</div>
					</form>

			  </div>
			  
			</div>
		  </div>
		</div>
	<!--popup ends here-->
	
	
	<!--Online Payment Popup Here-->
		
		<div class="online-payment">
			<div class="modal fade" id="online_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document" style="width: 367px;">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title">Pay Using Cash Card/Wallets</h3>
					  </div>
					  <div class="panel-body">
							<ul class="list-inline" style="margin-left: 18px;">
								<li><div class="payment-logo paytm online-paytm" title="Paytm(Wallet, Credit/Debit Card, Net Banking etc)"></div></li>
								<li><div class="payment-logo payu online-payu" title="PayU" style="margin-left: 100px;"></div></li>
							</ul>
							<div class="col-md-6" style="font-size:10px;">
								(Wallet, Credit/Debit Card, Net Banking etc)
							</div>
					  </div>
					  <!--<div class="panel-footer">
						<div class="checkbox">
							<label>
							  <input type="checkbox">I Agree All <a href="#">Terms & Conditions</a>
							</label>
						</div>
						<button type="button" class=" btn btn-pay-online">Proceed Securely</button>
					  </div>-->
					</div>
					
					
					
					
				  </div>
				  
				</div>
			  </div>
			</div>
		</div>
		
	<!----Online Payment Popup Ends Here----->
	
	
    <!--back to top-->
    <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
   <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>--->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/validate-js/2.0.1/validate.min.js"></script>
   <!-- <script src="js/custom.js"></script> -->


   
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
   <script src="/glimpse/src/js/confirm.js?v=0.3"></script>
   <script type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></script>

   <script>
    $('#carousel-example-generic').carousel({wrap: true});
   </script>
    <script defer type="text/javascript">
//document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></scr'+'ipt>');

	$( document ).ready(function() {

	// Clevertap
        clevertap.event.push("RS-Charged", {
            "station": '<?php echo $trackOrderResponse[0]["stationName"]?>',
            "amount": <?php echo $trackOrderResponse[0]['payable_amount']?>,
            "outletId": <?php echo $trackOrderResponse[0]['orderOutletId']?>,
            "orderId": <?php echo $trackOrderResponse[0]['userOrderId']?>,
            "channel":channel,
            
        });
		

	});
</script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script defer type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-37839621-1']);
        _gaq.push(['_setDomainName', 'travelkhana.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);
        (function () {
          var ga = document.createElement('script');
          ga.type = 'text/javascript';
          ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
        })();
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                  }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37839621-1', 'auto');
        ga('send', 'pageview');
      </script>
    </div>
    <script defer>
      window.onload = function () {
          /*TK.PlaceOrder.init();*/
          }
    </script>
   