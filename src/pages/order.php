<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">


<meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Food Delivery in Trains | Food for Train Journey | Train Food Service – TravelKhana.Com</title>
<meta name="description" content="Searching for Delivery of Food in Trains or Train Food Service Visit us at TravelKhana! Online food order in train and just wait for fresh, hot food to arrive at your seat!" />
<meta name="keywords" content="Food in train, Jain Food in Train, rail, food, delivery, PNR, Railway, food delivery in train, food for train journey, train food service, railway restaurant" />


<!-- CSS -->
<link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.7">
<link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" > -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.3">
<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2"> -->

<link rel="apple-touch-icon" sizes="57x57" href="https://desktop.travelkhana.com/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://desktop.travelkhana.com/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://desktop.travelkhana.com/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://desktop.travelkhana.com/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://desktop.travelkhana.com/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://desktop.travelkhana.com/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://desktop.travelkhana.com/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://desktop.travelkhana.com/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://desktop.travelkhana.com/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://desktop.travelkhana.com/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://desktop.travelkhana.com/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://desktop.travelkhana.com/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://desktop.travelkhana.com/img/favicon-16x16.png">
<!-- <link rel="manifest" href="img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- Favicon and touch icons -->
<style>
.ui-autocomplete { 
       height: 200px;
       overflow-y: scroll;
       overflow-x: hidden;
     }
</style>
</head>
<body>
<!--main menu-->
<!-- Static navbar -->
<header class="navbar navbar-inverse main-header">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#myNavmenu" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button>
<a class="navbar-brand hidden-xs visible-md visible-sm visible-lg" href="#"><div class="main-logo" alt="TravelKhana" title="TravelKhana"></div></a>
<a class="navbar-brand visible-xs hidden-lg visible-sm hidden-sm hidden-md" href="#"><div class="inner-logo" alt="TravelKhana" title="TravelKhana"></div></a>

<div class="app-icon visible-xs hidden-md">
	<ul class="list-inline">
		<li><a href="http://bit.ly/1a5lLVi" title="Google Play"><i class="material-icons">android</i></a></li>
		<li><a href="https://itunes.apple.com/in/app/travelkhana-train-food-service/id1080942221?mt=8" title="App Store"><i class="material-icons">stay_current_portrait</i></a></li>
	</ul>
</div>


</div>
<ul class="nav navbar-nav navbar-right scroll-to hidden-xs">
	<!-- <li class=""><a href="#">Partner With Us</a></li>
    <li><a href="track-order.html">Track Order</a></li> -->
    <li><a href="tel:08800-31-31-31">Call 08800-31-31-31 Now !</a></li>
	<!-- <li><a href="#"><img src="img/android-btn.png" alt="" title=""/></a></li>
	<li><a href="#"><img src="img/app-btn.png" alt="" title=""/></a></li> -->
	

</ul>
<div id="myNavmenu" class="navmenu navmenu-fixed-right offcanvas">
<ul class="nav navbar-nav navbar-right scroll-to">
<li class="hidden-lg hidden-md hidden-sm"><a href="https://www.travelkhana.com/travelkhana/jsp/about.jsp"><i class="material-icons">info_outline</i> About Us</a></li>
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="http://www.travelkhana.com/travelkhana/jsp/restaurant.jsp"><i class="material-icons">restaurant</i> Services</a></li> -->
<li class="hidden-lg hidden-md hidden-sm"><a href="#"><i class="material-icons">shopping_cart</i> Order Food</a></li>
<li class="hidden-lg hidden-md hidden-sm"><a href="https://www.travelkhana.com/travelkhana/indian-railways-reservation"><i class="material-icons">verified_user</i> Indian Railways Reservation</a></li>
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="https://www.travelkhana.com/travelkhana/jsp/groupBooking.jsp"><i class="material-icons">group</i> Group Travel</a></li> -->
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="track-order.html"><i class="material-icons">my_location</i> Track Order</a></li> -->
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="#"><i class="material-icons">train</i> Track Train</a></li> -->
<li class="hidden-lg hidden-md hidden-sm"><a href="https://www.travelkhana.com/travelkhana/jsp/contact.jsp"><i class="material-icons">pin_drop</i> Contact Us</a></li>
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="#"><i class="material-icons">border_color</i> Blog</a></li> -->
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="https://www.travelkhana.com/travelkhana/jsp/career.jsp"><i class="material-icons">work</i> Careers</a></li> -->
<!-- <li class="hidden-lg hidden-md hidden-sm"><a href="#"><i class="material-icons">device_hub</i> Site Map</a></li> -->

</ul>
</div><!--/.nav-collapse -->

</div><!--/.container-fluid -->
</header>
<!--end main menu-->


<!--start hero-->
<section class="hero-padded background-2">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
		<h1>Book Food for your Train Journey</h1>
		<form autocomplete="off" class="form-inline search-form" id="srchForm" action="https://www.travelkhana.com/travelkhana/jsp/menu.html" method="post">
        <div class="input_bg">
          <div class="form-group first_portion marginScale">
			   <input type="text" class="form-control" id="train1" name="train1"  required="" placeholder="Loading Trains List" disabled>
			   <input type="hidden" name="to" id="to" >
			   <input type="hidden" name="stsess" id="stsess" >
          </div>
          <div class="form-group first_portion marginScale">
			   <input type="text" class="form-control" id="from1" name="from1" required="" placeholder="Enter Boarding Station" disabled> 
			   <input type="hidden" id="fromstn" name="fromstn"  >
          </div>
          <div class="form-group first_portion">
           <input type="text" class="form-control datepicker"  name="jDate" id="jDate" required placeholder="Enter Boarding Date" value="" onfocus="blur();">
           <i class="fa fa-calendar" onclick="$('#jDate').focus();"></i>
          </div>
          
          <div class="or text-center hidden-lg hidden-md visible-xs visible-md"><span>OR</span></div>
          
          <div id="" class="form-group last second_portion">
           
           <span class="visible-lg visible-md hidden-xs visible-sm">OR</span>
           <input type="text" class=" form-control" name="pnr1" id="pnr1" maxlength="10" minlength="10" placeholder="Enter 10 Digit PNR No.">
          </div>
        </div>
        
        <button id="search" type="button" class="btn btn-search " onclick="allTripAdded()">SEARCH</button>
       </form>
	
	
</div>
</div>
</div>
</section>
<!--end main hero-->

<!--How it Works Start Here-->
<section id="works" class="text-center hidden-xs visible-sm visible-md visible-lg">
<div class="container">
<div class="row">
<div class="main_heading">
	<h2>How it Works</h2>
</div>
</div>

<div class="row">
<div class="col-md-4 col-sm-4 col-xs-12">
	<div class="work-box text-center">
		<h4>Step 1</h4>
		<div class="search-train" alt="select train" title=""></div>
		<h3>Select Train</h3>
	</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
	<div class="work-box text-center">
		<h4>Step 2</h4>
		<div class="food-option" alt="various food" title=""></div>
		<h3>Choose Food From Various Option</h3>
	</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
	<div class="work-box text-center">
		<h4>Step 3</h4>
		<div class="fresh-food" alt="fresh food" title=""></div>
		<h3>Receive Fresh Food On Seat</h3>
	</div>
</div>
</div>

</div>
</section>
<!--How it Works Ends Here-->

<!--get our app section starts here -->
<section id="our-app" class="download-app hidden-xs visible-md visible-sm visible-lg">
<div class="container">
<div class="row">
<div class="main_heading">
	<h2>Get the TravelKhana App</h2>
</div>
</div>

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
	
	<div class="overview-text">
		<h3>Download the Travelkhana App on Android or iOS and get fresh food delivered to your seat</h3>
	</div>
	
	<div class="download-area">
		<ul class="btn-group list-inline">
			<li class="android-btn">
				<a href="http://bit.ly/1a5lLVi">
				<div class="android-img" alt="android btn" title=""></div>
				</a>
			</li>
			<li class="app-btn">
				<a href="https://itunes.apple.com/in/app/travelkhana-train-food-service/id1080942221?mt=8">
				<div class="app-img" alt="app btn" title=""></div>
				</a>
			</li>
		</ul>
	</div>
</div>


<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="image-holder text-center" alt="travel iphone img" title=""></div>
</div>

</div>



</div>
</section>
<!--get our app section ends here -->


<!--media review section Start here-->
<section id="review" class="our-reviews hidden-xs visible-lg visible-md visible-sm">
<div class="container">
<div class="row">
<div class="main_heading text-center">
	<h2>Media & Reviews</h2>
</div>
</div>

<div class="row">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<div class="row">
				<div class="col-xs-12">
					<div class="thumbnail adjust1">
					 <a rel="nofollow" href="https://economictimes.indiatimes.com/wealth/earn/how-startup-travelkhana-offers-food-of-choice-to-railway-passengers/articleshow/51753557.cms"  target="_blank">
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="caption text-center">
								<p class="adjust2">How startup TravelKhana offers food of choice to railway passengers -  Economic Times</p>
								
								
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12"> 
							<img class="media-object img-responsive pull-left" src="https://desktop.travelkhana.com/img/economics-times.png" alt="review img" title=""> 
						</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item">
			<div class="row">
				<div class="col-xs-12">
					<div class="thumbnail adjust1">
					 <a rel="nofollow" href="https://inc42.com/startups/travelkhana/"  target="_blank">
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="caption text-center">
								<p class="adjust2">From Disruption To Leadership: Here's How TravelKhana Became A Food-Tech Major</p>
								
								
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="review-img media-object img-responsive pull-left" style="background:url('https://desktop.travelkhana.com/img/inc42.png') no-repeat 100%;width:308px;height:37px;" alt="review img" title=""></div> 
						</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row">
				<div class="col-xs-12">
					<div class="thumbnail adjust1">
					  <a rel="nofollow" href="http://www.firstpost.com/business/wanted-to-marry-creativity-with-technology-and-travelkhana-fits-the-bill-says-ceo-pushpinder-singh-2768718.html"  target="_blank">
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="caption text-center">
								<p class="adjust2">Doing business and running a marathon are similar, says Travelkhana CEO Pushpinder Singh</p>
								
								
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12"> 
							<img class="media-object img-responsive pull-left" src="https://desktop.travelkhana.com/img/firstpost-n.png" alt="review img" title=""> 
						</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
</div>
</div>


</div>
</section>
<!--footer Start Here-->
<footer class="">
<div class="footer-top-area">
<div class="container">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="quick-links clearfix">
			<div class="col-md-4 col-sm-4 col-xs-12 hidden-xs visible-md visible-sm visible-lg">
				<div class="quick-link-list">
					<ul class="list-unstyled">
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/about.jsp"><i class="material-icons">info_outline</i> About Us</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/career.jsp"><i class="material-icons">work</i> Careers</a></li>
						<li><a href="http://www.travelkhana.com/blog"><i class="material-icons">border_color</i> Blog</a></li>
						<li><a href="http://www.travelkhana.com/travelkhana/jsp/restaurant.jsp"><i class="material-icons">card_membership</i> Become a Partner</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/groupBooking.jsp"><i class="material-icons">group</i> Group Travel</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/contact.jsp"><i class="material-icons">pin_drop</i> Contact Us</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/sitemap.jsp"><i class="material-icons">device_hub</i> Site Map</a></li>
						 <li><a href="https://www.travelkhana.com/travelkhana/jsp/faq.jsp"><i class="material-icons">border_color</i>FAQ for Customers</a>
						 <li>
                         <li><a href="https://www.travelkhana.com/travelkhana/jsp/faq_foodPlaza.jsp"><i class="material-icons">border_color</i>FAQ for Restaurant Partners</a></li>
					</ul>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-4 col-xs-12 hidden-xs visible-md visible-sm visible-lg">
				<div class="quick-link-list">
					<ul class="list-unstyled">
						<!-- <li><a href="#"><i class="material-icons">restaurant</i> Services</a></li> -->
						<li><a href="#"><i class="material-icons">shopping_cart</i> Order Food</a></li>
						<!-- <li><a href="#"><i class="material-icons">my_location</i> Track Order</a></li> -->
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/popularTrainList.jsp"><i class="material-icons">info_outline</i> Food in Popular Trains</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/track-train"><i class="material-icons">tram</i> Track Train</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-reservation"><i class="material-icons">verified_user</i> Indian Railways Reservation</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-time-table"><i class="material-icons">today</i> Indian Railways Time Table</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/check-pnr-status"><i class="material-icons">tram</i> Check PNR status</a></li>
						<li><a href="https://www.travelkhana.com/travelkhana/jsp/userQuery.jsp"><i class="material-icons">group</i> Request CallBack</a></li>
						 <li><a href="https://www.travelkhana.com/travelkhana/jsp/team.jsp"><i class="material-icons">group</i>Our Team</a></li>
					</ul>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="verified-list hidden-xs visible-md visible-sm visible-lg">
						<ul class="list-inline">
							<li><a ><div class="vimg-1" alt="verified-img-1" title=""></div></a></li>
							<li><a ><div class="vimg-2" alt="verified-img-2" title=""></div></a></li>
							<li><a ><div class="vimg-3" alt="verified-img-3" title=""></div></a></li>
							<li><a ><div class="vimg-4" alt="verified-img-4" title=""></div></a></li>
							
						</ul>
					</div>
					
					
					
					
			</div>
			
		</div><!--quick-links ist ends here-->
		
		
		<div class="follow-us-list text-center">
			<ul class="list-inline">
				<li><a href="https://www.facebook.com/Travelkhana"><i class="circle fa fa-facebook"></i></a></li>
				<li><a href="https://twitter.com/Travelkhana"><i class="circle fa fa-twitter"></i></a></li>
				<li><a href="https://plus.google.com/+Travelkhanaservice"><i class="circle fa fa-google-plus"></i></a></li>
				<li><a href="https://www.instagram.com/travelkhana1/"><i class="circle fa fa-instagram"></i></a></li>
				<li><a href="https://www.linkedin.com/in/travelkhana/?ppe=1"><i class="circle fa fa-linkedin"></i></a></li>
			</ul>
		</div>


		<div class="copyright-text text-center hidden-xs visible-md visible-lg">
			<ul class="list-inline">
				<li>Copyright &copy; 2017, TravelKhana, All rights reserved.</li>
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/policy.jsp">Privacy Policy</a></li>
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/terms.jsp">Terms & Conditions</a></li>
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/disclaimer.jsp">Disclaimer</a></li>
			</ul>
		</div>
		
		<div class="divider"></div>
		
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="about-tk">
			<div class="footer-bottom-heading text-center">
				<h2>"Food Delivery in Train"</h2>
			</div>
			
			<div class="about-text">
			<p><strong>What is TravelKhana?</strong></p>
			<p>Did you ever ordered and grabbed a slice of your favourite meal while being on the wheels? Are you planning to do order food delivery in train this time you are going to be on the moving train? Seeking online to find a reliable and well acknowledged e-catering service provider taking care of all the aspects including richness and taste of the food, along with the quality and cost effective pricing of them? If yes, you are definitely at the right platform. TravelKhana is a reputed and recognized food in train service provider that is been catering across more than 250 stations and in more than 4000 trains as of now, and still the number is on the rise. Supposed to be serving people and travellers in the train with umpteen choices in food meals, the platform has gained recognition and is been serving thousands of passengers on daily basis. There is no doubt that with time, things have improvised and earlier, people are left with fewer choices in terms of variety and quality of the food. There were increasing concerns of bad health and people fallen ill after eating something at the railway station given by the local vendors and hawkers, with IRCTC food and seeing the same, TravelKhana came up with a notion to serve their passengers high quality and fresh choices in snacks, beverages, different kinds of cuisines that could fall in their customers’ budgetary constraint and that could help them in making the most of their travel.</p>
			<p>	The platform is been introduced by a young, passionate and dedicated panel of professionals, having a simple aim to render passengers food on train with high quality standard and hygienically prepared food choices at nominal prices and do not make them feel confined to not able to grab a slice of their desired meal just because they are in the moving train. People simply chose rail transportation in the country as compared to other mediums and everyone has a different sort of reason behind their travel in trains. We just want our passengers, irrespective of whichever reason they are travelling, simply enjoy their travel to the core and with the choicest of meals they would like to have during the journey. And this is why, whether people want to meet their hunger pangs with South Indian meal or North Indian or Continental, Italian, Jain Thali, light snack, anything in vegetarian or in non vegetarian, they have the option for almost everything.</p>
			<p><strong>How to order from TravelKhana?</strong></p>
			<p>Want to know now how to order from them? First of all, do understand that the order could be placed via direct call or through its website or through its mobile app that could be downloaded in your Smartphone. Think of the station at which you want your delivery to be done, go through the menu and decide what you are going to order that could satisfy your taste buds as well as could fit in your budget as well. There is an assurance of getting quality food and services from them and they have partnered up with recognized and well known food joints, vendors and service providers having their branches spread all around. Adding to this, whatever meal you are going to order from them is going to be worth to do order and turn your travel into a memorable one. To order your choice of meal, go through the following steps and place an order.  </p>
			<p>&nbsp;</p>
			<ul>
				<li>Feed in your PNR number. </li>
				<li>Select the station from the drop down at which meal needs to get delivered. </li>
				<li>Add food items in your basket.</li>
				<li>Select the payment mode and do the transaction as desired. </li>
				<li>Sit back, relax and wait for the station to come. </li>
			</ul>
			<p>&nbsp;</p>
			<p><strong>Why us:</strong></p>
			<p>Want to know now how TravelKhana is different and distinctive from others? Is there is a special reason to give preference to them and not others for the food on train journey and other features? Well, there are definitely many reasons behind the same and due to them, within such a short time span, the company has expanded and took growth by leaps and bounds.</p>
			<p>&nbsp;</p>
			<ul>
				<li>
					<strong>Variety - </strong>The platform is been providing all age group people on train with several choices that can suit them at nominal prices. We understand that being on the moving train doesn’t mean that your wishes could not be fulfilled. The company has been linked with famous restaurants and chains all around, so do not feel hesitate for even a second while ordering with them and whatever your heart is been craving for, get the same within time and of superior quality standards
				<li>
				<li>
					<strong>Class services – </strong>TravelKhana is been famous and known for offering class services to its passengers irrespective of religion, caste, creed, designation or others. For us, each of our customers stands the same and people simply need to order from our platform and rest will be taken care of with utmost accuracy covering the quality, preparation of food, delivery and packaging on time and so. 
				</li>
				<li>
					<strong>Customer Service –</strong> Our platform is been providing round the clock customer care assistance and support to our valuable customers trusting upon us and our services from all around. Feel free to track your orders, get in touch with us anytime, and give your valuable feedback and more at 08800-31-31-31.  
				</li>
			</ul>
			<p><strong>Wait, there's more! </strong></p>
			<p>There’s more to it! If you are placing an order with our mobile app, simply download it first. The best part is that our mobile app even works well and smooth without an active internet connection. Aside from our impeccable <a href="https://www.travelkhana.com"> food delivery in train</a> e-catering services to our clients, at our platform, people can seek many other features and services as well related to getting access to train schedule, updates and so. Feel free to explore them, get bookings, check pnr status, check seat availability, access Indian Railways time table, live time running status of trains and more without any trouble. Make use of the technology and enjoy your journey to the core. </p>
			</div>
			
		</div>
	</div>
</div>

</div>
</div>

<div class="footer-bottom-area hidden-lg hiddenmd visible-xs visible-sm">
<div class="container">
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12 lPadding">
		<div class="copyright-text">
			<p>Copyright &copy; 2017, TravelKhana, All rights reserved.</p>
		</div>
	</div>
	
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="more-links">
			<ul class="list-inline">
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/policy.jsp">Privacy Policy</a></li>
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/terms.jsp">Terms & Conditions</a></li>
				<li><a href="https://www.travelkhana.com/travelkhana/jsp/disclaimer.jsp">Disclaimer</a></li>
			</ul>
		</div>
	</div>
	
</div>
</div>
</div>
</footer>
<!-- footer Ends Here -->

<!-- back to top -->
<a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script defer src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/validate-js/2.0.1/validate.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<script defer type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script defer src="/glimpse/src/js/common/custom.js?v=2.2"></script>
<script type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></script>

	<!--update Detail Popup Here -->
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
				<h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
			</div>
		  </div>
		</div>
	<!--popup ends here-->
	<script>
	//document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></scr'+'ipt>');
    $(document).ready(function(){
    	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
    		dd = '0'+dd
		} 
		if(mm<10) {
    		mm = '0'+mm
		} 
		today = yyyy+ '-' +mm+ '-' +dd;
		//document.getElementById("jDate").value=today;
		
		if ($.cookie('indexError') != null ){
			var value = JSON.parse($.cookie("indexError"));
			document.getElementById("myModalLabel").innerHTML =value.message;
    		$('#myModal').modal('show');
    		var trainSearched=value.train;
    		var stationSearched=value.station;
    		var dateSearched=value.date;
    		delete_cookie('indexError');
    		setTimeout(function(){ $('#myModal').modal('hide');}, 2000);

    		if(trainSearched!=''){
    			//document.getElementById('train1').value=value.trainFull;
    			//document.getElementById('from1').value=value.stationFull;
    			//document.getElementById('jDate').value=dateSearched;
    			clevertap.event.push("RS-Not delivering", {
            	"Train":trainSearched,
            	"BoardingStation": stationSearched,
            	"SelectedDate": dateSearched,
            	"By": channel
            	});
    		}
    	}else{
    		console.log("w l event....");
    		clevertap.event.push("RS - Website Launched", {
	                        "Channel": channel
	                    });
    	}
    	
    	if ($.cookie('revaluateCookieObject') != null ){
    	delete_cookie('revaluateCookieObject');
    	}
    	if ($.cookie('minOrderAmount') != null ){
		delete_cookie('minOrderAmount');
		}
		if ($.cookie('trackOrderResponse') != null ){
		delete_cookie('trackOrderResponse');
		}
		if ($.cookie('orderPost') != null ){
		delete_cookie('orderPost');
		}
		if ($.cookie('pnr') != null ){
		delete_cookie('pnr');
		}
		if ($.cookie('menuObject') != null ){
		delete_cookie('menuObject');
		}
    });
    var delete_cookie = function(name) {
    document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
   </script>
   <script type="text/javascript">
   if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
   	$(':input').focus(function(){
    	var center = $(window).height()/4;
    	var top = $(this).offset().top ;
    	if (top > center){
     	   $(window).scrollTop(top-center);
    	}
	});
 }
   </script>
   <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-37839621-1']);
            _gaq.push(['_setDomainName', 'travelkhana.com']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
            (function () {
              var ga = document.createElement('script');
              ga.type = 'text/javascript';
              ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(ga, s);
            })();
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                      }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-37839621-1', 'auto');
            ga('send', 'pageview');
    </script>
    <script type="application/ld+json"> 
	{ "@context" : "http://schema.org", 
	"@type" : "Organization", 
	"name" : "TravelKhana", 
	"url" : "https://www.travelkhana.com/", 
     "logo": "https://www.travelkhana.com/travelkhana/images/TK-logo.png",
	"sameAs" : [ "https://www.facebook.com/Travelkhana/", 
	"https://www.instagram.com/travelkhana1/", 
	"https://twitter.com/Travelkhana ", 
	"https://plus.google.com/+Travelkhanaservice"] 
	} 
	</script>
	<script defer>
            var clevertap = clevertap || {event: [], profile: [], account: []};
            clevertap.account.push({"id": "6K9-Z9Z-554Z"});
            clevertap.enablePersonalization = true;
            (function () {
                var wzrk = document.createElement('script');
                wzrk.type = 'text/javascript';
                wzrk.async = true;
                wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.clevertap.com') + '/js/a.js?v=0';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wzrk, s);
            })();
    </script>
    <script type="text/javascript">

    function getParameterByName(name, url) {
    	if (!url) url = window.location.href;
    	name = name.replace(/[\[\]]/g, "\\$&");
    	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    	if (!results) return null;
    	if (!results[2]) return '';
    	return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
$(document).ready(function(){


 		if(window.location.search.indexOf('utm_source') > -1 && window.getParameterByName("utm_source")==='indian-rails')
 		{
 			document.cookie = 'utm_source='+window.getParameterByName("utm_source")+';path=/;';

 		}
    	else if( navigator.userAgent.match(/Android/i)
 		|| navigator.userAgent.match(/webOS/i)
 		|| navigator.userAgent.match(/iPhone/i)
 		|| navigator.userAgent.match(/iPad/i)
 		|| navigator.userAgent.match(/iPod/i)
 		|| navigator.userAgent.match(/BlackBerry/i)
 		|| navigator.userAgent.match(/Windows Phone/i)
 		){
    		//delete utm cookie
    		if ($.cookie('utm_source') != null ){
			delete_cookie('utm_source');
			}

  		}else{
    		if (window.location.search.indexOf('utm_source') > -1) {
   				document.cookie = 'utm_source='+window.getParameterByName("utm_source")+';path=/;';
			}else{
    			if ($.cookie('utm_source') != null ){
				delete_cookie('utm_source');
				}
    		}
  		}
  	});
    </script>
   </body>
</html>