<?php
session_start();
include 'common/main.php';
$tDetail= "";
$sDetail = "";
$dTime="";
$dDate="";

if(!isset($_COOKIE['revaluateCookieObject']))
	{
	header("Location: order.jsp"); /* Redirect browser */
	exit;
	}
$revaluateCookie=$_COOKIE["revaluateCookieObject"];
$revaluateObject = json_decode($revaluateCookie);

if(isset($_SESSION['trainDetail']) && !empty($_SESSION['trainDetail'])) {
	$tDetail= $_SESSION['trainDetail'];
	$sDetail = $_SESSION['stationDetail'];
	$dTime = $_SESSION['deliveryTime'];
	$dDate = $_SESSION['deliveryDate'];
}
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>checkout</title>
    <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/>

<!-- CSS -->
<link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.4">
<link rel="stylesheet" href="/glimpse/src/css/progressBar.css?v=0.2">
<!-- 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.2">

<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2"> -->

<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="https://desktop.travelkhana.com/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://desktop.travelkhana.com/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://desktop.travelkhana.com/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://desktop.travelkhana.com/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://desktop.travelkhana.com/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://desktop.travelkhana.com/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://desktop.travelkhana.com/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://desktop.travelkhana.com/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://desktop.travelkhana.com/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://desktop.travelkhana.com/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://desktop.travelkhana.com/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://desktop.travelkhana.com/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://desktop.travelkhana.com/img/favicon-16x16.png">
<!-- <link rel="manifest" href="https://desktop.travelkhana.com/img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://desktop.travelkhana.com/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
	
  </head>
  <body>
   
	
	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			 
			  <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<!-- <div class="collapse navbar-collapse search-inner-form" id="bs-example-navbar-collapse-1">
			  <form class="navbar-form navbar-right" id="inner_form">
				<div class="form-group">
				  <input type="text" class="form-control" name="train_id" placeholder="12622/Tamil Nadu SF Express">
				</div>
				<div class="form-group">
				  <input type="text" class="form-control" name="boarding_station" placeholder="DLI/Delhi">
				</div>
				<div class="form-group date-field">
				  <input type="text" class="form-control datepicker" name="date" placeholder="21-04-2017">
				  <i class="fa fa-calendar"></i>
				</div>
				<button type="submit" class="btn btn-inner-search"><img src="https://desktop.travelkhana.com/img/search_img.png" alt="" title=""/></button>
			  </form>
			  
			</div> --><!-- /.navbar-collapse -->
		</div>

	</header>
	
	<section class="inner-search-content">

			<div class="container">
				<div class="row">
				
					<div class="back-to-order text-center visible-xs">
						<h3>Your Order</h3>
						<a href="#" id="back-link"><img src="https://desktop.travelkhana.com/img/back.png" alt="back" title="back"/></a>

					</div>
				
					
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="hidden-xs payment-detail">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<span class="train_name"><?php echo $tDetail ?></span>
									<h3>At <?php echo $sDetail  ?> Station</h3>
									<input type="hidden" id="delDate" name="delDate" style="display:none;" value="<?php echo $dDate ?>">
									<input type="hidden" id="delTime" name="delTime" style="display:none;" value="<?php echo $dTime ?>">
								</div>
								
								<div class="col-md-6 col-sm-6 col-xs-12">
									<span class="del_date">DELIVERY AT : <?php $delTime=date('h:i a', strtotime($dTime)); echo $delTime.' |'.$dDate ?></span>
									
								</div>
								
							</div>
						</div>
						

						<div class="row mTop-15" >
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="payment-detail-accordion">
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									  <div class="panel panel-default active" id="accordion-one">
										<div class="panel-heading" role="tab" id="headingOne">
										  <h4 class="panel-title">
											<a style="pointer-events: none;cursor: default;" role="button" data-toggle="collapse" class="acc_one" data-parent="#accordion" href="#confirm-detail" aria-expanded="true" aria-controls="collapseOne">
											  Confirm Your Details
											</a>
											
											<span class="step">1</span>
											<span class="complete-step" style="display: none;"><i class="fa fa-check"></i></span>
										  </h4>
										</div>
										<div id="confirm-detail" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseOne	">
										  <div class="panel-body">
											<form id="confirm-details" method="post" action="">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="form-group">
															<input type="text" class="form-control numeric" onkeyup='passDetails()'  id="contact" maxlength="10" name="contact" placeholder="Traveller's mobile number">
															<!-- <span id="contactDetail" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span> -->
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="form-group">
															<input type="text" class="form-control" id="name" maxlength="50" name="name" placeholder="Enter your name">
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="form-group">
															<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" maxlength="50">
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="prcd_btn text-center">
													 <button type="button" class="btn btn-proceed button1" id="proceed-btn">Proceed</button>
													</div>
												</div>
											
										  </div>
										</div>
									  </div>									  
									</div>
									
									<div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
									  <div class="panel panel-default active" id="accordion-two">
										<div class="panel-heading" role="tab" id="headingTwo">
										  <h4 class="panel-title">
											<a style="pointer-events: none;cursor: default;" role="button" data-toggle="collapse" data-parent="#accordion1"  aria-expanded="true" aria-controls="collapseOne">
											  Delivery Detail
											</a>
											
											<span class="step">2</span>
											<span class="complete-step" style="display: none;"><i class="fa fa-check"></i></span>
										  </h4>
										</div>
										<div id="delivery-detail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="delivery-detail">
										  <div class="panel-body">
											
											<div class="row">
												<div class="col-md-4 col-sm-4 col-xs-12">
													<div class="form-group">
														<input type="text" class="form-control numeric" id="pnr" name="pnr" maxlength="10" placeholder="PNR">
													</div>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-6">
													<div class="form-group">
														<input type="text" class="form-control" id="coach" name="coach" maxlength="6" placeholder="Coach">
													</div>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-6">
													<div class="form-group">
														<input type="text" class="form-control" id="seat" name="seat" maxlength="3" placeholder="Seat">
													</div>
												</div>
												
												
											</div>
											
											<div class="row">
												<div class="prcd_btn text-center">
												<!-- <h4>Your Order will be placed as Cash on Delivery</h4> -->
												 <!-- <button type="button" class="btn btn-proceed" id="btn-proceed">Place Order</button> -->
												 <input type="button"  class="btn btn-proceed tempPlace button2" value="Proceed" />

												</div>
											</div>
											</form>	
										  </div>
										</div>
									  </div>
									  
									</div>
									<div class="progress" style="display:none;">
									  <div class="indeterminate"></div>
									</div>
									 <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
									  <div class="panel panel-default active">
										<div class="panel-heading" role="tab" id="headingThree">
										  <h4 class="panel-title">
											<a style="pointer-events: none;cursor: default;" role="button" data-toggle="collapse" data-parent="#accordion2" href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
											  Payment Method
											</a>
											<span class="step">3</span>
										  </h4>
										</div>
										<div id="payment-method" class="panel-collapse collapse " role="tabpanel" aria-labelledby="payment-method">
										  <div class="panel-body">
											<div class="payment-method">
												<div class="row">
													<div id="codPayable" class="amount text-center">
														<!-- <h3>TK Credit Used: <i class="fa fa-inr"></i> 50</h3> -->
														<h3>Amount to be Paid: <i class="fa fa-inr"></i> <?php echo round($revaluateObject->userOrderInfo->totalCustomerPayableCapped,2,PHP_ROUND_HALF_UP) ?></h3>
													</div>
													<div id="onlinePayable" class="amount text-center" style="display: none;">
														<h3>Amount to be Paid: <i class="fa fa-inr"></i> <?php echo round($revaluateObject->onlinePayableAmount,2,PHP_ROUND_HALF_UP) ?></h3>
													</div>
												</div>
												<div class="row" style="margin-left:170px;">
													<b>Pay only <i class="fa fa-inr"></i>
													<?php echo round($revaluateObject->onlinePayableAmount,2,PHP_ROUND_HALF_UP) ?>
													</h3> if payment done through Paytm or Payu </b>
												</div>
												
												<div class="row">
													<div class="payment-option-list">
														<ul class="list-inline" style="padding-left:99px;">
															<br>
															<li class="active">
																<label>
																  <input type="radio" name="payment_method" id="cod" checked="checked" value="cod"> Cash On Delivery
																</label>
																
															</li>
															<br>
															
															<!-- <li data-toggle="modal" data-target="#online_payment">
																<label class="radio-inline" id="op">
																  <input type="radio" name="payment_method" value="option2"> Online Payment
																</label>
																
															</li> -->
															<!-- <div class="" ><img src="download.png" width="83px" height="25px"/></div> -->
															
															<li class="active" >
																<label  id="paytm">
																  <input type="radio" name="payment_method" value="paytm" ><img src="https://desktop.travelkhana.com/img/paytm-logo.png" style="width: 59px;height:60px;padding-bottom: 5px; margin-left: 3px;" />
																  <!-- <input type="radio" name="payment_method" value="payu" > <img src="download.png" width="83px" height="25px"/> -->
																</label>
																<b>(Wallet, Credit/Debit Card, Net Banking etc) </b>
																
															</li> 
														</ul>
													</div>
												</div>
												
												<div class="row">
													<div class="prcd_btn finalProceed text-center">
														<button type="submit" class="btn btn-proceed">Place Order</button>
													</div>
												</div>
												
												
												
											</div>
										  </div>
										</div>
									  </div>
									  
									</div>
									
									
									
								</div>
							</div>
						</div>
						
						
						
					</div>
					
					
					<!-- <div class="mobile-cart-view visible-xs hidden-md" data-toggle="modal" data-target="#ItemDetail">
						<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
						<h5>Cart</h5>
						
					</div> -->
					
					
					<div class="col-md-4 col-sm-4 col-xs-12 hidden-xs">
						<!-- <div class="cart-content">
							<div class="cart-heading">
							<h3>Cart</h3>
							</div>
							
							<div class="cart-item-content">
								
								<div class="cart-item">
									<span class="item-name">Roti and Panner Veg Combo</span>
									<div class="item-quantity">
										<div class="input-group number-spinner">
											<span class="input-group-btn">
												<button class="btn btn-minus" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
											</span>
											<input type="text" class="form-control text-center" value="1">
											<span class="input-group-btn">
												<button class="btn btn-plus" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
											</span>
										</div>
									</div>
									<span class="item-price"><i class="fa fa-inr"></i> 135</span>
								</div>
								
								
								
								
								
								<div class="cart-subtotal">
									<p>Subtotal<span class="pull-right"><i class="fa fa-inr"> 335</i></span></p>
									<p>Discount<span class="pull-right"><i class="fa fa-inr"> 20</i></span></p>
									<p>Taxes<span class="pull-right"><i class="fa fa-inr"> 35.50</i></span></p>
									<p class="bold-text">Total<span class="pull-right"><i class="fa fa-inr"> 360</i></span></p>
								</div>
								
								 <div class="coupon-code">
									<input type="text" class="form-control" placeholder="Coupon Code">
									<i class="fa fa-gift"></i>
								</div>
								
								<div class="coupon-applied">
									<p>
									<span class="code"><i class="fa fa-gift"></i> FLAT500</span> has been successfully applied.
									<a href="#"><i class="fa fa-times"></i></a></span>
									</p>
								</div> 
								
								 <div class="checkout-btn">
									<button type="button" class=" btn btn-checkout">Checkout</button>
								</div>
							</div>
							 <div class="empty-cart">
								<div class="quotes text-center">
									<p>There is no love sincerer than the love of food.</p>
									<span><i>-George Hemard Shaw</i></span>
								</div>
								
								<div class="disable-cart-btn">
									<button type="button" disabled class=" btn btn-checkout">Checkout</button>
								</div>
							</div>
							
							
						</div> -->
						
						<div class="order-summary mrtop100">
							<h3 style="font-weight: bold; text-align: center;font-weight: bold;text-align: center;text-transform: none;">Your Order Summary</h3>
							
								<div class="cart-subtotal">
									<p style="color:#464646;line-height: 25px;"><span style="font-weight:600;">Subtotal</span><span class="pull-right"><i class="fa fa-inr"><span style="font-family: monospace;font-weight: 800;"> <?php echo round($revaluateObject->userOrderInfo->totalBasePrice,2)  ?></span></i></span></p>
									<?php
									if($revaluateObject->userOrderInfo->discount>0){
										echo '<p style="color:#464646;line-height: 25px;"><span  style="font-weight:600;">Discount</span><span class="pull-right"><i class="fa fa-inr"><span style="font-family: monospace;font-weight: 800;"> '.$revaluateObject->userOrderInfo->discount.'</span></i></span></p>';
									}
									if($revaluateObject->userOrderInfo->delivery_cost>0){
										echo '<p style="color:#464646;line-height: 25px;"><span  style="font-weight:600;">Delivery cost</span><span class="pull-right"><i class="fa fa-inr"><span style="font-family: monospace;font-weight: 800;"> '.$revaluateObject->userOrderInfo->delivery_cost.'</span></i></span></p>';
									}
									?>
									<p style="color:#464646;line-height: 25px;"><span style="font-weight:600;">Taxes</span><span class="pull-right"><i class="fa fa-inr"><span style="font-family: monospace;font-weight: 800;"> <?php echo round(($revaluateObject->userOrderInfo->taxes + $revaluateObject->userOrderInfo->totalCapped),2)  ?></span></i></span></p>
									<p style="color:#464646;line-height: 25px;"><span class="bold-text" style="font-weight:600;">Total</span><span class="pull-right"><i class="fa fa-inr"><span id="totalCapped" style="font-family: monospace;font-weight: 800;"> <?php echo round($revaluateObject->userOrderInfo->totalCustomerPayableCapped,2,PHP_ROUND_HALF_UP) ?></span></i></span></p>
								</div>
								
								<div class="coupon-applied">
									<p>
									<?php
									if(strcasecmp($revaluateObject->userOrderInfo->voucher_code, "")!=0){
									echo '<span class="code"><i class="fa fa-gift"></i> <span id="isVouchered">'.$revaluateObject->userOrderInfo->voucher_code.'</span></span> has been successfully applied.';
									}
									?>
									</p>
								</div> 
								
						</div>
					</div>
					
					
				</div>
			</div>
	</section>
	
	<!--Online Payment Popup Here-->
		
	<!-- 	<div class="online-payment">
			<div class="modal fade" id="online_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title">Pay Using Cash Card/ Wallets</h3>
					  </div>
					  <div class="panel-body">
						<ul class="list-inline">
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option1">
								Paytm
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option2" >
								Mobikwik
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option3" >
								Freecharge
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option4" >
								PayZapp
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option5" >
								Ola Money Wallet
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option6" >
								Airtel Money
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option7" >
								ICICI Pockets
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option8" >
								Idea Money
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option9" >
								Oxigen Wallet
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option10" >
								ZipCash
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option11" >
								Vodafone mPesa
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option12" >
								Icash
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option13" >
								Masterpass
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option14" >
								Money on Mobile
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option15" >
								Jio Money
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option16" >
								SBI Buddy
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option17" >
								The Mobile Wallet
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option18" >
								TTSL MRupee
							  </label>
							</div>
						</li>
						
						<li>
							<div class="radio">
							  <label>
								<input type="radio" name="PaymentOption" id="" value="option19" >
								PayU Money
							  </label>
							</div>
						</li>
					</ul>
					  </div>
					  <div class="panel-footer">
						<div class="checkbox">
							<label>
							  <input type="checkbox">I Agree All <a href="#">Terms & Conditions</a>
							</label>
						</div>
						<button type="button" class=" btn btn-pay-online">Proceed Securely</button>
					  </div>
					</div>
					
					
					
					
				  </div>
				  
				</div>
			  </div>
			</div>
		</div> -->
		
	<!--Online Payment Popup Ends Here-->
	<!--Online Payment Popup Here-->
		
		<div class="online-payment">
			<div class="modal fade" id="online_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title">Pay Using Cash Card/ Wallets</h3>
					  </div>
					  <div class="panel-body">
						<ul class="list-inline">
						<li><div class="payment-logo payumoney" title="PayUmoney"></div></li>
						<li><div class="payment-logo masterpass" title="Masterpass"></div></li>
						<li><div class="payment-logo jio-money" title="jio-money"></div></li>
						<li><div class="payment-logo mrupee" title="Mrupee"></div></li>
						<li><div class="payment-logo zipcash" title="Zipcash"></div></li>
						<li><div class="payment-logo icc-logo" title="ICC logo"></div></li>
						<li><div class="payment-logo idea-money-app" title="Idea Money wallet"></div></li>
						<li><div class="payment-logo m-pesa" title="M-Pesa"></div></li>
						<li><div class="payment-logo payzapp" title=""Payzapp></div></li>
						<li><div class="payment-logo oxigen-logo" title="Oxigen Wallet"></div></li>
						<li><div class="payment-logo airtel-money" title="Airtel Money"></div></li>
						<li><div class="payment-logo ola-money" title="Ola Money Wallet"></div></li>
						<li><div class="payment-logo freecharge" title="Freecharge"></div></li>
						<li><div class="payment-logo ebuddy-logo" title="SBI Buddy"></div></li>
						<li><div class="payment-logo momlogo" title="momlogo"></div></li>
						<li><div class="payment-logo pockets" title="Pockets"></div></li>
						<li><div class="payment-logo mobikwik" title="Mobikwik"></div></li>
						<li><div class="payment-logo paytm" title="paytm"></div></li>
						<li><div class="payment-logo tmw-trademark" title="TWM Trademark"></div></li>
					</ul>
					  </div>
					  <!--<div class="panel-footer">
						<div class="checkbox">
							<label>
							  <input type="checkbox">I Agree All <a href="#">Terms & Conditions</a>
							</label>
						</div>
						<button type="button" class=" btn btn-pay-online">Proceed Securely</button>
					  </div>-->
					</div>
					
					
					
					
				  </div>
				  
				</div>
			  </div>
			</div>
		</div>
		
	<!----Online Payment Popup Ends Here----->
	
	<!--back to top-->
        <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->
	
		<!--Cart Detail Popup Here-->
		
		<!-- Modal -->
		<div class="modal fade" id="ItemDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-header">
				
				<h4 class="modal-title text-center">Cart Item's</h4>
			</div>
			  <div class="modal-body">
				<div class="table-responsive">
					<table class="table">
						
						
						<tr>
							
							<td>Chicken Briyani</td>
							<td>
								<div class="item-quantity">
									<div class="input-group number-spinner">
										<span class="input-group-btn">
											<button class="btn btn-minus" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
										</span>
										<input type="text" class="form-control text-center" value="1">
										<span class="input-group-btn">
											<button class="btn btn-plus" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
										</span>
									</div>
								</div>
							</td>
							<td><i class="fa fa-inr"></i> 380</td>
						</tr>
						
						<tr>
							
							<td>Chicken Briyani</td>
							<td>
								<div class="item-quantity">
									<div class="input-group number-spinner">
										<span class="input-group-btn">
											<button class="btn btn-minus" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
										</span>
										<input type="text" class="form-control text-center" value="1">
										<span class="input-group-btn">
											<button class="btn btn-plus" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
										</span>
									</div>
								</div>
							</td>
							<td><i class="fa fa-inr"></i> 140</td>
						</tr>
						
						
						
						
					</table>
				</div>
			  </div>
			  
			  <div class="modal-footer">
				<div class="col-md-6 col-md-offset-6 col-xs-11 col-xs-offset-1">
					<div class="item-total-container">
						<p>Subtotal<span class="pull-right"><i class="fa fa-inr"></i> 335</span></p>
						<p>Discount<span class="pull-right"><i class="fa fa-inr"></i> 20</span></p>
						<p>Taxes &amp; Adjustments<span class="pull-right"><i class="fa fa-inr"></i> 35</span></p>
						<div class="footer-divider"></div>
						<h4 class="bold-text">Amount To Be Paid<span class="pull-right"><i class="fa fa-inr"> </i> 380</span></h4>
						
					</div>
				</div>
			  </div>
			 
			</div>
		  </div>
		</div>
		
	<!--Cart Detail Popup Ends Here-->
	<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:250px;text-align:center;">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
				<h4 class="modal-title alert alert-danger" id="myModalLabel"></h4>
			</div>
		  </div>
		</div>
		<div id="paymentForm" style="display:none;">

		</div>

	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
 

    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script  src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
    <script src="/glimpse/src/js/common/pricing.js?v=0.7"></script>
    <script src="/glimpse/src/js/common/checkout.js?v=0.4"></script>

    <script defer src="/glimpse/src/js/common/delivery-detailResize.js?v=0.2"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></script>
		<script defer type="text/javascript">


	//document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></scr'+'ipt>');
		$(document).ready(function() {
		if (typeof $.cookie('revaluateCookieObject') != 'undefined'){
			var value = $.cookie("revaluateCookieObject");
			var revalJson=JSON.parse(value);
			var userBasicInfoVar=revalJson.userBasicInfo;
			var discount = revalJson.userOrderInfo.discount;
			if(userBasicInfoVar!=null){
				if(userBasicInfoVar.contact_no.length>=10){
					//$("#contact").prop("readonly", true);
					if(discount>0){
						document.getElementById('contact').disabled = true;
					}
					document.getElementById('contact').value=userBasicInfoVar.contact_no;
					passDetails();
				}
				//document.getElementById('name').value=userBasicInfoVar.name;
				//document.getElementById('email').value=userBasicInfoVar.mail_id;
			}
		}
		if (typeof $.cookie('pnr') != 'undefined'){
			var value = $.cookie("pnr");
			if(value!=null){
				document.getElementById('pnr').disabled = true;
				document.getElementById('pnr').value=value;
			}
		}
	});

	function passDetails()
	{
		if(document.getElementById('contact').value.length<10){
		document.getElementById('name').value="";
		document.getElementById('email').value="";
		}else{	
	//	$('#contactDetail').show();
		var revaluateResponse='';
		var contact=document.getElementById('contact').value;
		document.getElementById('name').disabled = true;
		document.getElementById('email').disabled = true;


		$.ajax({
				url: URL+"customer/"+contact,
				type: "GET",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				beforeSend : function( xhr ) {
				xhr.setRequestHeader( "Authorization", authKey);
				},
				success: function(response){
				//	$('#contactDetail').hide();

					if(response.length>0){
						document.getElementById('name').disabled = false;
        				document.getElementById('email').disabled = false;
        				document.getElementById('name').value=response[0].name;
        				document.getElementById('email').value=response[0].mail_id;
        				 callCleverTap(contact,document.getElementById('email').value,document.getElementById('name').value);
					}else{
						document.getElementById('name').disabled = false;
        				document.getElementById('email').disabled = false;
        				document.getElementById('name').value="";
        				document.getElementById('email').value="";
        				callCleverTap(contact,document.getElementById('email').value,document.getElementById('name').value);
					}					
				},
				error: function(error){			        
						document.getElementById('name').disabled = false;
        				document.getElementById('email').disabled = false;
        				document.getElementById('name').value="";
        				document.getElementById('email').value="";
				}
				
		});
    
	 }
		
	}

	function callCleverTap(contact,email,name) {
		clevertap.profile.push({
			 "Site": {
			   "Phone": '+91'+contact,
			   "Name": name,
			   "Email":email
			 }
			});
			// Clevertap
        clevertap.event.push("RS-Checkout Details", {
            "contact": '+91'+contact,
            "email": email,
            "name": name
        });

	}

	var delete_cookie = function(name) {
		console.log("deleting cookie---->"+name);
    document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
    </script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-37839621-1']);
        _gaq.push(['_setDomainName', 'travelkhana.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);
        (function () {
          var ga = document.createElement('script');
          ga.type = 'text/javascript';
          ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
        })();
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                  }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37839621-1', 'auto');
        ga('send', 'pageview');
      </script>
    

    <script type="text/javascript">
	// $(document).ready(function() {
	// 	var radioValue = $("input[name='payment_method']:checked").val();
	// 	if(val=="cod"){
	// 		$(".onlinePayable").hide();
	// 		$(".codPayable").show(); 
	// 	} else if(val=="payu" || val=="paytm"){
	// 		$(".onlinePayable").show();
	// 		$(".codPayable").hide(); 
	// 	}
	//     // if($('.payment_method').is(":checked").val())   
	//     //     $(".answer").show();
	//     // else
	//     //     $(".answer").hide();
	// }

	$(document).ready(function() {
		$("input[name$='payment_method']").click(function() {
	        var val = $(this).val();
			if(val=="cod"){
				$("#onlinePayable").hide();
				$("#codPayable").show(); 
			} else if(val=="payu" || val=="paytm"){
				$("#onlinePayable").show();
				$("#codPayable").hide(); 
			}
	    });
	});

	</script>

