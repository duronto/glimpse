<?php
session_start(); 

$tDetail= "";
$sDetail = "";
$dTime="";
$dDate="";

if(isset($_SESSION['trainDetail']) && !empty($_SESSION['trainDetail'])) {
	$tDetail= $_SESSION['trainDetail'];
	$sDetail = $_SESSION['stationDetail'];
	$dTime = $_SESSION['deliveryTime'];
	$dDate = $_SESSION['deliveryDate'];
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#d51b00">
	<meta name="msapplication-navbutton-color" content="#d51b00">
	<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>cart</title>
   <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/> 


<!-- CSS -->
<link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.6">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.2">
<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2"> -->

<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
<!-- <link rel="manifest" href="img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">



  </head>
  <body>

	
	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
			</div>
		</div>
	</header>
	
	<section class="inner-search-content" style="color: #0d3e57;line-height: 25px;font-family: inherit;padding-top: 100px">
			<div class="container">
				<div class="row">
					
					<div class="back-to-order text-center visible-xs">
						<h3>Your Order</h3>
						<a href="https://www.travelkhana.com/travelkhana/jsp/menu.html"><img src="https://desktop.travelkhana.com/img/back.png" alt="back" title="back"/></a>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="payment-detail cart-add-mobile-number-detail hidden-xs">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<h1 class="pull-left">DELIVERY AT: <?php $delTime=date('h:i a', strtotime($dTime)); echo $delTime.' '.$dDate ?> | <span><?php echo $sDetail  ?></span></h1>			
								</div>
								
							</div>
						</div>
		
		<?php 

		if(!isset($_COOKIE['revaluateCookieObject']))
			{
			header("Location:https://www.travelkhana.com/travelkhana/jsp/menu.html"); /* Redirect browser */
			exit;
			}
		$revaluateObject = json_decode($_COOKIE["revaluateCookieObject"]);


		$userOrderMenuList=$revaluateObject->userOrderMenu;
		?>
						<!--mobile view detail-->
						<div class="train-name-time text-center visible-xs">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<h1 style="text-transform: uppercase;">DELIVERY AT <?php $delTime=date('h:i a', strtotime($dTime)); echo $delTime.' '.$dDate ?></h1>
									<h2><?php echo $sDetail  ?></h2>
									
								</div>
								
							</div>
						</div>
						<div class="row">
						</div>
						<!--mobile-view-detail ends-->			

						<?php

						

		
		//var_dump($userOrderMenuList);
?>
		<!-- <textarea style="display:none;" size="100000"  class="cartObject"  >
			<?php echo $cartObject ?>
		</textarea> -->
<?php
				foreach($userOrderMenuList as $item){	
					?>			
						
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="order-item-lists">

									<!-- first item -->
									<div class="single-item clearfix">
										<div class="col-md-2 col-sm-3 col-xs-3 item">
											<div class="item-img">
												<?php
												if (strpos($item->image, 'menu/') !== false) {
												?>
												<img  width="125px;"    height="71px;" src="<?php echo $item->image; ?>"  alt="" title=""/>
												<?php
													}
													else
													{
														?>
													<img  width="125px;"    height="71px;" src="https://desktop.travelkhana.com/img/tk_placeholder.png"  alt="" title=""/>
													<?php
													}
												?>
											</div>
										</div>
										<div class="col-md-3 col-sm-5 col-xs-5 item">
											<div class="item-name text-left">
												<?php
												$roundedItemBase=round($item->itemBasePrice);
												?>
												<h2><?php echo $item->item_name; ?></h2>
												<h5 class="hidden-md hidden-lg">Rs. <span class="item-amount" ><?php if($item->quantity==1){ echo $roundedItemBase;} else { echo  $roundedItemBase/($item->quantity);}  ?></span></h5>
											</div>
										</div>
										<div class="col-md-4 col-xs-12 item hidden-xs hidden-sm">
											<div class="item-price text-right">
												<h3>Rs. <span class="item-amount"><?php if($item->quantity==1){ echo $roundedItemBase;} else { echo  floatval(($roundedItemBase)/($item->quantity)) ;}  ?></span></h3>
												
											</div>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-4 item last-item pull-right">
											<div class="item-quantity item-price pull-right">
												<div class="input-group number-spinner">
													<span class="input-group-btn">
														<button id="<?php echo $item->itemId; ?>" class="btn btn-minus qty-minus btnMinus" data-dir="dwn"><span id="<?php echo $item->itemId; ?>" class="glyphicon glyphicon-minus"></span></button>
													</span>
													<input type="text" class="form-control text-center item-quantity-count numeric priceChange" readonly value="<?php echo $item->quantity ?>">
													<span class="input-group-btn">
														<button id="<?php echo $item->itemId; ?>" class="btn btn-plus qty-plus btnPlus" data-dir="up"><span id="<?php echo $item->itemId; ?>" class="glyphicon glyphicon-plus "></span></button>
													</span>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<?php
									
} ?>
					</div>
					
					
					<div class="mobile-cart-view visible-xs hidden-md hidden-xs" data-toggle="modal" data-target="#ItemDetail">
						<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
						<h5>Cart</h5>

					</div>
					
					
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="order-summary mrtop78 hidden-xs visible-md visible-sm visible-lg " >
							<h3 style="font-weight: bold; text-align: center;font-weight: bold;text-align: center;text-transform: none;">Your Order Summary</h3>
							
								<div class="cart-subtotal ">
									<!-- <p>Subtotal<span class="pull-right"><i class="fa fa-inr"> <span class="subtotal"><?php echo $response->userOrderInfo->totalBasePrice  ?></span></i></span></p>
									<div class="row"><div class="col-lg-6"><p>Subtotal</p></div><div class="col-lg-6 " ><span style='height:22px;width:22px;' class="pull-right spinner"></span></div></div>
									<p>Discount<span class="pull-right"><i class="fa fa-inr"> <span class="discount">20</span></i></span></p>
									<p>Taxes<span class="pull-right"><i class="fa fa-inr"> <span class="taxes"><?php echo $response->userOrderInfo->totalVat + $response->userOrderInfo->totalServiceTax  ?></span></i></span></p>
									<p class="bold-text">Total<span class="pull-right"><i class="fa fa-inr"> <span class="grandtotal"><?php echo $response->userOrderInfo->totalCustomerPayableCapped ?></span></i></span></p>
 -->							
 									<!-- <p>Subtotal<span class="pull-right"><i class="fa fa-inr"> <span class="subtotal"><?php echo $response->userOrderInfo->totalBasePrice  ?></span></i></span></p> -->
									<div class="row">
										<div class="col-lg-6"><p style="font-weight:600;color:#464646">Subtotal</p></div>
										<div class="col-lg-6 " >
											<span id="subTotalVal" style="" class="pull-right"><i class="fa fa-inr"> <span class="subtotal" style="font-family: monospace;font-weight: 800;"><?php echo round($revaluateObject->userOrderInfo->totalBasePrice,2)  ?></span></i></span>
											<span id="subTotalSpin" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span>
										</div>
									</div>
									<div style="display:none;" class="row DiscountDiv">
										<div class="col-lg-6"><p style="font-weight:600;color:#464646">Discount</p></div>
										<div class="col-lg-6 " >
											<span id="discountVal" class="pull-right"><i class="fa fa-inr"> <span class="discount" style="font-family: monospace;font-weight: 800;"><?php echo $revaluateObject->userOrderInfo->discount  ?></span></i></span>
											<span id="discountSpin" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span>
										</div>
									</div>
									<div style="display:none;" class="row DeliveryCostDiv">
										<div class="col-lg-6"><p style="font-weight:600;color:#464646">Delivery Cost</p></div>
										<div class="col-lg-6 " >
											<span id="deliveryVal" class="pull-right"><i class="fa fa-inr"> <span class="delivery" style="font-family: monospace;font-weight: 800;"><?php echo $revaluateObject->userOrderInfo->delivery_cost  ?></span></i></span>
											<span id="deliverySpin" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span>
										</div>
									</div>
									<div  class="row">
										<div class="col-lg-6"><p style="font-weight:600;color:#464646">Taxes</p></div>
										<div class="col-lg-6 " >
											<span id="taxesVal" style="" class="pull-right"><i class="fa fa-inr"> <span class="taxes" style="font-family: monospace;font-weight: 800;"><?php echo round(($revaluateObject->userOrderInfo->taxes + $revaluateObject->userOrderInfo->totalCapped),2)  ?></span></i></span>
											<span id="taxesSpin" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span>
										</div>
									</div>
									<div  class="row">
										<div class="col-lg-6"><p class="bold-text" style="font-weight:600;color:#464646">Total</p></div>
										<div class="col-lg-6 " >
											<span id="totalVal" style="" class="pull-right"><i class="fa fa-inr"> <span class="grandtotal" style="font-family: monospace;font-weight: 800;" step="0.01"><?php echo round($revaluateObject->userOrderInfo->totalCustomerPayableCapped) ?></span></i></span>
											<span id="totalSpin" style='display:none;height:22px;width:22px;' class="pull-right spinner"></span>
										</div>
									</div>
	
								</div>
								
								<div class="coupon-code ">
									<input type="text" class="form-control promo-click"  maxlength="6" placeholder="Promo Code">
									<span type="button" class="btn  apply-coupon-btn">Apply</span>
								</div>
								<div class=" alert alert-danger orderError ">
									
									<span class="errorText"></span>
								</div>
								<!-- orderError.col-md-5.col-md-offset-3.col-xs-12.col-sm-12.alert.alert-danger.error-div.cart-error
								 -->
								<div class="mobile-number-field">
									<input type="text" class="form-control mobile-no numeric"  maxlength="10" placeholder="Mobile Number">
								</div>
								<div class="couponError">
									<span class="couponErrorMsg">Invalid coupon</span>
								</div>
								<div  class="coupon-applied">
									<p>
									<span class="code"><i class="fa fa-gift"></i><span class="promoCode"></span> </span> has been successfully applied.
									<a href="#"><i class="fa fa-times removeCoupon"></i></a></span>
									</p>
								</div> 
								<div class="checkout-btn">
									<button id="proceedBtn" type="button" class=" btn btn-checkout">Proceed</button>
								</div>
						</div>
						
						<div class="checkout-mobile-view visible-xs hidden-lg hidden-sm hidden-md">
								<div class="coupon-code">
									<input type="text" class="form-control promo-click" placeholder="Promo Code">
									<button type="button" class="btn apply-coupon-btn mob-apply-coupon-btn">Apply</button>
								</div>
								
								<div class="mobile-number-field">
									<input type="text" class="form-control  mobile-no  numeric"  maxlength="10" placeholder="Mobile Number">
								</div>
								<div class="couponError">
									<span class="couponErrorMsg">Invalid coupon</span>
								</div>
								<div class="coupon-applied">
									<p>
									<span class="code"><i class="fa fa-gift"></i><span class="promoCode"></span> </span> has been successfully applied.
									<a href="#"><i class="fa fa-times removeCoupon"></i></a></span>
									</p>
								</div> 
								<div class=" alert alert-danger orderError ">
									
									<span class="errorText">Apply</span>
								</div>
								
								<div class="item-total-container">
									
									<p class="text-right " >Sub Total:  <i class="fa fa-inr"> <span class="subtotal"><?php echo round($revaluateObject->userOrderInfo->totalBasePrice,2)  ?></span></i></p>
									<p class="text-right DeliveryCostDiv" style="display:none;">Delivery Cost:  <i class="fa fa-inr"> <span class="delivery"><?php echo $revaluateObject->userOrderInfo->delivery_cost  ?></span></i></p>
									<p class="text-right  DiscountDiv" style="display:none;">Discount:  <i class="fa fa-inr"> <span class="discount"><?php echo $revaluateObject->userOrderInfo->discount  ?></span> </i></p>
									<p class="text-right ">Taxes & Adjustments:  <i class="fa fa-inr"> <span class="taxes"><?php echo round($revaluateObject->userOrderInfo->taxes + $revaluateObject->userOrderInfo->totalCapped,2) ?></span> </i></p>
									<p class="text-right checkout-total">Total:  <i class="fa fa-inr"> <span class="grandtotal" id="mobiletotal"><?php echo $revaluateObject->userOrderInfo->totalCustomerPayableCapped ?></span></i></p>
								</div>
								
								<div class="checkout-btn">
									<button type="button" class=" btn btn-checkout">Proceed</button>
								</div>
								
									
						</div>
					</div>
				</div>
				
				<!-- <div class="row hidden-xs hidden-md visible-lg visible-md">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="add-ons">
							<h4>Add ons</h4>
							
							<div class="add-ons-carousel">
								<div class="carousel slide media-carousel" id="media">
									<div class="carousel-inner">
										<div class="item active">
											<div class="row">
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>          
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>        
											</div>
										</div>
										
										<div class="item">
											<div class="row">
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>          
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>
											  <div class="col-md-4">
												<div class="food-box">
													<div class="thumbnail">
														<div class="img_box"></div>
														<span class="add-food" data-item="Chicken Briyani" data-price="135" id="foodid1">Add</span>
														<span class="fav-food"><i class="fa fa-star"></i></span>							
														<div class="caption">
														<h2>Chicken Briyani</h2>
														<span class="non-veg"></span>
														<p>Veg thali meal with paneer or seasonal veg gravy, yellow dal, jeera rice, 2 rotis and picklet.</p>
														<div class="food-price"> <i class="fa fa-inr"></i> 135</div>
														
														</div>
													</div>
												</div>
											  </div>        
											</div>
										</div>

									</div>
								<a data-slide="prev" href="#media" class="left carousel-control">‹</a>
								<a data-slide="next" href="#media" class="right carousel-control">›</a>
						</div>  
							</div>
						</div>
					</div>
				</div> -->
			</div>
	</section>
	<!--back to top-->
        <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->
	
	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="/glimpse/src/js/cart.js?v=0.5"></script>
	<script type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></script>

    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

               <!--  window.onload = function () {
                	//alert(window.onload);
                   //  cartInit();
                } -->
     </script>
     <script defer type="text/javascript">
//document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></scr'+'ipt>');

	$( document ).ready(function() {

	// Clevertap
        clevertap.event.push("RS-Cart opened", {
            "minimumAmount": <?php echo $revaluateObject->outletminorderAmount  ?>,
            "station": '<?php echo $revaluateObject->userOrderInfo->station_code  ?>',
            "payableAmount": <?php echo $revaluateObject->userOrderInfo->totalCustomerPayableCapped  ?>,
            "outletId": <?php echo $revaluateObject->userOrderInfo->order_outlet_id  ?>,
            "channel":channel,
            "deliveryCost":<?php echo $revaluateObject->userOrderInfo->delivery_cost  ?>
            
        });
		

	});
     </script>
     <script defer  type='text/javascript' >
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37839621-1']);
    _gaq.push(['_setDomainName', 'travelkhana.com']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);
    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
      a = s.createElement(o),
              m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-37839621-1', 'auto');
    ga('send', 'pageview');
    </script>
   