<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

<meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>About Us</title>

<!-- CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.2">
<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2">

<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="https://desktop.travelkhana.com/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://desktop.travelkhana.com/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://desktop.travelkhana.com/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://desktop.travelkhana.com/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://desktop.travelkhana.com/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://desktop.travelkhana.com/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://desktop.travelkhana.com/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://desktop.travelkhana.com/https://desktop.travelkhana.com/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://desktop.travelkhana.com/https://desktop.travelkhana.com/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://desktop.travelkhana.com/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://desktop.travelkhana.com/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://desktop.travelkhana.com/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://desktop.travelkhana.com/img/favicon-16x16.png">
<!-- <link rel="manifest" href="https://desktop.travelkhana.com/img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://desktop.travelkhana.com/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


	
	
  </head>
  <body>
    
	<!-- Loader -->
    	
	
	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="order.jsp"><span class=""><img src="https://desktop.travelkhana.com/img/order_ico.png" alt="" title=""/> Order Food</a></li>
					<!-- <li><a href="#"><span class=""><img src="https://desktop.travelkhana.com/img/track_ico.png" alt="" title=""/> Track Order</a></li>
					 -->
					<li><a href="https://www.travelkhana.com/travelkhana/jsp/contact.jsp"><span class=""><img src="https://desktop.travelkhana.com/img/contact_ico.png" alt="" title=""/> Contact Us</a></li>
					<li><a href="http://bit.ly/1a5lLVi"><img src="https://desktop.travelkhana.com/img/android-btn.png" alt="" title=""/></a></li>
					<li><a href="https://itunes.apple.com/in/app/travelkhana-train-food-service/id1080942221?mt=8"><img src="https://desktop.travelkhana.com/img/app-btn.png" alt="" title=""/></a></li>
				
				</ul>
			  
			</div><!-- /.navbar-collapse -->
		</div>
	</header>
	
	
	<div class="page-content">
		<section class="inner-page-header page-heading img-v1 text-center">
			<div class="container">
				<h1>About Us</h1>
				<span class="subtext">Travelkhana.com is a Service for transforming the experience of the Railway Passenger</span>
			</div>
		</section>
		
		
<section id="inner-page-content">
		<div class="container">
			
				<div class="about-content">
					<p>About 6 billion passengers travel in Indian Railways each year. If anyone of us has traveled they will vouch for the fact that currently the availability of quality food in Indian Trains is something not to be excited about. Travelkhana solves this very problem for the railway passenger.</p>

					<p>It offers a variety of choices to a Railway passenger so that the passengers can order food as per their liking and at their price point while they are travelling. The Travelkhana automated platform tracks trains in real time across India and ensures that fresh food is delivered to the passenger as per their choice in real time. It also helps vendors and restaurants in terms of effective and efficient fulfillment. Additionally it completes an entire fulfillment cycle right from the point of ordering to Delivery, Customer feedback and Reconciliation. What is most exciting is that Railway passengers are served fresh food thanks to the patented technology behind Travelkhana.</p>

					<p>The TravelKhana platform ensures that the delivery is made in real time. Travelkhana ensures that the entire lifecycle of an order fulfillment is completed by the Travelkhana patented platform. Travelkhana tracks the train in real time and makes sure that fresh food is made available to the passenger through its vast variety of restaurants on the Indian Railway network. Travelkhana currently operates in 80 cities and rapidly expanding across the Railway network.</p>

				</div>
			
		</div>
	
	</section>
	
	
	
	
	

<section id="inner-footer" class="">
		<div class="container">
			<div class="row">
				<div class="divider"></div>
			</div>
			
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">

							<li><a href="https://www.travelkhana.com/travelkhana/jsp/about.jsp">About Us</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/jsp/career.jsp">Career</a></li>
							<li><a href="https://www.travelkhana.com/tkblog/">Blog</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/jsp/sitemap.jsp">Site Map</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">
							<!-- <li><a href="#">Track Order</a></li>
 -->							<!-- <li><a href="#">Food in Popular Trains</a></li> -->
 							<li><a href="order.jsp">Order Food in Train</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/jsp/restaurant.jsp">Become a Partner</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/jsp/groupBooking.jsp">Group Travel</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/jsp/faq.jsp">FAQ</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">
							<li><a href="https://www.travelkhana.com/travelkhana/check-pnr-status">Check PNR Status</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/track-train">Track Train</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-reservation">Indian Railways Reservation</a></li>
							<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-time-table">Indian Railways Time Table</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="subscribe-from">
						<!-- <h2>Subscribe To Our Latest offers and Updates</h2>
						<form>
							<div class="input-group">
							  <input type="text" class="form-control" placeholder="Search for...">
							  <span class="input-group-btn">
								<button class="btn btn-subscribe" type="button"><i class="fa fa-paper-plane"></i></button>
							  </span>
							</div>
						  
						  
						</form> -->
						
						<div class="follow-us-list">
							<ul class="list-inline">
								<li><a href="https://www.facebook.com/Travelkhana"><i class="circle fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/Travelkhana"><i class="circle fa fa-twitter"></i></a></li>
								<li><a href="https://plus.google.com/+Travelkhanaservice"><i class="circle fa fa-google-plus"></i></a></li>
								<li><a href="https://www.instagram.com/travelkhana1/"><i class="circle fa fa-instagram"></i></a></li>
								<li><a href="https://www.linkedin.com/in/travelkhana/?ppe=1"><i class="circle fa fa-linkedin"></i></a></li>
							</ul>
						</div>
						
						
						
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
	<section class="inner-footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="copyright-text">
						<p>Copyright &copy; 2017, TravelKhana, All rights reserved.</p>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="more-links">
						<ul class="list-inline">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms & Conditions</a></li>
							<li><a href="#">Disclaimer</a></li>
						</ul>
					</div>
				</div>
						
			</div>
		</div>
	</section>
		
		
		
	</div>
	
	
	
	
	
	
	<!--back to top-->
        <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    