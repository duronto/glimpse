

<!DOCTYPE html>
<html lang="en">
  <!--  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>FAQ</title>
      <!-- CSS -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="faq-bg">
            <!--<div class="container">
               <div class="row">
                  <div class="col-md-12 heading-wrap">
                     <h2>Disclaimer</h2>
                     <p>The Travelkhana App on Android or iOS and get fresh food delivered to your seat</p>
                  </div>
               </div>
            </div>-->
         </div>
          <!--end of faq-bg-->
         <div class="faq-content">
            <div class="container">
               <div class="row">
			    <div class="col-md-12">
                  <h2>frequently asked question for customers</h2>
				  <div class="panel-group" id="accordion">
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							 What is travelkhana?
							</a>
						  </h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in panel-cont">
						  <div class="panel-body">
							Travelkhana is unique service designed to benefit rail travelers in their choice of quality food while traveling. You can easily order online or over the phone, and select the food of your choice from quality restaurants along the route you are traveling on. The restaurants are selected and verified by Travelkhana personnel on the basis of stringent quality parameters. Our platform tracks train status, coordinates food preparation time with the restaurant to ensure you receive your meal at your seat, fresh and on time, every time! 
						  </div>
						</div>
					  </div>
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							  how do i start ordering from my favorite restaurant?
							</a>
						  </h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							It’s as easy as one, two, three. From www.travelkhana.com select your train, departure and arrival stations and date of travel. The list of available restaurants will come up, with menus. Pick your food, select the payment method, receive the order confirmation SMS, and you are all set!
						  </div>
						</div>
					  </div>
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							 how do i need to pay extra to use this site?
							</a>
						  </h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							No, there are no extra charges for using Travelkhana’s website. 
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
							 How do I make payments for the food I order?
							</a>
						  </h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							You will be offered a choice of your preferred mode of payment while placing an order. We offer very convenient online payment options, via credit card (MasterCard, Visa, Diners), debit card (MasterCard, Visa, Maestro) from all banks, net banking with all major banks in India. You may also choose to pay Cash on Delivery. Your order will be delivered to your seat along with the bill and you can pay the amount directly to the delivery boy. Please make sure to have correct change in case you chose to pay cash on delivery.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
							 Can I make an advance order?
							</a>
						  </h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Yes, absolutely, you can make advance orders. Just select the date and time when you would want the delivery and you will receive it at the time of your choice.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
							 How to get Proforma Invoice for the food I ordered?
							</a>
						  </h4>
						</div>
						<div id="collapseSix" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Proforma Invoice can be generated for "Delivered Order" only via "Track Your Order" at homepage. To download your Proforma Invoice, Please enter your order no. and your registered phone no. with us at "Track Your Order" page and click "Proforma Invoice" link.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
							 Why should I use website instead of phone orders?
							</a>
						  </h4>
						</div>
						<div id="collapseSeven" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Online ordering provides several advantages over telephone based ordering. Online ordering is faster, you can complete the booking 24/7/365 from anywhere you like. You would be able to see the entire menu online instead of hearing someone speak it out to you. You will be able to read reviews on your selected restaurant by fellow travelers and Travelkhana users. If you chose to order by phone, you can reach Travelkhana’s customer care executives from 8:00 AM to 10:00 PM
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
							 Can I order food at any time?
							</a>
						  </h4>
						</div>
						<div id="collapseEight" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Ordering on www.travelkhana.com is available 24/7/365 - isn’t the Internet a great thing? If you chose to order by phone, you can reach Travelkhana’s customer care executives from 8:00 AM to 10:00 PM
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
							 Can I get food at any time?
							</a>
						  </h4>
						</div>
						<div id="collapseNine" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Food delivery depends on the restaurant operational time, usually between 7:00 AM to 11:00 PM.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
							 Is there a guaranteed delivery time?
							</a>
						  </h4>
						</div>
						<div id="collapseTen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							The time of delivery depends on the actual train's running schedules. Travelkhana tracks trains and coordinates with the restaurant to make sure that you receive the food you ordered at your seat when the train stops at the station you have selected. Please note that the restaurant will deliver the food to you. As you can understand, Travelkhana is not responsible for the scheduling or delays on the Indian railway network. We all wish trains run always on time!
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
							 What do I do in case there is some problem with my order?
							</a>
						  </h4>
						</div>
						<div id="collapseEleven" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Please mail us at contact@travelkhana.com or call us at our helpline number (0)8800-31-31-31. Our customer support executive will try their level best to assist you with any problems or doubts.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTweleve">
							 How do I cancel an order?
							</a>
						  </h4>
						</div>
						<div id="collapseTweleve" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							You can cancel an order till one hour before the train's scheduled time of arrival in the station where your food is to be delivered. Call our customer support representatives at (0)8800-31-31-31 and they will be happy to help you. For pre-paid orders there is an administrative fee of 10% for cancellation up to 24 hours before delivery time, and 25% for cancellation up to one hour before delivery time. Sorry, the credit card companies charge us twice: once for getting your order paid for, and second for refunding you in case of cancellation.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen">
							 What if my train is late?
							</a>
						  </h4>
						</div>
						<div id="collapseThirteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							We track your train running time to make sure we can serve you fresh food at your seat. In case your train is delayed by more than two hours, our customer care executive will call to ask if you are still interested in receiving your order. In case your train is scheduled to reach the delivery station beyond restaurant’s operational timings (usually 7:00 AM to 11:00 PM) your order will be automatically cancelled and our customer service executive will contact you.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen">
							I can't find my station...
							</a>
						  </h4>
						</div>
						<div id="collapseFourteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Contact us! We are adding many new stations and delivery restaurants every month, it is possible that they are not yet listed when you search them. You can give us a call at (0)8800-31-31-31, email us at contact@travelkhana.com, or use the feedback form or the chat window on our website.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen">
							Do I need to tip the delivery boy?
							</a>
						  </h4>
						</div>
						<div id="collapseFifteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							No, you do not need to tip the delivery boy. The meal price Travelkhana charges is inclusive of delivery cost. In fact we would suggest that you do not pay any extra amount that your pre-agreed price.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen">
							Where is my personal information used?
							</a>
						  </h4>
						</div>
						<div id="collapseSixteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							We do not share your personal details (phone number and mail ID) with the delivery restaurants. However we may use your number/mail id to send you promotional offers about Travelkhana. We take security seriously and we take precautions to keep your personal information secure. We have put in place appropriate physical, electronic and managerial procedures to safeguard the information we collect. However, due to the open communication nature of the Internet, Travelkhana cannot guarantee that communications between you and Travelkhana or information stored on Travelkhana servers, will be free from unauthorized access by third parties.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">
							What if internet connection is lost at any step in between?
							</a>
						  </h4>
						</div>
						<div id="collapseSeventeen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							If you received confirmation SMS, your order is booked. If you didn’t receive the order confirmation SMS, your transaction did not complete and order was not placed. You would have to place the order again online or call our helpful customer service representatives at (0)8800-31-31-31 to help you complete your order.
						  </div>
						</div>
					  </div>
					  
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen">
							I don't have access to the Internet. Can I still use your services?
							</a>
						  </h4>
						</div>
						<div id="collapseEighteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Of course! Our helpful customer service representatives are ready to take your order from 8:00 AM to 10:00 PM at (0)8800-31-31-31
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNineteen">
							What are your office hours?
							</a>
						  </h4>
						</div>
						<div id="collapseNineteen" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							We are open 24/7/365 for your train food booking needs at any time, day or night, at www.travelkhana.com. Should you rather talk to one of our helpful customer service representatives, they are available from 8:00AM to 10:00PM seven days a week.
						  </div>
						</div>
					  </div>
					  
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwenty">
							I am stuck and can't figure out how to order...?
							</a>
						  </h4>
						</div>
						<div id="collapseTwenty" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							No issues. Give us a call at (0)8800-31-31-31 between 8:00 AM and 10:00 PM and our helpful customer service representatives will assist you to complete the order.
						  </div>
						</div>
					  </div>
					  
					  
					</div>
				</div>
               </div>
            </div>
         </div>
         <!--end of faq-content-->
		  </section>
         <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
     
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </body>
</html>

