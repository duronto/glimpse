<!DOCTYPE html>
<html lang="en">
  <!--  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>Sitemap</title>
      <!-- CSS -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="site-map" class="inner-search-content">
         <div class="container">
            <div class="sitemap-content">
               <div class="row">
                  <div class="col-md-12">
				    <div class="site-area">
						<div class="sitemap-heading">
							<h1>Sitemap</h1>
						</div>
						
						<div class="sitemap-list text-center">
							<ul class="list-inline">
								<li><a href="https://www.travelkhana.com/">Home</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/locations.jsp">Locations</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/popularTrainList.jsp">Popular Trains</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/userQuery.jsp">Request Callback</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/groupBooking.jsp">Group Travel</a></li>
								<!-- <li><a href="#">Track Order</a></li> -->
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/about.jsp">About Us</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/team.jsp">Team</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/media.jsp">Media</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/review.jsp">Reviews</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/check-pnr-status">Check PNR Status</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/track-train">Track Train</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-reservation">Indian Railway Reservation</a></li>
								<li><a href="http://blog.travelkhana.com/">Blog</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/faq.jsp">FAQ's</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/pnr-status-on-mobile">PNR Status On Mobile</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/contact.jsp">Contact Us</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/indian-railways-time-table">Indian Railway Time Table</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/sitemap.jsp">Sitemap</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/faq_customer.jsp">FAQ's For Customers</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/policy.jsp">Privacy Policy</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/faq_foodPlaza.jsp">FAQ's For Food Plazas/ Food Courts/ Restaurants</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/terms.jsp">Terms & Conditions</a></li>
								<li><a href="https://www.travelkhana.com/travelkhana/jsp/disclaimer.jsp">Disclaimer</a></li>
							</ul>
						</div>
					</div>
                  </div>
               </div>
            </div>
         </div>
         <!--end of about-content-->
		 </section>
         <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      
   </body>
</html>

