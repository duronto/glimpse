

<!DOCTYPE html>
<html lang="en">
  <!--  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>Our Team</title>
      <!-- CSS -->
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
     <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="team-content">
            <div class="container">
               <div class="row">
			    <div class="col-md-12">
                 <div class="team-wrap">
				    <div class="team-detail">
					 <div class="row">
					 <span>THE GENIUSES BEHIND OUR WORK</span>
					    <h2>OUR TEAM</h2>
					    <div class="col-md-6">
						 <div class="member-des">
						    <h3>Pushpinder Singh</h3>
							<span>Co founder & CEO</span>
							<p>Pushpinder is well known in the industry for strong visionary skills, architecture and business acumen. Pushpinder was most recently the V.P Technology and acting CTO for INCA Informatics where he played a significant and major role in a turnaround creating the Telecom OSS business vision as well as it's execution resulting in $3 m in revenues and value. He has, in the past, provided Sustenance, Continuity and providing thrust to an existing world leading product (Quark Xpress). Creation & contribution has been a hallmark of his career and he has been instrumental in creating and taking to market multiple products of repute at Quark, ARI Labs and Aplion Networks.</p>
							<p>Realizing that Technology does not play any significant role in making a                  passenger's travel comfortable he ventured on to create Travelkhana which is a platform for travelers to use to buy food - a much needed and underserved          segment. Pushpinder has graduated with a Masters in Science Degree in             Computer Science from BITS- Pilani and B.Tech from IIT-BHU. </p>
						     </div>
						</div>
						 <div class="col-md-6">
						     <div class="member-des des-col2">
						    <h3>Matteo Chiampo</h3>
							<span>Chief Advisor & Strategic Investor</span>
							<p>Matteo has a history of delivering tangible and measurable results in entrepreneurial and high growth organizations, in multiple countries and across diverse cultures. In his last positions as COO he led operations at Eko India Financial Services, an innovative bottom-of-the-pyramid financial services organization, reaching more than 12 lakh customers over 10 Cr transactions. Prior to Eko, Matteo co-founded and was COO at William George Associates a leading project management consulting firm in Boston Massachusets which under Matteo's leadership has grown to become the largest EPM Microsoft partner in the North-East US, with annual revenues in excess of $ 1M. In his early career Matteo has contributed to the phenomenal growth of Parametric Technology Corporation the world leader in 3D CAD applications after starting his working life in the Formula One team BMS Scuderia Italia. Creating value at the bottom of the pyramid as well as creating successful startups is his passion and thus in addition to his role as a seasoned executive, Matteo is also an investor as well as an advisor to various early-stage startups in India. Matteo is an MBA in General Management from School of Management, Boston University & MS in Politecnico di Milano, Italy. </p>
							
						     </div>
						 </div>
					 
					 </div>
					</div>
				 
				 </div>
				  
				</div>
               </div>
            </div>
         </div>
         <!--end of team-content-->
		  </section>
         <!--footer Start Here-->
        <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
     
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </body>
</html>

