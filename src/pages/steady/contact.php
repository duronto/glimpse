<!DOCTYPE html>
<html lang="en">
   <head>
    <!--   <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>Contact Us</title>

      <!-- CSS -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
     <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="contact_us" class="inner-search-content">
         <div class="contact-bg">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 static-heading">
                     <h2>Contact Us</h2>
                     <p>How can we help you ?</p>
                  </div>
               </div>
            </div>
         </div>
         <!--end of contact-bg-->
         <div class="contact-content">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6">
				    <div class="contact-wrap pdRight-20">
					 <ul>
					 <li>To order your food on train <br/> call us at <a href="#" class="ph-number">08800313131</a> from 7:00 AM to 10:30 PM</li>
					 <li>For Media or any other inquiry,you can reach us at:
					 <span class="cont-num mrTop-5"> <img src="https://desktop.travelkhana.com/img/phone-ico.png" alt="" title=""/>
                     0120-4120335 <small>(9AM to 6PM,Mon to Sat)</small></span>
					 <span class="cont-num mrTop-5"> <img src="https://desktop.travelkhana.com/img/email-ico.png" alt="" title=""/> contact@travelkhana.com</span>
					 </li>
					 </ul>
					</div>
                  </div>
				  <div class="col-xs-12 col-sm-6 col-md-6">
				    <div class="contact-wrap contact-box2">
					 <ul>
					 <li><img src="https://desktop.travelkhana.com/img/address-ico-1.png" alt="" title=""/>
					 <span class="cont-num">Office Address:<br/> F-31,Ground Floor,Sector-6,<br/>Noida-201301,India.
                     </span></li>
					 <li><img src="https://desktop.travelkhana.com/img/address-ico-1.png" class="reg-address" alt="" title=""/>
					 <span class="cont-num">Registered Address:<br/>H-502, Mayur Dhwaj Apartment,Plot <br/> No.60, I.P. Extension, New Delhi- 110092
					</div></span>
					 </li>
					 </ul>
                  </div>
               </div>
            </div>
         </div>
         <!--end of contact-content-->
		  </section>
         <!--footer Start Here-->
        <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      </section>
   </body>
</html>

