<!DOCTYPE html>
<html lang="en">
   <!-- <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>Disclaimer</title>
      <!-- CSS -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="disclaimer-bg">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 static-heading">
                     <h2>Disclaimer</h2>
                     <p>The information contained in this website is for general information purposes only.</p>
                  </div>
               </div>
            </div>
         </div>
         <!--end of disclaimer-bg-->
         <div class="disclaimer-content">
            <div class="container">
               <div class="row">
			    <div class="col-md-12">
                  <div class="content-wrap">
                     <p>The information is provided by Duronto technologies Pvt ltd and it is presented on best-effort basis. While we continually try to improve its reliability and accuracy we want to disclaim the following with regards to the content presented on this website.</p>
                     <ul class="disclaimer-list">
                   <!-- myedit-->
                     <li>Rates are inclusive of all taxes.</li>
                     <li>All food order requests are subject to availability. Delivery time 8 a.m. to 10 p.m.</li>
                  <!-- myedit end-->
					    <li>Duronto technologies Pvt ltd endeavors to keep all information contained in the www.travelkhana.com web site up-to-date. However, prices, listings,          articles and other information are subject to change and are for information purposes only.</li>
                     <!-- myedit-->
                   <li>Passengers are advised to create their food order request one hour prior to reaching their desirable destination.</li>
                   <li>No order requests will be processed if the information provided is insufficient.</li>
                   <li>Creating online food order is just a request for supply of food and our company does not guarantee the delivery of the food. In case of extreme circumstances such as any exigency e.g. late running, train diversion or no availability of the passenger(s) on the berth/seat at the time of delivery, late request , incomplete information provided by the passengers/customers etc.</li>
                  <!-- myedit end-->
					    <li>We want to be as accurate as possible; however certain conditions beyond our control could delay orders. If this occurs, we will make reasonable                resolutions with a productive approach to solving the problem.</li>
					    <li>Please treat maps as indicative; these maps will definitely get you close to the location but please don't hesitate referring other sources including Indian Railways for such information.</li>
					   <li>The photographs of food items are only indicative and in many cases not actual photographs of the item to be delivered.</li>
					    <li>Duronto technologies Pvt. Ltd. makes no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability,            suitability or availability with respect to the website or the information, photographs, products, services, contained on the website for any purpose.</li>
					    <li>Through this website you may be able to link to other websites which are not under the control of Travelkhana. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them. </li>

                  <!-- myedit-->
                   <li>The customer/requesting party hereby waives all its right to claim any sort of damages from our company due to non delivery of the food. (Including but not limited to consequential punitive, special, incidental or any other damages.)</li>
                  <!-- myedit-->
					    </ul>
                     <p>Any reliance you place on such information is therefore strictly at your own risk.</p>
                     <p>Every effort is made to keep the website up and running smoothly. However, Duronto technologies Pvt Ltd. takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control or for regular maintenance or commissioning of upgrades. </p>
					</div> 
				</div>
               </div>
            </div>
         </div>
         <!--end of disclaimer-content-->
		 </section>
         <!--footer Start Here-->
        <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      </section>
   </body>
</html>

