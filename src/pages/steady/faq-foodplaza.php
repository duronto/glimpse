

<!DOCTYPE html>
<html lang="en">
  <!--  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>FAQ</title>
      <!-- CSS -->
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
     <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
      <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header>
      <section id="" class="inner-search-content">
         <div class="faq-bg">
            <!--<div class="container">
               <div class="row">
                  <div class="col-md-12 heading-wrap">
                     <h2>Disclaimer</h2>
                     <p>The Travelkhana App on Android or iOS and get fresh food delivered to your seat</p>
                  </div>
               </div>
            </div>-->
         </div>
         	<!--end of faq-bg-->
         <div class="faq-content">
            <div class="container">
               <div class="row">
			    <div class="col-md-12">
                  <h2>Frequently Asked Questions for Food Plazas / Food Courts / Restaurants</h2>
				  <div class="panel-group" id="accordion">
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							 What do I need to do to get started?
							</a>
						  </h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in panel-cont">
						  <div class="panel-body">
							You can get your Food Plazas/Food Courts registered by getting in touch with us at contact@travelkhana.com or by giving us a call at 8800-138-811. Our        representative will let you know the procedure in detail.
						  </div>
						</div>
					  </div>
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							  How will having a Web site help my Food Plazas/Food Courts?
							</a>
						  </h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Travelkhana is unique service designed to benefit rail travelers in their choice of quality food while traveling. You can easily order online or over the phone, and select the food of your choice from quality restaurants along the route you are traveling on. The restaurants are selected and verified by Travelkhana personnel on the basis of stringent quality parameters. Our platform tracks train status, coordinates food preparation time with the restaurant to ensure you receive your meal at your seat, fresh and on time, every time! 
						  </div>
						</div>
					  </div>
					  <div class="panel panel-default panel-main">
						<div class="panel-heading acc-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							 How long does it take to get Travelkhana set up?
							</a>
						  </h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse panel-cont">
						  <div class="panel-body">
							Travelkhana is unique service designed to benefit rail travelers in their choice of quality food while traveling. You can easily order online or over the phone, and select the food of your choice from quality restaurants along the route you are traveling on. The restaurants are selected and verified by Travelkhana personnel on the basis of stringent quality parameters. Our platform tracks train status, coordinates food preparation time with the restaurant to ensure you receive your meal at your seat, fresh and on time, every time! 
						  </div>
						</div>
					  </div>
					</div>
				</div>
               </div>
            </div>
         </div>
         <!--end of faq-content-->
		  </section>
         <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
     
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </body>
</html>

