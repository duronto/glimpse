<!DOCTYPE html>
<html lang="en">
  <!--  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <?php include '../common/header.html' ?>
      <title>Terms and Conditions </title>
      <!-- CSS -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
      <link rel="stylesheet" href="/glimpse/src/css/style.css">
      <link rel="stylesheet" href="/glimpse/src/css/trainPage.css">
      <link rel="stylesheet" href="/glimpse/src/css/media-queries.css"> -->
   </head>
   <body>
       <header class="inner-header navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
            </div>
         </div>
      </header> 

      <section id="" class="inner-search-content">
         <div class="condition-bg">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 static-heading">
                     <!--<h2>Privacy Policy</h2>-->
                    
                  </div>
               </div>
            </div>
         </div>
         <!--end of condition-bg-->
         <div class="policy-content">
            <div class="container">
               <div class="row">
			    <div class="col-md-12">
				  <div class="avail-wrap">
				    <h3>TRAVELKHANA.COM ("TRAVELKHANA") WEBSITE TERMS AND CONDITIONS ("WEBSITE TERMS") </h3>
				       <span  class="head-icon"></span>
				  </div>
				    <strong>IMPORTANT LEGAL NOTICE</strong>
				  <p>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use ("Website Terms"), all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law. </p>
                  <div class="content-wrap">
				  <div class="avail-wrap">
				    <h3>INTRODUCTION AND OUR ROLE</h3>
				       <span  class="head-icon"></span></div>
                      <ol class="policy-list no-listStyle">
					    <li> 
						 <ol class="sublist">
						    <li>1.1 &nbsp Duronto Technologies Private Limited ("Travelkhana") is a company registered in India under company number U72900DL2009PTC192210 and with our registered office at H.NO.48, Mausam Vihar, New Delhi - 110051 Delhi, India.</li>
							<li>1.2 &nbsp Travelkhana provides a way for you to communicate your orders to Delivery Restaurants ("Delivery Restaurants") displayed on this Website. </li>
							<li>1.3 &nbsp If you have any problems with your order you can contact our customer care team by clicking on the "Feedback" tab and filling in the required fields or by calling our customer care phone number shown on this Website. One of our customer care reps will contact you and try to solve the issue you are facing with your order. </li>
							<li>1.4 &nbsp An important part of our quality control process is that consumers provide ratings and feedback on the website to reflect their experiences with restaurants. Please note that any complaint must be lodged with our customer care team using the method described above within 48 hours of placing the order. </li>
							<li>1.5 &nbsp Travelkhana may call, send sms and/or use other possible modes but not limited to Electronic or Printed medium for collecting its customer's feedback. </li>
							<li>1.6 &nbsp You may access some areas of this Website without making a Travelkhana order, and registering your details with us. Most areas of this Website are open to everyone. </li>
							<li>1.7 &nbsp By accessing any part of this Website, you indicate that you accept these Website Terms. If you do not accept these Website Terms, you must leave this Website immediately.. </li>
							<li>1.8 &nbsp Travelkhana may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </li>
							<li>1.9 &nbsp You are responsible for making all arrangements necessary for you to have access to the Website. You are also responsible for ensuring that all persons who access the Website through your Internet connection are aware of these Website Terms and that they comply with them.</li>
						 </ol>
						</li>
						<li> <div class="avail-wrap">
				    <h3>HOW TO MAKE AN ORDER AND HOW IT IS PROCESSED</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>2.1 &nbsp Any contract for the supply of Food Delivery from this Website is between you and the Delivery Restaurant.</li>
							<li>2.2 &nbsp Once you have selected your order from the menu of your chosen Delivery Restaurants you will be given the opportunity to submit your order by clicking on the "Finish" button. Please note it is important that you check the information that you enter and correct any errors before clicking on the "Finish" button since once you click on this input errors cannot be corrected. </li>
							<li>2.3&nbsp If at any time prior to you clicking on the "Finish" button, you decide that you do not wish to proceed with your order, you should close the application window. </li>
							<li>2.4 &nbsp Upon clicking on the "Finish" button, in case you have selected Cash on Delivery (COD) payment Travelkhana will begin processing your order and we will send you notification by SMS that your order is being processed. For online payment orders, on receipt of your payment, Travelkhana will begin processing your order and we will send you notification by SMS and email that payment has been received and that your order is being processed. SMS and email confirmation will be produced automatically so that you have confirmation of your order details. You must inform us immediately if any details are incorrect. The fact that you receive an automatic confirmation does not necessarily mean that either we or the Delivery Restaurant will be able to fill your order.</li>
							<li>2.5 &nbsp In case of online payment, if any payment you make is not authorized you will be returned to the previous page on the Website and we shall not be obliged to provide the services. </li>
							<li>2.6 &nbsp Please note that once you have made your order and your payment has been authorized you will be able to cancel your order according to details in paragraph 5. </li>
							<li>2.7 &nbsp Please note that from time to time there may be delays with processing payments and transactions, on occasion this can take up to sixty (60) days to be deducted from your bank account. . </li>
						 </ol>
						</li>
						<li> <div class="avail-wrap">
				    <h3>PRICE AND PAYMENT</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>3.1 &nbsp Prices will be as quoted on this Website. These prices are inclusive of relevant sales tax and delivery charges. In case Delivery Restaurants requires a service charge (for instance for orders below the minimum order amount), this will be clearly indicated on this Website, in the itemized bill, and added to the total amount due.</li>
							<li>3.2&nbsp This Website contains a large number of menus items. Although we take great care to keep them up to date, it is always possible that some of the menus may be incorrectly priced. If the correct price for an order is higher than the price stated on the Website Travelkhana will normally contact you before the order in question is dispatched. Travelkhana is under no obligation to ensure that the order is provided to you at the incorrect lower price or to compensate you in respect of incorrect pricing. </li>
							<li>3.3 &nbsp Payment for all orders must be by credit or debit card as stated on this Website or in cash at the point of delivery to you. </li>
							<li>3.4 &nbsp If you choose online payment, you must pay for your order before it is delivered. To ensure that shopping online is secure, your debit/credit card details will be encrypted to prevent the possibility of someone being able to read them as they are sent over the internet. Your credit card company may also conduct security checks to confirm it is you placing the order.</li>
							<li>3.5 &nbsp A discount may apply to your order if you use a promotional code recognized by this Website and endorsed by Travelkhana. </li>
							<li>3.6 &nbsp Please note that from time to time there may be delays with processing payments and transactions, on occasion this can take up to sixty (60) days to be deducted from your bank account or credit card. </li>
							
						 </ol>
						</li>
						<li> <div class="avail-wrap">
				    <h3>DELIVERY</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>4.1 &nbsp  If delivery is done by the Delivery Restaurant or its delivery partners, it is the Delivery Restaurants sole responsibility to provide Food Delivery in a timely manner. </li>
							<li>4.2&nbsp We and the Delivery Restaurant will make every effort to deliver within the time stated at the requested train station. If Food Delivery is not provided at the estimated delivery time and train station requested by you, please contact us by telephone or email and we will try to ensure that you receive your order at an alternate train station along your train route. </li>
							<li>4.3 &nbsp All risk in the Food Delivery shall pass to you upon delivery. </li>
							<li>4.4 &nbsp If you fail to accept delivery of Food Delivery at the time they are ready for delivery, or we are unable to deliver at the nominated time due to your failure to provide appropriate train seat/coach information, or we are unable to deliver because of your failure to be present at the seat number you communicated to us, then such goods shall be deemed to have been delivered to you and all risk and responsibility in relation to such goods shall pass to you. Any costs which we incur as a result of the inability to deliver shall be your responsibility and you shall indemnify us in full for such cost. </li>
							<li>4.5 &nbsp Delivery Restaurants, who will prepare your order, aim: </li>
							<ol>
							<li>4.5.1 to deliver the product to you at the train seat/coach requested by you in your order;</li>
							<li>4  4.5.2 to deliver within the stoppage time a the station requested by you.</li>
							</ol>
							<li>4.6 &nbsp Delivery Restaurants and we shall not be liable to you for any losses, liabilities, costs, damages, charges or expenses arising out of late delivery.</li>
							
						 </ol>
						</li>
						<li><div class="avail-wrap">
				    <h3>CANCELLATION</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>5.1 &nbsp You can cancel your order up to the order cutoff time. You can cancel your order either calling our call center number, or through our website. You will need to quote mobile phone number and order ID to cancel the order. You may be required to login on the website in order to cancel your order, where applicable. We will not be able to cancel any order after order cutoff time, which is usually one hour before STA of train to delivery station. </li>
							<li>5.2 &nbsp If the cancellation is made up to 24 hours before cutoff time, and you have paid in advance online, we will refund or re-credit your debit or credit card with the full amount less a 10% cancellation administrative fee within 14 days. </li>
							<li>5.3 &nbsp If the cancellation is made between 24 hours and order cutoff time, and you have paid in advance online, we will refund or re-credit your debit or credit card with the full amount less a 25% cancellation administrative fee within 14 days. </li>
							<li>5.4 &nbsp In the unlikely event that the Delivery Restaurant delivers a wrong item, you have the right to reject the delivery of the wrong item and you shall be fully refunded for the missing item. If the Delivery Restaurant can only do a partial delivery (a few items might be not available), its staff should inform you or propose a replacement for missing items. You have the right to refuse a partial order before delivery and get a refund. We will put our best effort in ensuring that the delivery is complete and as per your order, however this may not be possible in all cases. </li>
							<li>5.5 &nbsp Travelkhana reserve the rights to cancel any order without assigning any reason in various cases but not limited to the following reasons where train are delayed, customer is not reachable, any orders placed using promotional coupon, misuse of promotional coupon, unauthorized usage of coupon or inaccuracies of coupon pricing, technical glitches and value thresholds, changes in food menu, menu and pricing issue, serving restaurant is closed etc. However, Travelkhana's first priority is customer and Travelkhana will do maximum of three attempts to update customer on phone and Travelkhana will strive to deliver food.  </li>
						</ol>
						</li>
						<li><div class="avail-wrap">
				    <h3>USE LICENCE</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>6.1 &nbsp Permission is granted to temporarily download one copy of the materials (information or software) on Travelkhana's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                             <ol>
							 <li>6.1.1 modify or copy the materials;</li>
							  <li>6.1.2 use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
							   <li> 6.1.3 attempt to decompile or reverse engineer any software contained on Travelkhana's web site;</li>
							    <li>6.1.4 remove any copyright or other proprietary notations from the materials; or</li>
								 <li> 6.1.5 transfer the materials to another person or "mirror" the materials on any other server.</li>
							 </ol>
                             <li>6.2 &nbsp This license shall automatically terminate if you violate any of these restrictions and may be terminated by Travelkhana at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format. </li>
							 <li>6.3 &nbsp Any rights not expressly granted in these Website Terms are reserved. </li>
							 

							</li>
						 </ol>
						</li>
						<li><div class="avail-wrap">
				    <h3>SERVICE ACCESS</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>7.1 &nbsp While Travelkhana tries to ensure this Website is normally available twenty four (24) hours a day, Travelkhana will not be liable if this Website is unavailable at any time or for any period. </li>
							<li>7.2 &nbsp Access to this Website may be suspended temporarily and without notice. </li>
							<li>7.3 &nbsp Unfortunately, the transmission of information via the internet is not completely secure. Although we will take steps to protect your information, we cannot guarantee the security of your data transmitted to the Website; any transmission is at your own risk. </li>

						 </ol>
						</li>
						<li><div class="avail-wrap">
				    <h3>VISITOR MATERIAL AND CONDUCT</h3>
				       <span  class="head-icon"></span></div>
						 <ol class="sublist">
						    <li>8.1 &nbsp Other than personally identifiable information, which is covered under the Travelkhana Privacy Policy, any material you transmit or post to this Website will be considered non-confidential and non-proprietary. Travelkhana will have no obligations with respect to such material. Travelkhana and anyone we designate will be free to copy, disclose, distribute, incorporate and otherwise use that material and all data, images, sounds, text and other things embodied in it for any and all commercial or non-commercial purposes. </li>
							<li>8.2 &nbsp You are prohibited from posting, uploading or transmitting to or from this Website any material that:
                               <ol>
							    <li>8.2.1 breaches any applicable local, national or international law; </li>
							   <li>   8.2.2is unlawful or fraudulent; </li>
							   <li>8.2.3amounts to unauthorized advertising; or </li>
							   <li> 8.2.4contains viruses or any other harmful programs. </li>
							   </ol>
                            </li>
							<li>8.3 &nbsp You may not misuse the Website (including by hacking).. </li>
							<li>8.4 &nbsp Any comments or feedback that you submit through the Website must not:
                            <ol>
							<li> 8.4.1 contain any defamatory, obscene or offensive material; </li>
							<li> 8.4.2 promote violence or discrimination;</li>
							<li>8.4.3 infringe the intellectual property rights of another person;</li>
							<li>8.4.4 breach any legal duty owed to a third party (such as a duty of confidence);</li>
							<li>  8.4.5 promote illegal activity or invade another's privacy;</li>
							<li>   8.4.6 give the impression that they originate from us; or</li>
							<li>   8.4.7 be used to impersonate another person or to misrepresent your affiliation with another person.</li>
							</ol>
                             </li>
							<li>8.5 &nbsp The prohibited acts listed in paragraphs 8.2 to 8.4 above are non-exhaustive. You will pay Travelkhana for all costs and damages which it incurs as a result of you breaching any of these restrictions. </li>
							<li>8.6 &nbsp Travelkhana will fully co-operate with any law enforcement authorities or court order requesting or directing us to disclose the identity or location of anyone posting any material in breach of paragraph 8.2 to 8.4.  </li>
						 </ol>
						</li>
						<li><div class="avail-wrap">
							<h3>LINKS TO AND FROM OTHER WEBSITES</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>9.1 &nbsp Links to third party websites on this Website are provided solely for your convenience. If you use these links, you leave this Website. Travelkhana has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Travelkhana of the site. Use of any such linked web site is at the user's own risk.</li>
								<li>9.2 &nbsp You may link to this Website homepage, provided that you do so in a fair and legal way which does not damage Travelkhana's reputation or take advantage of it.</li>
								<li>9.3 &nbsp You must not establish a link from a website that is not owned by you or in a way that suggests a form of association with or endorsement by Travelkhana where none exists. </li>
								<li>9.4 &nbsp The website from which you link must comply with the content standards set out in these Website Terms.</li>
								<li>9.5 &nbsp Travelkhana has the right to withdraw the linking permission at any time. .</li>
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>DISCLAIMER</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>10.1 &nbsp While Travelkhana tries to ensure that information on this Website is correct, we do not promise it is accurate or complete. Travelkhana may make changes to the material on this Website, or to the services and prices described in it, at any time without notice. The material on this Website may be out of date, and Travelkhana makes no commitment to update that material. In particular, we do not promise that the information provided by the Delivery Restaurants and displayed on this Website such as the menus and pricing is correct or up to date.</li>
								<li>10.2 &nbsp Travelkhana provides you with access to this Website and our services on the basis that, to the maximum extent permitted by law, we exclude all representations, warranties, conditions and other terms (including any conditions implied by law which but for these Website Terms might apply in relation to this Website and the services that we provide).</li>
								<li>10.3 &nbsp The products sold by us are provided for private domestic and consumer use only. Accordingly, we do not accept liability for any indirect loss, consequential loss, loss of data, loss of income or profit, loss of damage to property and/or loss from claims of third parties arising out of the use of the Website or for any products or services purchased from us. </li>
								<li>10.4 &nbsp We have taken all reasonable steps to prevent Internet fraud and ensure any data collected from you is stored as securely and safely as possible. However, we cannot be held liable in the extremely unlikely event of a breach in our secure computer servers or those of third parties.</li>
								<li>10.5 &nbsp You are responsible for the security of your password that you used to register with this Website. Unless Travelkhana negligently discloses your password to a third party, Travelkhana will not be liable for any unauthorized transaction entered into using your name and password.</li>
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>TERMINATION</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>11.1 &nbspravelkhana may terminate or suspend (at our absolute discretion) your right to use this Website and your use of the services immediately by notifying you in writing (including by email) if:
									<ol>
									<li> 11.1.1 Travelkhana believes you have posted material in breach of paragraphs 8.2, 8.3 or 8.4 (Visitor Material and Conduct);</li>
									<li> 11.1.2 Travelkhana believes that you have breached paragraphs 9.2, 9.3 or 9.4 (Links to and from other websites); or</li>
									<li> 11.1.3 If you breach any other material terms of these Website Terms.</li>
									</ol>
								</li>
								<li>11.2 &nbsp Upon termination or suspension you must immediately destroy any downloaded or printed extracts from this Website.</li>
								
							</ol>
						</li>
						
						
						<li><div class="avail-wrap">
							<h3>LIABILITY</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>12.1 &nbsp Travelkhana, and any of our group companies and the officers, directors, employees, shareholders or agents of any of them, exclude all liability and responsibility for any amount or kind of loss or damage that may result to you or a third party (including any direct, indirect, punitive or consequential loss or damages, or any loss of income, profits, goodwill, data, contracts, or loss or damages arising from or connected in any way to business interruption, loss of opportunity, loss of anticipated savings, wasted management or office time and whether in tort (including negligence), contract or otherwise, even if foreseeable) in connection with our services, this Website or in connection with the use, inability to use or the results of use of this Website, any websites linked to this Website or the material on these websites.</li>
								<li>12.2 &nbsp Travelkhana takes full responsibility for the content of this Website and for the communication of orders to the Delivery Restaurants as set out in these Website Terms. Travelkhana's customer care team will, subject to your compliance with these Website Terms and cooperation, use all reasonable endeavors to resolve any issues arising from the submission of orders via this Website including the processing of all credit or debit card refunds and chargebacks where appropriate. However, please note that the legal contract for the supply and purchase of food and beverages is between you and the Delivery Restaurants that you place your order with. Travelkhana cannot give any undertaking that the food and beverages ordered from the Delivery Restaurants through this Website will be of satisfactory quality and any such warranties are disclaimed by Travelkhana. These disclaimers do not affect your statutory rights against the Delivery Restaurants.</li>
								<li>12.3 &nbsp Nothing in these Website Terms excludes or limits our liability for death or personal injury arising from Travelkhana's negligence, nor Travelkhana's liability for fraudulent misrepresentation, nor any other liability which cannot be excluded or limited under applicable law. Nothing in these Website Terms affects your statutory rights.</li>
								<li>12.4 &nbsp With the exception of any liability referred to in paragraph 12.3 above, Travelkhana's total liability to you in relation to your use of the Website and the services that we provide including (but not limited) to liability for breach of these Website Terms and tort (including but not limited to negligence) is limited to an amount equivalent to trice the value of your order or three thousand Indian rupees, whichever is the lower.</li>
								<li>12.5 &nbsp If your use of material on this Website results in the need for servicing, repair or correction of equipment, software or data, you assume all associated costs.</li>
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>GOVERNING LAW AND JURISDICTION</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>13.1 &nbsp These Website Terms shall be governed by and construed in accordance with Indian law. Disputes arising in connection with these Website Terms (including non-contractual disputes) shall be subject to the exclusive jurisdiction of the courts of New Delhi (India). All dealings, correspondence and contacts between us shall be made or conducted in the English language.</li>
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>ADDITIONAL TERMS</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>14.1 &nbsp We are committed to protecting your privacy and security. All personal data that we collect from you will be processed in accordance with our Privacy Policy. You should review our Privacy Policy, which is incorporated into these Website terms by this reference and is available.</li>
								<li>14.2 &nbsp If any provision or part of a provision of these Website Terms is found by any court or authority of competent jurisdiction to be unlawful, otherwise invalid or unenforceable, such provisions or part provisions will be struck out of these Website Terms and the remainder of these Website Terms will apply as if the offending provision or part provision had never been agreed.</li>
								<li>14.3 &nbsp Any failure or delay by you or us in enforcing (in whole or in part) any provision of these Website Terms will not be interpreted as a waiver of your or our rights or remedies.</li>
								<li>14.4 &nbsp You may not transfer any of your rights or obligations under these Website Terms without our prior written consent. We may transfer any of our rights or obligations under these Website Terms without your prior written consent to any business that we enter into a joint venture with, purchase or are sold to.</li>
								<li>14.5 &nbsp The headings in these Website Terms are included for convenience only and shall not affect their interpretation.</li>
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>WRITTEN COMMUNICATIONS</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>15.1 &nbsp Applicable laws require that some of the information or communications we send to you should be in writing. When using the Website, you accept that communication with us will be mainly electronic. We will contact you by email or provide you with information by posting notices on our website. For contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights. </li>
								
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>EVENTS OUTSIDE OUR CONTROL</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>16.1 &nbsp We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under a contract that is caused by events outside our reasonable control (Force Majeure Event).</li>
								<li>16.2 &nbspA Force Majeure Event includes any act, event, non-happening, omission or accident beyond our reasonable control and includes in particular (without limitation) the following:
									<ol>
									<li> 16.1.1 strikes, lock-outs or other industrial action;</li>
									<li> 16.1.2 civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war;</li>
									<li> 16.1.3 fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster;</li>
									<li> 16.1.4 impossibility of the use of railways, shipping, aircraft, motor transport or other means of public or private transport;</li>
									<li> 16.1.5 impossibility of the use of public or private telecommunications networks; and</li>
									<li> 16.1.6 the acts, decrees, legislation, regulations or restrictions of any government.</li>
									</ol>
								</li>
								
								<li>16.3 &nbsp Our performance under any contract is deemed to be suspended for the period that the Force Majeure Event continues, and we will have an extension of time for performance for the duration of that period. We will use our reasonable endeavors to bring the Force Majeure Event to a close or to find a solution by which our obligations under the contract may be performed despite the Force Majeure Event. </li>
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>SEVERABILITY</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>17.1 &nbsp If any of these terms and conditions are determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such term, condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to the fullest extent permitted by law.</li>
								
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>ENTIRE AGREEMENT</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>18.1 &nbsp These terms and conditions and any document expressly referred to in them constitute the whole agreement between us and supersede all previous discussions, correspondence, negotiations, previous arrangement, understanding or agreement between us relating to the subject matter of any contract. </li>
								
								
							</ol>
						</li>
						
						<li><div class="avail-wrap">
							<h3>LIABILITY</h3>
							<span class="head-icon"></span></div>
							<ol class="sublist">
								<li>19.1 &nbsp Travelkhana, and any of our group companies and the officers, directors, employees, shareholders or agents of any of them, exclude all liability and responsibility for any amount or kind of loss or damage that may result to you or a third party (including any direct, indirect, punitive or consequential loss or damages, or any loss of income, profits, goodwill, data, contracts, or loss or damages arising from or connected in any way to business interruption, loss of opportunity, loss of anticipated savings, wasted management or office time and whether in tort (including negligence), contract or otherwise, even if foreseeable) in connection with our services, this Mobile Android Application or in connection with the use, inability to use or the results of use of this Mobile Android Application, any Android Application, mobile site, website or affiliate linked to Travelkhana.com's Mobile Android Application or the material on these places.</li>
								<li>19.2 &nbsp Travelkhana.com is not liable to you for any damage or alteration to your equipment including but not limited to computer equipment, handheld device or mobile telephones as a result of the installation or use of the Application.</li>
								<li>19.3 &nbsp Travelkhana takes full responsibility for the content of Android Application and for the communication of orders to the Delivery Restaurants as set out in these Android Application Terms. Travelkhana's customer care team will, subject to your compliance with these Android Application Terms and conditions, use all reasonable endeavors to resolve any issues arising from the submission of orders via Android Application including the processing of all credit or debit card refunds and chargebacks where appropriate. However, please note that the legal contract for the supply and purchase of food and beverages is between you and the Delivery Restaurants that you place your order with. Travelkhana cannot give any undertaking that the food and beverages ordered from the Delivery Restaurants through Mobile Android Application will be of satisfactory quality and any such warranties are disclaimed by Travelkhana. These disclaimers do not affect your statutory rights against the Delivery Restaurants.</li>
								<li>19.4 &nbsp Nothing in these Mobile Android Application Terms excludes or limits our liability for death or personal injury arising from Travelkhana's negligence, nor Travelkhana's liability for fraudulent misrepresentation, nor any other liability which cannot be excluded or limited under applicable law. Nothing in these Mobile Android Application Terms affects your statutory rights.</li>
								<li>19.5 &nbsp  If your use of material on this Mobile Android Application results in the need for servicing, repair or correction of equipment, software or data, you assume all associated costs.</li>
								
								
							</ol>
						</li>
						
					    
					    </ol>
                     </div> 
				</div>
               </div>
            </div>
         </div>
         <!--end of policy-content-->
		 </section>
         <!--footer Start Here-->
         <?php include '../common/footer.html' ?>
         <!-- footer Ends Here -->
      </section>
   </body>
</html>

