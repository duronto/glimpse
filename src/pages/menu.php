<?php
session_start();
date_default_timezone_set('Asia/Calcutta');
$timezone = date_default_timezone_get();

include 'common/main.php';
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");

$jDate= "";
$train = "";
$from = "";
$pnr="";
$url="";
$trainNameNumber="";
$errorDetail="";
$flg=0;
$stationSearched='';

//minutes or more.
$expireAfter = 30;
$cookieClear=0;
$getStationFlag=0;
if(isset($_COOKIE['revaluateCookieObject']))
	{
		setcookie('revaluateCookieObject', null, -1, '/');
	}
if(isset($_POST['stsess']) || !empty($_POST['stsess']))
{
	$cookieClear=1;
	if(isset($_COOKIE['menuObject']))
	{
	setcookie('menuObject', null, -1, '/');
	}
	if(isset($_COOKIE['minOrderAmount']))
	{
		setcookie('minOrderAmount', null, -1, '/');
	}
	if(isset($_COOKIE['trackOrderResponse']))
	{
		setcookie('trackOrderResponse', null, -1, '/');
	}
	if(isset($_COOKIE['orderPost']))
	{
		setcookie('orderPost', null, -1, '/');
	}
	if(isset($_COOKIE['pnr']))
	{
		setcookie('pnr', null, -1, '/');
	}

}
 
 //deleting previous or old  cookie that are not created now
    if(isset($_COOKIE['menuObject']))
	{
	setcookie('menuObject', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['minOrderAmount']))
	{
		setcookie('minOrderAmount', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['trackOrderResponse']))
	{
		setcookie('trackOrderResponse', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['orderPost']))
	{
		setcookie('orderPost', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['pnr']))
	{
		setcookie('pnr', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['revaluateCookieObject']))
	{
		setcookie('revaluateCookieObject', null, -1, '/travelkhana/jsp/');
	}

	if(isset($_COOKIE['menuObject']))
	{
	setcookie('menuObject', null, -1, '/travelkhana/jsp');
	}
	if(isset($_COOKIE['minOrderAmount']))
	{
		setcookie('minOrderAmount', null, -1, '/travelkhana/jsp');
	}
	if(isset($_COOKIE['trackOrderResponse']))
	{
		setcookie('trackOrderResponse', null, -1, '/travelkhana/jsp');
	}
	if(isset($_COOKIE['orderPost']))
	{
		setcookie('orderPost', null, -1, '/travelkhana/jsp');
	}
	if(isset($_COOKIE['pnr']))
	{
		setcookie('pnr', null, -1, '/travelkhana/jsp/');
	}
	if(isset($_COOKIE['revaluateCookieObject']))
	{
		setcookie('revaluateCookieObject', null, -1, '/travelkhana/jsp');
	}
//-----------------------------------------------------------------------	
//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){
    
    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];

    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;
    
    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        unset($_SESSION["stationList"]);
    }
    
}
//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();
if (isset($_POST["stationSearched"]) && !empty($_POST['stationSearched'])) { 
	$stationSearched=$_POST["stationSearched"];
}

if (isset($_POST["pnr1"]) && !empty($_POST['pnr1'])) { 

	$pnr=$_POST["pnr1"];
	$_SESSION['pnr'] =$pnr;
	if(isset($_SESSION['jDate']) && !empty($_SESSION['jDate'])) {
		unset($_SESSION['jDate']);
		unset($_SESSION['train']);
		unset($_SESSION['from']);
		unset($_SESSION['trainNameNo']);
	}
	setcookie('pnr', $pnr ,time() + (86400 * 30),"/");
}else if (isset($_POST["train1"]) && !empty($_POST['train1'])) { 
	$trainpost = explode("/",$_POST["train1"]);
	$frompost = explode("/", $_POST["from1"]);

	$jDate=$_POST["jDate"];
	$train = $trainpost[0];
	$from = $frompost[0];
	$fromfull=$_POST["from1"];
	$trainNameNumber=$_POST["train1"];
	$fromfull=$_POST["from1"];

	$_SESSION['jDate'] =$jDate;
	$_SESSION['train'] =$train;
	$_SESSION['from'] =$from;
	$_SESSION['trainNameNo'] =$trainNameNumber;
	if(isset($_SESSION['pnr']) && !empty($_SESSION['pnr'])) {
		unset($_SESSION['pnr']);
	}
}else if (isset($_GET["trainNumber"]) && !empty($_GET['trainNumber']) && isset($_GET["fromstation"]) && !empty($_GET['fromstation'])) { 
	$trainpost = explode("/",$_GET["trainNumber"]);
	$frompost = explode("/", $_GET["fromstation"]);
	$jDate=date("Y-m-d",strtotime("today"));
	$train = $trainpost[0];
	$from = $frompost[0];
	$trainNameNumber=$_GET["trainNumber"];
	$fromfull=$_GET["fromstation"];
	$_SESSION['jDate'] =$jDate;
	$_SESSION['train'] =$train;
	$_SESSION['from'] =$from;
	$_SESSION['trainNameNo'] =$trainNameNumber;
	if(isset($_SESSION['pnr']) && !empty($_SESSION['pnr'])) {
		unset($_SESSION['pnr']);
	}
}else if (isset($_GET["trainNumber"]) && !empty($_GET['trainNumber'])) { 
	$trainpost = explode("/",$_GET["trainNumber"]);
	//$frompost = explode("/", $_GET["fromstation"]);
	$jDate=date("Y-m-d",strtotime("today"));
	$train = $trainpost[0];
	//$from = $frompost[0];
	$trainNameNumber=$_GET["trainNumber"];
	//$fromfull=$_GET["fromstation"];
	$getStationFlag=1;

	$_SESSION['jDate'] =$jDate;
	$_SESSION['train'] =$train;
	//$_SESSION['from'] =$from;
	$_SESSION['trainNameNo'] =$trainNameNumber;
	if(isset($_SESSION['pnr']) && !empty($_SESSION['pnr'])) {
		unset($_SESSION['pnr']);
	}
}else if (isset($_POST["trainnum1"]) && !empty($_POST['trainnum1'])) { 
	$trainpost = explode("/",$_POST["trainnum1"]);
	$frompost = explode("/", $_POST["from1"]);

	$jDate=$_POST["jDate"];
	$train = $trainpost[0];
	$from = $frompost[0];
	$trainNameNumber=$_POST["trainnum1"];
	$fromfull=$_POST["from1"];

	$_SESSION['jDate'] =$jDate;
	$_SESSION['train'] =$train;
	$_SESSION['from'] =$from;
	$_SESSION['trainNameNo'] =$trainNameNumber;
	if(isset($_SESSION['pnr']) && !empty($_SESSION['pnr'])) {
		unset($_SESSION['pnr']);
	}
}else if(isset($_SESSION['pnr']) && !empty($_SESSION['pnr'])) {

	$pnr= $_SESSION['pnr'];
	
}else if(isset($_SESSION['train']) && !empty($_SESSION['train'])) {

	$jDate= $_SESSION['jDate'];
	$train = $_SESSION['train'];
	$from = $_SESSION['from'];
	$trainNameNumber=$_SESSION['trainNameNo'];
}else{
	setcookie('indexError',"{'message':'Some Problem Occurred Please Try Again','train':'','station':'','date':''}", time() + (86400 * 30), "/");
	header("Location: https://www.travelkhana.com"); /* Redirect browser */
	session_destroy();
	exit;
}

if (!empty($pnr)) {
	$url=$base_url.'orderBooking/trainRoutesNew?pnr='.$pnr;
}else{
	if($getStationFlag ==0){
		$url=$base_url.'orderBooking/trainRoutesNew?station='.$from.'&date='.$jDate.'&trainNo='.$train;
	}else{
		$url=$base_url.'orderBooking/trainRoutesNew?date='.$jDate.'&trainNo='.$train;
	}
}

$t=0;
$opts = array(
  'http'=>array(
    'method'=>"GET",
    //'header' => $authKey,
    'header' => "Authorization:".$authKey,
    'ignore_errors' => '1'                 
  )
);
$context = stream_context_create($opts);

$json = file_get_contents($url,false, $context);
$json_o=json_decode($json);

if (strpos($http_response_header[0], "200")) {

$responseData=$json_o->data; 
$t=(int)$responseData->availability;

	if($t == 0){
		$flg=1;
		if(!empty($json_o->message)){
			$errorDetail=$json_o->message;
		}else{
			$errorDetail='No food available on this route';
		}
	}

}
else if (strpos($http_response_header[0], "406")) { 
	$flg=1;
	$errorDetail=$json_o->Message;
}else{
	$flg=1;
	$errorDetail='Some Problem Occurred Pls. Try Again';
}


if($flg ==1){
$redirectUrl= '';
	if(isset($_SESSION['urlPage']) && !empty($_SESSION['urlPage'])) {
		if (strpos($_SESSION['urlPage'], 'stationSearch') !== false) {
			$_SESSION['indexError'] = $errorDetail;
			//$redirectUrl='stationSearch.php';
			$_SESSION['boardingStation']=$from;
			$_SESSION['selectedDate']=$jDate;
			$_SESSION['train']=$train;
			$_SESSION['urlPage'] = '';
			unset($_SESSION['urlPage']);
    		//$redirectUrl='../food-delivery-at-'.$stationSearched.'-railway-station-'.$from;
    		$redirectUrl='https://www.travelkhana.com';
		}else{
			$_SESSION['boardingStation']=$from;
			$_SESSION['selectedDate']=$jDate;
			$_SESSION['train']=$train;
			$redirectUrl='https://www.travelkhana.com';
		}	
	}else{
			
			$_SESSION['boardingStation']=$from;
			$_SESSION['selectedDate']=$jDate;
			$_SESSION['train']=$train;

			$redirectUrl='https://www.travelkhana.com';
		}

	// deleting all cookies first
	if(isset($_COOKIE['menuObject']))
	{
	setcookie('menuObject', null, -1, '/');
	}
	if(isset($_COOKIE['revaluateCookieObject']))
	{
		setcookie('revaluateCookieObject', null, -1, '/');
	}
	if(isset($_COOKIE['minOrderAmount']))
	{
		setcookie('minOrderAmount', null, -1, '/');
	}
	if(isset($_COOKIE['trackOrderResponse']))
	{
		setcookie('trackOrderResponse', null, -1, '/');
	}
	if(isset($_COOKIE['orderPost']))
	{
		setcookie('orderPost', null, -1, '/');
	}
	if(isset($_COOKIE['pnr']))
	{
		setcookie('pnr', null, -1, '/');
	}
//------------------------------------------

	setcookie('indexError','{"message":"'.$errorDetail.'","train":"'.$train.'","station":"'.$from.'","date":"'.$jDate.'","trainFull":"'.$trainNameNumber.'","stationFull":"'.$fromfull.'"}', time() + (86400 * 30), "/");
	session_destroy();
	header("Location: ".$redirectUrl);
	exit;
}

$stationList=$responseData->stations;


// getting food available station
$tempSessStationArray=array();

foreach($stationList as $item) { //foreach element in $arr
    $p=(int)$item->foodAvailablity;
    if($p == 1){
    	$tempSessStationArray[]= $item;
    }
}
$stationList=$tempSessStationArray;


//setting staion list for station change
if(isset($_POST['stsess']) || isset($_GET['trainNumber']))
{
	$sessionStationList= $stationList;
	$_SESSION['stationList'] =$stationList;
}else{
	//$sessionStationList= $_SESSION['stationList'];
	if(isset($_SESSION['stationList']) && !empty($_SESSION['stationList'])) {
	 	$sessionStationList= $_SESSION['stationList'];
	 }else{
	 	$sessionStationList= $stationList;
		$_SESSION['stationList'] =$stationList;
	 }
}

$elementCount  = count($stationList);
$categoriesName=$responseData->categoriesMenu;

if (strcasecmp($trainNameNumber, "") == 0) {
   	 $trainNameNumber=$responseData->train.'/'.$responseData->trainName;
}
if (strpos($trainNameNumber, '/') == false) {
    $trainNameNumber=$responseData->train.'/'.$responseData->trainName;
}
if (strcasecmp($train, "") == 0) {
   	 $train=$responseData->train;
}
if (strcasecmp($jDate, "") == 0) {
   	 $jDate=$stationList[0]->dateAtStation;
}

$date = date("d M Y");$match_date=date("d M Y", strtotime($stationList[0]->dateAtStation));$delDate=''; if($date == $match_date) { $delDate='today';} else {$delDate=$match_date;}
$_SESSION['trainDetail'] =$trainNameNumber;

$_SESSION['stationDetail'] =$stationList[0]->stationName.' ('.$stationList[0]->stationCode.')';
$_SESSION['deliveryDate'] =$delDate;
$_SESSION['deliveryTime'] =$stationList[0]->arrivalTime;

$vendorList=$responseData->vendor;
// getting selected vendor
$tempVendorArray=array();
foreach($vendorList as $item) { //foreach element in $arr
    $p=(int)$item->showMenu;
    if($p == 1){
    	$tempVendorArray[]= $item;
    }
}
$vendorList=$tempVendorArray;
if($getStationFlag ==1){
	$from = $stationList[0]->stationCode;
	$fromfull= $stationList[0]->stationCode.'/'.$stationList[0]->stationName;
	$_SESSION['from'] =$from;

}

setcookie('minOrderAmount', $vendorList[0]->minOrderAmount, time() + (86400 * 30) , "/");
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- CSS -->
<link rel="stylesheet" href="/glimpse/src/css/common.css?v=0.7">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.3">
<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2"> -->

<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="57x57" href="https://desktop.travelkhana.com/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://desktop.travelkhana.com/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://desktop.travelkhana.com/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://desktop.travelkhana.com/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://desktop.travelkhana.com/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://desktop.travelkhana.com/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://desktop.travelkhana.com/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="https://desktop.travelkhana.com/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="https://desktop.travelkhana.com/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="https://desktop.travelkhana.com/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://desktop.travelkhana.com/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="https://desktop.travelkhana.com/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://desktop.travelkhana.com/img/favicon-16x16.png">
<!-- <link rel="manifest" href="https://desktop.travelkhana.com/img/manifest.json"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="https://desktop.travelkhana.com/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
 <link rel="shortcut icon" href="https://desktop.travelkhana.com/img/favicon.ico" type="image/x-icon"/> 
<title>Food delivery in Train <?php  echo $trainNameNumber; ?> - TravelKhana.Com </title>

	
  </head>
  <style>
  .spinnerMob
  {
  border-left: 2px solid #f38a35;
  border-top: 2px solid #f38a35;
  border-bottom: 2px solid #f38a35;
}
  </style>
  <body>

	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="https://www.travelkhana.com/"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse search-inner-form" id="bs-example-navbar-collapse-1">
			  <form class="navbar-form navbar-right" id="srchForm" method="post">
				<div class="form-group">
				  <input type="text" class="form-control" name="train1" id="train1" placeholder="Enter Train No/ Name">
				  <input type="hidden" name="to" id="to" >
				  <input type="hidden" name="stsess" id="stsess" >
				  <input type="hidden" name="changeTrain" id="changeTrain" >
				  <input type="hidden" name="changeStation" id="changeStation" >
				  <input type="hidden" name="changeDate" id="changeDate" >
				  <input type="hidden" name="currentStation" id="currentStation" value="<?php echo $stationList[0]->stationCode?>" >
				</div>
				<div class="form-group">
				  <input type="text" class="form-control" id="from1" name="from1"  placeholder="Enter Boarding Station" >
				  <input type="hidden" id="fromstn" name="fromstn"  >
				</div>
				<div class="form-group date-field">
				  <input type="text" class="form-control datepicker" name="jDate" autocomplete="off" id="jDate" placeholder="Enter Boarding Date" value="">
				  <i class="fa fa-calendar" onclick="$('#jDate').focus();"></i>
				</div>
				<button type="button" class="btn btn-inner-search" onclick="allTripAdded()"><img src="https://desktop.travelkhana.com/img/search_img.png" alt="" title=""/></button>
			  </form>
			  
			</div><!-- /.navbar-collapse -->
		</div>
	</header>
	
	
	<!--inner search content here-->
		<section class="inner-search-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm">
						<!--<div class="mobile-view-cart text-center" >
							<ul class="list-inline mobile-cart-list">
								<li><a href="#"><span class="item"> Items</span></a></li>
								<li><a href="#"><span class="price"><i class="fa fa-inr"></i> </span></a></li>
								<li><a href="#"><span class="checkout">Checkout <i class="fa fa-arrow-right"></i></span></a></li>
							</ul>
						</div>-->
      <div class="mobile-view-cart text-center top-fixed" style="cursor:pointer">
      <?php
        echo '<ul class="list-inline mobile-cart-list" onclick= revalInputJson("'.$stationList[0]->stationCode.'","'.$train.'","'.$stationList[0]->dateAtStation.'","'.$vendorList[0]->outletId.'","'.$vendorList[0]->deliveryCost.'");>';
        ?>
        <li><span class="item count-item"></span></li>
        <li><span class="price total-item-price"></span></li>
        <li><span class="checkout">Checkout <i class="fa fa-arrow-right"></i></span>&nbsp;&nbsp;&nbsp;<span style='height:20px;width:20px;display:none;position: relative;' class="pull-right spinner spinnerMob startSpin"></span></li>       
       </ul>
      </div>
   
					</div> 
				
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<div class="station-list">
							<div class="train-name hidden-xs visible-lg visible-md">
								<h3><b>Food Delivery in Train Route</b></h3>
								<p><i class="fa fa-train"></i> <?php  echo $stationList[0]->stationCode; ?> || <?php  echo $stationList[$elementCount-1]->stationCode; ?> (ORG || EDS)</p>
								<span class="visible-xs visible-sm hidden-md- hidden-lg show-station-list" data-toggle="modal" data-target="#train-list-modal">Change</span>
							</div>
							
							<div class="train-list-mobile-view visible-xs hidden-lg hidden-md">
								<div class="train-detail-mob">
									<p><i class="fa fa-train"></i> <span><?php  echo $trainNameNumber; ?> <?php  echo $stationList[0]->stationCode; ?> || <?php  echo $stationList[$elementCount-1]->stationCode; ?> (ORG || EDS)</span></p>
								</div>
							
								<div class="delivery-mobile-view">
									<h3>Delivery at <?php $delTime=$stationList[0]->arrivalTime; echo $delTime." | ";$date = date("d M Y");$match_date=date("d M Y", strtotime($stationList[0]->dateAtStation));$delDate=''; if($date == $match_date) { $delDate='today';} else {$delDate=$match_date;} echo $delDate; ?><span><?php  echo $stationList[0]->stationName.'('.$stationList[0]->stationCode.')'; ?></span></h3>
									<span class="visible-xs visible-sm hidden-md- hidden-lg show-station-list" data-toggle="modal" data-target="#train-list-modal">Change</span>
								</div>
							</div>
							<!-- <p>Change Station for Food Delivery</p> -->
							
							<div class="station-name-list hidden-xs visible-lg visible-md visible-sm" id="stn_list">
								<h3>Select Station</h3>
								<ul class="list-unstyled">
								<?php
								$firstDate=date("d M Y", strtotime(2010-01-31));
								foreach($sessionStationList as $item) { //foreach element in $arr
									    
									    $apiDate=date("d M Y", strtotime($item->dateAtStation));
										if($firstDate!=$apiDate){
										$date = date("d M Y");$trainDate=''; if($date == $apiDate) { $trainDate='today';} else {$trainDate=$apiDate;}	
										echo '<li class="today">'.$trainDate.'</li>';
										$firstDate=$apiDate;
										}

										if (strcasecmp($item->stationCode, $stationList[0]->stationCode) == 0) {
											echo '<li onclick=changeStationModal("'.$item->stationCode.'","'.$train.'","'.$item->dateAtStation.'"); class="t_name active" data-stn='.$item->stationName.'> <span class="s_name">'.$item->stationName.' <small>('.$item->stationCode.')</small></span> <span class="t_time"><time>'.$item->arrivalTime.'</time></span></li>'; 
   									 	
										}else{
											echo '<li onclick=changeStationModal("'.$item->stationCode.'","'.$train.'","'.$item->dateAtStation.'"); class="t_name" data-stn='.$item->stationName.'><span class="s_name">'.$item->stationName.' <small>('.$item->stationCode.')</small></span> <span class="t_time"><time>'.$item->arrivalTime.'</time></span></li>'; 
   									 	
										}
  									}
								?>
								</ul>
							</div>
							
							
						</div>
					</div><!--left Content Ends here-->
					
					
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="middle-conent">
							
							<div class="row">
								<div class="col-sm-12 col-md-12 col-xs-12">
									<div class="middle-heading hidden-xs hidden-sm">
										<span class="food-heading"><?php  echo $trainNameNumber; ?></span>
									</div>
									<div class="middle-heading hidden-xs hidden-sm">
										<span class="food-time" style="float:left">Food Delivery at <?php  echo $stationList[0]->stationName.' ('.$stationList[0]->stationCode.')'; ?></span>
										<span class="food-time">Delivery At <time> <?php echo $delTime; ?></time> | <?php echo $delDate; ?></span>
									</div>
								</div> 
								
								 <div class="featured-slider-detail" style="display:none;">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="offer-middle-heading">
												<a href="#">
													<span class="food-heading"><img src="https://desktop.travelkhana.com/img/back-offer.png" alt="offer image" title=""/> Domino's</span>
												</a>
												
											</div>
										</div>
								
										<div class="full-detail">
											<div class="col-md-6 col-xs-12">
												<div class="offer-detail clearfix">
													<div class="media">
													  <div class="media-left">
														<a href="#">
														  <img class="media-object" src="https://desktop.travelkhana.com/img/offer-img-inner.png" alt="inner image" title=""/>
														</a>
													  </div>
													  <div class="media-body">
														<h4 class="media-heading">Domino's Pizza</h4>
															<small>Minimum Order: <i class="fa fa-inr"></i> 150 </small>
													  </div>
													</div>
												</div>
											</div>
											
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="offer-deliver-date hidden-xs">
													<span class="food-time">Delivery At: <time> 21:05</time> | Today</span>
												</div>
											</div>
										</div>
								
									</div>
								
								
							</div>
							
<!-- 							<div class="row">
								<div class="offer-slider clearfix">
								<h2>featured</h2>
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										<div class="col-md-12 col-sm-12">
											

										
											<div class="carousel-inner" role="listbox">
											
													<div class="item active">
													<img src="https://desktop.travelkhana.com/img/offer-img-3.jpg" alt="" title=""/>
													</div>
													
													<div class="item">
													<img src="https://desktop.travelkhana.com/img/offer-img.jpg" alt="" title=""/>
													</div>
													
													<div class="item">
													<img src="https://desktop.travelkhana.com/img/offer-img-2.jpg" alt="" title=""/>
													</div>
													
													

											</div>

											
											<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
								</div> 
								
								
								
								
								
							</div> -->
							
							<div class="row"  style="margin-top:30px;">

									<?php
									foreach($categoriesName as $cat) {
										$catType= $cat->menu;
										foreach($catType as $menuItem) {
											$s=$menuItem->webDisplay;
											if($s==1){
											echo '<div class="col-md-6 col-sm-6 col-xs-12">

												<div class="food-box">
													<div class="thumbnail">';
													if (strpos($menuItem->image, 'menu/') !== false) {
   	 														echo '<div class="img_box" style="background-image: url('.$menuItem->image.');"></div>';
														}else{
															echo '<div class="img_box" style="background-image: url(https://desktop.travelkhana.com/img/tk_placeholder.png);"></div>';
														}
														
														echo '<span class="add-food" data-item="'.$menuItem->itemName.'" data-outlet="'.$vendorList[0]->outletId.'" data-price="'.$menuItem->basePrice.'" data-id="'.$menuItem->itemId.'" id="foodid'.$menuItem->itemId.'">Add</span>
														<!-- <span class="fav-food"><i class="fa fa-star"></i></span> -->
														<div class="caption">
														<h2>'.$menuItem->itemName.'</h2>';
														if (strcasecmp($menuItem->menuTag, "veg") == 0) {
   	 														echo '<span class="veg"></span>';
														}else{
															echo '<span class="non-veg"></span>';
														}
														//echo '<p>'.$menuItem->description.'.</p>';
														echo '<p class="iffyTip wd100">'.$menuItem->description.'.</p>';
														echo '<div class="food-price"> <i class="fa fa-inr"></i> '.$menuItem->basePrice.'</div>
												
														</div>
													
													</div>
												</div>
											</div>';
										  }
										}
									}
									
									?>										
							</div>
							
						</div>
					
					</div><!--middle-conent ends here-->
					
					<div class="col-md-3 col-sm-3 col-xs-12 hidden-xs visible-lg visible-md visible-sm">
									
						 <div class="ad-box">
						 	<a href="https://www.travelkhana.com/tkblog/travelkhana-chais-terms-conditions" target="_blank">
							<img src="https://desktop.travelkhana.com/img/ad-img7.jpg" class="img-responsive" alt="" title=""/>
							</a>
						</div>
						
					<form action="cart.html" method="post">	
						<div class="main-cart" style="display:none;">
							<div class="main-cart-content text-center">
								<ul class="list-inline">
								<li><a href="#" class="count-item"> 0 Items</a></li>
								<li><a href="#"><i class="fa fa-inr"></i> <span class="total-item-price">0.0</span> </a></li>
								</ul>
								
								<div class="cart-checkout-btn">
									<!-- <a href="cart.php" class="btn btn-total-checkout">Checkout</a> -->
									<?php

									echo '<input type="button"   onclick= revalInputJson("'.$stationList[0]->stationCode.'","'.$train.'","'.$stationList[0]->dateAtStation.'","'.$vendorList[0]->outletId.'","'.$vendorList[0]->deliveryCost.'"); class="btn btn-total-checkout checkoutButton" value="Checkout" />';
									?>
									<span style='height:35px;width:35px;display:none;position: relative;margin-left:5px;' class="pull-right spinner startSpin"></span>
								</div>
							</div>
						</div>
					</form>
						
						
						
					</div>
				</div>
			</div>
		</section>
	<!--inner search content  Ends here-->
	
	<!-- onclick="changeCart()"; -->
	<!--back to top-->
        <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->

	<!--Empty Cart Popup Here-->
		
		<div class="clear-cart">
			<div class="modal fade" id="clear_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					<img src="https://desktop.travelkhana.com/img/popup-img.png" alt="" title=""/>
					<h3>Your previous item(s) will be deleted after changing the station</h3>
					<h2>Do You  want to go?</h3>
					<button type="button"  class="btn btn-go action-clearbasket" data-dismiss="modal" title="Yes" onclick="changeStation();">Yes</button>
					<button type="button" class="btn btn-cancel" data-dismiss="modal" title="No">No</button>
				  </div>
				  
				</div>
			  </div>
			</div>
		</div>
		
	<!-- Empty Cart Popup Ends Here-->
	
	
	<!-- Modal -->
		<div class="t-list">
			<div class="modal fade" id="train-list-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  
				  <div class="modal-body">
					<div class="station-name-list" id="train-list-modal">
						<!-- <h3>Select Station</h3> -->
						<ul class="list-unstyled">
							<?php
								$firstDate=date("d M Y", strtotime(2010-01-31));
								foreach($sessionStationList as $item) { //foreach element in $arr
									    
									    $apiDate=date("d M Y", strtotime($item->dateAtStation));
										if($firstDate!=$apiDate){
										$date = date("d M Y");$trainDate=''; if($date == $apiDate) { $trainDate='today';} else {$trainDate=$apiDate;}	
										echo '<li class="today">'.$trainDate.'</li>';
										$firstDate=$apiDate;
										}

										if (strcasecmp($item->stationCode, $stationList[0]->stationCode) == 0) {
											echo '<li onclick=changeStationModal("'.$item->stationCode.'","'.$train.'","'.$item->dateAtStation.'"); class="t_name active" data-stn='.$item->stationName.'>'.$item->stationName.' <small>('.$item->stationCode.')</small> <span class="t_time"><time>'.$item->arrivalTime.'</time></span></li>'; 
   									 	
										}else{
											echo '<li onclick=changeStationModal("'.$item->stationCode.'","'.$train.'","'.$item->dateAtStation.'"); class="t_name" data-stn='.$item->stationName.'>'.$item->stationName.' <small>('.$item->stationCode.')</small> <span class="t_time"><time>'.$item->arrivalTime.'</time></span></li>'; 
   									 	
										}
  									}	
								?>							
						</ul>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		</div>
		

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script defer src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" ></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/validate-js/2.0.1/validate.min.js"></script>
    <script defer type="text/javascript" src="https://www.travelkhana.com/travelkhana/js/jquery.autocomplete.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script  src="/glimpse/src/js/common/custom.js?v=1.8" ></script>
    <script type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></script>
    
    <script defer type="text/javascript">
		$(document).on('mouseenter', ".iffyTip", function() {
     		var $this = $(this);
     		if(this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
       		   $this.tooltip({
          		     title: $this.text(),
           		    placement: "bottom"
          		});
          		$this.tooltip('show');
    		 }
		});
    </script>
    <script defer>
    //document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.3" ></scr'+'ipt>');
  var trainC=<?php  echo $train; ?>;
	var stationC='<?php  echo $stationList[0]->stationName; ?>';
	var outletC=<?php  echo $vendorList[0]->outletId; ?>;
	$(document).ready(function(){
	console.log("m l event...");
	clevertap.event.push("RS - Menu Loaded", {
    				"Station": stationC,
    				"OutletId":outletC,
    				"Train":trainC,
	                "Channel": channel
	              });
      });
    </script>
    <script defer type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-37839621-1']);
        _gaq.push(['_setDomainName', 'travelkhana.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);
        (function () {
          var ga = document.createElement('script');
          ga.type = 'text/javascript';
          ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
        })();
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                  }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37839621-1', 'auto');
        ga('send', 'pageview');
      </script>
      
  </body>
</html>