<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#d51b00">
<meta name="msapplication-navbutton-color" content="#d51b00">
<meta name="apple-mobile-web-app-status-bar-style" content="#d51b00">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Track Order</title>

    <!-- CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css">
	<link rel="stylesheet" href="/glimpse/src/css/style.css?v=0.2">
	<link rel="stylesheet" href="/glimpse/src/css/media-queries.css?v=0.2">
  </head>
  <body>
    
	<!-- Loader -->
    	
	
	<header class="inner-header navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="order.jsp"><img src="https://desktop.travelkhana.com/img/inner-logo.png" alt="" title=""/></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class=""><img src="https://desktop.travelkhana.com/img/order_ico.png" alt="" title=""/> Order Food</a></li>
					<li><a href="track-order.html"><span class=""><img src="https://desktop.travelkhana.com/img/track_ico.png" alt="" title=""/> Track Order</a></li>
					<li><a href="#"><span class=""><img src="https://desktop.travelkhana.com/img/contact_ico.png" alt="" title=""/> Contact Us</a></li>
					<li><a href="#"><img src="https://desktop.travelkhana.com/img/android-btn.png" alt="" title=""/></a></li>
					<li><a href="#"><img src="https://desktop.travelkhana.com/img/app-btn.png" alt="" title=""/></a></li>
				
				</ul>
			  
			</div><!-- /.navbar-collapse -->
		</div>
	</header>
	
	<div class="page-content">
		<section class="track-order-container">
			<div class="container">
				<h1>Track Order</h1>
				
				<div class="track-order">
					<form class="form-inline" id="search-order">
						<div class="form-group col-sm-5 col-md-5 col-xs-12">
							<label class="sr-only" for="">Email address</label>
							<input type="text" class="form-control order-id" id="" placeholder="Order ID">
							<span><i class="fa fa-shopping-cart"></i></span>
						</div>
						<div class="form-group col-sm-5 col-md-5 col-xs-12">
							<label class="sr-only" for="">Password</label>
							<input type="text" class="form-control phone-number" id="" placeholder="Phone Number">
							<span class="mobile"><i class="fa fa-mobile"></i></span>
						</div>
						<div class="form-group col-sm-2 col-md-2 col-xs-12">
						  <button type="submit" class="btn btn-track">SEARCH</button>
						</div>
					</form>
				</div>
				
				
				
			</div>
		</section>
		
		<section class="recent-order-container">
			<div class="container">
				<h1 class="hidden-xs hidden-sm">Recent Orders</h1>
				
					<div class="row">
						<div class="recent-order-box clearfix hidden-xs hidden-sm">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="order-box">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-detail">
												<h3>order#<span>700123</span></h3>
												<h4>110015/Kushinagar Expres</h4>
												<h5>Khandwa JN(KNW)</h5>
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-status">
												<h3><time>21:00 Today</time></h3>
												<button type="button" class="btn btn-status"><i class="fa fa-map-marker"></i> Confirmed</button>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="order-box active">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-detail">
												<h3>order# <span>700124</span></h3>
												<h4>110015/Kushinagar Expres</h4>
												<h5>Khandwa JN(KNW)</h5>
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-status">
												<h3><time>21:00 Today</time></h3>
												<button type="button" class="btn btn-status"><i class="fa fa-check"></i> Delivered</button>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="order-box">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-detail">
												<h3>order#<span>700125</span></h3>
												<h4>110015/Kushinagar Expres</h4>
												<h5>Khandwa JN(KNW)</h5>
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-status">
												<h3><time>21:00 Today</time></h3>
												<button type="button" class="btn btn-status"><i class="fa fa-map-marker"></i> Confirmed</button>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="order-box">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-detail">
												<h3>order#<span>700126</span></h3>
												<h4>110015/Kushinagar Expres</h4>
												<h5>Khandwa JN(KNW)</h5>
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="order-status">
												<h3><time>21:00 Today</time></h3>
												<button type="button" class="btn btn-status"><i class="fa fa-map-marker"></i> Confirmed</button>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
						</div><!--recent-order box ends here-->
						
						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="order-item-container clearfix">
								<div class="row" id="printarea">
								
									<div class="col-md-12 col-sm-12 col-xs-12 noprint">
												<div class="order-item-list-mobile pull-right visible-xs">
													<ul class="list-inline">
														<li><a href="#"><i class="fa fa-paper-plane-o"></i></a></li>
														<li><a href="#"><i class="fa fa-crosshairs"></i></a></li>
														<li><a href="javascript:void(0)" onclick="printDiv();"><i class="fa fa-file-text-o"></i></a></li>
														<li><a href="#" data-toggle="modal" data-target="#clear_cart"><i class="fa fa-trash-o"></i></a></li>
														
													</ul>
												</div>
											</div>
											
											
									<div class="col-md-3 col-sm-4 col-xs-12">
										<div class="order-detail">
											<h3>order# <span>700123</span></h3>
											<h4>110015/Kushinagar Expres</h4>
											<h5>Khandwa JN(KNW)</h5>
										</div>
										
										<div class="order-item-list hidden-xs  noprint">
											<ul class="list-unstyled">
												<li><span class="ico"><i class="fa fa-paper-plane-o"></i></span> Browse Trip</li>
												<li><span class="ico"><i class="fa fa-crosshairs"></i></span> Live Status</li>
												<li class="print_invoice" onclick="printDiv();"><span class="ico"><i class="fa fa-file-text-o"></i></span> Print Invoice</li>
												<li data-toggle="modal" data-target="#clear_cart"><span class="ico"><i class="fa fa-trash-o"></i></span> Cancel Order</li>
												<li class="pay" data-toggle="modal" data-target="#online_payment"><span class="ico"><i class="fa fa-credit-card"></i></span> Pay Online</li>
											</ul>
										</div>
										
									</div>
									
									
									<div class="col-md-9 col-sm-8 col-xs-12">
										<div class="row">
											<div class="item-list-detail clearfix">
												<div class="col-md-6 col-sm-6 col-xs-12">
													<div class="date-deliver">
														<h3><time>21:00 Today</time></h3>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6 col-xs-12">
													<div class="item-status hidden-xs">
														<button type="button" class="btn btn-item-status"><i class="fa fa-user"></i> Booked</button>
													</div>
												</div>
												
											</div>
										</div>
										
										<div class="row">
											
											
											
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="track-order order-item-lists">
													<div class="">
														<div class="single-item clearfix hidden-xs hidden-sm">
															<div class="col-md-3 item">
																<div class="item-img">
																	<img src="https://desktop.travelkhana.com/img/booked-item-img.png" alt="" title=""/>
																</div>
															</div>
															<div class="col-md-3 item">
																<div class="item-name text-left">
																	<h3>Mini Veg Thali<span class="veg"></span> 
																</div>
															</div>
															<div class="col-md-3 item">
																<div class="item-quantity text-center">
																	<h3><i class="fa fa-inr"></i> 305 (x1)</h3>
																</div>
															</div>
															<div class="col-md-3 item">
																<div class="item-price text-right">
																	<h3><i class="fa fa-inr"></i> 305</h3>
																</div>
															</div>
															
														</div>
														
														<div class="single-item-mv hidden-md hidden-lg">
															<div class="row">
																<div class="col-md-6 col-xs-10">
																	<div class="item-img">
																		<div class="media">
																		  <div class="media-left">
																			<a href="#">
																			  <img class="media-object" src="https://desktop.travelkhana.com/img/booked-item-img.png" alt="...">
																			</a>
																		  </div>
																		  <div class="media-body lPadding-8">
																			<h4 class="media-heading">Mini Veg Thali</h4>
																			<h5><i class="fa fa-inr"></i> 305 <span> (x1)</span></h5>
																			
																		  </div>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 col-xs-2  hidden-sm">
																	<div class="booked-item-price pull-right">
																		<h5><i class="fa fa-inr"></i> 305</h5>
																	</div>
																</div>
															</div>
														</div>
														
														<!--<div class="single-item-mv hidden-md hidden-lg">
															<div class="row">
																<div class="col-md-6 col-xs-10">
																	<div class="item-img">
																		<div class="media">
																		  <div class="media-left">
																			<a href="#">
																			  <img class="media-object" src="https://desktop.travelkhana.com/img/booked-item-img.png" alt="...">
																			</a>
																		  </div>
																		  <div class="media-body">
																			<h4 class="media-heading">Mini Veg Thali</h4>
																			<h5><i class="fa fa-inr"></i> 0 <span> (2)</span></h5>
																			
																		  </div>
																		</div>
																	</div>
																</div>
																<div class="col-md-6 col-xs-2">
																	<div class="booked-item-price pull-right">
																		<h5><i class="fa fa-inr"></i> 0</h5>
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>-->
												</div>
											</div>
										</div>
										
										
										<div class="">
											<div class="col-md-6 col-md-offset-6">
												<div class="item-total-container rPadding-18 hidden-xs">
													<p>Subtotal<span class="pull-right"><i class="fa fa-inr"></i> 305</span></p>
													<p>Discount<span class="pull-right"><i class="fa fa-inr"></i> 20</span></p>
													<p>Taxes & Adjustments<span class="pull-right"><i class="fa fa-inr"></i> 35</span></p>
													<h4 class="bold-text">Amount To Be Paid<span class="pull-right"><i class="fa fa-inr"> </i> 360</span></h4>
													<h5 class="bold-text"><span class="check"><i class="fa fa-check"></i></span> Cash On Delivery</h5>
												</div>
												
												<div class="mobile-item-total hidden-lg hidden-md hidden-sm">
													<div class="mobile-subtotal clearfix">
														<div class="col-xs-9">
															<h4>Sub Total</h4>
														</div>
														<div class="col-xs-3">
															<h4><i class="fa fa-inr"></i> 305</h4>
														</div>
													</div>
													
													<div class="subcharge clearfix">
														<div class="col-xs-9">
															<h4>Delivey Charges</h4>
														</div>
														<div class="col-xs-3">
															<h4><i class="fa fa-inr"></i> 20</h4>
														</div>
														
														<div class="col-xs-9">
															<h4>Taxes and adjustments</h4>
														</div>
														<div class="col-xs-3">
															<h4><i class="fa fa-inr"></i> 35.00</h4>
														</div>
													</div>
													
													<div class="amount-paid clearfix">
														<div class="col-xs-9">
															<h4>Total Amount to be Paid</h4>
														</div>
														<div class="col-xs-3">
															<h4><i class="fa fa-inr"></i> 360</h4>
														</div>
													</div>
													
													<div class="payment-opt clearfix">
														<div class="col-xs-12 lPadding">
															<h4 class="pull-right"><span><i class="fa fa-check"></i></span> Cash on Delivery</h4>
														</div>
													</div>
													
													<div class="mobile-view-pay pull-right noprint">
														<button type="button" class="btn btn-payu" data-toggle="modal" data-target="#online_payment">Pay Online</button>
													</div>
													
													
												</div>
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
						
						
						
						
					</div>
			</div>
		
		</section>
		
		<section id="inner-footer" class="hidden-xs hidden-sm">
		<div class="container">
			<div class="row">
				<div class="divider"></div>
			</div>
			
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Carrers</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">
							<li><a href="#">Track Order</a></li>
							<li><a href="#">Food in Popular Trains</a></li>
							<li><a href="#">Become a Partner</a></li>
							<li><a href="#">Group Travel</a></li>
							<li><a href="#">FAQ</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="quick-links">
						<ul class="list-unstyled">
							<li><a href="#">Order Food in Train</a></li>
							<li><a href="#">Check PNR Status</a></li>
							<li><a href="#">Track Train</a></li>
							<li><a href="#">Indian Railways Reservation</a></li>
							<li><a href="#">Indian Railways Time Table</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="subscribe-from">
						<h2>Subscribe To Our Latest offers and Updates</h2>
						<form>
							<div class="input-group">
							  <input type="text" class="form-control" placeholder="Search for...">
							  <span class="input-group-btn">
								<button class="btn btn-subscribe" type="button"><i class="fa fa-paper-plane"></i></button>
							  </span>
							</div>
						  
						  
						</form>
						
						<div class="follow-us-list">
							<ul class="list-inline">
								<li><a href="#"><i class="circle fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="circle fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="circle fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="circle fa fa-instagram"></i></a></li>
								<li><a href="#"><i class="circle fa fa-linkedin"></i></a></li>
							</ul>
						</div>
						
						
						
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
	<section class="inner-footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="copyright-text">
						<p>Copyright &copy; 2017, TravelKhana, All rights reserved.</p>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="more-links">
						<ul class="list-inline">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms & Conditions</a></li>
							<li><a href="#">Disclaimer</a></li>
						</ul>
					</div>
				</div>
						
			</div>
		</div>
	</section>
		
		
	</div>
	
	<!--back to top-->
        <a href="#" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
    <!--back to top end-->
	
	<!----Cancel Order Popup Here----->
		
		<div class="clear-cart">
			<div class="modal fade" id="clear_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					<img src="https://desktop.travelkhana.com/img/cancel-order.png" alt="" title=""/>
					<h3>Are you sure, you want to cancel order?</h3>
					<button type="button"  class="btn btn-go" data-dismiss="modal" title="Yes">Yes</button>
					<button type="button" class="btn btn-cancel" data-dismiss="modal" title="No">No</button>
				  </div>
				  
				</div>
			  </div>
			</div>
		</div>
		
	<!----Empty Cart Popup Ends Here----->
	
	
	<!----Online Payment Popup Here----->
		
		<div class="online-payment">
			<div class="modal fade" id="online_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <div class="modal-body">
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title">Pay Using Cash Card/ Wallets</h3>
					  </div>
					  <div class="panel-body">
						<ul class="list-inline">
						<li><div class="payment-logo payumoney" title="PayUmoney"></div></li>
						<li><div class="payment-logo masterpass" title="Masterpass"></div></li>
						<li><div class="payment-logo jio-money" title="jio-money"></div></li>
						<li><div class="payment-logo mrupee" title="Mrupee"></div></li>
						<li><div class="payment-logo zipcash" title="Zipcash"></div></li>
						<li><div class="payment-logo icc-logo" title="ICC logo"></div></li>
						<li><div class="payment-logo idea-money-app" title="Idea Money wallet"></div></li>
						<li><div class="payment-logo m-pesa" title="M-Pesa"></div></li>
						<li><div class="payment-logo payzapp" title=""Payzapp></div></li>
						<li><div class="payment-logo oxigen-logo" title="Oxigen Wallet"></div></li>
						<li><div class="payment-logo airtel-money" title="Airtel Money"></div></li>
						<li><div class="payment-logo ola-money" title="Ola Money Wallet"></div></li>
						<li><div class="payment-logo freecharge" title="Freecharge"></div></li>
						<li><div class="payment-logo ebuddy-logo" title="SBI Buddy"></div></li>
						<li><div class="payment-logo momlogo" title="momlogo"></div></li>
						<li><div class="payment-logo pockets" title="Pockets"></div></li>
						<li><div class="payment-logo mobikwik" title="Mobikwik"></div></li>
						<li><div class="payment-logo paytm" title="paytm"></div></li>
						<li><div class="payment-logo tmw-trademark" title="TWM Trademark"></div></li>
					</ul>
					  </div>
					  <!--<div class="panel-footer">
						<div class="checkbox">
							<label>
							  <input type="checkbox">I Agree All <a href="#">Terms & Conditions</a>
							</label>
						</div>
						<button type="button" class=" btn btn-pay-online">Proceed Securely</button>
					  </div>-->
					</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
		
	<!----Online Payment Popup Ends Here----->
	
	
	 <!-- Include all compiled plugins (below), or include individual files as needed -->
	 
   <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" defer></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
   <!--<script src="js/custom.unminified.js"></script>-->
    <script src="/glimpse/src/js/common/pricing.js?v=0.4"></script>    
    <!--<script src="js/custom.js"></script> 
   <script src="js/bootstrap-datepicker.js"></script>-->
   <script>
     $('.order-box').on('click',function(){
     $('div').removeClass('active');
     $(this).addClass('active');
    });
   </script>
	