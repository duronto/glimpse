document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.4" ></scr'+'ipt>');

function onlyDigits(value){
	value=value.trim();
		if(parseInt(value) < 0 || isNaN(value)){
			if(value.length>1){
				value = value.slice(0,value.length-1);
				return value;
			}
			else{
				return "";
			}
		}
		else return value;
	}


jQuery(function($) {

	_gaq.push(['_trackEvent', 'Cart', 'Open Cart-NM', 'View Cart']);
	$('.promo-click').keyup(function() { 
        if ($('.coupon-applied').is(':visible'))
        {
        	removeCoupon();
        	// $('.mobile-number-field').show();
        }
        
    });
    $('.mobile-no').keyup(function() { 
        if ($('.coupon-applied').is(':visible'))
        {
        	removeCoupon();
        	// $('.mobile-number-field').show();
        }
      
    });

	$(document).on('keypress','.numeric,input[type="number"]', function(evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 46) {
		return true;
	}
	
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
});

$('.numeric,input[type="number"]').bind('paste drop',function(e){
	e.preventDefault();
});


	// proceed to delivery functionality
	$(document).on('click', '.btn-checkout', function() {
		var revaluateCookieObject=JSON.parse($.cookie('revaluateCookieObject'));
		var minOrderAmount=parseInt(revaluateCookieObject.outletminorderAmount);
		var minAmountThreshold=parseInt(revaluateCookieObject.minAmountThreshold);
		var contactNo='';
		var voucher ='';
		var isMobile = '';
		if(revaluateCookieObject.hasOwnProperty('userBasicInfo'))
		{
			contactNo=revaluateCookieObject.userBasicInfo.contact_no;
			voucher = revaluateCookieObject.userOrderInfo.voucher_code;
		}
		else
		{
			contactNo = $(this).closest('.order-summary').find('.mobile-no').val();
			isMobile = 'false';
			if(contactNo== undefined){
				isMobile = 'true';
				contactNo = $(this).closest('.checkout-mobile-view').find('.mobile-no').val();
			}
		}
		if (contactNo == undefined || contactNo === '' || contactNo.length < 10) {
			$('.promo-click').css("borderColor", "#d4dce1");
			if(isMobile==='true'){
				$(this).closest('.checkout-mobile-view').find('.mobile-no').val('');
			}else{
				$(this).closest('.order-summary').find('.mobile-no').val('');
			}
			//$('.mobile-number-field').show();
			$('.mobile-no').attr("placeholder", "Please enter 10 digit mobile number");
			$('.mobile-no').css("borderColor", "#d51b00");
			return false;
		} else {
			if ((isMobile === 'true' || isMobile == '') 
			&& ($(this).closest('.checkout-mobile-view').find('.mobile-no').val()!= undefined && $(this).closest('.checkout-mobile-view').find('.mobile-no').val()!= contactNo)){
				contactNo = $(this).closest('.checkout-mobile-view').find('.mobile-no').val();
			} else if ((isMobile === 'false' || isMobile == '') 
			&& ($(this).closest('.order-summary').find('.mobile-no').val()!=undefined && $(this).closest('.order-summary').find('.mobile-no').val() != contactNo)){
				contactNo = $(this).closest('.order-summary').find('.mobile-no').val();
			}
			if (!contactNo){
				$('.mobile-no').attr("placeholder", "Please enter 10 digit mobile number");
				$('.mobile-no').css("borderColor", "#d51b00");
				return false;
			}
			var filteredResponse = prepareCookieCouponObject(voucher , contactNo);
			//revaluateCookieObject.userBasicInfo = filteredResponse.userBasicInfo;
			$.cookie("revaluateCookieObject", filteredResponse,{path: "/" });

		}
		if(minAmountThreshold<minOrderAmount && revaluateCookieObject.userOrderInfo.delivery_cost<1)
		{
			$('.errorText').html("Minimum Order amount is :₹" + minOrderAmount);
            $('.orderError').show();
            setTimeout(function () {
                $('.orderError').fadeOut();
            }, 3000);

            // Clevertap
            clevertap.event.push("RS - Minimum Order amount", {
                "MinimumAmount": minOrderAmount,
                "station": revaluateCookieObject.userOrderInfo.station_code,
                "payableAmount": revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped,
                "outletId":revaluateCookieObject.userOrderInfo.order_outlet_id,
                "deliveryCost":revaluateCookieObject.userOrderInfo.delivery_cost,
                "mobileNumber":contactNo,
                "Channel":channel
            });

			return false;
		}
		_gaq.push(['_trackEvent', 'Cart', 'Open Cart-NM', 'Place Order Button Click']);
		// Clevertap
            clevertap.event.push("RS - Charge requested", {
                "station": revaluateCookieObject.userOrderInfo.station_code,
                 "channel":channel,
                 "payableAmount": revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped,
                "outletId":revaluateCookieObject.userOrderInfo.order_outlet_id,
                "deliveryCost":revaluateCookieObject.userOrderInfo.delivery_cost,
                "mobileNumber":contactNo,

            });
		$(location).attr('href', 'checkout.html');
	});
	// proceed checkout functionality
	/*$(document).on('click', '.btn-checkout', function() {
		var total=$('.grandtotal').text();
		$(location).attr('href', '../pages/checkout.php');

	});
*/
	$(document).on('click', '.fa-times', function() {
		$('.coupon-applied').hide();
		$('.order-summary').find('.promo-click').val("");
		//$('.order-summary').find('.mobile-no').val("");
		$('.checkout-mobile-view').find('.promo-click').val("");
		$('.checkout-mobile-view').find('.mobile-no').val("");

	
	});
	
	// order minimum amount
	$('.orderError').hide()

	if(($('.delivery').text()>0))
	{
		$('.DeliveryCostDiv').show();

	}
	
	if(!(parseInt($('.discount').text())>0))
	{
		$('.coupon-applied').removeAttr("style").hide();
		$('.order-summary').find('.promo-click').val('');
		//$('.order-summary').find('.mobile-no').val('');
		$('.checkout-mobile-view').find('.promo-click').val('');
		// $('.checkout-mobile-view').find('.mobile-no').val('');
	}
	else
	{	var revaluateCookieObject=JSON.parse($.cookie('revaluateCookieObject'));
		$('.DiscountDiv').show();
		if(revaluateCookieObject.hasOwnProperty('userBasicInfo'))
		{
			$('.order-summary').find('.mobile-no').val(revaluateCookieObject.userBasicInfo.contact_no);
			$('.checkout-mobile-view').find('.mobile-no').val(revaluateCookieObject.userBasicInfo.contact_no);

		}
		else
		{
			$('.order-summary').find('.mobile-no').val('');
			$('.checkout-mobile-view').find('.mobile-no').val('');
		}
		$('.order-summary').find('.promo-click').val(revaluateCookieObject.userOrderInfo.voucher_code);
		$('.checkout-mobile-view').find('.promo-click').val(revaluateCookieObject.userOrderInfo.voucher_code);
		$('.promoCode').text(revaluateCookieObject.userOrderInfo.voucher_code);
		//$('.mobile-number-field').show();
	}
	// show mobile number field for promo code
	// $(document).on('focus click', '.promo-click', function() {
	// 	$('.mobile-number-field').show();

	// });

	// show mobile number field for promo code
	$(document).on('click', '.removeCoupon', function() {
		
		removeCoupon();

	});

	// apply coupon
	$(document).on('click', '.apply-coupon-btn', function() {
		var promoCode='';
		var mobileNumber='';
		$('.coupon-applied').removeAttr("style").hide();
		$('.couponError').removeAttr("style").hide();
			if($(this).closest('.order-summary').length==1)
			{
				if($(this).closest('.order-summary').find('.promo-click').val()==='')
				{
				$('.promo-click').css("borderColor","#d51b00");
					 $('.promo-click').attr("placeholder", "Please enter coupon code");
					 return false;
				}
				if($(this).closest('.order-summary').find('.promo-click').val().length<6)
				{
				$('.promo-click').css("borderColor","#d51b00");
				$(this).closest('.order-summary').find('.promo-click').val('');
					 $('.promo-click').attr("placeholder", "Please enter 6 digit coupon code");
					 return false;
				}
				 if($(this).closest('.order-summary').find('.mobile-no').val()==='' || $(this).closest('.order-summary').find('.mobile-no').val().length<10)
				{
					$('.promo-click').css("borderColor","#d4dce1");
					$(this).closest('.order-summary').find('.mobile-no').val('');
					//$('.mobile-number-field').show();
					$('.mobile-no').attr("placeholder", "Please enter 10 digit mobile number");
				$('.mobile-no').css("borderColor","#d51b00");
				return false;
				}
				promoCode=$(this).closest('.order-summary').find('.promo-click').val();
				mobileNumber=$(this).closest('.order-summary').find('.mobile-no').val();
			}
			else if($(this).closest('.checkout-mobile-view').length==1)
			{
				if($(this).closest('.checkout-mobile-view').find('.promo-click').val()==='')
				{
				$('.promo-click').css("borderColor","#d51b00");
					 $('.promo-click').attr("placeholder", "Please enter coupon code");
					 return false;
				}
				 if($(this).closest('.checkout-mobile-view').find('.mobile-no').val()==='' || $(this).closest('.checkout-mobile-view').find('.mobile-no').val().length<10)
				{
					$('.promo-click').css("borderColor","#d4dce1");
					$(this).closest('.checkout-mobile-view').find('.mobile-no').val('');
					//$('.mobile-number-field').show();
					$('.mobile-no').attr("placeholder", "Please enter 10 digit mobile number");
				$('.mobile-no').css("borderColor","#d51b00");
				return false;
				}
				promoCode=$(this).closest('.checkout-mobile-view').find('.promo-click').val();
				mobileNumber=$(this).closest('.checkout-mobile-view').find('.mobile-no').val();
			}
			$('.mobile-no').css("borderColor","#d4dce1");
			$('.promo-click').css("borderColor","#d4dce1");
			
			if($(this).closest('.checkout-mobile-view').length==1)
			{
				 promoCode=$(this).closest('.checkout-mobile-view').find('.promo-click').val();
				 $('.order-summary').find('.promo-click').val(promoCode);
				 $('.order-summary').find('.mobile-no').val($(this).closest('.checkout-mobile-view').find('.mobile-no').val());
			}else
			{
				 promoCode=$(this).closest('.order-summary').find('.promo-click').val();
				  $('.checkout-mobile-view').find('.promo-click').val(promoCode);
				 $('.checkout-mobile-view').find('.mobile-no').val($(this).closest('.order-summary').find('.mobile-no').val());
			}
		$('.apply-coupon-btn').text("Applying ...");
		var cartObject=prepareCookieCouponObject(promoCode,mobileNumber);
		var parsedCouponObject=jQuery.parseJSON(cartObject);
		//parsedObject.cartDetailBean[0].couponCode=$('.promo-click').val();
		//$('.cartObject').attr('value',JSON.stringify(parsedObject));
		_gaq.push(['_trackEvent', 'Cart', 'Open Cart-NM', 'Enter Coupon = ' + promoCode]);

		// Clevertap
       /* clevertap.event.push("NM - Promocode Requested", {
            "Promocode": promoCode,
            "CustomerPayable": $('.grandtotal').text(),
            "Channel": "desktop"
        });
        clevertap.profile.push({
            "Site": {
                "Phone": '+91'+mobileNumber
            }
        });*/
		applyCouponHit(JSON.stringify(parsedCouponObject),promoCode);
		
	});


	function prepareCookieCouponObject(promoCode,mobileNumber)

{
		var revaluateCookieObject={};
		var revaluateCookieObject=JSON.parse($.cookie('revaluateCookieObject'));

		var passengerInfoBeanObject = {};
		passengerInfoBeanObject.login_id= "";
		passengerInfoBeanObject.contact_no= mobileNumber;
		passengerInfoBeanObject.mail_id="info@duronto.in";
		passengerInfoBeanObject.name= "testUser";
		
		revaluateCookieObject.userBasicInfo={};
		revaluateCookieObject.userBasicInfo=passengerInfoBeanObject;


		revaluateCookieObject.userOrderInfo.voucher_code=promoCode;

		return JSON.stringify(revaluateCookieObject);
}


	function applyCouponHit(parsedCouponObject,promoCode)
	{
		var jsonParseCouponObj=JSON.parse(parsedCouponObject);
		delete jsonParseCouponObj.userOrderInfo.taxes;
		delete jsonParseCouponObj.onlinePayableAmount;
		var stringifyCouponObj=JSON.stringify(jsonParseCouponObj);

		$.ajax({
				url: URL+"orderBooking/menuPriceCalculation?token=portal",
				type: "POST",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: stringifyCouponObj,
				beforeSend : function( xhr ) {
				xhr.setRequestHeader( "Authorization", authKey);
				},
				success: function(response){
					console.log("response ===>"+response);
					$('.apply-coupon-btn').text("Apply");

					if(response.userOrderInfo.discount >0)
					{
						_gaq.push(['_trackEvent', 'Cart', 'Open Cart-NM', 'Successful Apply of Promo Code = ' + promoCode]);
						$('.promoCode').text(promoCode);
						$('.coupon-applied').show();
						$('.DiscountDiv').show();


						
						var filteredResponse= prepareFilteredRevaluateObject(response);
					var totalTax=(parseFloat(filteredResponse.userOrderInfo.taxes)+parseFloat(filteredResponse.userOrderInfo.totalCapped));
					$('.taxes').text(totalTax.toFixed(2));
					$('.grandtotal').text(Math.round(filteredResponse.userOrderInfo.totalCustomerPayableCapped));
					$('.subtotal').text(parseFloat(filteredResponse.userOrderInfo.totalBasePrice).toFixed(2));
					$('.discount').text(filteredResponse.userOrderInfo.discount);

						$.cookie("revaluateCookieObject", JSON.stringify(filteredResponse),{path: "/" });
						// Clevertap
	                    clevertap.event.push("RS - Promocode Applied", {
	                        "customerPayable":jsonParseCouponObj.userOrderInfo.totalCustomerPayableCapped,
	                        "Channel":channel,
	                        "station":jsonParseCouponObj.userOrderInfo.station_code,
	                        "promoCode":promoCode,
	                        "mobile number":jsonParseCouponObj.userBasicInfo.contact_no
	                    });
					}
					else
					{
						_gaq.push(['_trackEvent', 'Cart', 'Open Cart-NM', 'Invalid Promo Code = ' + promoCode]);
						$('.couponError').show();
						setTimeout(function () {
						$('.couponError').fadeOut();
						}, 3000);
						// Clevertap
	                    clevertap.event.push("RS - Promocode Failed", {
	                       
	                        "customerPayable":jsonParseCouponObj.userOrderInfo.totalCustomerPayableCapped,
	                        "Channel":channel,
	                        "station":jsonParseCouponObj.userOrderInfo.station_code,
	                        "reason":"Not valid",
	                        "promoCode":promoCode,
	                        "mobile number":jsonParseCouponObj.userBasicInfo.contact_no
                    });
					}
				},
				error: function(error){		
						$('.apply-coupon-btn').text("Apply");   
						$('.couponError').show();
						var stringVal=JSON.stringify(error);
						var resp=JSON.parse(stringVal);
						$('.couponErrorMsg').text(resp.responseJSON.Message); 
						
						setTimeout(function () {
						$('.couponError').fadeOut();
						}, 3000);   

						// Clevertap
	                    clevertap.event.push("RS - Promocode Failed", {
	                        "customerPayable":jsonParseCouponObj.userOrderInfo.totalCustomerPayableCapped,
	                        "Channel":channel,
	                        "station":jsonParseCouponObj.userOrderInfo.station_code,
	                        "reason":resp.responseJSON.Message,
	                        "promoCode":promoCode,
	                        "mobile number":jsonParseCouponObj.userBasicInfo.contact_no
                    });  
				}
		});
	}
	

	// while quantity changes
	$(document).on('input', '.priceChange', function() {
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');

		if (input.val() > 0) {
			input.val(parseFloat(input.val()));
			var price = $(this).closest('.single-item').find('.item-amount').text();
			computeTotal();
		}
	});


	// while quantity decreases
	$(document).on('click', '.qty-minus', function(event) {
		event.preventDefault();
		var itemId=$(event.target)[0].id;
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');
		if(parseInt($('.discount').text())>0)
		{
			$('.coupon-applied').hide();
		$('.order-summary').find('.promo-click').val("");
		//$('.order-summary').find('.mobile-no').val("");
		$('.checkout-mobile-view').find('.promo-click').val("");
		//$('.checkout-mobile-view').find('.mobile-no').val("");

			$(".btnMinus").removeClass("qty-minus");
				var itemId=$(event.target)[0].id;
				var revaluateObject=$.cookie('revaluateCookieObject');
				input.val(parseFloat(input.val()) - 1);
				var price = $(this).closest('.single-item').find('.item-amount').text();
				loadSpin(true);
				var revaluateJsonObject=prepareRevaluateObject(itemId,parseFloat(input.val()),revaluateObject);
					$("#proceedBtn").removeClass("btn-checkout");
				

				var revaluateCookieObject=JSON.parse(revaluateJsonObject);

		/*var totalCustomerPayableCapped= (parseFloat(revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped)+parseFloat(revaluateCookieObject.userOrderInfo.discount));*/
		revaluateCookieObject.userOrderInfo.voucher_code='';
		revaluateCookieObject.userOrderInfo.discount=0;
		delete revaluateCookieObject.userBasicInfo;
		delete revaluateCookieObject.userOrderInfo.taxes;
		delete revaluateCookieObject.onlinePayableAmount;
		//loadSpin(true);

				var revaluateResponse =revaluateCall(JSON.stringify(revaluateCookieObject));
			//removeCoupon();
			$('.coupon-applied').hide();
		$('.DiscountDiv').hide();
		$('.discount').text('');
		// $('.mobile-number-field').hide();

			if( input.val() == 0 )
				{
				  $(this).closest( '.single-item' ).remove();
				}
				
				if($('.single-item' ).length==0)
				{
					document.cookie = 'menuObject=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'revaluateCookieObject=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					$('.order-summary' ).parent().hide();
					$('.mobile-cart-view' ).hide();    
					$(location).attr('href', 'https://www.travelkhana.com/travelkhana/jsp/menu.html')                  

				}

				$('.errorText').html("Please reapply the coupon");
            $('.orderError').show();
            setTimeout(function () {
                $('.orderError').fadeOut();
            }, 3000);
				
		}
		else
		{	
		if (input.val() > 0) {
			$(".btnMinus").removeClass("qty-minus");
			var itemId=$(event.target)[0].id;
			var revaluateObject=$.cookie('revaluateCookieObject');
			input.val(parseFloat(input.val()) - 1);
			var price = $(this).closest('.single-item').find('.item-amount').text();
			loadSpin(true);
			var revaluateJsonObject=prepareRevaluateObject(itemId,parseFloat(input.val()),revaluateObject);
				$("#proceedBtn").removeClass("btn-checkout");

			var parsedJson=JSON.parse(revaluateJsonObject);
			delete parsedJson.userOrderInfo.taxes;
			delete parsedJson.onlinePayableAmount;
			var revaluateResponse =revaluateCall(JSON.stringify(parsedJson));

			
			//$(".btn-checkout").remove();
			//computeTotal();
			}
			if( input.val() == 0 )
				{
				  $(this).closest( '.single-item' ).remove();
				}
				
				if($('.single-item' ).length==0)
				{
					document.cookie = 'menuObject=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'revaluateCookieObject=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					$('.order-summary' ).parent().hide();
					$('.mobile-cart-view' ).hide();    
					$(location).attr('href', 'https://www.travelkhana.com/travelkhana/jsp/menu.html')                  

				}
			}
	});

	// while quantity increases
	$(document).on('click', '.qty-plus', function(event) {
		 event.preventDefault();
		 var itemId=$(event.target)[0].id;
		 if(parseInt($('.discount').text())>0)
		{
		$("#proceedBtn").removeClass("btn-checkout");
		$(".btnPlus").removeClass("qty-plus");
		$('.coupon-applied').hide();
		$('.order-summary').find('.promo-click').val("");
		//$('.order-summary').find('.mobile-no').val("");
		$('.checkout-mobile-view').find('.promo-click').val("");
		//$('.checkout-mobile-view').find('.mobile-no').val("");


		var revaluateObject=$.cookie('revaluateCookieObject');
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');

		input.val(parseFloat(input.val()) + 1);
		var price = $(this).closest('.single-item').find('.item-amount').text();
		loadSpin(true);
		var revaluateJsonObject=prepareRevaluateObject(itemId,parseFloat(input.val()),revaluateObject);
		
		var revaluateCookieObject=JSON.parse(revaluateJsonObject);
		/*var totalCustomerPayableCapped= (parseFloat(revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped)+parseFloat(revaluateCookieObject.userOrderInfo.discount));*/
		revaluateCookieObject.userOrderInfo.voucher_code='';
		revaluateCookieObject.userOrderInfo.discount=0;
		delete revaluateCookieObject.userBasicInfo;
		delete revaluateCookieObject.userOrderInfo.taxes;
		delete revaluateCookieObject.onlinePayableAmount;
		//loadSpin(true);

		var revaluateResponse =revaluateCall(JSON.stringify(revaluateCookieObject));

		$('.coupon-applied').hide();
		$('.DiscountDiv').hide();
		$('.discount').text('');
		// $('.mobile-number-field').hide();



	
			//removeCoupon();

			$('.errorText').html("Please reapply the coupon");
            $('.orderError').show();
            setTimeout(function () {
                $('.orderError').fadeOut();
            }, 3000);
		}
		else
		{
		var itemId=$(event.target)[0].id;

		$("#proceedBtn").removeClass("btn-checkout");
		$(".btnPlus").removeClass("qty-plus");
		
		//$("button").removeClass("btn-checkout");

		/*if(parseInt($('.discount').text())>0)
		{
			$('.coupon-applied').hide();
		$('.order-summary').find('.promo-click').val("");
		$('.order-summary').find('.mobile-no').val("");
		$('.checkout-mobile-view').find('.promo-click').val("");
		$('.checkout-mobile-view').find('.mobile-no').val("");


			removeCoupon();
		}*/
		var revaluateObject=$.cookie('revaluateCookieObject');
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');

		input.val(parseFloat(input.val()) + 1);
		var price = $(this).closest('.single-item').find('.item-amount').text();
		loadSpin(true);
		var revaluateJsonObject=prepareRevaluateObject(itemId,parseFloat(input.val()),revaluateObject);
		var parsedJson=JSON.parse(revaluateJsonObject);
		delete parsedJson.userOrderInfo.taxes;
		delete parsedJson.onlinePayableAmount;
		var revaluateResponse =revaluateCall(JSON.stringify(parsedJson));
	}
	//	computeTotal();

	});
	function prepareRevaluateObject(itemId,quantity,revaluateObject)
	{
		var revaluateParseObject=JSON.parse(revaluateObject);
		delete revaluateParseObject.onlinePayableAmount;
			$.grep(revaluateParseObject.userOrderMenu, function(value, key){
			    if(value==undefined)
			    	return;
			    if((quantity==0) && (value.itemId==parseInt(itemId)))
				{
					revaluateParseObject.userOrderMenu.splice(key, 1);
					/*return;*/
				}
				if(value.itemId==itemId)
			    {
			    	value.quantity=quantity;
			    	return;
			    }
			});
			return JSON.stringify(revaluateParseObject);
	}

	function revaluateCall(revaluateJsonObject)
	{
		var revaluateResponse='';
		$.ajax({
				url: URL+"orderBooking/menuPriceCalculation?token=portal",
				type: "POST",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: revaluateJsonObject,
				beforeSend : function( xhr ) {
				xhr.setRequestHeader( "Authorization", authKey);
				},
				success: function(response){
					var discountVal=response.userOrderInfo.discount;
					if(discountVal>0){
						var voucherVal=response.userOrderInfo.voucher_code;
						var contactVal=response.userBasicInfo.contact_no;
						$('.order-summary').find('.promo-click').val(voucherVal);
						$('.order-summary').find('.mobile-no').val(contactVal);
						$('.checkout-mobile-view').find('.promo-click').val(voucherVal);
						$('.checkout-mobile-view').find('.mobile-no').val(contactVal);
						$('.coupon-applied').show();
						$('.DiscountDiv').show();
						$('.discount').text(discountVal);
						//$('.mobile-number-field').show();
					}
					if(response.userOrderInfo.delivery_cost>0)
					{
						$('.delivery').text(response.userOrderInfo.delivery_cost);
						$('.DeliveryCostDiv').show();

					}
					else
					{
							$('.DeliveryCostDiv').hide();
					}

					 loadSpin(false);
					var filteredResponse= prepareFilteredRevaluateObject(response);

					$('.subtotal').text(parseFloat(filteredResponse.userOrderInfo.totalBasePrice).toFixed(2));
						var totalTax=(parseFloat(filteredResponse.userOrderInfo.taxes)+parseFloat(filteredResponse.userOrderInfo.totalCapped));
						
					//var taxes=(parseFloat(filteredResponse.userOrderInfo.totalVat)+parseFloat(filteredResponse.userOrderInfo.totalServiceTax));
					$('.taxes').text(totalTax.toFixed(2));
					$('.mobileTaxes').text(totalTax.toFixed(2));
					$('.grandtotal').text(Math.round(filteredResponse.userOrderInfo.totalCustomerPayableCapped));
					
					//$("button").addClass("btn-checkout");
					//$(".btn-checkout").remove();
					$("#proceedBtn").addClass("btn-checkout");
					$(".btnPlus").addClass("qty-plus");
					$(".btnMinus").addClass("qty-minus");
					
				},
				error: function(error){			        
				}
		});
		
	}
	function prepareFilteredRevaluateObject(revaluateResponse)
	{


		// removing unrequired elements from userOrderMenu object
		/*$.each(revaluateResponse.userOrderMenu, function(index, value) {
			 delete revaluateResponse.userOrderMenu[index]['menuRevisionId'];
			 delete revaluateResponse.userOrderMenu[index]['sellingPrice'];
			 delete revaluateResponse.userOrderMenu[index]['costPrice'];
			 delete revaluateResponse.userOrderMenu[index]['itemVat'];
			 delete revaluateResponse.userOrderMenu[index]['itemServiceTax'];
			 //delete revaluateResponse.userOrderMenu[index]['itemBasePrice'];
			 delete revaluateResponse.userOrderMenu[index]['itemVendorVat'];
			 delete revaluateResponse.userOrderMenu[index]['itemVendorServiceTax'];
			 delete revaluateResponse.userOrderMenu[index]['itemVendorCostPrice'];
			 delete revaluateResponse.userOrderMenu[index]['itemVendorBasePrice'];
			 delete revaluateResponse.userOrderMenu[index]['itemIRCTCfee'];
			 delete revaluateResponse.userOrderMenu[index]['tkServiceTax'];
			 delete revaluateResponse.userOrderMenu[index]['SwachhBharatCess'];
			 delete revaluateResponse.userOrderMenu[index]['KrishiKalyanCess'];
			 delete revaluateResponse.userOrderMenu[index]['marketingExpense'];
			// revaluateResponse.userOrderMenu[index]['image']="https://desktop.travelkhana.com/img/booked-item-img.png";
			});*/

				// removing unrequired elements from userOrderInfo object
				/*delete revaluateResponse.userOrderInfo.is_shift;
				delete revaluateResponse.userOrderInfo.is_reminder;
				delete revaluateResponse.userOrderInfo.agent_commission;
				delete revaluateResponse.userOrderInfo.filler6;
				delete revaluateResponse.userOrderInfo.is_confirm;
				delete revaluateResponse.userOrderInfo.company_agent;
				delete revaluateResponse.userOrderInfo.cancel_amount;
				delete revaluateResponse.userOrderInfo.online_pay_status;
				delete revaluateResponse.userOrderInfo.cnf_msg_flag;
				delete revaluateResponse.userOrderInfo.adv_option;
				delete revaluateResponse.userOrderInfo.adv_option;
				delete revaluateResponse.userOrderInfo.adv_txn_id;
				delete revaluateResponse.userOrderInfo.adv_bank;
				delete revaluateResponse.userOrderInfo.adv_pay_date;
				delete revaluateResponse.userOrderInfo.issue_type;
				delete revaluateResponse.userOrderInfo.issue_reason;
				delete revaluateResponse.userOrderInfo.agent_comment;
				delete revaluateResponse.userOrderInfo.delivery_remarks;
				delete revaluateResponse.userOrderInfo.uncapped_sp;
				delete revaluateResponse.userOrderInfo.capped_stax;
				delete revaluateResponse.userOrderInfo.cancel_sub_reason;
				delete revaluateResponse.userOrderInfo.cancel_comment;
				delete revaluateResponse.userOrderInfo.bulk_agent_name;
				delete revaluateResponse.userOrderInfo.boarding_station;
				delete revaluateResponse.userOrderInfo.bulk_agent_name;
				delete revaluateResponse.userOrderInfo.destination_station;
				delete revaluateResponse.userOrderInfo.device_id;
				delete revaluateResponse.userOrderInfo.is_penalty;
				delete revaluateResponse.userOrderInfo.pnr_status;
				delete revaluateResponse.userOrderInfo.resell_close;
				delete revaluateResponse.userOrderInfo.vendorShare;
				delete revaluateResponse.userOrderInfo.tkServiceTax;
				delete revaluateResponse.userOrderInfo.SwachhBharatCess;
				delete revaluateResponse.userOrderInfo.KrishiKalyanCess;
				delete revaluateResponse.userOrderInfo.marketingExpense;
				//delete revaluateResponse.userOrderInfo.totalCapped;
				delete revaluateResponse.userOrderInfo.cappedServiceTax;*/

				// removing unrequired elements from trainUpdateInfo object
				if(revaluateResponse.trainUpdateInfo!==undefined)
				{
				delete revaluateResponse.trainUpdateInfo.last_dep;
				delete revaluateResponse.trainUpdateInfo.last_dep_time;
				delete revaluateResponse.trainUpdateInfo.next_station;
				delete revaluateResponse.trainUpdateInfo.next_station_time;
				delete revaluateResponse.trainUpdateInfo.state;
				delete revaluateResponse.trainUpdateInfo.track_query;
				delete revaluateResponse.trainUpdateInfo.order_query;
				delete revaluateResponse.trainUpdateInfo.modified_by;
				delete revaluateResponse.trainUpdateInfo.modified_on;
				}

				var revaluateObject={};
				revaluateObject["userOrderInfo"]=revaluateResponse.userOrderInfo;
				revaluateObject["userOrderMenu"]=revaluateResponse.userOrderMenu;
				revaluateObject["minAmountThreshold"]=revaluateResponse.minAmountThreshold;

				revaluateObject["outletminorderAmount"]=revaluateResponse.outletminorderAmount;
				revaluateObject["onlinePayableAmount"]=revaluateResponse.onlinePayableAmount;
				if(revaluateResponse.hasOwnProperty('trainUpdateInfo'))
				{
					revaluateObject["trainUpdateInfo"]=revaluateResponse.trainUpdateInfo;
				}

				if(revaluateResponse.hasOwnProperty('userBasicInfo'))
				{
					revaluateObject["userBasicInfo"]=revaluateResponse.userBasicInfo;
				}
				else
				{

				}

					var menuCookie={};
					menuCookie.userOrderMenu=revaluateObject.userOrderMenu;
					$.cookie("menuObject", JSON.stringify(menuCookie),{path: "/" });

				$.cookie("revaluateCookieObject", JSON.stringify(revaluateObject),{path: "/" });
				return revaluateObject;
	}
	function loadSpin(spin)
	{
		if(spin)
		{
			$('#subTotalVal').hide();
			$('#taxesVal').hide();
			$('#totalVal').hide();

			$('#subTotalSpin').show();
			$('#taxesSpin').show();
			$('#totalSpin').show();
			
		}
		else
		{
			$('#subTotalVal').show();
			$('#taxesVal').show();
			$('#totalVal').show();

			$('#subTotalSpin').hide();
			$('#taxesSpin').hide();
			$('#totalSpin').hide();
		}
	}

	function removeCoupon()
	{

		var revaluateCookieObject=JSON.parse($.cookie('revaluateCookieObject'));

		/*var totalCustomerPayableCapped= (parseFloat(revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped)+parseFloat(revaluateCookieObject.userOrderInfo.discount));*/
		revaluateCookieObject.userOrderInfo.voucher_code='';
		revaluateCookieObject.userOrderInfo.discount=0;
		delete revaluateCookieObject.userBasicInfo;
		delete revaluateCookieObject.userOrderInfo.taxes;
		delete revaluateCookieObject.onlinePayableAmount;
		loadSpin(true);
		revaluateCall(JSON.stringify(revaluateCookieObject));
		//$('.grandtotal').text(totalCustomerPayableCapped);
		$('.coupon-applied').hide();
		$('.DiscountDiv').hide();
		$('.discount').text('');
		// $('.mobile-number-field').hide();
		// Clevertap

        clevertap.event.push("RS- Promocode Removed", {
            "Promocode": revaluateCookieObject.userOrderInfo.voucher_code,
            "CustomerPayable": revaluateCookieObject.userOrderInfo.totalCustomerPayableCappeds,
            "Channel": channel
        });

		/*var totalCustomerPayableCapped =(parseFloat(revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped)+parseFloat(revaluateCookieObject.userOrderInfo.discount));
		$('.grandtotal').text(totalCustomerPayableCapped);
		revaluateCookieObject.userOrderInfo.voucher_code='';
		revaluateCookieObject.userOrderInfo.totalCustomerPayableCapped=totalCustomerPayableCapped;
		revaluateCookieObject.userOrderInfo.discount='0';
		$.cookie("revaluateCookieObject", JSON.stringify(revaluateCookieObject),{path: "/" });*/
	}
	function computeTotal() {
		var totalItems = $('.single-item');
		var subtotal = total = 0;
		var tax = 35.50;
		var discount = 0;

		$.each(totalItems, function(index, value) {
			var amount = parseFloat($(this).find('.item-amount').data('price')) * parseFloat($(this).find('.item-quantity-count').val());
			subtotal = (parseFloat(amount) + parseFloat(subtotal));
		});

		total = subtotal + discount + tax;
		$('.cart-subtotal').find('.subtotal').text(subtotal);
		$('.cart-subtotal,.item-total-container').find('.grandtotal').text(total);
	}
	//computeTotal();
	
});