jQuery(function($){
    var maxUploadSize = 15 * 1024 * 1024;      // 15 MB
    
    $('#supportForm #feedback').change(function(){
        if( $(this).val() == 'Complaint' ) {
            $('#supportForm #subtype-container, #supportForm #order-container').show();
            $('#supportForm #subtype, #supportForm #order').addClass('required');
        } else {
            $('#supportForm #subtype-container, #supportForm #order-container').hide();
            $('#supportForm #subtype, #supportForm #order').removeClass('required');
        }
    });

    // File upload validation
    $('#supportForm #file-1').change(function(){
        
        if( ! $(this)[0].files.length ) {
            $('#supportForm #uploaded').html('');
            return;
        }

        // Check if size is greater than max defined upload size
        if( $(this)[0].files[0].size  > maxUploadSize ) 
        {
            alert('File size exceeds Max file upload size.');
            $(this).val('');
            $('#supportForm #uploaded').html('');
            return;
        }
        
        $('#supportForm #uploaded').html( $(this)[0].files[0].name );
    });

    // Phone number field validation
    // var allowedPhoneKeyCodes = [32, 40, 41, 43, 45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    var allowedPhoneKeyCodes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
    $(document).on('keypress','.phone,.mobile', function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ( $.inArray(charCode, allowedPhoneKeyCodes) == -1 ) {
            return false;
        }
        
        return true;
    });
    
    $(document).on('paste drop', '.phone,.mobile', function(e){
        e.preventDefault();
    });

    $(document).on('click', '#supportForm .required', function(){
        $(this).removeClass('error');
    });

    // Validate Form
    $('#supportForm').submit(function(){
        var error = false;
        $(this).find('.required').each(function(){
            if( !$(this).val() ) {
                error = true;
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });

        // Phone number validation (10 digits)
        if( $('#supportForm #phone').val().length != 10 ) {
            error = true;
            $('#supportForm #phone').addClass('error');
        }

        // Max length validations
        var json = {
            'name': 50,
            'email': 100
        };
        
        if( $('#supportForm #feedback').val()=='Complaint' ) {
            json.order = 15;
        }

        var obj;
        $.each(json, function(index, value){
            obj = $('#supportForm #' + index);
            if( obj.val().length > value ) {
                error = true;
                obj.addClass('error');
            }
        });

        if( error ) {
            return false;
        }

        return true;
    });
});