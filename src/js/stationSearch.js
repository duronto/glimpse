var  keyTrains=[];
$(document).ready(function () {
//remove error class of any textfield
$( "input" ).click(function(e) {

         $( this ).val('');
         $( this ).removeClass('error');
 
   if($( this ).attr('id')=='stnfrom' || $( this ).attr('id')=='stnto')
   {
      $("#trainnum").val('');
       $("#trainnum").removeClass('error');
   }
   else if($( this ).attr('id')=='trainnum')
   {
    $("#stnfrom,#stnto").val('').removeClass('error');
     //$("#stnto").val('');
 
   }
});



$('#searchTrainTime').on('click',function()
{

   if($("#trainnum").val()!='' || $("#trainnum").hasClass('error'))
   {
    
       $("#trainnum").removeClass("error"); 
       if(!validateTrain('trainnum') || $("#trainnum").hasClass('error'))
       {
         return false;
       }

   }
   else if($("#stnfrom").val()=='' && $("#stnto").val()=='')
   {
      $("#trainnum").removeClass("error");
     $("#trainnum").val("Please select train");
     $("#trainnum").addClass("error");
     return false;
   }
  else if(($("#stnfrom").val()!='' && $("#stnto").val()=='') || $("#stnfrom").hasClass('error'))
   {
      $("#stnto").removeClass("error");
     $("#stnto").val("Please select station");
     $("#stnto").addClass("error");
     return false;
   }
   else if(($("#stnfrom").val()=='' && $("#stnto").val()!='') || $("#stnto").hasClass('error'))
   {
     $("#stnfrom").removeClass("error");
     $("#stnfrom").val("Please select station");
     $("#stnfrom").addClass("error");
      return false;
   }
   $('#srchTimeTableForm').submit();
});

$('#searchStation').on('click',function()
{
   if($("#trainnum1").val()!='' || $("#trainnum1").hasClass('error'))
   {
       $("#trainnum1").removeClass("error"); 
       if(!validateTrain('trainnum1') || $("#trainnum1").hasClass('error'))
       {
         return false;
       }

   }
   else if($("#trainnum1").val()=='')
   {
      $("#trainnum1").removeClass("error");
     $("#trainnum1").val("Please select train");
     $("#trainnum1").addClass("error");
     return false;
   }
   if($("#jDate").val()=='')
   {
    $("#jDate").removeClass("error");
    $("#jDate").val("Please select Date");
    $("#jDate").addClass("error");
    return false;
   }
   $('#srchTimeTableForm').submit();
});

if(jQuery("#trainnum").length>0)
{
 getTrainList();
}
if(jQuery("#trainnum1").length>0)
{
 getTrainStationList(document.getElementById("from1").value);
}

});



	
 