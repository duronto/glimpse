document.write('<scr'+'ipt type="text/javascript" src="/glimpse/src/js/common/urlConfig.js?v=0.2" ></scr'+'ipt>');

jQuery(function($) {

	$(document).on('keypress','.numeric,input[type="number"]', function(evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 46) {
		return true;
	}
	
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
	});

	$('.numeric,input[type="number"]').bind('paste drop',function(e){
		e.preventDefault();
	});

	$(document).on('click', '.openModal', function() {

		$('.updateDivSuccess').hide();
		$('.updateDivFailed').hide();

	});
	
	$('.online-payu').click(function() {

		var response=JSON.parse($.cookie('trackOrderResponse'));
		$.ajax({
			            type: 'POST',
			            
			            	url :URL+"payuPaymentNew?AMOUNT="+response[0].payable_amount+"&ORDERID="+response[0].userOrderId+"&PRODUCTINFO=vegthali%2Cnonvegthali&FIRSTNAME="+response[0].name+"&EMAILID="+response[0].mailId+"&PHONE="+response[0].contactNo+"&onlineDiscount=1",
							contentType: "application/json; charset=utf-8",
							/*url :"https://stage.travelkhana.com/api/payuPaymentNew",
							contentType: "application/json; charset=utf-8",*/
			            data: {

			                AMOUNT:response[0].payable_amount,
			                ORDERID:response[0].userOrderId,
			                PRODUCTINFO:"vegthali,nonvegthali",
			                FIRSTNAME:response[0].name,
			                EMAILID:response[0].mailId,
			                PHONE:response[0].contactNo,
			                onlineDiscount:1,
			                
			            },
			            beforeSend : function( xhr ) {
							xhr.setRequestHeader( "Authorization", authKey);
						},
			            success: function (response) {
			            	$('#paymentForm').html(response);
			                    $('#payuForm').submit();
			               
			            },
			            error: function (error) {

			            }
			      	  });
	});
	
	$('.online-paytm').click(function() {

		var response=JSON.parse($.cookie('trackOrderResponse'));

		$.ajax({
			            type: 'POST',
			            
			            	url :URL+"paytmPayment?EMAIL="+response[0].mailId+"&PHONE="+response[0].contactNo+"&ORDERID="+response[0].userOrderId+"&onlineDiscount=1",
							contentType: "application/json; charset=utf-8",
							/*url :"https://stage.travelkhana.com/api/payuPaymentNew",
							contentType: "application/json; charset=utf-8",*/
			            data: {

			                AMOUNT:response[0].payable_amount,
			                ORDERID:response[0].userOrderId,
			                
			                FIRSTNAME:response[0].name,
			                EMAILID:response[0].mailId,
			                PHONE:response[0].contactNo,
			                onlineDiscount:1,
			                
			            },
			            beforeSend : function( xhr ) {
							xhr.setRequestHeader( "Authorization", authKey);
						},
			            success: function (response) {
			            	$('#paymentForm').html(response);
			                    $('#paytmForm').submit();
			               
			            },
			            error: function (error) {

			            }
			      	  });
	});


	// Validate Modal Form In Order Confirm Page
$("#modal-form").validate({
	onfocusout: false,
	onkeyup: false,
	rules: {
		name: {
			required: true,
			minlength: 4
		},
		mobile_number: {
			required: true,
			maxlength: 10,
			minlength: 10
		}/*,
		seat: {
			required: true,
			maxlength: 15
		},
		coach: {
			required: true,
			maxlength: 15
		}*/
	},
	messages: {
		name: {
			required: "Name is required",
			minlength: "At least 4 characters are required"
		},
		mobile_number: {
			required: "Mobile Number is required",
			maxlength: "Maximum 10 digits",
			minlength: "Minimum 10 digits"
		}/*,
		seat: {
			required: "Seat is required",
			maxlength: "Maximum length is 15 digits"
		},
		coach: {
			required: "Coach is required",
			maxlength: "Maximum length is 15 digits"
		}*/
	},
	errorPlacement: function(error, element) {
    	element.val('').attr("placeholder", error.text());
	},
	submitHandler: function( form ) {
		var postOrder=JSON.parse($.cookie("orderPost"));
	
		prepareUpdateOrder(postOrder);
	}
});
	


	function prepareUpdateOrder(postOrder)
	{
		postOrder.userBasicInfo.contact_no=$('.mobileUpdate').val();
		postOrder.userBasicInfo.name=$('.nameUpdate').val();
		postOrder.userOrderInfo.seat=$('.seatUpdate').val();
		postOrder.userOrderInfo.coach=$('.coachUpdate').val();
		postOrder.userOrderInfo.user_order_id=$('.orderId').text();
		updateCustomerInfo(JSON.stringify(postOrder));
		 $(".btn-update-detail").css("width", "87%");
		 setTimeout(function(){
			$('.startSpin').show(); 
		}, 500);
		 
	}
	function updateCustomerInfo(postOrder)
	{
		$.ajax({
				url: URL+"order/",
				type: "PUT",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: postOrder,
				beforeSend : function( xhr ) {
				xhr.setRequestHeader( "Authorization",authKey);
				},
				success: function(response){
					var parsedJson=JSON.parse(postOrder);
					$('.updateDivSuccess').show();
					$('.successUpdate').text('Information updated successfully');
					 $(".btn-update-detail").css("width", "100%");
					$('.startSpin').hide();
					$('.passengerName').text(parsedJson.userBasicInfo.name);
					$('.passengerContact').text(parsedJson.userBasicInfo.contact_no);
					$('.passengerSeatCoach').text(parsedJson.userOrderInfo.coach+"/"+parsedJson.userOrderInfo.seat);
					$('.passengerSeat').text(" "+parsedJson.userOrderInfo.seat);

					$('.passengerCoach').text(" "+parsedJson.userOrderInfo.coach);
					$(".passengerSeat").addClass("fa-bed");
					$(".passengerSeat").addClass("fa");
					$(".passengerCoach").addClass("fa-train");
					$(".passengerCoach").addClass("fa");
					 setTimeout(function(){
						$(".modal").modal("hide");
					}, 500);
					 var post=JSON.parse(postOrder);
					  var trackOrderResp=JSON.parse($.cookie("trackOrderResponse"));
					  trackOrderResp[0].name=parsedJson.userBasicInfo.name;
					  trackOrderResp[0].contactNo=parsedJson.userBasicInfo.contact_no;
					  trackOrderResp[0].seat=parsedJson.userOrderInfo.seat;
					  trackOrderResp[0].coach=parsedJson.userOrderInfo.coach;
					   var date = new Date();
						 var m = 1;
						 date.setTime(date.getTime() + (m * 60 * 1000));
					  $.cookie("orderPost",postOrder,{ expires: date ,path: "/"});
					  $.cookie("trackOrderResponse",JSON.stringify(trackOrderResp),{ expires: date ,path: "/"});
					 
				},
				error: function(error){		 
					$('.updateDivSuccess').show();
					$('.errorUpdate').text('Information not updated.Please contact customer care at  08800-31-31-31');
				}
		});
	}

});