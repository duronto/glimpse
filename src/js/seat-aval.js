var  keyTrains=[];
$(document).ready(function () {

  $('.datepicker').datepicker({
          startDate: '-4d',
          minDate: 0,
          autoclose: !0,
          todayHighlight: !0,
          minDate: new Date,
          format: 'yyyy-mm-dd'
         });

   if( jQuery('#jDateseat2').length > 0 ) {      var date = new Date();      date =  date.getFullYear()  + '-' + ( getTwoDigitMonth(date.getMonth() + 1) ) + '-' + ( getTwoDigitDate(date.getDate()) );      jQuery('#jDateseat2').val( date );    }    

function getTwoDigitMonth( month ) {      return month < 10 ? '0' + month : month;    }
function getTwoDigitDate( date ) {      return date < 10 ? '0' + date : date;    }



  if($(".trainNo").text()!='')
  {
    $("#train1").val($(".trainNo").text()+"/"+$(".trainName").text());
    getStationsListByTrain($(".trainNo").text());
  }

$("#from1").val($(".fromStation").text());
$("#from2").val($(".destStation").text());

if($("#jDate").val()!==undefined)
{
$("#jDateseat2").val($("#jDate").val());
}


$("#searchAvai").on("click", function() {
     
       
            if (!validateTrain(1)) return  !1;
            if (!validateSrcStn()) return  !1;
            if (!validateDestStn()) return !1;
            $('#searchAvaiForm').submit();
           // $("#srchForm input[name='pnr1']").removeClass("error"), validatesrcstn() && ($("#srchForm input[name='form1']").removeClass("error"), validatejDate() ? ($('.btn-search').prop('disabled', true), $("#srchForm").submit()) : console.log("Invalid jDate"))
        
    });



function validateTrain(e) {
    var t = document.getElementById("train" + e).value;
  
    return "" == t ? ($("#train1").addClass("error"), document.getElementById("train1").value = "Please Select Train", !1) : -1 == t.indexOf("/") || t.indexOf("/") > 5 ? ($("#train1").addClass("error"), document.getElementById("train1").value = "Please Select Train", !1) : 0 == $.isNumeric(t.substring(0, t.indexOf("/"))) ? ($("#train1").addClass("error"), document.getElementById("train1").value = "Please Select Train", !1) : t.substring(t.indexOf("/") + 1, t.length).length < 5 ? ($("#train1").addClass("error"), document.getElementById("train1").value = "Please Select Train", !1) : !0
}

function validateSrcStn() {
    return "" == document.getElementById("from1").value ? ($("#from1").addClass("error"), document.getElementById("from1").value = "Please select station", !1) : checkStationFound(document.getElementById("from1").value) ? !0 : ($("#from1").addClass("error"), document.getElementById("from1").value = "Please Select Station", !1)
}

function validateDestStn() {
    return "" == document.getElementById("from2").value ? ($("#from2").addClass("error"), document.getElementById("from2").value = "Please select station", !1) : checkStationFound(document.getElementById("from2").value) ? !0 : ($("#from2").addClass("error"), document.getElementById("from2").value = "Please Select Station", !1)
}

  window.onload = function () {
                    getTrainList();
                }
function checkStationFound(e) {
  return $.inArray(e, keyArray) > -1 ? !0 : !1
  /*try
  {
    return $.inArray(e, keyArray) > -1 ? !0 : !1
}catch(err)
{
  
  return !0;
}*/
}
function getTrainList() {
   jQuery("#train1").attr('placeholder','Please wait');
   jQuery("#train1").attr('disabled',true);

   if (localStorage.getItem("trainList") === null) {
    $.ajax({
        url: "https://s3.ap-south-1.amazonaws.com/gatimaancms/trainJson/trainList/trainsList.json?__random=1",
        type: "get",
        success: function(e) {
              jQuery("#train1").attr('placeholder','Please Select Train');
               jQuery("#train1").attr('disabled',false);
            keyTrains = JSON.parse(e); resetTrainAutoComplete();

            localStorage.setItem('trainList', JSON.stringify(e));
            var retrievedObject = JSON.parse(localStorage.getItem('trainList'));
        }
    });
         jQuery("#train1").attr('placeholder','Please Select Train');
         jQuery("#train1").attr('disabled',false);
    }else{
        jQuery("#train1").attr('placeholder','Please Select Train');
        jQuery("#train1").attr('disabled',false);
        var retrievedObject = JSON.parse(localStorage.getItem('trainList'));
        keyTrains = retrievedObject; resetTrainAutoComplete();
    }           
}

function resetTrainAutoComplete() {
    jQuery("#train1").autocomplete(keyTrains, {
        max: 30,
        matchContains: !0,
        width: 420,
        appendTo: "#train1"
    }).result(function(e, t) {
        if (null != t && 1 == t.length) {
            var r = t[0].split("/");
            e.target.value = t[0], $("#to").val($.trim(r[0]));
            var a = $.trim(r[0]);
            $("#fromstn").val(""), $("#from1").val(""), $("#from1").focus(), getStationsListByTrain(a)
        } else e.target.value = "", $("#to").val(""), dataArray = [], resetAutoForm()
    }).select(function() {
        $("#from1").focus()
    })
}
function getStationsListByTrain(e) {
    jQuery("#from1").attr('placeholder','Loading Boarding Station');
    jQuery("#from1").attr('disabled',true);
    var trainNo=e;
    if(localStorage.getItem(trainNo+"stations") === null) {
    e && ("undefined" != typeof ajaxAct && ajaxAct.abort(), ajaxAct = $.ajax({
        url: "https://s3.ap-south-1.amazonaws.com/gatimaancms/trainJson/stationNameByTrain/" + e + ".json",

        type: "get",
        success: function(e) {
            stationList(JSON.parse(e));
            localStorage.setItem(trainNo+"stations", JSON.stringify(e));
            jQuery("#from1").attr('placeholder','Please Select Station');
            jQuery("#from1").attr('disabled',false);
        }
    }));
    }else{
        var retrievedObject = JSON.parse(localStorage.getItem(trainNo+"stations"));
        stationList(retrievedObject);
    }
               jQuery("#from1").attr('placeholder','Please Select Station');
               jQuery("#from1").attr('disabled',false);

}
function stationList(e) {

    keyArray = [];
    for (var t in e) keyArray.push(e[t].station);
    dataArray = keyArray, resetAutoForm()
}
function resetAutoForm() {
    $("#from1").autocomplete(dataArray, {
        max: 30,
        matchContains: !0,
        width: 200
    }).result(function(e, t) {
        return null != t && 1 == t.length ? (e.target.value = t[0], $("#fromstn").val($.trim(t[0])), flagfrom = 1, !1) : void 0
    });

    $("#from2").autocomplete(dataArray, {
        max: 30,
        matchContains: !0,
        width: 200
    }).result(function(e, t) {
        return null != t && 1 == t.length ? (e.target.value = t[0], $("#tostn").val($.trim(t[0])), flagfrom = 1, !1) : void 0
    });
}
//remove error class of any textfield
$( "input" ).click(function(e) {
    

         $( this ).val('');
         $( this ).removeClass('error');
 
   if($( this ).attr('id')=='stnfrom' || $( this ).attr('id')=='stnto')
   {

      $("#train1").val('');
       $("#train1").removeClass('error');
   }
   else if($( this ).attr('id')=='train1')
   {
    $("#stnfrom,#stnto").val('').removeClass('error');
     //$("#stnto").val('');
 
   }
});

/*$('#searchAvai').on('click',function()
{

   var errorFound=false;
    if($("#train1").val()=='')
    {
        $("#train1").removeClass("error");
        $("#train1").val("Please select train");
        $("#train1").addClass("error");
        errorFound=true;
    }
    else if($("#from1").val()=='')
    {
         $("#from1").removeClass("error");
         $("#from1").val("Please select station");
         $("#from1").addClass("error");
         errorFound=true;
    }
    else if(($("#from2").val()=='' ))
    {
         $("#from2").removeClass("error");
         $("#from2").val("Please select station");
         $("#from2").addClass("error");
         errorFound=true;
    }
    else if($("#jDateseat2").val()=='' || $("#jDateseat2").hasClass('error'))
    {
    
       $("#jDateseat2").removeClass("error");
       $("#jDateseat2").val("Please select Date");
       $("#jDateseat2").addClass("error");
       errorFound=true;
   }
    console.log($("#jDateseat2").val());
    if(!errorFound)
    {
      $('#searchAvaiForm').submit();
    }
   return false;


   
});*/



         
if(jQuery("#train1").length>0)

{
 getTrainList();
}


});


    