(function($){$.fn.bootcomplete=function(options){var defaults={url:"/search.php",method:'get',wrapperClass:"bc-wrapper",menuClass:"bc-menu",idField:!0,idFieldName:$(this).attr('name')+"_id",minLength:3,dataParams:{},formParams:{}}
var settings=$.extend({},defaults,options);$(this).attr('autocomplete','off')
$(this).wrap('<div class="'+settings.wrapperClass+'"></div>')
if(settings.idField){if($(this).parent().parent().find('input[name="'+settings.idFieldName+'"]').length!==0){}else{$('<input type="hidden" name="'+settings.idFieldName+'" value="">').insertBefore($(this))}}
$('<div class="'+settings.menuClass+' list-group"></div>').insertAfter($(this))
$(this).on("keyup",searchQuery);$(this).on("focusout",hideThat)
var xhr;var that=$(this)
function hideThat(){if($('.list-group-item'+':hover').length){return}
$(that).next('.'+settings.menuClass).hide()}
function searchQuery(){var arr=[];$.each(settings.formParams,function(k,v){arr[k]=$(v).val()})
var dyFormParams=$.extend({},arr);var Data=$.extend({query:$(this).val()},settings.dataParams,dyFormParams);if(!Data.query){$(this).next('.'+settings.menuClass).html('')
$(this).next('.'+settings.menuClass).hide()}
if(Data.query.length>=settings.minLength){if(xhr&&xhr.readyState!=4){xhr.abort()}
xhr=$.ajax({type:settings.method,url:settings.url,data:Data,dataType:"json",success:function(json){var results=''
$.each(json,function(i,j){results+='<a href="#" class="list-group-item" data-id="'+j.id+'" data-label="'+j.label+'">'+j.label+'</a>'});$(that).next('.'+settings.menuClass).html(results)
$(that).next('.'+settings.menuClass).children().on("click",selectResult)
$(that).next('.'+settings.menuClass).show()}})}}
function selectResult(){$(that).val($(this).data('label'))
if(settings.idField){if($(that).parent().parent().find('input[name="'+settings.idFieldName+'"]').length!==0){$(that).parent().parent().find('input[name="'+settings.idFieldName+'"]').val($(this).data('id'));$(that).parent().parent().find('input[name="'+settings.idFieldName+'"]').trigger('change')}
else{$(that).prev('input[name="'+settings.idFieldName+'"]').val($(this).data('id'));$(that).prev('input[name="'+settings.idFieldName+'"]').trigger('change')}}
$(that).next('.'+settings.menuClass).hide();return!1}
return this}}(jQuery))