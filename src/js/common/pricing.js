jQuery( function( $ ) {

    $('input').on('keydown', function(event){
       $(this).removeClass('error');
     });


	// while quantity decreases
	$(document).on('click', '.qty-minus', function() {
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');
		if( input.val() > 0 )
		{
			input.val( parseFloat(input.val()) - 1 );
			var price = $(this).closest( '.single-item' ).find( '.item-amount' ).text();
			computeTotal();
		}
	});

	// while quantity increases
	$(document).on('click', '.qty-plus', function() {
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');
		input.val( parseFloat(input.val()) + 1 );
		var price = $(this).closest( '.single-item' ).find( '.item-amount' ).text();
		computeTotal();

	});

	function computeTotal() 
	{
		var totalItems = $('.single-item');
		var subtotal = total = 0;
		var tax = 35.50;
		var discount = 0;

		$.each( totalItems, function( index, value ) {
			var amount = $(this).find('.item-amount').text() * $(this).find('.item-quantity-count').val();
			subtotal += parseFloat(amount);
		});

		total = subtotal + discount + tax;
		$('.cart-subtotal').find('.subtotal').text( subtotal );
		$('.cart-subtotal').find('.grandtotal').text( total );
		
		$('#mobiletotal').text( total );
	}

	computeTotal();
});

// Validating fields based on numerics
$(document).on('keypress','.numeric,input[type="number"]', function(evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 46) {
		return true;
	}
	
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
});

$('.numeric,input[type="number"]').bind('paste drop',function(e){
	e.preventDefault();
});


// Collapse accordion 
// $("#accordion1").accordion({ header: "h3", collapsible: true, active: false });
$(".acc_one").click( function () {

    var panelDiv = $('#accordion');
	
	var chekDe=panelDiv.find('#confirm-detail');
	if(chekDe.hasClass('in'))
	{
		return false;
	}
	
	panelDiv.find('.complete-step').hide();
	panelDiv.removeClass('completed');
	
	var panelDiv1 = $('#accordion1').find('#delivery-detail');
	if ( panelDiv1.hasClass('in') ) panelDiv1.removeClass('in');


});

$.validator.addMethod("alphanumerics", function( value, element ) {
    return /^[a-z0-9\-\s]+$/i.test( value );
}, "Special characters are not allowed");



// Validate Search Order Form
$("#search-order").validate({
	onfocusout: false,
	onkeyup: false,
	rules: {
		order_id: {
			required: true,
			minlength: 2,
			maxlength: 10,
			digits: true
		},
		contact: {
			required: true,
			maxlength: 10,
			minlength: 10,
			digits: true
		}
	},
	messages: {
		order_id: {
			required: "Order Id is required",
			minlength: "Minimum 2 characters",
			maxlength: "Maximum 10 characters",
			digits: "Invalid order id"
		},
		contact: {
			required: "Phone Number is required",
			maxlength: "Maximum 10 characters",
			minlength: "Minimum 10 characters",
			digits: "Invalid phone number"
		}
	},
	errorPlacement: function(error, element) {
    	element.val('').attr("placeholder", error.text());
	},
	submitHandler: function( form ) {
		// form.submit();
		$(form).ajaxSubmit();
	}
});

$(document).ready(function() {
		$('.order-box').click(function() {
            $('.order-item-container').slideToggle("slide");
		});
		
			 $('.button1,.button2').on('click', function() {
				var isValid = true;		    
			 	 if($( this ).hasClass("button1"))
			 	 {		 	
			    var name = $('#name');
			    if (name.val() == '') {
			        name.val('').attr('placeholder', 'Provide Name.');
			        name.addClass('error');
			        isValid = false;
			    } else if (name.val().length < 4) {
			        name.val('').attr('placeholder', 'Provide minimum 4 chars.');
			        name.addClass('error');
			        isValid = false;
			    }
			    var contact = $('#contact');
			    if (!$('#contact').val().match('[0-9]{10}')) {
			        contact.val('').attr('placeholder', 'Provide 10 digit number.');
			        contact.addClass('error');
			        isValid = false;
			    }
			    if(document.getElementById("isVouchered") != null){
			 		var email = $('#email');
			 		

			 			if (!isValidEmailAddress(email.val())) {
			      	  	email.val('').attr('placeholder', 'Provide valid email');
			       		 email.addClass('error');
			        	isValid = false;
			 		}
			   	 
			 	}
			 	else
			 	{
			 		var email = $('#email');
			 		if(email.val()==='')
			 		{
			 			
			 		}
			 		else
			 		{

			 			if (!isValidEmailAddress(email.val())) {
			      	  	email.val('').attr('placeholder', 'Provide valid email');
			       		 email.addClass('error');
			        	isValid = false;
			 		}

			   	 }
			   	}
			   	 	if (isValid) 
			   	 			{
			   	 				try {
			   	 				cleverUserDetail(contact.val(),name.val(),email.val());
			   	 				}catch(err) {}
						   	 if ($(window).width() < 768) {

						            $("#back-link").show();
						            $("#accordion").hide();
						            $("#accordion1").show();
						            var panelDiv1 = $('#accordion1');
						            if (!panelDiv1.find('#delivery-detail').hasClass('in')) panelDiv1.find('#delivery-detail').addClass('in');
						            if (!panelDiv1.find('.panel-default').hasClass('active')) panelDiv1.find('.panel-default').addClass('active');
						
						            var panelDiv = $('#accordion');
						            panelDiv.addClass('completed').find('#confirm-detail').hide();
						            panelDiv.find('.panel-default').removeClass('active');
						            panelDiv.find('.complete-step').hide();

						            
						            $(".tempPlace").addClass("finalProceed");
						            $(".tempPlace").attr('value', 'Place Order');
						
						        } else {
						
						            $("#accordion,#accordion1").show();
						            var panelDiv = $('#accordion');
						            panelDiv.addClass('completed').find('#confirm-detail').removeClass('in');
						            panelDiv.find('.panel-default').removeClass('active');
						            panelDiv.find('.complete-step').show();
						
						            var panelDiv1 = $('#accordion1');
						            if (!panelDiv1.find('#delivery-detail').hasClass('in')) panelDiv1.find('#delivery-detail').addClass('in');
						            if (!panelDiv1.find('.panel-default').hasClass('active')) panelDiv1.find('.panel-default').addClass('active');
						        }
						    }
			 	

				 }
				 else if($( this ).hasClass("button2"))
				{
					
						
					var pnr = $('#pnr');
					if (!$('#pnr').val().match('[0-9]{10}')) {
						pnr.val('').attr('placeholder', 'Provide 10 digit pnr number.');
						pnr.addClass('error');
						isValid = false;
					}
					var seat = $('#seat');
					if ($.trim(seat.val()) == '') {
						seat.val('').attr('placeholder', 'Provide Seat.');
						seat.addClass('error');
						isValid = false;
					}
					var coach = $('#coach');
					if ($.trim(coach.val()) == '') {
						coach.val('').attr('placeholder', 'Provide Coach.');
						coach.addClass('error');
						isValid = false;
					}

					if (isValid) 
			   	 			{
						   	 if ($(window).width() < 768) {
							$('.tempPlace').prop('disabled', true);
					var value = $.cookie("revaluateCookieObject");
					var revalJson=JSON.parse(value);


					revalJson.userOrderInfo.booked_by='website';
					revalJson.userOrderInfo.customer_comment='';
				 	revalJson.userOrderInfo.coach=document.getElementById('coach').value;
				 	revalJson.userOrderInfo.seat=document.getElementById('seat').value;
					if(document.getElementById('pnr').value.length<10){
				 		revalJson.userOrderInfo.pnr="1111111111";
				 	}else{
						revalJson.userOrderInfo.pnr=document.getElementById('pnr').value;
				 	}
				 
				 	var trainUpdatedInfo = {};
					 trainUpdatedInfo.train_number=revalJson.userOrderInfo.train_no;
					 trainUpdatedInfo.station_code=revalJson.userOrderInfo.station_code;
					 trainUpdatedInfo.date=revalJson.userOrderInfo.date;
					 trainUpdatedInfo.eta=document.getElementById('delTime').value;

					 var userBasicInfo = {};
					 userBasicInfo.contact_no=document.getElementById('contact').value;
					 userBasicInfo.name=document.getElementById('name').value;
					 userBasicInfo.mail_id=document.getElementById('email').value;


					 var submitOrderJson = {};
					 submitOrderJson.submitDuplicate= true;
					 submitOrderJson.userBasicInfo=userBasicInfo;
					 submitOrderJson.trainUpdateInfo=trainUpdatedInfo;
					 submitOrderJson.userOrderInfo=revalJson.userOrderInfo;
					 submitOrderJson.userOrderMenu=revalJson.userOrderMenu;
					  delete submitOrderJson.userOrderInfo.taxes;
					 submitOrder(JSON.stringify(submitOrderJson));
						            // $("#back-link").show();
						            // $("#accordion").hide();
						            // $("#accordion1").show();
						            // var panelDiv1 = $('#accordion1');
						            // if (!panelDiv1.find('#delivery-detail').hasClass('in')) panelDiv1.find('#delivery-detail').addClass('in');
						            // if (!panelDiv1.find('.panel-default').hasClass('active')) panelDiv1.find('.panel-default').addClass('active');
						
						            // var panelDiv = $('#accordion');
						            // panelDiv.addClass('completed').find('#confirm-detail').hide();
						            // panelDiv.find('.panel-default').removeClass('active');
						            // panelDiv.find('.complete-step').hide();
						
						        } else {
						
						            $("#accordion,#accordion1,#accordion2").show();
						            var panelDiv = $('#accordion1');
						            panelDiv.addClass('completed').find('#delivery-detail').removeClass('in');
						            panelDiv.find('.panel-default').removeClass('active');
						            panelDiv.find('.complete-step').show();
						
						            var panelDiv1 = $('#accordion2');
						            if (!panelDiv1.find('#payment-method').hasClass('in')) panelDiv1.find('#payment-method').addClass('in');
						            if (!panelDiv1.find('.panel-default').hasClass('active')) panelDiv1.find('.panel-default').addClass('active');
						        }
						    }
			}
    
			    
			    return isValid;
			});
			function cleverUserDetail(contact,name,email){
				clevertap.event.push("RS - User Details", {
    				"Name": name,
    				"Mobile":contact,
    				"Email":email,
	                "Channel": channel
	              });
			}
			function cleverChargedFail(reasonC,stationCodeC,mobileNoC,outletIdC){
				clevertap.event.push("RS - Charged Fail", {
    				"Reason": reasonC,
    				"stationCode":stationCodeC,
    				"outletId":outletIdC,
    				"Mobile":mobileNoC,
	                "Channel": channel
	              });
			}
			//$("#back-link").hide();
			//$("#accordion1").hide();
	var delete_cookie = function(name) {
		console.log("deleting cookie---->"+name);
    document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
			function submitOrder(submitOrderJson)
	{
			var utmSource = $.cookie("utm_source");

					if(utmSource!==undefined && utmSource==='indian-rails')
					authKey='Basic aW5kaWFucmFpbHM6YWtZNkBoJXppcDA5aHFkVA==';
		var revaluateResponse='';
		$.ajax({
				url: URL+"order?token=portal",
				type: "POST",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: submitOrderJson,
				beforeSend : function( xhr ) {
				xhr.setRequestHeader( "Authorization", authKey);
				},
				success: function(response){
					delete_cookie('revaluateCookieObject');
					delete_cookie('minOrderAmount');
					if ($.cookie('pnr') != null ){delete_cookie('pnr');}
					if ($.cookie('menuObject') != null ){delete_cookie('menuObject');}
					 var date = new Date();
					 var m = 30;
					 date.setTime(date.getTime() + (m * 60 * 1000));
					$.cookie("orderPost", submitOrderJson,{path: "/" });
					$.cookie("trackOrderResponse", JSON.stringify(response),{path: "/" });


					if($("input[name='payment_method']:checked").val()=='payu')
					{
						$.ajax({
			            type: 'POST',
			            
			            	url :URL+"payuPaymentNew?AMOUNT="+response[0].payable_amount+"&ORDERID="+response[0].userOrderId+"&PRODUCTINFO=vegthali%2Cnonvegthali&FIRSTNAME="+response[0].name+"&EMAILID="+response[0].mailId+"&PHONE="+response[0].contactNo+"&onlineDiscount=1",
							contentType: "application/json; charset=utf-8",
							
			            data: {

			                AMOUNT:response[0].payable_amount,
			                ORDERID:response[0].userOrderId,
			                PRODUCTINFO:"vegthali,nonvegthali",
			                FIRSTNAME:response[0].name,
			                EMAILID:response[0].mailId,
			                PHONE:response[0].contactNo,
			                onlineDiscount:1,
			                
			            },
			            beforeSend : function( xhr ) {
							xhr.setRequestHeader( "Authorization", authKey);
						},
			            success: function (response) {
			            	$('#paymentForm').html(response);
			                    $('#payuForm').submit();
			               
			            },
			            error: function (error) {

			            }
			      	  });
					}
					else if($("input[name='payment_method']:checked").val()=='paytm')
					{
						

		
						$.ajax({
				            type: 'POST',
				            
				            	url :URL+"paytmPayment?EMAIL="+response[0].mailId+"&PHONE="+response[0].contactNo+"&ORDERID="+response[0].userOrderId+"&onlineDiscount=1",
								contentType: "application/json; charset=utf-8",
				            data: {

				                AMOUNT:response[0].payable_amount,
				                ORDERID:response[0].userOrderId,
				                
				                FIRSTNAME:response[0].name,
				                EMAILID:response[0].mailId,
				                PHONE:response[0].contactNo,
				                onlineDiscount:1,
				                
				            },
				            beforeSend : function( xhr ) {
								xhr.setRequestHeader( "Authorization", authKey);
							},
				            success: function (response) {
				            	$('#paymentForm').html(response);
				                    $('#paytmForm').submit();
				               
				            },
				            error: function (error) {

				            }
			      	  });
	
					}
					else
					{
						$(location).attr('href', 'confirm.html');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){	
				    $('.tempPlace').prop('disabled', false);
				    
				    var mobileNoC=JSON.parse(submitOrderJson).userBasicInfo.contact_no;
				    var outletIdC=JSON.parse(submitOrderJson).userOrderInfo.order_outlet_id;
				    var stationCodeC=JSON.parse(submitOrderJson).userOrderInfo.station_code;
				    		
				    if (XMLHttpRequest.readyState == 4) {
				    	obj = JSON.parse(XMLHttpRequest.responseText);
				    	document.getElementById("myModalLabel").innerHTML =obj.Message;
				    	var reasonC=obj.Message;
				    	try {
			   	 				cleverChargedFail(reasonC,stationCodeC,mobileNoC,outletIdC);
			   	 		}catch(err) {}
    					$('#myModal').modal('show');
    					setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
				    	window.location = "https://www.travelkhana.com/travelkhana/jsp/menu.html";
       				 }
        			else if (XMLHttpRequest.readyState == 0) {
        				var reasonC="Please check your network conn. and try again";
        				try {
			   	 				cleverChargedFail(reasonC,stationCodeC,mobileNoC,outletIdC);
			   	 		}catch(err) {}
           			 // Network error (i.e. connection refused, access denied due to CORS, etc.)
           			 	document.getElementById("myModalLabel").innerHTML ="Please check your network conn. and try again";
    				 	$('#myModal').modal('show');
    				 	setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
        			}
        			else {
        				var reasonC="Some problem occured please try again";
        				try {
			   	 				cleverChargedFail(reasonC,stationCodeC,mobileNoC,outletIdC);
			   	 		}catch(err) {}
           			 // something weird is happening
           				document.getElementById("myModalLabel").innerHTML ="Some problem occured please try again";
    					$('#myModal').modal('show');
    					setTimeout(function(){ $('#myModal').modal('hide');}, 2000);
					    window.location = "order.jsp";
        			}
				}
		});
		
	}

			$('#back-link').click(function() {
			    if ($("#delivery-detail").is(':visible')) {
			        // $("#accordion,#confirm-detail").show();
			        // var panelDiv1 = $('#accordion1');
			        // panelDiv1.find('#delivery-detail').removeClass('in');
			        // panelDiv1.find('.panel-default').reClass('active');	
			
			        // $("#accordion1").hide();
			        // $('#accordion').find('.complete-step').hide();
			        $("#accordion1").hide();
			        $("#accordion").removeClass('completed').show();
			        $("#confirm-detail").show();
			
			        var panelDiv = $('#accordion').find('#delivery-detail');
			        if (!panelDiv.hasClass('in')) panelDiv.addClass('in');
			
			        var panelDiv1 = $('#accordion1').find('#delivery-detail');
			        if (panelDiv1.hasClass('in')) panelDiv1.removeClass('in');
			
			        $('#accordion').find('.complete-step').hide();
			
			
			
			    } else {
			        window.location.href = "cart.html";
			    }
			});
			
			$('.finalProceed').click(function()
	{

					$(".progress").css("display", "block");
					$('.tempPlace').prop('disabled', true);
					var value = $.cookie("revaluateCookieObject");
					var revalJson=JSON.parse(value);

					var utmSource = $.cookie("utm_source");

					if(utmSource!==undefined)
					revalJson.userOrderInfo.utm_source=utmSource;
						
					
					revalJson.userOrderInfo.customer_comment='';
				 	revalJson.userOrderInfo.coach=document.getElementById('coach').value;
				 	revalJson.userOrderInfo.seat=document.getElementById('seat').value;
					if(document.getElementById('pnr').value.length<10){
				 		revalJson.userOrderInfo.pnr="1111111111";
				 	}else{
						revalJson.userOrderInfo.pnr=document.getElementById('pnr').value;
				 	}
				 
				 	var trainUpdatedInfo = {};
					 trainUpdatedInfo.train_number=revalJson.userOrderInfo.train_no;
					 trainUpdatedInfo.station_code=revalJson.userOrderInfo.station_code;
					 trainUpdatedInfo.date=revalJson.userOrderInfo.date;
					 trainUpdatedInfo.eta=document.getElementById('delTime').value;

					 var userBasicInfo = {};
					 userBasicInfo.contact_no=document.getElementById('contact').value;
					 userBasicInfo.name=document.getElementById('name').value;
					 userBasicInfo.mail_id=document.getElementById('email').value;


					 var submitOrderJson = {};
					 submitOrderJson.submitDuplicate= true;
					 submitOrderJson.userBasicInfo=userBasicInfo;
					 submitOrderJson.trainUpdateInfo=trainUpdatedInfo;
					 submitOrderJson.userOrderInfo=revalJson.userOrderInfo;
					 submitOrderJson.userOrderMenu=revalJson.userOrderMenu;
					 delete submitOrderJson.userOrderInfo.taxes;

					 submitOrder(JSON.stringify(submitOrderJson));
		    

			
		
	});
			
			// Delivery Detail step2 form submit
			
			$('#btn-proceed').on('click', function() {
			var isValid = true;
			var pnr = $('#pnr');
			if (pnr.val() == '') {
			    pnr.val('').attr('placeholder', 'Provide PNR.');
			    pnr.addClass('error');
			    isValid = false;
			} else if (!$('#pnr').val().match('[0-9]{10}')) {
			    pnr.val('').attr('placeholder', 'Provide 10 digit PNR.');
			    pnr.addClass('error');
			    isValid = false;
			}
			var coach = $('#coach');
			if (coach.val() == '') {
			    coach.val('').attr('placeholder', 'Provide coach.');
			    coach.addClass('error');
			    isValid = false;
			}
			var seat = $('#seat');
			if (seat.val() == '') {
			    seat.val('').attr('placeholder', 'Provide seat.');
			    seat.addClass('error');
			    isValid = false;
			}
			// else if(seat.val().length <3){
			// seat.val('').attr('placeholder','Provide number number.');
			// seat.addClass('error');
			// isValid=false;
			// }
			
			if (isValid) {
			    $("#confirm-details").submit();
			}
			return isValid;
			
			
			});
			
			
			});
			
			function isValidEmailAddress(emailAddress) {
			    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
			    return pattern.test(emailAddress);
			}
			
			function printDiv() {
			    var originalContents = document.body.innerHTML;
			    $('.noprint').hide();
			    var printContents = document.getElementById('printarea').innerHTML;
			
			    document.body.innerHTML = printContents;
			    window.print();
			    document.body.innerHTML = originalContents;
			}

