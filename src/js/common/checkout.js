jQuery( function( $ ) {

	// while quantity decreases
	$(document).on('click', '.qty-minus', function() {
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');
		if( input.val() > 0 )
		{
			input.val( parseFloat(input.val()) - 1 );
			var price = $(this).closest( '.single-item' ).find( '.item-amount' ).text();
			computeTotal();
		}
                if( input.val() == 0 )
                {
                  $(this).closest( '.single-item' ).remove();
                }
                
                if($('.single-item' ).length==0)
                {
                    $('.order-summary' ).parent().hide();
                    $('.mobile-cart-view' ).hide();                      

                }

	});

	// while quantity increases
	$(document).on('click', '.qty-plus', function() {
		var input = $(this).closest('.item-quantity').find('.item-quantity-count');
		input.val( parseFloat(input.val()) + 1 );
		var price = $(this).closest( '.single-item' ).find( '.item-amount' ).text();
		computeTotal();

	});

	function computeTotal() 
	{
		var totalItems = $('.single-item');
		var subtotal = total = 0;
		var tax = 35.50;
		var discount = 0;

		$.each( totalItems, function( index, value ) {
			var amount = parseFloat($(this).find('.item-amount').data('price')) * parseFloat($(this).find('.item-quantity-count').val());
			subtotal = (parseFloat(amount)+ parseFloat(subtotal));
		});
                 
		total = subtotal + discount + tax;
		$('.cart-subtotal').find('.subtotal').text( subtotal );
		$('.cart-subtotal,.item-total-container').find('.grandtotal').text( total );
	}

	computeTotal();
});