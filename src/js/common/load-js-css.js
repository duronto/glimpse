  var $ = $.noConflict();
jQuery.uaMatch = function(e) {
    e = e.toLowerCase();
    var t = /(chrome)[ \/]([\w.]+)/.exec(e) || /(webkit)[ \/]([\w.]+)/.exec(e) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) || /(msie)[\s?]([\w.]+)/.exec(e) || /(trident)(?:.*? rv:([\w.]+)|)/.exec(e) || e.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e) || [];
    return {
        browser: t[1] || "",
        version: t[2] || "0"
    }
}, matched = jQuery.uaMatch(navigator.userAgent), matched.browser = "trident" == matched.browser ? "msie" : matched.browser, browser = {}, matched.browser && (browser[matched.browser] = !0, browser.version = matched.version), browser.chrome ? browser.webkit = !0 : browser.webkit && (browser.safari = !0), jQuery.browser = browser,
$(document).ready(function () {
    "use strict";

 

//animated scroll menu
     $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            $('.navbar-transparent').addClass('shrink');
            $('.inner-header').addClass('shrink');
        }
        if (scroll <= 0) {
            $('.navbar-transparent').removeClass('shrink');
            $('.inner-header').removeClass('shrink');
        }
    }); 
  

//back to top
    //Check to see if the window is top if not then display button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
  
//Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });


$( "input" ).click(function(e) {    

    if($( this ).hasClass('error'))
    {
     $( this ).val('');
     $( this ).removeClass('error'); 
    }
  
});


       if($(".integerfiled").length>0)
        {
          $(".integerfiled").keydown(function (e) {
        
        // Allow: backspace, delete, tab, escape, enter and .
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+V
            (e.keyCode == 86 && e.ctrlKey === true) ||
            
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
         }
        // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
          }
       });
     }

    $('#submit_form').submit(function() {

                var errorfound=false;
                $(".required").each(function(){
                     var input = $(this); // This is the jquery object of the input, do what you will
                     
                     var id=input.attr('id');
                     if(input.val()=='' || input.hasClass('error') || (!$.isNumeric(input.val()) &&  input.hasClass('integerfiled'))   )
                     {
                                                  input.removeClass('error');
                                                 var p=input.attr('placeholder');
                           input.val('Provide '+p);
                                                   input.addClass('error');
                           errorfound=true;
                     }
                                         else if(input.data('minimum')!='' && input.val().length< input.data('minimum'))
                                         {
                                                 var p=input.hasClass('integerfiled')?'digits':'chars';
                                                 
                                                 input.val('Minimum '+input.data('minimum')+' '+p);
                                                 input.addClass('error');
                         errorfound=true;

                                         }
                     
                    
                });

                            if(errorfound)
                {               
                    return false;
                }
            return true;
        // your code here
        });     
   
});


function getTrainList(station) {
    jQuery("#trainnum").attr('disabled',true);
    $.ajax({
        url: "https://beta.travelkhana.com/trainJson/trainsList.json?__random=1",
        type: "get",
        success: function(e) {
            keyTrains = e; resetTrainAutoComplete();
            jQuery("#trainnum").attr('disabled',false);
        }
    })
}

function resetTrainAutoComplete() {
    jQuery("#trainnum").autocomplete(keyTrains, {
        max: 30,
        matchContains: !0,
        width: 420,
        appendTo: "#trainnum"
    }).result(function(e, t) {
        if (null != t && 1 == t.length) {
            var r = t[0].split("/");
            e.target.value = t[0];
            var a = $.trim(r[0]);
           
        } else e.target.value = "";
    })
}
function getTrainStationList(station) {
    jQuery("#trainnum1").attr('disabled',true);
    $.ajax({
        url: "https://beta.travelkhana.com/trainJson/trainsAtStation/"+station.toUpperCase()+".json?__random=1",
        type: "get",
        success: function(e) {
            keyTrains = e; resetTrainAutoComplete();
            jQuery("#trainnum1").attr('disabled',false);
        }
    })
}
function resetTrainAutoComplete() {
    jQuery("#trainnum1").autocomplete(keyTrains, {
        max: 30,
        matchContains: !0,
        width: 420,
        appendTo: "#trainnum1"
    }).result(function(e, t) {
        if (null != t && 1 == t.length) {
            var r = t[0].split("/");
            e.target.value = t[0];
            var a = $.trim(r[0]);
           
        } else e.target.value = "";
    })
}


function validateTrain(id) {
    var t = document.getElementById(id).value;
    return "" == t ? ($("#"+id).addClass("error"), document.getElementById(id).value = "Please select train", console.log("Empty Train name if:1"), !1) : -1 == t.indexOf("/") || t.indexOf("/") > 5 ? ($("#"+id).addClass("error"), document.getElementById(id).value = "Please select train", console.log("Invalid Train name if:2"), !1) : 0 == $.isNumeric(t.substring(0, t.indexOf("/"))) ? ($("#"+id).addClass("error"), document.getElementById(id).value = "Please select train", console.log("Invalid Train name if:3"), !1) : t.substring(t.indexOf("/") + 1, t.length).length < 5 ? ($("#"+id).addClass("error"), document.getElementById(id).value = "Please select train", console.log("Invalid Train name if:4"), !1) : !0
}

 var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);