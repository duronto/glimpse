var keyTrains = [];
var keyStations = [];
var dataArray = [];
$(document).ready(function () {
  
  //remove error class of any textfield
  $("input").click(function (e) {
    $(this).val('').removeClass('error');
    if ($(this).attr('id') == 'track-train') {
      $("#track-stn").val('').removeClass('error');
    }
  });

  // Validate form
  $('#trackTrainBtn').on('click', function () {
    var isValid = true;
    if ($("#track-train").val() == '' || $("#track-train").hasClass('error') || validateTrains('track-train') === false) {
        $("#track-train").addClass("error").val("Please select train");
        isValid = false;
    }
    /*if ($("#track-stn").val() == '' || !validatesrcstn() || $("#track-stn").hasClass('error')) {
      $("#track-stn").addClass("error").val("Please select station");
      isValid = false;
    }*/
    if ($("#jDate").val() == '' || $("#jDate").hasClass('error')) {
      $("#jDate").addClass("error").val("Please select date");
      isValid = false;
    }
    /*var eAdd = $("#email-opt").val();
    if (eAdd != '' && !isValidEmailAddress(eAdd)) {
      $("#email-opt").addClass("error").val("Invalid email");
      isValid = false;
    }*/

    if (isValid) {
      $('#trackTrainForm').submit();
    }

    return isValid;
  });

  if (jQuery("#track-train").length > 0) {
    getTrackTrainList();
  }

});

function validatesrcstn() {
  return "" == document.getElementById("track-stn").value ? ($("#track-stn").addClass("error"), document.getElementById("track-stn").value = "Please select station", console.log("Station Name invalid if:1"), !1) : checkStationFound(document.getElementById("track-stn").value) ? !0 : ($("#track-stn").addClass("error"), document.getElementById("track-stn").value = "Please select station", console.log("Station Name invalid if:2"), !1)
}

function checkStationFound(e) {
  return $.inArray(e, keyStations) > -1 ? !0 : !1;
}

function getTrackTrainList() {
  jQuery("#trainnum").attr('disabled', true);
  $.ajax({
    url: "https://stage.travelkhana.com/trainJson/trainsList.json?__random=1",
    type: "get",
    success: function (e) {
      keyTrains = e;
      setTrainAutoComplete();
      jQuery("#track-train").attr('disabled', false);
    }
  })
}

function setTrainAutoComplete() {
  jQuery("#track-train").autocomplete(keyTrains, {
    max: 30,
    matchContains: !0,
    width: 420,
    appendTo: "#track-train"
  }).result(function (e, t) {
    if (null != t && 1 == t.length) {
      var r = t[0].split("/");
      e.target.value = t[0];
      var a = $.trim(r[0]);
      $("#track-stn").val(""), $("#track-stn").val(""), $("#track-stn").focus(), getStationsListByTrain(a)
    } else e.target.value = "", dataArray = [], resetAutoForm()
  }).select(function () {
    $("#track-stn").focus()
  })
}

function resetAutoForm() {
  $("#track-stn").autocomplete(dataArray, {
    max: 30,
    matchContains: !0,
    width: 200
  }).result(function (e, t) {
    return null != t && 1 == t.length ? (e.target.value = t[0], $("#track-stn").val($.trim(t[0])), flagfrom = 1, !1) : void 0
  })
}

function getStationsListByTrain(e) {
  jQuery("#track-stn").attr('disabled', true);
  e && ("undefined" != typeof ajaxAct && ajaxAct.abort(), ajaxAct = $.ajax({
    url: "https://stage.travelkhana.com/trainJson/stationsByTrain/" + e + ".json",
    type: "get",
    success: function (e) {
      stationList(e);
      jQuery("#track-stn").attr('disabled', false);
    }
  }));
  jQuery("#track-stn").attr('disabled', false);
}

function stationList(e) {
  keyStations = [];
  for (var t in e) keyStations.push(e[t].station);
  dataArray = keyStations, resetAutoForm()
}

function isValidEmailAddress(emailAddress) {
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return pattern.test(emailAddress);
};